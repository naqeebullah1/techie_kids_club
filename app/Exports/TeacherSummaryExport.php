<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TeacherSummaryExport implements FromCollection, WithHeadings {

    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;

    function __construct($data) {
        $this->data = $data;
    }

    public function collection() {
        $pos = $this->data;

        return collect($pos);
    }

    public function headings(): array {
        return [
            'District',
            'RD',
            'Teacher',
            'Total Student',
        ];
    }

}
