<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\District;
use Illuminate\Support\Facades\DB;

class DistrictsController extends Controller {

    public function index(Request $request) {
        $districts = District::paginate(15);
        
        if ($request->ajax()) {
            $view = view('admin.districts.table', compact('districts'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.districts.index', compact('districts'));
    }

    public function create($id = null) {
        $district = new District();
        if ($id) {
            $district = District::find($id);
        }
        $view = view('admin.districts.form', compact('district'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $district = $request->record;
            $id = $request->id;
//            dd($request->all());
            if ($id) {
                $district['updated_by'] = auth()->user()->id;
//                dd($district);
                $district = District::find($id)->update($district);
                $outcome = "District Updated Successfully";
            } else {
                $district = District::create($district);
                $outcome = "District Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {

        $outcome = '';
        try {
            DB::beginTransaction();
            $district = District::find($id);

//            if ($district->schools->count()) {
//                return redirect()->back()->with('error', 'Some schools are associated to this district. Please delete cities first');
//            }

            $district->delete();

            $outcome = "District deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
