<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PriceSetting;
use Illuminate\Support\Facades\DB;

class PriceSettingsController extends Controller {

    public function index(Request $request) {
        $states = PriceSetting::paginate(15);
        if ($request->ajax()) {
            $view = view('admin.price-settings.table', compact('states'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.price-settings.index', compact('states'));
    }

    public function create($id = null) {
        $state = new PriceSetting();
        if ($id) {
            $state = PriceSetting::find($id);
        }
        $view = view('admin.price-settings.form', compact('state'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $state = $request->record;
            $id = $request->id;
//            dd($);
            if ($id) {
//                $state['updated_by'] = auth()->user()->id;
                $state = PriceSetting::find($id)->update($state);
                $outcome = "Price Setting Updated Successfully";
            } else {
                $state['status'] = 1;
                $state = PriceSetting::create($state);
                $outcome = "Price Setting Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {

        $outcome = '';
        try {
            DB::beginTransaction();
            $state = State::find($id);

            if ($state->cities->count()) {
                return redirect()->back()->with('error', 'Some cities are associated to this state. Please delete cities first');
            }

            $state->delete();

            $outcome = "State deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }
    public function pd($type){
        return PriceSetting::where('type',$type)->first();
    }
}
