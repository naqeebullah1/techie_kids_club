<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PurchaseOrdersExport;
use App\Exports\TeacherSummaryExport;
use App\Models\District;

class ReportsController extends Controller {

    public function searchAll() {
        $rec = 10;
        $user = auth()->user();
        $userId = $user->id;
        $roles = $user->ModelHasRoles->pluck('role_id')->toArray();

        if (request()->print) {
            $data = User::selectRaw('concat(users.first_name," ",users.last_name) as rm_name,
                    schools.name as school_name,
                    school_classes.title as class_title,
                    concat(teachers.first_name," ",teachers.last_name) as teacher_name,
                    concat(students.first_name," ",students.last_name) as student_name,
                    concat(parents.first_name," ",parents.last_name) as parent_name                    
                    ')
                    ->leftJoin('schools', 'schools.user_id', 'users.id')
                    ->leftJoin('states as school_state', 'school_state.id', 'schools.state_id')
                    ->leftJoin('cities as school_city', 'school_city.id', 'schools.city_id')
                    ->leftJoin('school_classes', 'school_classes.school_id', 'schools.id')
                    ->leftJoin('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->leftJoin('admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->leftJoin('students', 'admission_details.student_id', 'students.id')
                    ->leftJoin('users as parents', 'parents.id', 'students.user_id');

            if (in_array(4, $roles)) {
                $data = $data->where('users.id', $userId);
            }
            if (in_array(2, $roles)) {
                $data = $data->where('teachers.id', $userId);
            }

            /*
             * RM
             */
            $keyword = request()->keyword;
            if (request()->only_this == '4') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(users.first_name," ",users.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('users.email', 'like', '%' . $keyword . '%');
                });
            }
            /*
             * Teachers
             */
            if (request()->only_this == '2') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('teachers.email', 'like', '%' . $keyword . '%');
                });
            }
            /*
             * Parents
             */
            if (request()->only_this == '3') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('parents.email', 'like', '%' . $keyword . '%');
                });
            }




            if (request()->only_this == 'c') {
                $data = $data
                        ->where(function($q) use($keyword) {
                    return $q->where('school_classes.title', 'like', '%' . $keyword . '%')
                            ->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                            ->orWhere('schools.name', 'like', '%' . $keyword . '%');
                });
            }
            if (request()->only_this == 'sc') {
                $data = $data
                        ->where(function($q) use($keyword) {
                    return $q->where('schools.name', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.address', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.email', 'like', '%' . $keyword . '%')
                            ->orWhere('schools.contact', 'like', '%' . $keyword . '%')
                            ->orWhere('school_state.state', 'like', '%' . $keyword . '%')
                            ->orWhere('school_city.city', 'like', '%' . $keyword . '%');
                });
            }
            if (request()->only_this == 's') {
                $data = $data->where(function($q) use($keyword) {
                    return $q->where(DB::raw('concat(students.first_name," ",students.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                                    ->orWhere('parents.email', 'like', '%' . $keyword . '%');
                });
            }

            return Excel::download(new PurchaseOrdersExport($data), 'summary-report.xlsx');
//            return redirect()->back();
        }

        if (request()->keyword) {

            $keyword = request()->keyword;
            /*
             * Reginal Managers Starts
             */
            $managers = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 4);
            });
            if (in_array(4, $roles)) {
                $managers = $managers->where('users.id', $userId);
            }
            if (in_array(2, $roles)) {
                $managers = $managers
                        ->join('schools as s', 's.user_id', 'users.id')
                        ->join('school_classes as sc', 'sc.user_id', 's.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('users.id');
            }
            $managers = $managers->paginate($rec);

            if (request()->type == '4') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.regional_director_table', compact('managers'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * Regional Manager Ends
             */

            /*
             * Teacher Starts
             */
            $teachers = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 2);
            });
//            if (in_array(2, $roles)) {
//                $teachers = $teachers->where('users.id', $userId);
//            }

            if (in_array(4, $roles)) {
                $teachers = $teachers
                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->join('schools as s', 's.id', 'sc.school_id')
                        ->where('s.user_id', $userId)
                        ->groupBy('users.id');
//                dd($userId);
            }
            if (in_array(2, $roles)) {
                $teachers = $teachers->where('users.id', $userId);
            }
            $teachers = $teachers->paginate($rec);

            /*
             * Teacher Ends
             */
            if (request()->type == '2') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.teachers_table', compact('teachers'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * Parents STARTS
             */


            $parents = User::where(function($q) use($keyword) {
                        return $q->orWhere(DB::raw('concat(first_name," ",last_name)'), 'like', '%' . $keyword . '%')
                                        ->orWhere('users.email', 'like', '%' . $keyword . '%');
                    })->whereHas('ModelHasRoles', function($q) {
                $q->where('role_id', 3);
            });
            if (in_array(4, $roles)) {
                $parents = $parents
                        ->join('admissions as a', 'a.parent_id', 'users.id')
                        ->join('admission_details as ad', 'ad.admission_id', 'a.id')
//                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->join('schools as s', 's.id', 'ad.school_id')
                        ->where('s.user_id', $userId)
                        ->groupBy('users.id');
            }
            if (in_array(2, $roles)) {
                $parents = $parents
                        ->join('admissions as a', 'a.parent_id', 'users.id')
                        ->join('admission_details as ad', 'ad.admission_id', 'a.id')
                        ->join('school_classes as sc', 'sc.user_id', 'users.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('users.id');
            }

            $parents = $parents->paginate($rec);



            if (request()->type == '3') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.parents_table', compact('parents'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }

            /*
             * PARENT ENDS
             */

            /*
             * Schools STARTS
             */
            $schools = School::select('schools.*', 'states.state', 'cities.city')
                    ->join('states', 'states.id', 'schools.state_id')
                    ->join('cities', 'cities.id', 'schools.city_id')
                    ->where(function($q) use($keyword) {
                return $q->where('name', 'like', '%' . $keyword . '%')
                        ->orWhere('address', 'like', '%' . $keyword . '%')
                        ->orWhere('schools.email', 'like', '%' . $keyword . '%')
                        ->orWhere('contact', 'like', '%' . $keyword . '%')
                        ->orWhere('states.state', 'like', '%' . $keyword . '%')
                        ->orWhere('cities.city', 'like', '%' . $keyword . '%');
            });
//            dd($schools);
            if (in_array(4, $roles)) {
                $schools = $schools->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $schools = $schools
                        ->join('school_classes as sc', 'sc.school_id', 'schools.id')
                        ->where('sc.user_id', $userId)
                        ->groupBy('schools.id');
            }

            $schools = $schools->paginate($rec);
            /*
             * SCHOOLS ENDS
             */
            if (request()->type == 'schools') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.schools_table', compact('schools'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * Students STarts
             */
          $students = Student::select('students.*', 'parents.email', 'parents.phone1', 'parents.id as parentId', DB::raw('concat(parents.first_name," ",parents.last_name) as parent'))
                    ->join('users as parents', 'parents.id', 'students.user_id')
                    ->where(function($q) use($keyword) {
                return $q->where(DB::raw('concat(students.first_name," ",students.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere(DB::raw('concat(parents.first_name," ",parents.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere('parents.email', 'like', '%' . $keyword . '%');
            });

            if (in_array(4, $roles)) {
                $students = $students
                        ->join('admission_details as ad', 'ad.student_id', 'students.id')
                        ->join('schools as schools', 'ad.school_id', 'schools.id')
                        ->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $students = $students
                        ->join('admission_details as ad', 'ad.student_id', 'students.id')
                        ->join('schools as schools', 'ad.school_id', 'schools.id')
                        ->join('school_classes as sc', 'sc.school_id', 'schools.id')
                        ->where('sc.user_id', $userId);
            }

            $students = $students->groupBy('students.id');
            $students = $students->paginate($rec);



            if (request()->type == 'students') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.students_table', compact('students'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * STudent Ends
             */

            /*
             * Classes Starts 
             */
            $classes = SchoolClass::select('school_classes.*', 'schools.name as school', DB::raw('concat(teachers.first_name," ",teachers.last_name) as teacher'))
                    ->join('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->where(function($q) use($keyword) {
                return $q->where('title', 'like', '%' . $keyword . '%')
                        ->orWhere(DB::raw('concat(teachers.first_name," ",teachers.last_name)'), 'like', '%' . $keyword . '%')
                        ->orWhere('schools.name', 'like', '%' . $keyword . '%');
            });

            if (in_array(4, $roles)) {
                $classes = $classes->where('schools.user_id', $userId);
            }
            if (in_array(2, $roles)) {
                $classes = $classes->where('school_classes.user_id', $userId);
            }

            $classes = $classes->paginate($rec);


            if (request()->type == 'school-classes') {
                if (request()->ajax()) {
                    $view = view('admin.reports.includes.school_classes_table', compact('classes'))
                            ->render();
                    return response()->json(['html' => $view, 'type' => request()->type]);
                }
            }
            /*
             * Classes Ends
             */

            $view = view('admin.reports.includes.all_tables', compact('managers', 'teachers', 'parents', 'schools', 'classes', 'students'))
                    ->render();
            return response()->json(['html' => $view, 'type' => 'main']);

//            dd(request()->all());
        }
        return view('admin.reports.search_all');
//        return view('admin.reports.search_all', compact('users', 'schools', 'classes'));
    }

    public function studentsSummary() {
        $districts = District::select('id', 'name');
        $roleId = auth()->user()->role;
        if ($roleId == 2 || $roleId == 4) {
            $classes = SchoolClass::select('schools.district_id')->join('schools', 'schools.id', 'school_classes.school_id');
            if ($roleId == 2) {
                $classes = $classes->where('school_classes.user_id', auth()->user()->id);
            }
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('district_id')
                    ->pluck('district_id')
                    ->toArray();
            $districts = $districts->whereIn('id', $classes);
        }
        $districts = $districts->pluck('name', 'id');

        if (request()->district_id) {
            $districtId = request()->district_id;
            $classes = SchoolClass::select(
                            'schools.name as school_name', 'schools.id as school_id', 'teachers.id as teacher_id', 'school_classes.id as sc_id', 'school_classes.title', 'directors.first_name as d_first_name', 'directors.last_name as d_last_name', 'teachers.first_name as t_first_name', 'teachers.last_name as t_last_name'
                    )
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->join('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->join('users as directors', 'directors.id', 'schools.user_id')
                    ->join('admission_details as admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->where('schools.district_id', $districtId);
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->get();
//dd($classes);
            $records = [];
            foreach ($classes as $class):
                $schoolId = $class->school_id;
                $key = $class->teacher_id;
                $scId = $class->title;
//                $records[$schoolId]['school'] = $class->school_name;
                $records[$schoolId][$key]['teacher_name'] = $class->t_first_name . ' ' . $class->t_last_name;
                $records[$schoolId][$key]['school_name'] = $class->school_name;
                $records[$schoolId][$key]['rd'] = $class->d_first_name . ' ' . $class->d_last_name;

                if (isset($records[$schoolId][$key]['classes'][$scId])) {
                    $records[$schoolId][$key]['classes'][$scId] = $records[$schoolId][$key]['classes'][$scId] + 1;
                } else {
                    $records[$schoolId][$key]['classes'][$scId] = 1;
                }
            endforeach;
//            dd($records);

            $uniqueClasses = SchoolClass::select('school_classes.id', 'school_classes.title')
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->where('schools.district_id', $districtId);
            if ($roleId == 4) {
                $uniqueClasses = $uniqueClasses->where('schools.user_id', auth()->user()->id);
            }

            $uniqueClasses = $uniqueClasses->orderBy('title', 'asc')
                    ->groupBy('title')
                    ->pluck('title', 'id')
                    ->toArray();
            return view('admin.reports.student_summary_table', compact('uniqueClasses', 'records'));
        }
        return view('admin.reports.student_summary', compact('districts'));
    }

    public function teacherGR() {
//        dd('all');
        $districts = District::select('id', 'name');
        $roleId = auth()->user()->role;
        if ($roleId == 2 || $roleId == 4) {
            $classes = SchoolClass::select('schools.district_id')->join('schools', 'schools.id', 'school_classes.school_id');
            if ($roleId == 2) {
                $classes = $classes->where('school_classes.user_id', auth()->user()->id);
            }
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('district_id')
                    ->pluck('district_id')
                    ->toArray();
            $districts = $districts->whereIn('id', $classes);
        }

        $districts = $districts->pluck('name', 'id');
        if (request()->district_id) {

            $districtId = request()->district_id;
            $classes = SchoolClass::select(
                            'schools.name as school_name', 'schools.id as school_id', 'directors.id as rd_id', 'teachers.id as teacher_id', 'school_classes.id as sc_id', 'school_classes.title', 'directors.first_name as d_first_name', 'directors.last_name as d_last_name', 'teachers.first_name as t_first_name', 'teachers.last_name as t_last_name'
                    )
                    ->selectRaw('count(admission_details.id) as class_students')
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->join('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->join('users as directors', 'directors.id', 'schools.user_id')
                    ->join('admission_details as admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->where('schools.district_id', $districtId);
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('teachers.id')
                    ->get();

            $records = [];
            foreach ($classes as $class):
                $key = $class->rd_id;

                $records[$key]['school'] = $class->school_name;
                $records[$key]['rd'] = $class->d_first_name . ' ' . $class->d_last_name;
                $records[$key]['teacher'][] = $class->t_first_name . ' ' . $class->t_last_name;
                $records[$key]['students'][] = $class->class_students;
                $records[$key]['bgs'][] = $this->rand_color();

                if (isset($records[$key]['rd_count'])) {
                    $records[$key]['rd_count'] = $records[$key]['rd_count'] + $class->class_students;
                } else {
                    $records[$key]['rd_count'] = $class->class_students;
                }
            endforeach;
            return $records;
        }
        return view('admin.reports.teacher_gr', compact('districts'));
    }

    public function teacherSummary() {
//        $districts = District::select('id', 'name')->pluck('name', 'id');
        $roleId = auth()->user()->role;
        if (request()->district_id) {
//dd('');
            $districtId = request()->district_id;
            $classes = SchoolClass::select(
                            'districts.name as district_name', 'schools.name as school_name', 'schools.id as school_id', 'directors.id as rd_id', 'teachers.id as teacher_id', 'school_classes.id as sc_id', 'school_classes.title', 'directors.first_name as d_first_name', 'directors.last_name as d_last_name', 'teachers.first_name as t_first_name', 'teachers.last_name as t_last_name')
                    ->selectRaw('count(admission_details.id) as class_students')
                    ->join('schools', 'schools.id', 'school_classes.school_id')
                    ->join('districts', 'schools.district_id', 'districts.id')
                    ->join('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->join('users as directors', 'directors.id', 'schools.user_id')
                    ->join('admission_details as admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->where('schools.district_id', $districtId);
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('teachers.id');

            if (request()->excel_export) {
                $classes = $classes->select('districts.name')
                                ->selectRaw('concat(directors.first_name," ",directors.last_name) as rd_name,concat(teachers.first_name," ",teachers.last_name) as t_name,count(admission_details.id) as class_students')->get();

                return Excel::download(new TeacherSummaryExport($classes), 'teacher-summary-report.xlsx');
            }

            $classes = $classes->get();
            return view('admin.reports.teacher_summary_table', compact('classes'));
        }
//        return view('admin.reports.teacher_summary', compact('districts'));
    }

    public function rand_color() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

}
