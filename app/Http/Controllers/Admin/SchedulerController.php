<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\DB;
use App\Models\TeacherNote;
use App\Models\School;
use App\Models\District;
use App\Models\NonScheduleTime;
use Illuminate\Support\Facades\Mail;
use Exception;

class SchedulerController extends Controller {

    public function index() {

        $districts = District::select('id', 'name');
//        dd();
        $roleId = auth()->user()->role;
        if ($roleId == 2 || $roleId == 4) {
            $classes = SchoolClass::select('schools.district_id')->join('schools', 'schools.id', 'school_classes.school_id');
            if ($roleId == 2) {
                $classes = $classes->where('school_classes.user_id', auth()->user()->id);
            }
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('district_id')
                    ->pluck('district_id')
                    ->toArray();
//            dd($classes);
            $districts = $districts->whereIn('id', $classes);
        }
        $districts = $districts->pluck('name', 'id');

        return view('admin.scheduler.index', compact('districts'));
    }

    public function formatDates($startDate, $endDate, $schoolClass, $onDays) {
//                dump($ignore);

        $startDate = new \DateTime($startDate);
        $endDate = new \DateTime($endDate);

        /*
         * SCHOOL CLASS START DATE
         */
        $classStartDate = $schoolClass->start_on;
        $classEndDate = $schoolClass->end_on;

        $weekDates = array();

        while ($startDate <= $endDate) {
            $eachDate = $startDate->format('Y-m-d');

            if (in_array($startDate->format('w'), $onDays) && ($eachDate >= $classStartDate) && ($eachDate <= $classEndDate)) {
                $weekDates[] = $eachDate;
            }
//            dd($weekDates);
            $startDate->modify('+1 day');
        }
//        dd($weekDates);
        return $weekDates;
    }

    public function splitMinutewise($startDate, $endDate, $schoolClass = null) {
//        dd($endDate);
        $startDate = new \DateTime($startDate);
        $endDate = new \DateTime($endDate);

        $onlyFromTime = "09:00";
        $onlyToTime = "17:00";
        $holidays = [];
        if ($schoolClass) {
            $onlyFromTime = date('H:i', strtotime($schoolClass->start_at));
            $onlyToTime = date('H:i', strtotime($schoolClass->end_at));
            if ($schoolClass->weekly_off_days) {
//                $holidays = explode(',', $schoolClass->weekly_off_days);
                $offDays = explode(',', $schoolClass->weekly_off_days);
                $weekAllDays = $this->weekDays();
                $holidays = array_diff($weekAllDays, $offDays);
//                dd($holidays);
            }
        }


        $weekDates = array();
        while ($startDate <= $endDate) {
            if (!in_array($startDate->format('w'), $holidays)) {
                $eachDate = $startDate->format('Y-m-d');
                $fromTime = new \DateTime($eachDate . " " . $onlyFromTime);
                $toTime = new \DateTime($eachDate . " " . $onlyToTime);
                while ($fromTime <= $toTime) {
                    $minDiff = $fromTime->format('Y-m-d H:i:s');
                    $fromTime->modify('+5 min');
                    $weekDates[date('dHi', strtotime($minDiff))] = $minDiff;
                }
            }
            $startDate->modify('+1 day');
        }

//        dd($weekDates);
        return $weekDates;
    }

    public function weekDays() {
        return [0, 1, 2, 3, 4, 5, 6];
    }

    public function schedulerData(Request $request) {
//        dd('yes');

        $classes = SchoolClass::select('school_classes.id', 'school_classes.user_id', 'school_classes.title', 'teachers.first_name', 'teachers.last_name', 'weekly_off_days', 'start_on', 'end_on', 'start_at', 'end_at')->join('users as teachers', 'teachers.id', 'school_classes.user_id');

        $data = $request->filter;
        $monthYear = explode('/', $data['month_year']);

        $teacherIds = $request->teacher_id;
        $districtId = $data['district_id'];
        $monthStart = $monthYear[1] . '-' . $monthYear[0] . '-01';
//        $teacherIds = [37];
//        $districtId = 1;
//        $monthStart = date('Y-m-d');



        $monthEnd = Date('Y-m-t', strtotime($monthStart));

        $classes = $classes->where('end_on', '>=', $monthStart); // ignore classes which ended in past
        $classes = $classes->where('start_on', '<=', $monthEnd); // Ignore classes which will start in future
        $classes = $classes->whereIn('user_id', $teacherIds); // Teacher Id
        $classes = $classes->whereHas('school', function($q) use($districtId) {
            return $q->where('district_id', $districtId);
        });


        $classes = $classes->orderBy('start_at' ,'asc')->get();
//dd($classes);

        $appointments = TeacherNote::where('note_date', '>=', $monthStart)
                ->where('note_date', '<=', $monthEnd)
                ->where('note_type', '=', 'appointment')
                ->whereHas('teacher_notes', function($q) use($teacherIds) {
                    return $q->whereIn('user_id', $teacherIds);
                })->orderBy('note_date' ,'desc')
                ->get();

        $finalResponse = [];
        $ind = 0;
//        dd($teacherIds);
        $nonScheduledTimes = NonScheduleTime::whereIn('user_id', $teacherIds)
                ->where('date', '>=', $monthStart)
                ->where('date', '<=', $monthEnd)
                ->get();
        $nonScheduledTimes = $nonScheduledTimes->groupBy('user_id')->toArray();

//        dd($nonScheduledTimes);

        foreach ($classes as $schoolClass):
//            $offDays = explode(',', $schoolClass->weekly_off_days);
//            $weekAllDays = $this->weekDays();
//            $onDays = array_diff($weekAllDays, $offDays);
            $onDays = explode(',', $schoolClass->weekly_off_days);
            $schoolClass->weekly_on_days = $onDays;

            $dates = $this->formatDates($monthStart, $monthEnd, $schoolClass, $onDays);
//            dd($dates);
            foreach ($dates as $date):
                $notes = TeacherNote::where('note_date', '=', $date)
                        ->where('note_type', '=', 'note')
                        ->where('school_class_id', '=', $schoolClass->id)
                        ->whereHas('teacher_notes', function($q) use($teacherIds) {
                            return $q->whereIn('user_id', $teacherIds);
                        })->orderBy('note_date' ,'desc')
                        ->get()
                        ->first();


                if ($notes) {
                    $uril = '?class_id=' . $schoolClass->id . '&date=' . $date . '&teacher_one_note_id=' . $notes->id;
                } else {
                    $notes = "";
                    $uril = '?class_id=' . $schoolClass->id . '&date=' . $date;
                }
//            $color = "#1dbb99";
                $color = "#3788d8";

                if ($notes != '') {
                    $color = '#f7cf5f';
                }

//                $startAt = date('H:i', strtotime($schoolClass->start_at));
//                $endAt = date('H:i', strtotime($schoolClass->end_at));

                $startAt = formatTime($schoolClass->start_at);
                $endAt = formatTime($schoolClass->end_at);
                $uril = $uril . "&start_at=$startAt&end_at=$endAt";


                /*
                 * Logic for Deleting the schedules which are exists in NON Scheduled Times
                 */

                $teacherId = $schoolClass->user_id;
                $createdIndex = $date . date('H:i:s', strtotime($schoolClass->start_at)) . date('H:i:s', strtotime($schoolClass->end_at));
                $ignore = [];
                if (isset($nonScheduledTimes[$teacherId])) {
                    $nonSchedules = $nonScheduledTimes[$teacherId];
                    foreach ($nonSchedules as $nonSchedule):
                        $ignore[] = $nonSchedule['date'] . $nonSchedule['start_at'] . '' . $nonSchedule['end_at'];
                    endforeach;
                }

                if (!in_array($createdIndex, $ignore)) {
//                if (true) {
                    $finalResponse[$ind] = [
                        'title' => "$schoolClass->first_name $schoolClass->last_name (" . $startAt . '-' . $endAt . ')',
                        'color' => $color,
                        'start' => $date,
                        'url' => $uril,
                    ];
                    $ind++;
                }









            endforeach;
        endforeach;


//        dd($finalResponse);
        foreach ($appointments as $date):
            $color = '#07c25f';
            /* if ($date->note_type == 'note') {
              $color = '#ffc107';
              } */
             
            $finalResponse[$ind] = [
                'title' => json_decode($date->teacher_notes, true)[0]['first_name'] ." ".json_decode($date->teacher_notes, true)[0]['last_name'] . " (" . $date->subject . ")",
                'start' => $date->note_date,
                'color' => $color,
                'url' => '?class_id=' . $schoolClass->id . '&date=' . $date->note_date . '&teacher_note_id=' . $date->id,
            ];
            $ind++;
        endforeach;

        usort($finalResponse, function ($a, $b) {
            $timeA = $this->extractTime($a["title"]);
            $timeB = $this->extractTime($b["title"]);

            // Extract AM/PM part
            $ampmA = substr($timeA, -2);
            $ampmB = substr($timeB, -2);

            // If AM/PM parts are different, sort by AM/PM
            if ($ampmA != $ampmB) {
                return strcmp($ampmA, $ampmB);
            }

            // If times are the same, sort by start date
            if ($timeA == $timeB) {
                return strcmp($a['start'], $b['start']);
            }

            // Otherwise, sort by time
            return strcmp($timeA, $timeB);
        });

// Function to extract time from the title


//dd('fuct');
        return ([
            'headerToolbar' => [
//                'left' => 'prev,next today',
//                'center' => 'title',
                'right' => 'dayGridMonth,listMonth'
            ],
            'initialDate' => $monthStart,
//            'navLinks' => true,
//            'editable' => true,
            'selectable' => true,
//            'businessHours' => true,
            'dayMaxEvents' => true, // allow "more" link when too many events
            'events' => $finalResponse
        ]);
    }

    private function extractTime($title)
    {
        preg_match('/\(([^)]+)\)/', $title, $matches);
        return isset($matches[1]) ? $matches[1] : '';
    }

    public function loadVacantTime(Request $request) {
//        dd($request->all());
        $classes = SchoolClass::select('school_classes.id', 'school_classes.title', 'teachers.first_name', 'teachers.last_name', 'weekly_off_days', 'start_on', 'end_on', 'start_at', 'end_at')->join('users as teachers', 'teachers.id', 'school_classes.user_id');
        $data = $request->filter;
        $monthYear = explode('/', $data['month_year']);

        $teacherIds = $request->teacher_id;
//        dd($teacherIds);
        $districtId = $data['district_id'];
        $monthStart = $monthYear[1] . '-' . $monthYear[0] . '-01';

        $monthEnd = Date('Y-m-t', strtotime($monthStart));

        $classes = $classes->where('end_on', '>=', $monthStart); // ignore classes which ended in past
        $classes = $classes->where('start_on', '<=', $monthEnd); // Ignore classes which will start in future
        $classes = $classes->whereIn('user_id', $teacherIds); // Teacher Id
        $classes = $classes->whereHas('school', function($q) use($districtId) {
            return $q->where('district_id', $districtId);
        });

        $classes = $classes->get();

        /*
         * List Of teacher Schedules/ Appointments
         */
        $teacherSchedules = TeacherNote::where('note_date', '>=', $monthStart)
                ->where('note_date', '<=', $monthEnd)
                ->whereHas('teacher_notes', function($q) use($teacherIds) {
                    return $q->whereIn('user_id', $teacherIds);
                })
                ->get();


        $timePeriodIntervals = $this->splitMinutewise($monthStart, $monthEnd);
        /*
         * This Loop will subtract Class Times
         */
        foreach ($classes as $schoolClass):
            $endOn = $schoolClass->end_on;
            $startOn = $schoolClass->start_on;
            if ($startOn < $monthStart) {
                $startOn = $monthStart;
            }
            if ($endOn > $monthEnd) {
                $endOn = $monthEnd;
            }
            $classTimesIntervals = $this->splitMinutewise($startOn, $endOn, $schoolClass);
            foreach ($classTimesIntervals as $index => $classTimesInterval):
                $timePeriodIntervals[$index] = "--";
            endforeach;
        endforeach;

        /*
         * This time will subtract whole day in which a teacher is having Appointment
         */
        foreach ($teacherSchedules as $schoolClass):
            $endOn = $schoolClass->note_date;
            $startOn = $schoolClass->note_date;

            if ($startOn < $monthStart) {
                $startOn = $monthStart;
            }
            if ($endOn > $monthEnd) {
                $endOn = $monthEnd;
            }
            $classTimesIntervals = $this->splitMinutewise($startOn, $endOn, $schoolClass);

            foreach ($classTimesIntervals as $index => $classTimesInterval):
                $timePeriodIntervals[$index] = "--";
            endforeach;
        endforeach;

        $ff = [];
        $a = 0;

        while ($current = current($timePeriodIntervals)) {
            if (end($ff) == false) {
                $ff[$a]['title'] = date('H:i', strtotime($current));
                $ff[$a]['start'] = date('Y-m-d', strtotime($current));
//            dd($ff[$a]['start']);
            }


            $next = next($timePeriodIntervals);

            if ($next == '--' && $current != '--') {
//                $ff[$a]['end'] = date('Y-m-d H:i:s', strtotime('+5 min', strtotime($current)));

                $endTime = date('H:i', strtotime($current));
                if ($endTime !== '17:00') {
                    $endTime = date('H:i', strtotime('+5 min', strtotime($current)));
                }

                $ff[$a]['title'] .= '-' . $endTime;
            }
            if ($current == '--' && $next != '--' && $next != false) {
                $a++;
//                $ff[$a]['title'] = date('H:i', strtotime('-5 min', strtotime($next)));
//                $ff[$a]['start'] = date('Y-m-d', strtotime('-5 min', strtotime($next)));

                $startTime = date('H:i', strtotime($next));

                if ($startTime == '09:00') {
                    $startTime = strtotime($next);
                } else {
                    $startTime = strtotime('-5 min', strtotime($next));
                }
                $ff[$a]['title'] = date('H:i', $startTime);
                $ff[$a]['start'] = date('Y-m-d', $startTime);
            }

            $currentDate = date('Y-m-d', strtotime($current));
            $nextDate = date('Y-m-d', strtotime($next));

            if ($current != '--' && $next != '--' && $currentDate != $nextDate) {
                $ff[$a]['title'] .= '-' . ' 17:00';

                if ($next != false) {
                    $a++;
                    $ff[$a]['title'] = '09:00';
                    $ff[$a]['start'] = $nextDate;
                }
            }
        }
//        dd('');
        return ([
            'headerToolbar' => [
//                'left' => 'prev,next today',
//                'center' => 'title',
                'right' => 'dayGridMonth,listMonth'
            ],
            'navLinks' => true, // can click day/week names to navigate views
//      'businessHours'=> true, // display business hours
//      'editable'=> true,
            'initialDate' => $monthStart,
            'selectable' => true,
            'dayMaxEvents' => true, // allow "more" link when too many events
            'events' => $ff
        ]);
    }

    public function weeklyDays() {
        return [];
    }

    public function vacantTime() {
        $districts = District::select('id', 'name');
        $roleId = auth()->user()->role;
        if ($roleId == 2 || $roleId == 4) {
            $classes = SchoolClass::select('schools.district_id')->join('schools', 'schools.id', 'school_classes.school_id');
            if ($roleId == 2) {
                $classes = $classes->where('school_classes.user_id', auth()->user()->id);
            }
            if ($roleId == 4) {
                $classes = $classes->where('schools.user_id', auth()->user()->id);
            }
            $classes = $classes->groupBy('district_id')
                    ->pluck('district_id')
                    ->toArray();
//            dd($classes);
            $districts = $districts->whereIn('id', $classes);
        }
        $districts = $districts->pluck('name', 'id');

        $teachers = User::getList(2)->get()->pluck('value', 'id');

        return view('admin.scheduler.vacant_time', compact('teachers', 'districts'));
    }

    public function notesStore(Request $request) {

        try {
            DB::beginTransaction();

            if (request()->is_delete) {
                $data = request()->all();
                $storeArray = [
                    'date' => date('Y-m-d', strtotime($data['notes']['note_date'])),
                    'start_at' => date('H:i:s', strtotime($data['start_at'])),
                    'end_at' => date('H:i:s', strtotime($data['end_at'])),
                    'user_id' => $data['teacher_ids']
                ];
                $save = NonScheduleTime::create($storeArray);
                $outcome = "Schedule Deleted Successfully.";
                DB::commit();

                $franchise = getFranchise();
                $teacherData = User::where(['id' => $data['teacher_ids']])->where(['status' => 1])->get()->first();
                $content = '<p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', your class deleted<br>'.
                    '<strong>' . date('M-d-Y', strtotime($data['notes']['note_date'])) . ' from ' .
                    date('h:i a', strtotime($data['start_at'])) . ' to ' .
                    date('h:i a', strtotime($data['end_at'])) . '</strong><br>
                                            <br />Thanks for being part of our '.$franchise->name.' team!
                                        </p>';

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="'.asset('frontend/images/logo/'.$franchise->header_logo).'" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                    ' . $content . '
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    $subject = ", your class delete";
                    try {
                    // Your email sending logic here
                    $subject = $teacherData['first_name'] . $subject;
                    sendEmail($subject,$teacherData['email'],$html);
                    
                    /*Mail::send(array(), array(), function ($message) use ($html, $teacherData, $subject) {
                                        $message->to($teacherData['email'])
                                            ->subject($teacherData['first_name'] . $subject)
                                            ->setBody($html, 'text/html');
                                    });*/
                } catch (Exception $e) {
                    // Log the error or handle it gracefully
                    \Log::error('Email sending error: ' . $e->getMessage());
                }
                }

                return array('success' => 1, 'message' => $outcome);
            }

            $note = $request->notes;
            $teacherIds = explode(',', $request->teacher_ids);

            $note['note_date'] = date('Y-m-d', strtotime($note['note_date']));
            if ($note['note_type'] == 'appointment') {
                $note['start_at'] = date('H:i:s', strtotime($note['start_at']));
                $note['end_at'] = date('H:i:s', strtotime($note['end_at']));
            }
            $id = $request->id;

            if ($id) {
                $getNote = TeacherNote::find($id);
                $getNote->update($note);

//                $getNote->teacher_notes()->detach();
//                $getNote->teacher_notes()->attach($teacherIds);

                $outcome = ucwords($note['note_type']) . " Updated Successfully";
            } else {
                $note = TeacherNote::create($note);
//                dd($teacherIds);
                $note->teacher_notes()->attach($teacherIds);
                $outcome = ucwords($note['note_type']) . " Created Successfully";
            }
            DB::commit();

            $franchise = getFranchise();
            foreach ($teacherIds as $teacherId) {
                $teacherData = User::where(['id' => $teacherId])->where(['status' => 1])->get()->first();
                if ($note['note_type'] == 'appointment') {
                    $content = '<p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', here are the details for the '
                        . ($id ? 'updated' : 'new') .' appointment on:<br />
                                            <strong>' . date('M-d-Y', strtotime($note["note_date"])) . ' from ' .
                        date('h:i a', strtotime($request->notes['start_at'])) . ' to ' .
                        date('h:i a', strtotime($request->notes['end_at'])) . '</strong><br>
                                            <strong>Subject: </strong>'.$note['subject'].'<br>
                                            <strong>Notes: </strong>'.$note['note'].'<br>
                                            <br />Thanks for being part of our '.$franchise->name.' team!
                                        </p>';
                } else {
                    $content = '<p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', your class details updated<br>
                                            <strong>Subject: </strong>'.$note['subject'].'<br>
                                            <strong>Notes: </strong>'.$note['note'].'<br>
                                            <br />Thanks for being part of our '.$franchise->name.' team!
                                        </p>';
                }

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="'.asset('frontend/images/logo/'.$franchise->header_logo).'" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                    ' . $content . '
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    $date = $note['note_date'];
                    if ($id || $note['note_type'] == 'note') {
                        $subject = ", your appointment details updated";
                    } else {
                        $subject = ", you have a new appointment at $date";
                    }
                    try {
                    // Your email sending logic here
                       $subject = $teacherData['first_name'] . $subject;
                     sendEmail($subject,$teacherData['email'],$html); 
                    
                       /*Mail::send(array(), array(), function ($message) use ($html, $teacherData, $date, $subject) {
                        $message->to($teacherData['email'])
                            ->subject($teacherData['first_name'] . $subject)
                            ->setBody($html, 'text/html');
                    });*/
                } catch (Exception $e) {
                    // Log the error or handle it gracefully
                    \Log::error('Email sending error: ' . $e->getMessage());
                }
                }
            }

            return array('success' => 1, 'message' => $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function notesForm() {
        $note = new TeacherNote();
        if (request()->teacher_note_id) {
            $note = TeacherNote::find(request()->teacher_note_id);

            if ($note->note_type == 'appointment') {
                $tId = $note->user_id;

                if ($tId == null)
                    $teacherIds = request()->teacher_id;

                $schools = School::whereHas('school_classes', function($q) use($tId) {
                            return $q->where('user_id', $tId);
                        })->select('name', 'id')
                        ->pluck('name', 'id')
                        ->toArray();

                $classesList = SchoolClass::where('user_id', $tId)->where('school_id', $note->school_id)->select('id', 'title')->pluck('title', 'id')->toArray();

                $view = view('admin.scheduler.note_form', compact('schools', 'note', 'tId', 'teacherIds', 'classesList'))
                        ->render();
                return response()->json(['ref' => '#side-container', 'html' => $view]);
            }
        }

        if (request()->teacher_one_note_id) {
            $note = TeacherNote::find(request()->teacher_one_note_id);
        }
        $classId = request()->class_id;
        $schoolClass = SchoolClass::find($classId);

        $view = view('admin.scheduler.notes_form', compact('schoolClass', 'note'))
                ->render();
        return response()->json(['ref' => '#side-container', 'html' => $view]);
    }

    public function noteForm() {
        $tId = request()->teacher_id;
        $teacher_name = User::where('id', '=', $tId)->get()->first();

//        dd($tId);
//        $schools = School::whereHas('school_classes', function($q) use($tId) {
//                    return $q->whereIn('user_id', $tId);
//                })->select('name', 'id')
//                ->pluck('name', 'id')
//                ->toArray();
        $note = new TeacherNote();
//        $classesList = [];
        $view = view('admin.scheduler.note_form', compact('note', 'tId', 'teacher_name'))
                ->render();
        return $view;
    }

    public function deleteNote(Request $request) {
        $find = TeacherNote::find($request->id);
        $franchise = getFranchise();
        if ($find) {
            try {
                TeacherNote::destroy($request->id);
                $deleteRelation = DB::delete('Delete From teacher_note_user where teacher_note_id=' . $request->id);
                $outcome = ucwords($find['note_type']) . " Deleted Successfully";

                $teacherData = User::where(['id' => $request->teacherId])->where(['status' => 1])->get()->first();
                $content = '<p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', your ' . ($request->note_type == 'note' ? 'class' : 'appointment') . ' deleted<br>'.
                    '<strong>' . date('M-d-Y', strtotime($request->date)) . ' from ' .
                    date('h:i a', strtotime($request->start_at)) . ' to ' .
                                            date('h:i a', strtotime($request->end_at)) . '</strong><br>
                                            <br />Thanks for being part of our '.$franchise->name.' team!
                                        </p>';

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="'.asset('frontend/images/logo/'.$franchise->header_logo).'" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                    ' . $content . '
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    if ($request->note_type == 'note') {
                        $subject = ", your class delete";
                    } else {
                        $subject = ", your appointment deleted";
                    }
                    try {
                    // Your email sending logic here
                     $subject = $teacherData['first_name'] . $subject;
                    sendEmail($subject,$teacherData['email'],$html); 
                    
                       /*Mail::send(array(), array(), function ($message) use ($html, $teacherData, $subject) {
                        $message->to($teacherData['email'])
                            ->subject($teacherData['first_name'] . $subject)
                            ->setBody($html, 'text/html');
                    });*/
                    } catch (Exception $e) {
                        // Log the error or handle it gracefully
                       
                    }

                return array('success' => 1, 'message' => $outcome);
            } catch (QueryException $e) {

                dd($e);
            }
        }
    }

    public function getTeachers() {


        $users = User::getList(2)
                ->join('school_classes', 'school_classes.user_id', 'users.id')
                ->join('schools', 'schools.id', 'school_classes.school_id')
                ->where('schools.district_id', request()->district_id);


        if (auth()->user()->role == 4) {
            $users = $users->where('schools.user_id', auth()->user()->id);
        }

        $users = $users->groupBy('users.id')
                ->get();
//        dd($users);

        $html = '<option value="">Select Teacher</option>';
        foreach ($users as $user):
            $html .= '<option value="' . $user->id . '">' . $user->value . '</option>';
        endforeach;
        return $html;
//        dd($users);
    }

    /*
     * Graphical Representation of teachers
     */
}
