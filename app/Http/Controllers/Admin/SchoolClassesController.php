<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\User;
use App\Models\AdmissionDetail;
use App\Models\Franchise;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;
use Stripe;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PurchaseOrdersExport;
use Exception;

class SchoolClassesController extends Controller {

    public function index(Request $request, $schoolId) {
        $school = School::find($schoolId);

        $classes = SchoolClass::where('school_id', $schoolId)->paginate(15);
        if (request()->print) {
            $classId = request()->class_id;
            $user = auth()->user();
            $roles = $user->ModelHasRoles->pluck('role_id')->toArray();
            $data = User::selectRaw('concat(users.first_name," ",users.last_name) as rm_name,
                    schools.name as school_name,
                    school_classes.title as class_title,
                    concat(teachers.first_name," ",teachers.last_name) as teacher_name,
                    concat(students.first_name," ",students.last_name) as student_name,
                    concat(parents.first_name," ",parents.last_name) as parent_name                    
                    ')
                    ->leftJoin('schools', 'schools.user_id', 'users.id')
                    ->leftJoin('states as school_state', 'school_state.id', 'schools.state_id')
                    ->leftJoin('cities as school_city', 'school_city.id', 'schools.city_id')
                    ->leftJoin('school_classes', 'school_classes.school_id', 'schools.id')
                    ->leftJoin('users as teachers', 'teachers.id', 'school_classes.user_id')
                    ->leftJoin('admission_details', 'admission_details.school_class_id', 'school_classes.id')
                    ->leftJoin('students', 'admission_details.student_id', 'students.id')
                    ->leftJoin('users as parents', 'parents.id', 'students.user_id');

            if (in_array(4, $roles)) {
                $data = $data->where('users.id', $user->id);
            }
            if (in_array(2, $roles)) {
                $data = $data->where('teachers.id', $user->id);
            }

            $data = $data
                    ->where(function($q) use($classId) {
                return $q->where('school_classes.id', $classId);
            });

            return Excel::download(new PurchaseOrdersExport($data), 'report-summary.xlsx');
        }


        if ($request->ajax()) {
            $view = view('admin.school_classes.table', compact('classes'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.school_classes.index', compact('classes', 'school'));
    }

    public function create($schoolId, $id = null) {
//        dd($id);
        $school = School::find($schoolId);
//        dd($school);
        $class = new SchoolClass();
        if ($id) {
            $class = SchoolClass::find($id);
        }
        $teachers = User::getList(2)->get();
//        dd();
//        dd($teachers->toArray());
        $view = view('admin.school_classes.form', compact('class', 'teachers', 'school'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            

            $record = $request->record;
            
            if($record['franchise_id']) {
                $franchise = Franchise::where('id',$record['franchise_id'])->first();
            } else {
                $franchise= getFranchise();
            }
            
            
            $record['start_on'] = date('Y-m-d', strtotime($record['start_on']));
            $record['end_on'] = "2099-01-01";
            $start_at = $record['start_at'];
            $record['start_at'] = date('H:i:00', strtotime($record['start_at']));
            $duration = $record['duration'] * 60;
            $end_at = date('H:i:00', strtotime($start_at) + $duration);
            $record['end_at'] = $end_at;
            $record['franchise_id'] = $franchise->id;
            unset($record['duration']);
            $id = $request->id;
            
            if (isset($record['weekly_off_days'])) {
                $record['weekly_off_days'] = implode(',', $record['weekly_off_days']);
            } else {
                $record['weekly_off_days'] = "";
            }
            if ($id) {
                $record['updated_by'] = auth()->user()->id;
                // Code to update price item in stripe if stripe_product_id exist
                // check if price or duration changed in edit
                $class_data = SchoolClass::where(['id' => $id])->get()->first();
                //$change_made = 0;
                if ($class_data['stripe_product_id'] != NULL && ($class_data['class_charges'] != $record['class_charges'] || strtolower($class_data['duration_type']) != strtolower($record['duration_type']))) {
                    //$change_made = 1;


                    $stripe_price_id = $class_data['stripe_product_id'];
                    $charges = round($record['class_charges'] * 100);
                    $duration = strtolower($record['duration_type']);
                    if ($duration == "monthly") {
                        $duration = "month";
                    } else if ($duration == "weekly") {
                        $duration = "week";
                    } else if ($duration == "yearly") {
                        $duration = "year";
                    }
                    // Now update at stripe
                    $stripe = new \Stripe\StripeClient($franchise->stripe_client);
                    $price = $stripe->prices->retrieve($stripe_price_id);


                    if ($price && ($price->unit_amount != $charges)) {
                        $product_id = $price->product;
                        // create new price and set active
                        $new_price = $stripe->prices->create([
                            'product' => $product_id,
                            'unit_amount' => $charges,
                            'currency' => 'usd',
                            'recurring' => ['interval' => $duration],
                            'active' => true
                        ]);
                        // Update school class with new price id
                        $class_id = $class_data['id'];
                        $updatedPrice = SchoolClass::where('id', $class_id)->update(['stripe_product_id' => $new_price->id]);
                        // Get All subscriptions against class id
                        $admission_data = AdmissionDetail::where(['school_class_id' => $class_id])->where('stripe_subscription_id', '!=', '0')->get();

                        // Update All subscriptions with new price id at stripe
                        foreach ($admission_data as $a_data) {
                            //check if subscription exist in stripe
                            try {
                                $get_subs = $stripe->subscriptions->retrieve($a_data->stripe_subscription_id);
                                //echo "<pre>".var_dump($get_subs->items['data'][0]['id'])."</pre>";
                                $subs_item_id = $get_subs->items['data'][0]['id'];
                                if ($get_subs) {
                                    // we need to update subscription item price


                                    try {

                                        $subs_updated = $stripe->subscriptions->update(
                                                $a_data->stripe_subscription_id, [
                                            'items' => [
                                                    [
                                                    'id' => $subs_item_id,
                                                    'price' => $new_price->id
                                                ],
                                            ],
                                            'proration_behavior' => 'none'
                                                ]
                                        );
                                    } catch (Stripe\Exception\InvalidRequestException $e) {
                                        $api_error = $e->getMessage();
                                        echo $api_error;
                                        continue;
                                    } catch (Stripe\Exception\ApiConnectionException $e) {
                                        $api_error = $e->getMessage();
                                        echo $api_error;
                                        continue;
                                    } catch (Exception $e) {
                                        $api_error = $e->getMessage();
                                        echo $api_error;
                                        continue;
                                    }
                                }
                            } catch (Stripe\Exception\InvalidRequestException $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            } catch (Stripe\Exception\ApiConnectionException $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            } catch (Exception $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            }
                        }
                        $update_price = $stripe->prices->update($stripe_price_id, ['active' => false]);
                    }
                    //dd($price);
                }


                // end of stripe code
                $record = SchoolClass::find($id)->update($record);
                $outcome = "School Class Updated Successfully";
            } else {
                $record['status'] = 1;
                
                $record = SchoolClass::create($record);
                
                $outcome = "School Class Created Successfully";
                // Send Email to Teacher

                $getTeacherId = SchoolClass::where(['id' => $record->id])->get()->first()['user_id'];
                $teacherData = User::where(['id' => $getTeacherId])->where(['status' => 1])->get()->first();

                $school_name = School::where(['id' => $record->school_id])->get()->first()['name'];
                $class_name = $record->title;


                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="'.asset('frontend/images/logo/'.$franchise->header_logo).'" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . $teacherData["first_name"] . ', here are the details for the new class you\'ll be teaching:<br />

<strong>' . $school_name . '</strong><br /> <strong>' . date('M-d-Y', strtotime($record->start_on)) . ' ' . date('h:m:i a', strtotime($record->start_at)) . '</strong><br />Thanks for being part of our '.$franchise->name.' team!
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    try {
                    // Your email sending logic here
                      $subject = $teacherData['first_name'] . ", you have a new class at $school_name";
                    sendEmail($subject,$teacherData['email'],$html); 
                      /* Mail::send(array(), array(), function ($message) use ($html, $teacherData, $school_name) {
                        $message->to($teacherData['email'])
                                ->subject($teacherData['first_name'] . ", you have a new class at $school_name")
                                ->setBody($html, 'text/html');
                    });*/
                    } catch (Exception $e) {
                        // Log the error or handle it gracefully
                        \Log::error('Email sending error: ' . $e->getMessage());
                    }
                }
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = SchoolClass::find($id);

            //ANY CHECK IF NEEDED
            /* if ($record->school_classes->count()) {
              return redirect()->back()->with('error', 'School is associated to this classes. Please delete Classes first');
              }
             * 
             */

            $record->delete();
            $outcome = "School Class deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function loadStd($id) {
        $students = \App\Models\AdmissionDetail::where('school_class_id', $id)->get();
//        dd($students);
        $view = view('admin.school_classes.cls_std', compact('students'))
                ->render();
        return response()->json(['html' => $view]);
    }

}
