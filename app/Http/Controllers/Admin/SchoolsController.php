<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\AdmissionDetail;
use App\Models\Admission;
use App\Models\State;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Franchise;
use Stripe;

class SchoolsController extends Controller {

    public function index(Request $request) {
//        dump($request->all());

        $schools = School::query();
        if ($request->state_id) {
            $schools = $schools->where('state_id', $request->state_id);
        }
        if ($request->city_id) {
            $schools = $schools->where('city_id', $request->city_id);
        }
        if ($request->school_name) {
            $schools = $schools->where('name', 'like', "%" . $request->school_name . "%");
        }
        $schools = $schools->paginate(15);

        if ($request->ajax()) {
            $view = view('admin.schools.table', compact('schools'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
        $states = State::select('state', 'id')->where('status', 1)->get();
//        dd($states);
        return view('admin.schools.index', compact('schools', 'roles', 'states'));
    }

    public function create($id = null) {
//        dd('');
        $school = new School();
        if ($id) {
            $school = School::find($id);
        }
        $states = State::dropdown()->getAll();
        $districts = \App\Models\District::where('status', 1)->select('id', 'name')->pluck('name', 'id')->toArray();

        $cities = [];
        if ($school->state_id) {
            $sId = $school->state_id;
            $cities = $this->citiesList($sId);
        }
        $managers = User::getList(4)->get();
        $schoolAdmins = User::selectRaw('concat(last_name," - ",email) value,id')->getList(5)->pluck('value', 'id');
//        dd($schoolAdmins);

        $view = view('admin.schools.form', compact('school', 'states', 'cities', 'managers', 'schoolAdmins', 'districts'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function citiesDD() {
        $sId = request()->state_id;
        $value = request()->city_id;
        $selectlabel = "City";

        $options = $this->citiesList($sId);
        $view = view('partials.dropdowns', compact('options', 'value', 'selectlabel'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }

    public function schoolsDD() {
        $cityId = request()->city_id;
        $schools = School::where('city_id', $cityId)->select('name', 'id')->whereHas('school_classes', function($q) {
                    //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                    return $q->whereNotNull('school_classes.start_on');
                })->get();
        $schoolHtml = '<option value="">Select School</option>';

        foreach ($schools as $school):
            $schoolHtml .= '<option ' . (request()->school_id == $school->id ? 'selected' : '') . ' value="' . $school->id . '">' . $school->name . '</option>';
        endforeach;
        return $schoolHtml;
    }

    public function schoolsParentsDD() {
        $cityId = request()->city_id;
        $schools = School::where('city_id', $cityId)->select('name', 'id')->whereHas('school_classes', function($q) {
                    //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                    return $q->whereNotNull('school_classes.start_on');
                })->whereHas('school_classes.admission_details', function($q) {
                    //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                    return $q;
                })->get();
        $schoolHtml = '<option value="">Select School</option>';

        foreach ($schools as $school):

            $schoolHtml .= '<option value="' . $school->id . '">' . $school->name . '</option>';
        endforeach;
        return $schoolHtml;
    }

    public function schoolClasses() {
        $cityId = request()->school_id;
        $schools = SchoolClass::where('school_id', $cityId)->select('title', 'id')->get();
//        $schoolHtml = '<div class="col-12 text-danger">No Class Found</div>';
        $schoolHtml = '<div class="mt-2 mb-2 col-12" style="font-weight:bold;">Select Classes</div>';

        foreach ($schools as $school):
            $schoolHtml .= '<div class="col-2">'
                    . '<label>'
                    . '<input name="school_class_ids[]" type="checkbox" checked value="' . $school->id . '"> ' . $school->title
                    . '</label>'
                    . '</div>';
        endforeach;
        return $schoolHtml;
    }

    public function loadSchoolClasses() {

        $cityId = request()->school_id;
        $teacherId = request()->teacher_id;
        if (auth()->user()->role == 2) {
            $teacherId = auth()->user()->id;
        }

        $schools = SchoolClass::where('school_id', $cityId);
        if ($teacherId) {
            $schools = $schools->where('user_id', $teacherId);
        }


        $schools = $schools->select('title', 'id')
                ->get();
        $schoolHtml = '<option value="">Select Class</option>';

        foreach ($schools as $school):
            $schoolHtml .= '<option value="' . $school->id . '">' . $school->title . '</option>';
        endforeach;
        return $schoolHtml;
    }

    public function schoolClassesWithParents() {
        $schoolIds = request()->school_id;
        $cityId = request()->city_id;

        $schools = SchoolClass::select('title', 'id')
                ->whereHas('admission_details', function($q) {
            //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
            return $q;
        });

        if (request()->all_schools != "1") {
            if ($schoolIds) {
                $schoolIds = explode(',', $schoolIds);
                $schools = $schools->whereIn('school_id', $schoolIds);
            }
            if ($cityId) {
//           dd('yes');
                $schools = $schools->whereHas('school', function($q) use($cityId) {
                    return $q->where('city_id', $cityId);
                });
            }
        } else {
            $schools = $schools->whereNotNull('school_id');
        }

        $schools = $schools->get();

        if (request()->is_dd) {
            $schoolHtml = '<div class="mt-2 mb-2 col-12" style="font-weight:bold;"><label>Select Classes [showing classes that contain admissions]</label>'
                    . '<select name="school_class_ids[]" id="school_class_ids" class="ajax-select2" multiple="">';
            foreach ($schools as $school):
                $schoolHtml .= '<option selected value="' . $school->id . '">' . $school->title . '</option>';
            endforeach;
            $schoolHtml = $schoolHtml . '</select></div>';

            return $schoolHtml;
        }

        $schoolHtml = '<div class="mt-2 mb-2 col-12" style="font-weight:bold;">Select Classes [showing classes that contain admissions]</div>';
        foreach ($schools as $school):
            $schoolHtml .= '<div class="col-2">'
                    . '<label>'
                    . '<input name="school_class_ids[]" type="checkbox" checked value="' . $school->id . '"> ' . $school->title
                    . '</label>'
                    . '</div>';
        endforeach;
        return $schoolHtml;
    }

    public function citiesList($sId) {
        $sId = State::find($sId)->state_code;
        $cities = City::dropdown()
                        ->whereState($sId)->orderBy('city', 'ASC');
        if (request()->ignore_without_schools) {
            $getSchools = School::select('city_id')->whereHas('school_classes', function($q) {
                        //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                        return $q->whereNotNull('school_classes.start_on');
                    })->groupBy('city_id')->get();
            $cityWithData = $getSchools->pluck('city_id')->toArray();
            $cities = $cities->whereIn('id', $cityWithData);
        }
        return $cities->getAll();
    }

    public function store(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $record = $request->record;
            $id = $request->id;

            if ($request->school_admin_id == "") {
                $record['school_admin_id'] = 0;
            }

            if ($request->hasFile('image')) {

                $originName = $request->file('image')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = date('ymdhis') . '.' . $extension;

                $request->file('image')->move(public_path('images/uploads'), $fileName);
                $record['image'] = $fileName;
            }
            if ($id) {
                $record['updated_by'] = auth()->user()->id;
                $school = School::find($id);

                if (request()->form_franchise_id && request()->form_franchise_id != $school->franchise_id) {
                    $record['franchise_id'] = request()->form_franchise_id;
                    /*
                     * Cancel all subscriptions if existing Franchise of school was not Techie Kids
                     */

                    if($school->franchise_id != 1) {
                    //Get Stripe Info from the school existing franchise
                    $stripeInfo =  Franchise::find($school->franchise_id);

                    $stripeSecret = $stripeInfo->stripe_client;
                    if($stripeSecret != "") {
                        // Get All admission details by school Id with Active Subscriptions
                        $admissionDetails = AdmissionDetail::where('school_id',$id)->where('status',1)->get();
                        $failedSubscription = [];
                        //Loop the record to get Subscription Id and cancel it
                        foreach($admissionDetails as $k => $data) {
                               // Get Stripe Subscription Id
                               $stripeSubscriptionId = $data->stripe_subscription_id;
                               if($stripeSubscriptionId != "" && $stripeSubscriptionId != 0) {
                                // Now Start Cancelling Subscription in Stripe

                                Stripe\Stripe::setApiKey($stripeSecret);

                                try {
                                    $subscriptionData = \Stripe\Subscription::retrieve($stripeSubscriptionId);
                                    $subscriptionData->cancel();
                                    $subs = AdmissionDetail::where(['stripe_subscription_id' => $stripeSubscriptionId])->get()->first();

                                    // Update Admission Details Table with Status 0
                                    if ($subs) {
                                        $subs->status = 0;
                                        $subs->save();
                                    }

                                } catch (Exception $e) {
                                    $api_error = $e->getMessage();
                                    // Add Subscription Id to Failed array
                                    $failedSubscription[] = $stripeSubscriptionId;
                                }
                                catch (Stripe\Exception\InvalidRequestException $e) {
                                    $api_error = $e->getMessage();
                                    // Add Subscription Id to Failed array
                                    $failedSubscription[] = $stripeSubscriptionId;
                                }
                               }
                            }
                        }
                    }// End of If franchise != 1
                     /*
                     * Update Classes here
                     */
                    $schoolClasses = $school->school_classes()->update(['franchise_id' => request()->form_franchise_id,'stripe_product_id'=>'']);
                    // Update User table with new franchise Id for Parent. This will be required on Parent Dashboard for ino purpose only
                    // First we find users connecting to admision tables
                    $users = AdmissionDetail::where('school_id',$id)->get();
                    //dd($users);
                    $parent_ids = [];
                    foreach($users as $ad_user) {
                        /*$array = $ad_user->admission()->get();
                        $collection = $collection->map(function ($array) {
                            return collect($array)->unique('parent_id')->all();
                        });*/

                        foreach($ad_user->admission()->get() as $fn_user) {
                            $fn_user_exist = $fn_user->admission_parent()->get();
                            if(count($fn_user_exist)>0) {
                                 array_push($parent_ids, $fn_user->parent_id);
                            }
                        }
                    }


                     foreach(array_unique($parent_ids) as $final_update) {
                        // Here we will update parents with new franchise id
                        $parent_up = User::find($final_update);

                        $parent_up->franchise_id = request()->form_franchise_id;
                        $parent_up->save();
                     }

                    /*$admission_parents = Admission::whereHas('details', function ($query) use ($school) {
                        $query->where('school_id', $school->id);
                    })->get();
                    dd($admission_parents->school_id);*/
                    /*$users = AdmissionDetail::whereHas('admission', function ($query) use ($school) {
                    $query->where('school_id', $school->id);
                })->get();
                    dd($users->admission());*/
                }

                $school->update($record);
                if(!empty($failedSubscription)) {
                    $failedSubs = implode(', ', $failedSubscription);
                    $outcome = "School Updated Successfully But Some Subscription Did Not Cancelled ( ".$failedSubs." )";
                } else {
                    $outcome = "School Updated Successfully";
                }

            } else {
                School::create($record);
                $outcome = "School Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            //dd($e);
        }
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = School::find($id);
            if ($record->school_classes->count()) {
                return redirect()->back()->with('error', 'School is associated to this classes. Please delete Classes first');
            }
            $record->delete();
            $outcome = "School deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
