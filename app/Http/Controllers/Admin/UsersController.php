<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ParentsExport;
use App\Http\Controllers\Controller;
use App\Models\ModelHasRole;
use App\Models\Student;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use Spatie\Permission\Models\Role;
use Exception;

class UsersController extends Controller {

    function __construct() {
        $this->middleware('permission:user-list', ['only' => ['index']]);
//        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $roleId = NULL) {

        $roles = Role::all();
        $field = 'id';
        $order = 'DESC';
        if (!empty($request->sort_field) && !empty($request->sort_order)) {
            $field = $request->sort_field;
            $order = $request->sort_order;
        }

        if ($roleId) {
            $user = User::with('ModelHasRoles')
                    ->singleRole($roleId)
                    ->orderBy($field, $order);
        } else {
            $user = User::with('ModelHasRoles')
                    ->orderBy($field, $order);
        }

        if ($request->first_name) {
            $user = $user->where('first_name', 'like', '%' . $request->first_name . '%');
        }
        if ($request->last_name) {
            $user = $user->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $user = $user->where('email', 'like', '%' . $request->email . '%');
        }
        /*
         * Ignore parents
         */
        $user = $user->whereHas('ModelHasRoles', function ($query) use($roleId) {
            $query->where('role_id', '!=', 3);
        });

        /*
         * Admin Schools
         *
         */

        /*
         * $authRoles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
          if (in_array(5, $authRoles)):
          $user = $user->whereHas('ModelHasRoles', function ($query) use($roleId) {
          $query->whereNotIn('role_id', [5, 1]);
          });

          endif;
         *
         */

        if ($request->role) {
            $roleId = $request->role;
            $user = $user->whereHas('ModelHasRoles', function ($query) use($roleId) {
                $query->where('role_id', '=', $roleId);
            });
        }
        $records = 10;
        if ($request->status != null) {
            $user = $user->where('status', $request->status);
        }
        $user = $user->paginate($records);
        if ($request->ajax()) {
            $pageNo = $request->input('page', 1);
            if ($request->is_fresh_data == 1) {
                $pageNo = 1;
            }
            $view = view('admin.users.partials.loop.list', ['users' => $user, 'roles' => $roles])->with('i', ($pageNo - 1) * $records)->render();
            return response()->json(['html' => $view]);
        }
        return view('admin.users.list', ['users' => $user, 'roles' => $roles, 'roleId' => $roleId])->with('i', ($request->input('page', 1) - 1) * $records);
    }

    public function parentlist(Request $request, $roleId = 3) {
//dd($request->all());
        $roles = Role::all();
        $field = 'id';
        $order = 'DESC';
        if (!empty($request->sort_field) && !empty($request->sort_order)) {
            $field = $request->sort_field;
            $order = $request->sort_order;
        }

        $user = User::with('ModelHasRoles')
                ->singleRole($roleId)
                ->orderBy($field, $order);

        if ($request->first_name) {
            $user = $user->where('first_name', 'like', '%' . $request->first_name . '%');
        }
        if ($request->last_name) {
            $user = $user->where('last_name', 'like', '%' . $request->last_name . '%');
        }
        if ($request->email) {
            $user = $user->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->childfname) {
            $childfname = $request->childfname;
            $user = $user->whereHas('Students', function ($query) use($childfname) {
                $query->where('first_name', '=', $childfname);
            });
        }
        if ($request->from_date) {
            $childfname = date('Y-m-d', strtotime($request->from_date));
            $user = $user->whereDate('created_at', '>=', $childfname);
        }
        if ($request->to_date) {
            $childfname = date('Y-m-d', strtotime($request->to_date));
            $user = $user->whereDate('created_at', '<=', $childfname);
        }

        /* if ($request->role) {
          $roleId = $request->role;
          $user = $user->whereHas('ModelHasRoles', function ($query) use($roleId) {
          $query->where('role_id', '=', $roleId);
          });
          } */
        $records = 10;
        if ($request->status != null) {
            $user = $user->where('status', $request->status);
        }

        if ($request->state_id) {
            $state_id = $request->state_id;
            $user = $user->whereHas('Admissions', function ($query) use ($state_id) {
                $query->whereHas('Details', function ($query) use ($state_id) {
                    $query->whereHas('School', function ($query) use ($state_id) {
                        $query->where('schools.state_id', '=', $state_id);
                        $city_id = request()->city_id;
                        $school_id = request()->school_id;
                        if ($city_id) {
                            $query->where('schools.city_id', '=', $city_id);
                        }
                        if ($school_id) {
                            $query->where('schools.id', '=', $school_id);
                        }
                    });
                });
            });
        }

        if ($request->export) {
            $user = $user->select('first_name', 'last_name', 'email', 'phone1', 'phone2');
//            dd($user->first());
            return Excel::download(new ParentsExport($user), 'parents-list.xlsx');
            dd($user);
        }
        $user = $user->withCount('Students')->paginate($records);
        if ($request->ajax()) {
            $pageNo = $request->input('page', 1);
            if ($request->is_fresh_data == 1) {
                $pageNo = 1;
            }
            $view = view('admin.users.partials.loop.parentlist', ['users' => $user, 'roles' => $roles])->with('i', ($pageNo - 1) * $records)->render();
            return response()->json(['html' => $view]);
        }

        $states = \App\Models\State::orderBy('state', 'ASC')->get();
        return view('admin.users.parentlist', ['users' => $user, 'roles' => $roles, 'roleId' => $roleId, 'states' => $states])->with('i', ($request->input('page', 1) - 1) * $records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
//        dd('');
        $franchises = \App\Models\Franchise::pluck('name', 'id')->toArray();
        return view('admin.users.create', ['roles' => Role::all(), 'franchises' => $franchises]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function refreshToken($refreshToken = "6Jat4YF0mV-4GOMdRyubkSQgT0Ui1jvPwJp5DexsN4U", $clientId = "e1438694-f30a-44e5-b1c8-65ef35d08ef0", $clientSecret = "d1ieVcCzR7SOMOcxD9mZiQ") {
    public function refreshToken($refreshToken = "wdB_oaZxFbEPP0ytq_PuSPNI3cgeFswXOTLtjSEhkVw", $clientId = "2a963702-8428-42c6-956e-a5ea67bce796", $clientSecret = "aSlhvz0AGdbZaCcQBY8GZA") {
        /*
         * for detail review
         * D:\xampp\htdocs\techie_kids_club\documentation\constant_contact_api.txt
         */

        // Use cURL to get a new access token and refresh token

        $ch = curl_init();
        // Define base URL
        $base = 'https://authz.constantcontact.com/oauth2/default/v1/token';

        // Create full request URL
        $url = $base . '?refresh_token=' . $refreshToken . '&grant_type=refresh_token';
        curl_setopt($ch, CURLOPT_URL, $url);

        // Set authorization header
        // Make string of "API_KEY:SECRET"
        $auth = $clientId . ':' . $clientSecret;
        // Base64 encode it
        $credentials = base64_encode($auth);
        // Create and set the Authorization header to use the encoded credentials, and set the Content-Type header
        $authorization = 'Authorization: Basic ' . $credentials;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization, 'Content-Type: application/x-www-form-urlencoded'));

        // Set method and to expect response
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Make the call
        $result = curl_exec($ch);
        curl_close($ch);
//        dd($result);
        return json_decode($result);
    }

    public function postConstantContact($data) {
        $postData = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email_address' => [
                'address' => $data['email'],
                'permission_to_send' => 'implicit',
            ],
            'phone' => $data['phone1'],
            'phone2' => $data['phone2'],
            "create_source" => "Account"
        ];
//        dd();

        /*
         * Sample Data
          '{
          "email_address": {
          "address": "jhondoe@gmail.com",
          "permission_to_send": "implicit"
          },
          "first_name": "jhon",
          "last_name": "doe",
          "job_title": "Musician",
          "company_name": "Acme Corp.",
          "create_source": "Account",
          "birthday_month": 11,
          "birthday_day": 24,
          "anniversary": "2006-11-15",
          "steet_addresses":{
          "kind":"home",
          "street": "123 Kashmir Valley Road",
          "city": "Chicago",
          "state": "Illinois",
          "country": "United States"
          }
          }'
         */

        $refreshToken = $this->refreshToken();
        $accessToken = $refreshToken->access_token;
//        dd($refreshToken);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.cc.email/v3/contacts');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer ' . $accessToken;
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return ($result);
//        dd($result);
    }

    public function store(Request $request) {
//        dd($request->all());
        $request->merge(['status' => 1]);
        $request->merge(['password' => 'cyberclouds@6']);
        if ($request->is_parent) {

            $today = date('Y-m-d H:i:s', time());
            $request->merge(['check_privacy_date' => $today]);
            $request->merge(['check_terms_date' => $today]);

            $valdidateData = Validator::make($request->all(), [
                        'first_name' => 'required | min:3',
                        'last_name' => 'required | min:2',
                        'email' => 'required | email | confirmed | unique:users',
                        'email_confirmation' => 'required',
                        'password' => 'required | min:8',
                        'roles' => 'required',
                        'status' => 'required',
                        'phone1' => 'required',
                        'phone2' => '',
                        'address1' => '',
                        'address2' => '',
                        'city' => '',
                        'state' => '',
                        'zip' => '',
                        'check_privacy' => 'required',
                        'check_terms' => 'required',
                        'check_privacy_date' => 'required',
                        'check_terms_date' => 'required',
                        'students.*.first_name' => 'required',
                        'students.*.last_name' => 'required',
                            ], [
                        'students.*.first_name.required' => 'First name of the Child(ren) is required',
                        'students.*.last_name.required' => 'Last name of the Child(ren) is required',
            ]);
            if ($valdidateData->fails()) {
                return redirect()->back()->withErrors($valdidateData)->withInput()->withFragment('#pills-register');
            }
        } else {
            $valdidateData = $request->validate([
                'first_name' => 'required | min:3',
                'last_name' => 'required | min:2',
                'email' => 'required | email | confirmed | unique:users',
                'email_confirmation' => 'required',
                'password' => 'required | min:8',
                //'password_confirmation' => 'required',
                'roles' => 'required',
                'status' => 'required',
                'phone1' => '',
                'phone2' => '',
                'address1' => '',
                'address2' => '',
                'city' => '',
                'state' => '',
                'zip' => '',
            ]);
        }

        try {
            DB::beginTransaction();

            $valdidateData = $request->all();
//dd($request->roles);

            if (in_array(2, $request->roles) || in_array(4, $request->roles)) {
                if ($request->hasFile('files')) {
                    //get filename with extension
                    $files = $request->file('files');
0
                    $storeFiles = [];
                    foreach ($files as $k => $file):
                        $filenamewithextension = $file->getClientOriginalName();
                        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                        $extension = $file->getClientOriginalExtension();
                        $filenametostore = $filename . '-' . time() . '.' . $extension;
                        $file->move(public_path('user_files'), $filenametostore);

                        $storeFiles[] = $filenametostore;
                    endforeach;
                    $valdidateData['files'] = implode(',', $storeFiles);
                }
            }
            if ($request->is_parent) {
//                dd($valdidateData);
                $postcc = $this->postConstantContact($valdidateData);
//                dd($postcc);
            }
            if (request()->user_franchise_id) {
                $valdidateData['franchise_id'] = request()->user_franchise_id;
            }
//            dd($valdidateData);
            $user = User::create($valdidateData);
//dd($user);
            $franchise = getFranchise();

            if (request()->user_franchise_id) {
                \App\Models\Franchise::find(request()->user_franchise_id)
                        ->update(['user_id' => $user->id]);
            $franchise = IDToSlug(request()->user_franchise_id);
            }

            $students = $request->students;
            if ($students) {
                foreach ($students as $student):
                    if (isset($student['image'])) {
                        $file = $student['image'];
                        $originName = $file->getClientOriginalName();
                        $fileName = pathinfo($originName, PATHINFO_FILENAME);
                        $extension = $file->getClientOriginalExtension();
                        $fileName = date('ymdhis') . '.' . $extension;

                        $file->move(public_path('images/uploads'), $fileName);
                        $student['image'] = $fileName;
                    }
                    $student['user_id'] = $user->id;
                    Student::create($student);
                endforeach;
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
        $user->assignRole($request->input('roles'));
        if ($user) {
            $reset_token = base64_encode(rand() . time());
            $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
            $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];

            $user_update = DB::table('users')
                    ->where(['id' => $user->id])
                    ->update($data_update);
//            dd();
//            dd($franchise);
            if ($request->input('roles')[0] == 3) { // For Parent
                // Email Stuff for Parent
                $reset_url = frontendBaseUrl() . "/resetPassword/" . $reset_token;
                // Welcome Email
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">It\'s nice to meet you!</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            ' . $franchise->name . ' teaches the foundations of computer programming while creating an entirely new world of learning for your child.
                                                <br />Now what? Your ' . $franchise->name . ' account lets you:
                                                <br />- Enroll your child in classes
                                                <br />- Meet the coach and find out what they\'re learning each week
                                                <br />- Get bonus On-Demand video classes from anywhere!
                                                <br />- Ready to get back into your account?
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(url('front-franchise/' . $franchise->slug))) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';

                try {
                    $subject = "Welcome to " . $franchise->name . "  " . $user->first_name;
                    sendEmail($subject,$user->email,$html);

                   /* $verify = Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                            $message->to($user->email)
                                    ->subject("Welcome to " . $franchise->name . "  " . $user->first_name)
                                    ->setBody($html, 'text/html');
                        });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }
                // Send Reset Password Email
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Let\'s set your account password</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you a password. A unique link to set your new
                                            password has been generated for you. To create your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(url('front-franchise/' . $franchise->slug))) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                try{
                 $subject = $franchise->name . " Account Password Reset";
                    sendEmail($subject,$user->email,$html);  
                    /*$verify = Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                            $message->to($user->email)
                                    ->subject($franchise->name . " Account Password Set")
                                    ->setBody($html, 'text/html');
                        });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }   


//                dd('i am here');
            } else if ($request->input('roles')[0] == 2 || $request->input('roles')[0] == 4 || $request->input('roles')[0] == 5) {
                // Email Stuff Front End Users
                //$reset_url = frontendBaseUrl() . "/resetPassword/" . $reset_token;
                $reset_url = frontendBaseUrl($franchise->slug) . "/backend/resetPassword/" . $reset_token;
                // Send Reset Password Email
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Welcome to ' . $franchise->name . '</h1>
                                        <h2 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Let\'s set your account password</h2>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you a password. A unique link to set your new
                                            password has been generated for you. To create your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(url('front-franchise/' . $franchise->slug))) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                try {

                $subject = $franchise->name . " Account Password Reset";
                    sendEmail($subject,$user->email,$html);  
                    
               /* $verify = Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                            $message->to($user->email)
                                    ->subject($franchise->name . " Account Password Set")
                                    ->setBody($html, 'text/html');
                        });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }
            } else { // If non frontend user
                $reset_url = frontendBaseUrl($franchise->slug) . "/backend/resetPassword/" . $reset_token;
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl($franchise->slug) . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Your Login Credentials for ' . $franchise->name . '</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you your old password. A unique link to reset your
                                            password has been generated for you. To reset your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">
                                            Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(url('front-franchise/' . $franchise->slug))) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                //$html = "Hi " . $user->first_name." ". $user->last_name . ", <br /><br /> Below is your password reset link. Please click it to set your Password.<br><br><a href='$reset_url'>$reset_url</a><br /><br />Thanks<br />" . env('COMPANY_NAME');
                try {

                $subject = $franchise->name . " Welcome to '.$franchise->name.'";
                    sendEmail($subject,$user->email,$html);  
                    
               /*                     
                $verify = Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                            $message->to($user->email)
                                    ->subject($franchise->name . " Welcome to '.$franchise->name.'")
                                    ->setBody($html, 'text/html');
                        });*/
                }catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }
            }// end of else

            if ($request->is_parent) {
                $responseBody = null;
                if ($request->check_sms_service) {
                    $responseBody = sendOptIn($user);
                }
                if ($responseBody != null && $responseBody->Success) {
                    return redirect()->back()->with([
                        'success' => 'Success: A password reset link has been sent on your email, Please check and follow instructions',
                        'messaging_status' => "A welcome message was sent to your phone. Please reply 'yes' to complete the opt-in process."
                    ]);
                } else {
                    return redirect()->back()->with('success', 'Success: A password reset link has been sent on your email, Please check and follow instructions');
                }
            }
            return redirect()->route('admin.users.index')->with('success', 'User account created, A link to set your password has been sent on your email.');
        } else {
            if ($request->is_parent) {
                return redirect()->back()->with('error', 'Error')->withFragment('#pills-register');
            }
            return redirect()->route('admin.users.create')->with('error', 'Account not created, please try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        return view('admin.users.edit', [
            'user' => User::find($id),
            'roles' => Role::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('admin.users.index')->with('error', 'You cannot edit this user');
        }
        $valdidateData = $request->validate([
            'first_name' => 'required | min:3',
            'last_name' => 'required | min:3',
            'email' => 'required | email | confirmed | unique:users,email,' . $id,
            'email_confirmation' => 'required',
            'roles' => 'required',
            'status' => 'required',
            'phone1' => '',
            'phone2' => '',
            'address1' => '',
            'address2' => '',
            'city' => '',
            'state' => '',
            'zip' => ''
        ]);
        $valdidateData['updated_by'] = '';
        if (in_array(2, $request->roles) || in_array(4, $request->roles)) {

            if ($request->hasFile('files')) {
                //get filename with extension
                $files = $request->file('files');
                if ($user->files) {
                    $storeFiles = explode(',', $user->files);
                } else {
                    $storeFiles = [];
                }
                foreach ($files as $k => $file):
                    $filenamewithextension = $file->getClientOriginalName();
                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $filenametostore = $filename . '-' . time() . '.' . $extension;
                    $file->move(public_path('user_files'), $filenametostore);

                    $storeFiles[] = $filenametostore;
                endforeach;
                $valdidateData['files'] = implode(',', $storeFiles);
            }
        }

        $user->update($valdidateData);


        $u = ModelHasRole::where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));
        if ($user) {
            return redirect()->route('admin.users.index')->with('success', 'User Updated');
        } else {
            return redirect()->route('admin.users.edit')->with('error', 'User not Updated');
        }
    }

    public function removeFile($id) {
        $file = request()->file;
        $user = User::find($id);
        $userFiles = explode(',', $user->files);
        foreach ($userFiles as $k => $userFile):
            if ($userFile == $file) {
                unset($userFiles[$k]);
            }
        endforeach;
        if ($userFiles) {
            $userFiles = implode(',', $userFiles);
        } else {
            $userFiles = null;
        }
        $user->update(['files' => $userFiles]);
//        dd();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request) {
        //

        $find = User::find($id)->getRoleNames();
        if ($find[0] == 'Admin') {
            $count = User::join('model_has_roles as mhr', 'mhr.model_id', 'users.id')->where('role_id', 1)->count();
//            dd($count);
            if ($count == 1) {
                return redirect()->route('admin.users.index')->with('error', 'Last Admin Can not be deleted.');
            }
        }
//        exit;
        if ($id != Auth::user()->id) {
            User::destroy($id);
            if (isset($request->requestfrom) && $request->requestfrom == 3) {
                return redirect()->route('admin.users.parentlist')->with('success', 'Parent Deleted Successfully');
            }
            return redirect()->route('admin.users.index')->with('success', 'User Deleted Successfully');
        } else {
            return redirect()->route('admin.users.index')->with('error', 'You are not allowed to delete that user');
        }
    }

    public function login(Request $request) {
//dd('yep');
        if ($request->method() == 'POST') {
            $franchise = findBySlug(request()->franchise_id);
//            dd();
            $email = $request->email;
            $password = $request->password;
            $request->validate([
                'green_box' => 'required | in:' . $franchise->name,
                    ], [
                'green_box.required' => 'Please enter "' . $franchise->name . '" in green box',
                'green_box.in' => 'Captcha verification failed. Please try again',
            ]);

            if ($auth = Auth::attempt(['email' => $email, 'password' => $password])) {
                $currentUser = Auth::user();
                $allRoles = $currentUser->ModelHasRoles->pluck('role_id')->toArray();

                if (!in_array($request->login_type, $allRoles)) {
                    Auth::logout();
                    return redirect()->back()->with('error', 'Invalid Login Details');
                }

                Auth::user()->role = $request->login_type;

//                $userRole = $currentUser->ModelHasRoles->first()->role_id;
                $userRole = $request->login_type;

                if ($auth) {
                    if (($currentUser->status == 0)) {
                        Auth::logout();
                        $request->session()->invalidate();
                        $request->session()->regenerateToken();
                        return redirect()->back()->with('error', 'Your Account Is Not Active');
                    } else if (($currentUser->reset_token != '')) {
                        if ($userRole == 3 || $userRole == 5) {
                            Auth::logout();
                            $request->session()->invalidate();
                            $request->session()->regenerateToken();
                            return redirect(url(franchiseLink()).'/forgotPassword')->with('error', 'Please Reset Your Password');
                        }
                        Auth::logout();
                        $request->session()->invalidate();
                        $request->session()->regenerateToken();
                        return redirect(url(franchiseLink()).'/backend/forgotPassword')->with('error', 'Please Reset Your Password');
                    }
                }

                if ($request->allow_photo_release)
                    Student::where('user_id', Auth::user()->getAuthIdentifier())->update(['allow_photo_release' => 1]);

                $request->session()->regenerate();
                if ($userRole != 3 && $request->is_parent) {
                    Auth::logout();
                    return redirect()->back()->with('error', 'Invalid Login Details');
                }
                if ($userRole == 3) {
                    if (session('classlink') != "") {
                        return redirect()->intended(session('classlink'))->with('success', 'Logged In Successfully');
                    }
                    $url = url(parentfranchiseLink()) . "/dashboard";
                    return redirect()->intended($url)->with('success', 'Logged In Successfully');
                } elseif ($userRole == 5) {
                    $url = url(schoolAdminfranchiseLink()) . "/dashboard";
                    return redirect()->intended($url)->with('success', 'Logged In Successfully');
                } elseif ($userRole == 2) {
                    return redirect()->intended('teacher/dashboard')->with('success', 'Logged In Successfully');
                } else {
                    return redirect()->intended('backend/dashboard')->with('success', 'Logged In Successfully');
                }
            }
            return redirect()->back()->with('error', 'Invalid Login Details');
        }
        return view('admin.users.login');
    }

    public function logout(Request $request) {

        $franchise = IDToSlug(auth()->user()->franchise_id);

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
        if ($request->is_parent) {
            return redirect('/front-franchise/' . $franchise->slug)->with('success', 'Logged Out Successfully');
        }
        return redirect('/front-franchise/' . $franchise->slug . '/backend')->with('success', 'Logged Out Successfully');
    }

    public function recoverPassword(Request $request) {
        if ($request->method() == "POST") {
            // Firt check if email exist in DB
            $request->validate([
                'email' => 'required'
            ]);
            $user = User::where(['email' => $request->email])->first();
//dd($user);
            if ($user) {
                // Logic to create reset token, expiry datetime, send in email the reset link
                $reset_token = base64_encode(rand() . time());
                $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
                $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];

                $user_update = User::where(['id' => $user->id])->update($data_update);

                if ($request->request_from == "parent") {
                    $reset_url = url(franchiseLink()) . "/resetPassword/" . $reset_token;
                } else {
                    $franchise = getFranchise();
                    $reset_url = frontendBaseUrl($franchise->slug) . '/backend/resetPassword/' . $reset_token;
                }
//                dd(franchiseLink());
                // Send Email
                $franchise = getFranchise();
//                dd($franchise);
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
                                            requested to reset your password</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you your old password. A unique link to reset your
                                            password has been generated for you. To reset your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(frontendBaseUrl())) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';

                //$html = "Hi " . $user->first_name." ". $user->last_name .  ", <br /><br /> Below is your password reset link. Please click it to recover your password.<br><br><a href='$reset_url'>$reset_url</a><br /><br />Thanks<br />" . env('COMPANY_NAME');

                 try {
                
                $subject = $franchise->name . " Account Password Reset";
                    sendEmail($subject,$user->email,$html);
                    
                /*Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                    $message->to($user->email)
                            ->subject($franchise->name . " Account Password Reset")
                            ->setBody($html, 'text/html');
                });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }

                return back()->with('success', 'If the information entered is associated with an account we have sent you an email with password reset instructions');
            } else {
                return back()->with('success', 'If the information entered is associated with an account we have sent you an email with password reset instructions');
            }
        }
        if ($request->request_from == "parent") {
            return view('admin.users.forgotparentpassword');
        } else {
            return view('admin.users.forgotpassword');
        }
    }

    public function resetpasswordmanually($email) {
//        dd('abc');
        if ($email) {
            // Firt check if email exist in DB

            $user = User::where(['email' => $email])->first();
            $franchise = getFranchise();
//            dd($franchise);
            if ($user) {
                // Logic to create reset token, expiry datetime, send in email the reset link
                $reset_token = base64_encode(rand() . time());
                $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
                $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];

                $user_update = User::where(['id' => $user->id])->update($data_update);


                $reset_url = url('/backend') . "/resetPassword/" . $reset_token;
                // Send Email

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
                                            requested to reset your password</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you your old password. A unique link to reset your
                                            password has been generated for you. To reset your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(frontendBaseUrl())) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                //$html = "Hi " . $user->first_name." ". $user->last_name . ", <br /><br /> Below is your password reset link. Please click it to reset your password.<br><br><a href='$reset_url'>$reset_url</a><br /><br />Thanks<br />" . env('COMPANY_NAME');
                try {
                
                $subject = $franchise->name . " Account Password Reset";
                    sendEmail($subject,$email,$html);
                    
                /*Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                    $message->to($user->email)
                            ->subject($franchise->name . " Account Password Reset")
                            ->setBody($html, 'text/html');
                });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }



                return back()->with('success', 'Password reset link send to the email address.');
            } else {
                return back()->with('success', 'Password reset link send to the email address.');
            }
        }
        return view('admin.users.edit', [
            'user' => User::find($user->id),
            'roles' => Role::all()
        ]);
    }

    public function resetPassword() {
        $token = request()->token;
//        dd($token);
        if ($token) {
            // check token valid and if expired
            $user = User::where(['reset_token' => $token])->first();

            if ($user && date('Y-m-d H:i:s', strtotime($user->reset_token_expiry)) > date('Y-m-d H:i:s', time())) {
                return view('admin/users/resetpassword', ['token' => $token]);
            } else {
                return redirect(url(franchiseLink()).'/backend/forgotPassword')->with('error', 'Your reset link got expired or in-valid. Try Again');
            }
        } else {
            return redirect(url(franchiseLink()).'/backend/forgotPassword');
        }
    }

    public function resetParentPassword() {
        $token = request()->token;
        if ($token) {
            // check token valid and if expired
            $user = User::where(['reset_token' => $token])->first();

            if ($user && date('Y-m-d H:i:s', strtotime($user->reset_token_expiry)) > date('Y-m-d H:i:s', time())) {
                return view('admin/users/resetparentpassword', ['token' => $token]);
            } else {
                $url = url(franchiseLink()) . "/forgotPassword";
                return redirect($url)->with('error', 'Your reset link got expired or in-valid. Try Again');
            }
        } else {
            return redirect(url(franchiseLink()).'/forgotPassword');
        }
    }

    public function changePassword(Request $request) {
        if ($request->method() == "POST") {
            if (isset($request->changepassword)) {
                $request->validate([
                    'currentpassword' => 'required',
                    'password' => 'required|between:8,255|confirmed',
                    'password_confirmation' => 'required'
                ]);
                if (Hash::check($request->currentpassword, Auth::user()->password)) {
                    $update_pass = User::find(Auth::user()->id)->update($request->only('password'));
                    if ($update_pass) {
                        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
                        if (in_array(2, $roles)) {
                            return redirect('/teacher/dashboard')->with('success', 'Password Updated Successfully');
                        }
                        return redirect('/backend/dashboard')->with('success', 'Password Updated Successfully');
                    } else {
                        return back()->with('error', 'Some Error Occured, Please Try Again');
                    }
                } else {
                    return back()->with('error', 'Current password is invalid');
                }
            } elseif (isset($request->reset_token)) {


                $user = User::where(['reset_token' => $request->reset_token])->first();
                $roles = $user->ModelHasRoles->pluck('role_id')->toArray();

//                dd($roles);
                if ($user && date('Y-m-d H:i:s', strtotime($user->reset_token_expiry)) > date('Y-m-d H:i:s', time())) {
                    $request->validate([
                        'password' => 'required|between:8,255|confirmed',
                        'password_confirmation' => 'required'
                    ]);
                    $request->merge(['reset_token' => Null, 'reset_token_expiry' => Null]);
                    $update_pass = User::find($user->id)->update($request->only('reset_token', 'reset_token_expiry', 'password'));
                    if ($update_pass) {
                        if (in_array(3, $roles) || in_array(5, $roles)) {
                            return redirect('/front-franchise/' . request()->franchise_id . '/parentlogin')->with('success', 'Password Updated Successfully');
                        } else {
                            return redirect(frontendBaseUrl() . '/backend')->with('success', 'Password Updated Successfully');
                        }
                    } else {
                        return back()->with('error', 'Some Error Occured, Please Try Again');
                    }
                } else {
                    User::where(['reset_token' => $token])->update(['id' => $user->id]);
                    return redirect('front-franchise/' . request()->franchise_id . '/backend/forgotPassword')->with('error', 'Your reset link got expired or in-valid. Try Again');
                }
            }
        }
        return view('admin.users.changepassword');
    }

    public function profileEdit() {
        return view('admin/users/profile', ['user' => Auth::user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request) {
        //
        $user = User::find(Auth::user()->id);
        if (!$user) {
            return redirect('/backend/dashboard')->with('error', 'You cannot edit this user');
        }
        $request->merge(['status' => 1]);
        $valdidateData = $request->validate([
            'first_name' => 'required | min:3',
            'last_name' => 'required | min:3',
            'email' => 'required | email | unique:users,email,' . Auth::user()->id,
            'status' => 'required'
        ]);

        $user->update($valdidateData);

        if ($user) {
            return redirect('/backend/dashboard')->with('success', 'Profile Updated');
        } else {
            return redirect('/backend/dashboard')->with('error', 'Profile not Updated');
        }
    }

    public function recoverParentPassword(Request $request) {
        if ($request->method() == "POST") {
            // Firt check if email exist in DB
            $request->validate([
                'email' => 'required'
            ]);
            $user = User::where(['email' => $request->email])->first();

            if ($user) {
                // Logic to create reset token, expiry datetime, send in email the reset link
                $reset_token = base64_encode(rand() . time());
                $reset_expiry = date('Y-m-d H:i:s', strtotime('+1 hours'));
                $data_update = ['reset_token' => $reset_token, 'reset_token_expiry' => $reset_expiry];

                $user_update = User::where(['id' => $user->id])->update($data_update);
                $franchise = getFranchise();

                $reset_url = url('/backend') . "/resetPassword/" . $reset_token;
                // Send Email

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">You have
                                            requested to reset your password</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            We cannot simply send you your old password. A unique link to reset your
                                            password has been generated for you. To reset your password, click the
                                            following link and follow the instructions.
                                        </p>
                                        <a href="' . $reset_url . '"
                                            style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                            Password</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(frontendBaseUrl())) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';

                //$html = "Hi " . $user->first_name." ". $user->last_name .  ", <br /><br /> Below is your password reset link. Please click it to recover your password.<br><br><a href='$reset_url'>$reset_url</a><br /><br />Thanks<br />" . env('COMPANY_NAME');

                  try {
            $subject = $franchise->name . " Account Password Reset";
                    sendEmail($subject,$user->email,$html);  
                    
               /* Mail::send(array(), array(), function ($message) use ($html, $user, $franchise) {
                    $message->to($user->email)
                            ->subject($franchise->name . " Account Password Reset")
                            ->setBody($html, 'text/html');
                });*/
                } catch (\Exception $e) {
                                    \Log::error('Email sending error: ' . $e->getMessage());
                                }
                return back()->with('success', 'If the information entered is associated with an account we have sent you an email with password reset instructions');
            } else {
                return back()->with('success', 'If the information entered is associated with an account we have sent you an email with password reset instructions');
            }
        }
        return view('admin.users.forgotparentpassword');
    }

}
