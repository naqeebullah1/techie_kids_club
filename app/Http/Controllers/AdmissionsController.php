<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admission;
use App\Models\AdmissionDetail;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\User;
use App\Models\Student;
use App\Models\FranchiseUser;
use Stripe;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
class AdmissionsController extends Controller {

     public function index(Request $request) {

        if(auth()->user()->franchise_id != 1 && auth()->user()->role != 3) {
            $listSchools = School::where('franchise_id',auth()->user()->franchise_id)->pluck('id')->toArray();

            $admissions = Admission::withoutGlobalScope(\App\Scopes\FranchiseScope::class)->with(['details' =>

                function ($q) use ($listSchools) {
                    // Query the name field in status table
                    $q->whereIn('school_id', $listSchools)->withoutGlobalScope(\App\Scopes\FranchiseScope::class);
                }])->whereHas('details', function (Builder $query) use ($listSchools) {
                $query->whereIn('school_id', $listSchools)->withoutGlobalScope(\App\Scopes\FranchiseScope::class);
            })->orderBy('id', 'DESC');
//dd($admissions->get());
            /*$admissions = DB::table('school_classes')
             ->join('admission_details', 'school_classes.id', '=', 'admission_details.school_class_id')
             ->join('students', 'students.id', '=', 'admission_details.student_id')
             ->join('users', 'users.id', '=', 'students.user_id')
             ->join('admissions', 'admissions.id', '=', 'admission_details.admission_id')
             ->select('users.*','students.*','admissions.*','school_classes.*', 'school_classes.franchise_id as f_id', 'admission_details.*')
             ->where('school_classes.franchise_id' , '=',auth()->user()->franchise_id)
             ->groupBy('admissions.id')
             ->get();  */
            $is_franchise = 1;
            /*$schools = School::query();
        $schools = $schools->whereHas('school_classes', function($q) {
            //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
            return $q->whereNotNull('school_classes.start_on')->where('status', '=', '1');
        });
        $schools = $schools->orderBy('name', 'ASC')->get();
        $parents = User::with('ModelHasRoles')
                        ->singleRole(3)
                        ->orderBy('last_name', 'ASC')->get();*/

            if ($request->first_name != "") {
                $parent_id = User::where('first_name', 'like', '%' . $request->first_name . '%')->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->last_name != "") {
                $parent_id = User::where('last_name', 'like', '%' . $request->last_name . '%')->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->phone != '') {
                $parent_id = User::where('phone1', $request->phone)->orWhere('phone2', $request->phone)->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->email != '') {
                
                    $parent_id = User::where('email', 'like', '%' . $request->email . '%')->select('id')->get();
                    $admissions->whereIn('parent_id', $parent_id);
            }

            $admissions = $admissions->paginate(15);

            return view('admin.admissions.index', compact('admissions', 'is_franchise','request'));
        } else {
            $admissions = Admission::orderBy('id', 'DESC');
            if ($request->first_name != "") {
                $parent_id = User::where('first_name', 'like', '%' . $request->first_name . '%')->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->last_name != "") {
                $parent_id = User::where('last_name', 'like', '%' . $request->last_name . '%')->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->phone != '') {
                $parent_id = User::where('phone1', $request->phone)->orWhere('phone2', $request->phone)->select('id')->get();
                $admissions->whereIn('parent_id', $parent_id);
            }
            if ($request->email != '') {
                
                    $parent_id = User::where('email', 'like', '%' . $request->email . '%')->select('id')->get();
                    $admissions->whereIn('parent_id', $parent_id);
            }

            if ($request->from_date) {
                $childfname = date('Y-m-d', strtotime($request->from_date));
                $admissions->whereDate('created_at', '>=', $childfname);
            }
            if ($request->to_date) {
                $childfname = date('Y-m-d', strtotime($request->to_date));
                $admissions->whereDate('created_at', '<=', $childfname);
            }

            $admissions = $admissions->paginate(15);

        if ($request->ajax()) {
            $view = view('admin.admissions.table', compact('admissions'))
                    ->render();
            return response()->json(['html' => $view]);
        }

        $schools = School::query();
        $schools = $schools->whereHas('school_classes', function($q) {
            //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
            return $q->whereNotNull('school_classes.start_on')->where('status', '=', '1');
        });
        $schools = $schools->orderBy('name', 'ASC')->get();

        $parents = User::with('ModelHasRoles')
                        ->singleRole(3)
                        ->orderBy('last_name', 'ASC')->get();

        return view('admin.admissions.index', compact('admissions', 'schools', 'parents','request'));
        }
    }

    public function classDD() {
        $sId = request()->school_id;
        $value = request()->class_id ? request()->class_id : "";

        $options = $this->classList($sId);

        $view = view('partials.classdropdown', compact('options', 'value'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }

    public function classList($sId) {
        $class = SchoolClass::dropdown()
                        ->where('school_id', '=', $sId)->where('status', '=', '1')->orderBy('title', 'ASC');

        return $class->get();
    }

    public function childDD() {
        $sId = request()->parent_id;
        $value = request()->child_id ? request()->child_id : "";

        $options = $this->childList($sId);

        $view = view('partials.childdropdown', compact('options', 'value'))
                ->render();
//        $empty="<option>Select</option>"
        return response()->json(['html' => $view]);
    }

    public function childList($sId) {
        $class = Student::dropdown()
                        ->where('user_id', '=', $sId)->orderBy('first_name', 'ASC');

        return $class->get();
    }

    public function search_stripe(Request $request) {

        if ($request) {

            $school_id = $request->school_id;
            $class_id = $request->class_id;
            $parent_id = $request->parent_id;
            $child_id = $request->child_id;
            $stripe_subid = $request->subscription_id;
            $classroom_name = $request->classroom_name;

            // First Check if Subscription Already Exist
            $subscriptionExist = AdmissionDetail::where(['stripe_subscription_id' => $stripe_subid])->get()->first();
            if ($subscriptionExist) {
                $customerEmail = $subscriptionExist['customer_email'];
                return redirect()->back()->with('error', "Subscription Already Exist In Record For $customerEmail")->withInput($request->input());
            }


            $user_email = User::where(['id' => $parent_id])->get()->first()['email'];
            $class_charges = SchoolClass::where(['id' => $class_id])->get()->first()['class_charges'];

            //echo "School ".$school_id . " - Class ".$class_id . " - Parent ".$parent_id . " - Child ".$child_id." - Stripe ".$stripe_subid;
            // Get Subscription Data from Stripe
            
            $franchise = getFranchise();
            $stripeAPiKey = $franchise->stripe_client;
            
//$stripeAPiKey = "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";
            //$stripeAPiKey = "sk_test_51HyGh4B3WkD7XcJGzOo6cTQRB4f2D9QQhPUkYiAXHcqEmkf2npVre8PV9Z9MsxBufAyN5hczg7lqy9co3updar5000KiCSOmnq";

//            if (env('STRIPE_API_KEY')) {
//                $stripeAPiKey = env('STRIPE_API_KEY');
//            }

            Stripe\Stripe::setApiKey($stripeAPiKey);

            try {
                $subscriptionData = \Stripe\Subscription::retrieve($stripe_subid);
                $latest_invoice = $subscriptionData->latest_invoice;

                $invoiceData = \Stripe\Invoice::retrieve($latest_invoice);
                //dd($invoiceData);

                $amount_paid = number_format(($invoiceData->amount_paid / 100), 2);

                $parent_email = $invoiceData->customer_email;
                $parent_name = $invoiceData->customer_name;
                $stripe_customer_id = $invoiceData->customer;
                $stripe_payment_intent_id = $invoiceData->payment_intent;
                $stripe_currency = $invoiceData->currency;
                $plan_start = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['start']);
                $plan_end = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['end']);

                $cancel_at = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));

                if ($user_email == $parent_email) {
                    //All good, procced
                    $addmission = Admission::create([
                                'parent_id' => $parent_id,
                    ]);
                    $admission_id = $addmission->id;

                    $admissionDetail = [
                        'admission_id' => $admission_id,
                        'student_id' => $child_id,
                        'school_id' => $school_id,
                        'school_class_id' => $class_id,
                        'class_charges' => $class_charges,
                        'status' => 1,
                        'classroom_name' => $classroom_name,
                        'stripe_subscription_id' => $stripe_subid,
                        'stripe_customer_id' => $stripe_customer_id,
                        'stripe_payment_intent_id' => $stripe_payment_intent_id,
                        'paid_amount' => $amount_paid,
                        'paid_amount_currency' => $stripe_currency,
                        'plan_interval' => 'month',
                        'customer_name' => $parent_name,
                        'customer_email' => $parent_email,
                        'plan_period_start' => $plan_start,
                        'plan_period_end' => $plan_end,
                        'plan_cancel_at' => $cancel_at
                    ];

                    AdmissionDetail::create($admissionDetail);
                    //return redirect("parent/add-details/$admission_id")->with('success', "Stripe Payment Fetched Successfully");
                    return redirect()->back()->with('success', "Stripe Payment Fetched Successfully");
                } else {
                    // Send error to search page
                    return redirect()->back()->with('error', "Error Occured, Parent Does Not Match To Subscription You Provided")->withInput($request->input());
                }
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                return redirect()->back()->with('error', $api_error)->withInput($request->input());
            } catch (Exception $e) {
                $api_error = $e->getMessage();

                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->with('error', $api_error)->withInput($request->input());
            }
        }
    }
       // Import data from stripe using CSV
    public function processCSVLegacy(Request $request)
    {
        // Initialize Stripe with your secret key
        
        $franchise = getFranchise('techie-kids-club');
       
        $stripeAPiKey = $franchise->stripe_client;
       
        Stripe\Stripe::setApiKey($stripeAPiKey);

        // Read CSV file
    

        $filePath = public_path('import-stripe.csv');

        // Read CSV file
        $csvData = array_map('str_getcsv', file($filePath));
        //dd($csvData);
        $sno = 0;
        foreach ($csvData as $row) {
            
        $sno++;    

            // Check if the user exists by email
            $user = User::withTrashed()->where('email', $row[0])->first();
            
            // If user does not exist, create a new user
            
            if (!$user) {
               
                // Get user stripe id from stripe
                
                 $customer = \Stripe\Customer::all(['email' => $row[0]])->data[0];
                 
                 $stripe_customer_id = $customer['id'];
                 $password = Hash::make('cyberclouds@6');
                 $email = $row[0];
                 $first_name = $row[2];
                 $last_name = $row[3];
                 $status = 1;
                 
                  $user = User::create([
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'password' => $password,
                        'status' => '1',
                        'check_privacy' => '1',
                        'check_terms' => '1',
                        'check_privacy_date' => date('Y-m-d',time()),
                        'check_terms_date' => date('Y-m-d',time()),
                        'franchise_id' => 1
                ]);
                
                
                if($user) {
                    // add role
                    $user->assignRole(3);
                    $franchiseUserData['user_id'] = $user->id;
                    $franchiseUserData['franchise_id'] = 1;
                    $franchiseUserData['customer_id'] = $stripe_customer_id;
                    
                    //add franchise with stripe id
                    $franchiseUser = FranchiseUser::create($franchiseUserData);
                    
                    
                }
                 
            
                $emailLogFile = public_path('emails_not_found.txt');

                // Append the email to the text file
                file_put_contents($emailLogFile, $row[0] . PHP_EOL, FILE_APPEND);
            }
            
            $userid = $user->id;

            // Check if the student exists by first and last name for the user_id
            $student = Student::where([
                'user_id' => $userid,
                'first_name' => $row[4],
                'last_name' => $row[5],
            ])->first();

            // If student does not exist, create a new student
            if (!$student) {
                $student = Student::create([
                    'user_id' => $userid,
                    'first_name' => $row[4],
                    'last_name' => $row[5],
                    'franchise_id' => '1',
                    // Add other student fields as needed
                ]);
            }
            
            $customer = \Stripe\Customer::all(['email' => $row[0]])->data[0];
            
            $student_id = $student->id;
            $school_id = $row[6];
            $class_id = $row[7];
            
            // Assuming $customer is not null, you found the customer
            if($customer->id) {
                $subscriptions = \Stripe\Subscription::all(['customer' => $customer->id])->data;
                foreach ($subscriptions as $subscription) {
                    $latest_invoice = $subscription->latest_invoice;

                    $invoiceData = \Stripe\Invoice::retrieve($latest_invoice);
                    //dd($invoiceData);
    
                    $amount_paid = number_format(($invoiceData->amount_paid / 100), 2);
    
                    $parent_email = $invoiceData->customer_email;
                    $parent_name = $invoiceData->customer_name;
                    $stripe_customer_id = $invoiceData->customer;
                    $stripe_payment_intent_id = $invoiceData->payment_intent;
                    $stripe_currency = $invoiceData->currency;
                    $plan_start = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['start']);
                    $plan_end = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['end']);
    
                    $cancel_at = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));
                    $subs_status = ($subscription->status == "active")?1:0;
                    
                    $admission_detail = AdmissionDetail::where('stripe_subscription_id', $subscription->id)->where('stripe_customer_id', $customer->id)->where('student_id', $student_id)->first();
                    if($admission_detail) {
                        // Update admissiondetail table if subscription exist   
                        
                        $admission_detail->update([
                            'school_id' => $school_id,
                            'school_class_id' => $class_id,
                            'status' => $subs_status
                        ]);
                        //var_dump($admission_detail);
                    }
                    else {
                        // Add new record if subscription does not exist   
                        //first add admission
                        $addmission = Admission::create([
                                'parent_id' => $user->id,
                    ]);
                        $admission_id = $addmission->id;
                        //echo $school_id." - ".$class_id."<br />";
                        $class_charges = SchoolClass::withTrashed()->where(['id' => $class_id])->get()->first()['class_charges'];
                       
                        $admissionDetail = [
                            'admission_id' => $admission_id,
                            'student_id' => $student_id,
                            'school_id' => $school_id,
                            'school_class_id' => $class_id,
                            'class_charges' => $class_charges,
                            'status' => $subs_status,
                            'classroom_name' => 'Techie Kids Club',
                            'stripe_subscription_id' => $subscription->id,
                            'stripe_customer_id' => $customer->id,
                            'stripe_payment_intent_id' => $stripe_payment_intent_id,
                            'paid_amount' => $amount_paid,
                            'paid_amount_currency' => $stripe_currency,
                            'plan_interval' => 'month',
                            'customer_name' => $parent_name,
                            'customer_email' => $parent_email,
                            'plan_period_start' => $plan_start,
                            'plan_period_end' => $plan_end,
                            'plan_cancel_at' => $cancel_at
                        ];
    
                        $ad_detail_create = AdmissionDetail::create($admissionDetail);
                    var_dump($ad_detail_create);
                    }
                    
                }
            }
            
        }

        return response()->json(['message' => 'CSV processed successfully']);
    }
        public function processCSV(Request $request)
    {
        // Initialize Stripe with your secret key
        
        $franchise = getFranchise('techie-kids-club');
       
        /*$stripeAPiKey = $franchise->stripe_client;
       
        Stripe\Stripe::setApiKey($stripeAPiKey);*/

        // Read CSV file
    

        $filePath = public_path('update-classes.csv');

        // Read CSV file
        $csvData = array_map('str_getcsv', file($filePath));
        //dd($csvData);
        $sno = 0;
        foreach ($csvData as $row) {
            
        $sno++;    

            // Check if the user exists by email
            $user = User::withTrashed()->where('email', $row[0])->first();
            
            // If user does not exist, create a new user
            
            if (!$user) {
               
                // Get user stripe id from stripe
                
            //     $customer = \Stripe\Customer::all(['email' => $row[0]])->data[0];
                 
              /*   $stripe_customer_id = $customer['id'];
                 $password = Hash::make('cyberclouds@6');
                 $email = $row[0];
                 $first_name = $row[2];
                 $last_name = $row[3];
                 $status = 1;*/
                 
                  /*$user = User::create([
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'email' => $email,
                        'password' => $password,
                        'status' => '1',
                        'check_privacy' => '1',
                        'check_terms' => '1',
                        'check_privacy_date' => date('Y-m-d',time()),
                        'check_terms_date' => date('Y-m-d',time()),
                        'franchise_id' => 1
                ]);
                
                
                if($user) {
                    // add role
                    $user->assignRole(3);
                    $franchiseUserData['user_id'] = $user->id;
                    $franchiseUserData['franchise_id'] = 1;
                    $franchiseUserData['customer_id'] = $stripe_customer_id;
                    
                    //add franchise with stripe id
                    $franchiseUser = FranchiseUser::create($franchiseUserData);
                    
                    
                }*/
                 
            
                $emailLogFile = public_path('parent_not_found.txt');

                // Append the email to the text file
                file_put_contents($emailLogFile, $row[0] . PHP_EOL, FILE_APPEND);
                continue;
            }
            
            $userid = $user->id;

            // Check if the student exists by first and last name for the user_id
            /*$student = Student::where([
                'user_id' => $userid,
                'first_name' => $row[4],
                'last_name' => $row[5],
            ])->first();*/
            $student = Student::where('user_id', $userid)
            ->where('first_name', 'LIKE', '%' . trim($row[4]) . '%')
            //->where('last_name', 'LIKE', '%' . $row[5] . '%')
            ->first();

            // If student does not exist, create a new student
            if (!$student) {
                $emailLogFile = public_path('student_not_found.txt');

                // Append the email to the text file
                file_put_contents($emailLogFile, $row[4] . PHP_EOL, FILE_APPEND);
                continue;
            }
            
            /*$customer = \Stripe\Customer::all(['email' => $row[0]])->data[0];*/
            
            $student_id = $student->id;
            $school_id = $row[6];
            $class_id = $row[7];
            
            // Assuming $customer is not null, you found the customer
            if(1) {
                //$subscriptions = \Stripe\Subscription::all(['customer' => $customer->id])->data;
                //foreach ($subscriptions as $subscription) {
                    /*$latest_invoice = $subscription->latest_invoice;

                    $invoiceData = \Stripe\Invoice::retrieve($latest_invoice);
                    //dd($invoiceData);
    
                    $amount_paid = number_format(($invoiceData->amount_paid / 100), 2);
    
                    $parent_email = $invoiceData->customer_email;
                    $parent_name = $invoiceData->customer_name;
                    $stripe_customer_id = $invoiceData->customer;
                    $stripe_payment_intent_id = $invoiceData->payment_intent;
                    $stripe_currency = $invoiceData->currency;
                    $plan_start = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['start']);
                    $plan_end = date('Y-m-d H:i:s', $invoiceData->lines['data'][0]['period']['end']);
    
                    $cancel_at = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));
                    $subs_status = ($subscription->status == "active")?1:0;*/
                    
                    
                    
                    $class_charges = SchoolClass::withTrashed()->where(['id' => $class_id])->get()->first()['class_charges'];
                    
                    $admission_detail = AdmissionDetail::where('student_id', $student_id)->first();
                    
                    if($admission_detail) {
                    $admission_detail->update([
                            'school_id' => $school_id,
                            'school_class_id' => $class_id,
                            'class_charges' => $class_charges
                            //'status' => $subs_status
                        ]);
                    }
                        
                    if(0) {
                        // Update admissiondetail table if subscription exist   
                        
                        
                        //var_dump($admission_detail);
                    }
                    else {
                        // Add new record if subscription does not exist   
                        //first add admission
                 /*       $addmission = Admission::create([
                                'parent_id' => $user->id,
                    ]);
                        $admission_id = $addmission->id;
                        //echo $school_id." - ".$class_id."<br />";
                        $class_charges = SchoolClass::withTrashed()->where(['id' => $class_id])->get()->first()['class_charges'];
                       
                        $admissionDetail = [
                            'admission_id' => $admission_id,
                            'student_id' => $student_id,
                            'school_id' => $school_id,
                            'school_class_id' => $class_id,
                            'class_charges' => $class_charges,
                            'status' => $subs_status,
                            'classroom_name' => 'Techie Kids Club',
                            'stripe_subscription_id' => $subscription->id,
                            'stripe_customer_id' => $customer->id,
                            'stripe_payment_intent_id' => $stripe_payment_intent_id,
                            'paid_amount' => $amount_paid,
                            'paid_amount_currency' => $stripe_currency,
                            'plan_interval' => 'month',
                            'customer_name' => $parent_name,
                            'customer_email' => $parent_email,
                            'plan_period_start' => $plan_start,
                            'plan_period_end' => $plan_end,
                            'plan_cancel_at' => $cancel_at
                        ];
    
                        $ad_detail_create = AdmissionDetail::create($admissionDetail);
                    var_dump($ad_detail_create);*/
                    }
                    
                //}
            }
            
        }

        return response()->json(['message' => 'CSV processed successfully']);
    }

}
