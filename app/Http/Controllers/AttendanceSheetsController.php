<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdmissionDetail;
use App\Models\AttendanceSheet;
use App\Models\AttendanceSheetList;
use Illuminate\Support\Facades\DB;

class AttendanceSheetsController extends Controller {

    public function index() {
        if (request()->isMethod('post')) {

            $fDate = date('Y-m-d', strtotime(request()->from_date));
            $tDate = date('Y-m-d', strtotime(request()->to_date));

            $students = AttendanceSheet::whereDate('date', '>=', $fDate)
                    ->whereDate('date', '<=', $tDate)
                    ->where('school_class_id', request()->school_class_id)
                    ->get();
            $view = view('attendance_sheets.index_table', compact('students'))->render();

            return response()->json(['html' => $view]);
        }

        $findRecord = null;
        if (request()->isMethod('get') && request()->class_id) {
            $findRecord = AttendanceSheet::whereDate('date', '=', request()->date)
                    ->where('school_class_id', request()->class_id)
                    ->first();
//            dd($findRecord);
        }
        $schools = \App\Models\School::where('status', 1)->select('id', 'name');
        if (auth()->user()->role == 2) {
            $authId = auth()->user()->id;
            $schools = $schools->whereHas('school_classes', function($q) use($authId) {
                return $q->where('user_id', $authId);
            });
        }
        $schools = $schools->pluck('name', 'id')->toArray();
        return view('attendance_sheets.index', compact('schools', 'findRecord'));
    }

    public function details($id) {

        $attendance = AttendanceSheet::find($id);
        $students = AttendanceSheetList::where('attendance_sheet_id', $id)
                ->get();

        $view = view('attendance_sheets.details', compact('students', 'attendance'))->render();
        return response()->json(['html' => $view]);
    }

    public function create() {

        if (request()->isMethod('post')) {

            $students = AdmissionDetail::join('students', 'admission_details.student_id', 'students.id')
                    ->select('students.id', DB::raw('CONCAT_WS(" ",students.first_name,students.last_name) as student_name'))
                    ->where('school_class_id', request()->school_class_id)
                    ->where('status', 1)
                    ->get();

            $lastEntry = AttendanceSheet::where('school_class_id', request()->school_class_id)
                    ->orderBy('id', 'desc')
                    ->first();
//            dd($lastEntry);

            $view = view('attendance_sheets.students', compact('students', 'lastEntry'))->render();

            return response()->json(['html' => $view]);
        }
        $schools = \App\Models\School::where('status', 1)->select('id', 'name');
//dd();
        if (auth()->user()->role == 2) {
            $authId = auth()->user()->id;
            $schools = $schools->whereHas('school_classes', function($q) use($authId) {
                return $q->where('user_id', $authId);
            });
        }
        $schools = $schools->pluck('name', 'id')->toArray();
//        dd($schools);
        return view('attendance_sheets.create', compact('schools'));
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $record = $request->attendance;
            $students = [];
            $id = $request->id;

//            dd($record);
            if ($id) {
                $record['updated_by'] = auth()->user()->id;
                School::find($id)->update($record);
                $outcome = "School Updated Successfully";
            } else {
                $school_class_id = \App\Models\SchoolClass::find($record['school_class_id']);
                $record['date'] = date('Y-m-d', strtotime($record['date']));
                $record['school_class_id'] = $school_class_id->id;
                $record['school_id'] = $school_class_id->school_id;

                $as = AttendanceSheet::create($record);
                $lId = $as->id;
//                $as->attendance_sheet_lists()->saveMany($students);
                foreach ($request->students as $skill) {
                    $skill['attendance_sheet_id'] = $lId;
                    $students[] = $skill;
                }
                AttendanceSheetList::insert($students);
                $outcome = "Attendance Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

}
