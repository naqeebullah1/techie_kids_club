<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Banner;
use App\Models\Page;
use Illuminate\Support\Facades\Gate;
use Image;
use App\Models\PageDetail;
use App\Models\Category;
use App\Models\CategoryGallery;
use App\Models\Testimonial;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Models\Team;

class BannerController extends Controller {

    // public function index() {
    //     $banners = Banner::orderBy('active', 'desc')->orderBy('sort_order', 'asc')->get();
    //     return view('admin.banner.index', compact('banners'));
    // }

    public function index($type = null) {
        $banner = '';
        if ($type) {
            $banner = Banner::find($type);
            if (!$banner) {
                $banner = new Banner;
            }
        }

        $banners = Banner::orderBy('active', 'desc')->paginate(20);
        return view('admin.banner.index', compact('banners', 'type', 'banner'));
    }

    public function bannar_ajax_upload(Request $request) {


        //        if (Gate::denies('trending-products.index', 'update')) {
        //            abort(403);
        //        }
        $Image_Id = request()->Image_Id;
        $id = explode('-', $Image_Id);
        $folderPath = public_path('uploads/' . $request->folder);
        $assets = url('uploads/' . $request->folder);
        $myName = '';
        if (isset($request->file_name)) {
            $myName = $request->file_name;
        }

        $imageParts = explode(";base64,", $request->image);
        $imageTypeAux = explode("image/", $imageParts[0]);
        $imageType = $imageTypeAux[1];
        $image_base64 = base64_decode($imageParts[1]);
        $uniqueId = uniqid();
        $fileName = $myName . $uniqueId . '.png';
        $file = $folderPath . '/' . $fileName;
        $p = $assets . '/' . $fileName;
        file_put_contents($file, $image_base64);
        if ($request->Image_src === 'large_image') {
            Banner::where('id', $id[1])->update(['large_image' => $fileName]);
        } else if ($request->Image_src === 'mobile_image') {
            Banner::where('id', $id[1])->update(['mobile_image' => $fileName]);
        } else {
            Banner::where('id', $id[1])->update(['mobile_secondary_image' => $fileName]);
        }
        return response()->json(['success' => 'success', 'file' => $fileName, 'path' => $p]);
    }

    public function store(Request $request) {
        $rules = [
            'title' => ['required'],
            'section' => ['required'],
        ];

        if ($request->id == 'create') {
            $rules['image'] = ['required'];
        }

        if ($request->btn_title != "") {
            $rules['btn_link'] = ['required'];
            $customMessages = [
                'btn_link.required' => 'Please provide the button link',
            ];
            $this->validate($request, $rules, $customMessages);
        }
        if ($request->btn_link != "") {
            $rules['btn_title'] = ['required'];
            $customMessages = [
                'btn_title.required' => 'Please provide the button title',
            ];
            $this->validate($request, $rules, $customMessages);
        }
        $data = request()->validate($rules);
        try {
            DB::beginTransaction();

            $active = 1;
            $id = $request->id;
            $page = [
                'title' => $request->title,
                'btn_title' => $request->btn_title,
                'btn_link' => $request->btn_link,
                'description' => $request->section,
            ];

            $page['active'] = $active;
            if ($request->hasFile('image')) {
                $res = app('App\Http\Controllers\BannerController')->resizeImagePost($request);
                $page['image'] = $res;
            }
            if ($id != 'create') {
                $page['updated_by'] = "";
                Banner::find($id)->update($page);
                $outcome = 'Banner has been updated successfully.';
            } else {
                // dd($page);
                Banner::create($page);
                $outcome = 'Banner has been created successfully.';
            }

            DB::commit();
            return redirect()->route('admin.banners.index')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function sortData() {
        //        if (Gate::denies('banner.index', 'update')) {
        //            abort(403);
        //        }
        if (request()->all()) {
            if (request()->table == 'banners') {

                $data = request()->data;
                foreach ($data as $key => $cat) {
                    $entity = Banner::where('id', $cat)->update(['sort_order' => $key + 1]);
                }
            }
            if (request()->table == 'page_details') {
                $data = request()->data;
                foreach ($data as $key => $cat) {
                    $entity = PageDetail::where('id', $cat)->update(['sort_order' => $key + 1]);
                }
            }
            if (request()->table == 'gallery') {
                $data = request()->data;
                foreach ($data as $key => $cat) {
                    $entity = CategoryGallery::where('id', $cat)->update(['sort_order' => $key + 1]);
                }
            }
        }
        return ['status' => 1, 'message' => 'ordering changed successfully'];
    }

    public function changeStatus($table, $id, $status) {
        //        if (Gate::denies('banner.index', 'update')) {
        //            abort(403);
        //        }
        $outcome = '';
        try {
            DB::beginTransaction();
            $status = (int) $status;
            if ($status == 1) {
                $status = 0;
            } else {
                $status = 1;
            }
            $blockUnblockText = $status == 1 ? "unblocked" : "blocked";

            if ($table == "banners") {
                $updateBanner = Banner::where('id', $id)->update(['active' => $status]);
                $outcome = 'Banner ' . $blockUnblockText . " successfully.";
            }
            if ($table == "promo") {
                $updateBanner = \App\Models\PromoCode::where('id', $id)->update(['status' => $status]);
                $outcome = 'Promocode ' . $blockUnblockText . " successfully.";
            }
            if ($table == "notifications") {
                $updateBanner = \App\Models\Notification::where('id', $id)->update(['status' => $status]);
                $outcome = 'Notification ' . $blockUnblockText . " successfully.";
            }
            if ($table == "services") {
                $updateBanner = \App\Models\Service::where('id', $id)->update(['status' => $status]);
                $outcome = 'Service ' . $blockUnblockText . " successfully.";
            }
            if ($table == "pages") {
                $updateBanner = Page::where('id', $id)->update(['active' => $status]);
                $outcome = 'Page ' . $blockUnblockText . " successfully.";
            }
            if ($table == "page_details") {
                $updateBanner = PageDetail::where('id', $id)->update(['active' => $status]);
                $outcome = 'Section ' . $blockUnblockText . " successfully.";
            }
            if ($table == "categories") {
                $updateBanner = Category::where('id', $id)->update(['active' => $status]);
                $outcome = 'Category ' . $blockUnblockText . " successfully.";
            }
            if ($table == "gallery") {
                $updateBanner = CategoryGallery::where('id', $id)->update(['active' => $status]);
                $outcome = 'Image ' . $blockUnblockText . " successfully.";
            }

            if ($table == "testimonials") {
                $updateBanner = Testimonial::where('id', $id)->update(['is_active' => $status]);
                $outcome = 'Testimonial ' . $blockUnblockText . " successfully.";
            }

            if ($table == "teams") {
                $updateBanner = Team::where('id', $id)->update(['is_active' => $status]);
                $outcome = 'Team Member ' . $blockUnblockText . " successfully.";
            }
            if ($table == "states") {
                $updateBanner = \App\Models\State::where('id', $id)->update(['status' => $status]);
                $outcome = 'State ' . $blockUnblockText . " successfully.";
            }
            if ($table == "districts") {
                $updateBanner = \App\Models\District::where('id', $id)->update(['status' => $status]);
                $outcome = 'District ' . $blockUnblockText . " successfully.";
            }
            if ($table == "cities") {
                $updateBanner = \App\Models\City::where('id', $id)->update(['status' => $status]);
                $outcome = 'City ' . $blockUnblockText . " successfully.";
            }
            if ($table == "schools") {
                $updateBanner = \App\Models\School::where('id', $id)->update(['status' => $status]);
                $outcome = 'School ' . $blockUnblockText . " successfully.";
            }
            if ($table == "demand_liberaries") {
                $updateBanner = \App\Models\DemandLiberary::where('id', $id)->update(['active' => $status]);
                $outcome = 'Demand Liberary ' . $blockUnblockText . " successfully.";
            }
            if ($table == "school_classes") {
                $updateBanner = \App\Models\SchoolClass::where('id', $id)->update(['status' => $status]);
                $outcome = 'School Class' . $blockUnblockText . " successfully.";
            }

            //            $user = Auth::user();
            //            $description = 'Banner ' . $blockUnblockText . ' by ' . $user->displayName() . '.';
            //            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            $outcome = $e->getMessage();
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function destroy(Banner $hierarchy, $id) {
        //        if (Gate::denies('banner.index', 'update')) {
        //            abort(403);
        //        }
        //        dd('kjkh');
        $outcome = '';
        try {
            DB::beginTransaction();
            Banner::where('id', $id)->delete();
            $outcome = "Banner deleted successfully.";

            // save audit
            //            $user = Auth::user();
            //            $description = $user->displayName() . ' has deleted Banner.';
            //            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    private function validatedBanner() {

        $data = request()->validate([
            'image' => ['required'],
        ]);
        return $data;
    }

    public function resizeImagePost(Request $request, $image = null) {

        /* if ($request->hasFile('image')) {

          $originName = $request->file('image')->getClientOriginalName();
          $fileName = pathinfo($originName, PATHINFO_FILENAME);
          $extension = $request->file('image')->getClientOriginalExtension();
          $fileName = date('ymdhis') . '.' . $extension;

          $request->file('image')->move(public_path('images/thumbnails'), $fileName);

          return $fileName;
          //                $record['image'] = $fileName;

          } */

        if (!$image) {
            $image = $request->file('image');
        }
        
        $on = $image->getClientOriginalName();
//        dd($on);
        
        $input['imagename'] = time() . '-' . $on;
//        dd($input['imagename']);

        $destinationPath = public_path('/images/thumbnails');

        $img = Image::make($image->getRealPath());
        $img->resize(1000, 1000, function ($constraint) {

            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($destinationPath . '/' . $input['imagename']);

        /*
         * Large Image
         */

        //        $destinationPath = public_path('/images');
        //
        //        $image->move($destinationPath, $input['imagename']);


        return $input['imagename'];
    }

    public function ckeditorUpload(Request $request) {

        if ($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;

            //Upload File
            $request->file('upload')->storeAs('public/uploads', $filenametostore);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/' . $filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output 
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }

}
