<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Broadcast;
use App\Models\BroadcastClass;
use App\Models\BroadcastUser;
use App\Models\State;
use App\Models\School;
use App\Models\City;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolClass;
use App\Traits\CronJobTrait;

class BroadcastsController extends Controller {

    use CronJobTrait;

    public function index(Request $request) {

        $schools = Broadcast::join('broadcast_users', 'broadcasts.id', 'broadcast_users.broadcast_id')
                ->join('broadcast_classes', 'broadcasts.id', 'broadcast_classes.broadcast_id')
                ->join('school_classes', 'broadcast_classes.school_class_id', 'school_classes.id')
                ->join('schools', 'broadcast_classes.school_id', 'schools.id')
                ->select([
                    'broadcasts.*',
                    DB::raw('(SELECT COUNT(user_id) FROM broadcast_users WHERE broadcasts.id = broadcast_users.broadcast_id and broadcast_users.status = 1) as success_sent'),
                    DB::raw('(SELECT COUNT(user_id) FROM broadcast_users WHERE broadcasts.id = broadcast_users.broadcast_id and broadcast_users.status = 0) as failed_sent'),
                    DB::raw('(Select (GROUP_CONCAT(DISTINCT school_classes.title)) WHERE broadcast_classes.school_class_id = school_classes.id and broadcasts.id = broadcast_classes.broadcast_id) as class_title'),
                    DB::raw('(Select (GROUP_CONCAT(DISTINCT schools.name)) WHERE broadcast_classes.school_id = schools.id and broadcasts.id = broadcast_classes.broadcast_id) as school_titles'),
                ])
                ->where('broadcasts.franchise_id',auth()->user()->franchise_id)
                ->groupby('broadcasts.id')
                ->orderby('broadcasts.id','DESC')
                ->franchiseCheck()
                ->paginate(10);



        //$schools = Broadcast::paginate(15);
        //dd($schools);

        if ($request->ajax()) {
            $view = view('admin.broadcasts.table', compact('schools'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();


        return view('admin.broadcasts.index', compact('schools', 'roles'));
    }

    public function create($id = null) {
        $school = new Broadcast;
        if ($id) {
            $school = Broadcast::find($id);
        }
        /*
         * States with schools
         */
        $getSchools = School::select('state_id')->whereHas('school_classes', function($q) {
                    return $q->whereNotNull('school_classes.start_on');
                })->groupBy('state_id')->get();
        $statesWithData = $getSchools->pluck('state_id')->toArray();

        $states = \App\Models\State::whereIn('id', $statesWithData)->orderBy('state', 'ASC')->Dropdown()->getAll()->pluck('value', 'id')->toArray();
        $states = ['' => 'Select State'] + $states;

        if ($school->state_id) {
            $sId = $school->state_id;
        }
        $view = view('admin.broadcasts.form', compact('school', 'states'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {

        try {
            DB::beginTransaction();

            $record = $request->record;


            $broadcast = Broadcast::create($record);
            $broadcastLastId = $broadcast->id;
            $outcome = "Broadcast Created Successfully";
            /*
             * Update Broadcast Class
             */
            $classes = $request->school_class_ids;
            foreach ($classes as $class):
                $school = SchoolClass::select('school_id')->where('id', $class)->first();
                $schoolId = $school->school_id;

                $data['Broadcast_id'] = $broadcastLastId;
                $data['school_class_id'] = $class;
                $data['school_id'] = $schoolId;
                BroadcastClass::create($data);
            endforeach;

//            dd($broadcast);
            /*
              Get Parents by Class


             */
            $classes_ids = implode(',', $classes);

            $parents = DB::table('users')
                    ->selectRaw('DISTINCT users.id, users.phone1, users.phone2')
                    ->join('admissions', 'users.id', '=', 'admissions.parent_id')
                    ->join('admission_details', 'admissions.id', '=', 'admission_details.admission_id')
                    ->whereRaw("admission_details.school_class_id in ($classes_ids) and (users.phone1 IS NOT NULL or users.phone2 IS NOT NULL)")
                    ->get();

            // Send SMS CURL
            foreach ($parents as $key => $val) {
                //$broadcastLastId
                $userid = $val->id;

                $data = [];
                $data['broadcast_id'] = $broadcastLastId;
                $data['user_id'] = $userid;

//                $data['status'] = $status;
//                $data['response'] = json_decode($response, true)['MessageID'];
                //Create Parent - Broadcast User
                BroadcastUser::create($data);
            }

            $this->checkScheduledBroadcast();





            DB::commit();

            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

}
