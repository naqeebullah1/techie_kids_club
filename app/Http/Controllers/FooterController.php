<?php

namespace App\Http\Controllers;

use App\Models\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Intervention\Image\Facades\Image;

class FooterController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy() {
        $content = Footer::franchiseCheck()->first();

        return view('footer.privacy', compact('content'));
    }

    public function terms() {
        $content = Footer::franchiseCheck()->first();
//        dd($content);
        return view('footer.terms', compact('content'));
    }

    public function index() {
        $footer = Footer::franchiseCheck()->get();
        return view('footer.index', compact('footer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $type = 'create';
        $footer = new Footer();
        return view('footer.form', compact('type', 'footer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        return "create not available";

        $outcome = '';

        try {
            DB::beginTransaction();
            $user = auth()->user();

            $valdidateData = request()->footer;
            $valdidateData['updated_by'] = $user->id;
            $valdidateData['description'] = $request->section;
            $valdidateData['hours'] = $request->hours;
            $valdidateData['privacy'] = $request->privacy;
            $valdidateData['terms'] = $request->terms;


            Footer::create($valdidateData);

            DB::commit();
            $outcome = 'Footer Info has been added successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.footers.index')->with('outcome', $outcome);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $footer = Footer::find($id);
        $type = 'update';
        return view('footer.form', compact('type', 'footer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $outcome = '';

        try {
            DB::beginTransaction();
            $user = auth()->user();

            $valdidateData = request()->footer;
            $valdidateData['updated_by'] = $user->id;
            $valdidateData['description'] = $request->section;
            $valdidateData['hours'] = $request->hours;
            $valdidateData['privacy'] = $request->privacy;
            $valdidateData['terms'] = $request->terms;

//            dd($id);
//            dd($valdidateData);

            Footer::find($id)->update($valdidateData);

            DB::commit();
            $outcome = 'Footer Info has been updated successfully.';
        } catch (QueryException $e) {
            DB::rollBack();
            dd($e);
        }

        return redirect()->route('admin.footers.index')->with('outcome', $outcome);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
