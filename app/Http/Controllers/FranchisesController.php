<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Franchise;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class FranchisesController extends Controller {

    public function index(Request $request) {
        $schools = Franchise::query();

        $schools = $schools->paginate(15);
        $usersIds = $schools->pluck('user_id')->toArray();

        $users = DB::table('users')
                ->selectRaw('concat(last_name," - ",email) value,id')
                ->whereIn('id', $usersIds)
                ->pluck('value', 'id')
                ->toArray();

        if ($request->ajax()) {
            $view = view('franchises.table', compact('schools', 'users'))
                    ->render();
            return response()->json(['html' => $view]);
        }

//        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
//        $states = State::select('state', 'id')->where('status', 1)->get();
//        dd($states);
        return view('franchises.index', compact('schools', 'users'));
    }

    public function create($id = null) {
        $school = new Franchise();
        if ($id) {
            $school = Franchise::find($id);
        }
        $schoolAdmins = DB::table('users')
                ->selectRaw('concat(last_name," - ",email) value,id')
                ->join('model_has_roles', 'model_has_roles.model_id', 'users.id')
                ->where('role_id', 1)
                ->groupBy('users.id')
                ->pluck('value', 'id');

//        dd($schoolAdmins);


        $view = view('franchises.form', compact('school', 'schoolAdmins'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $record = $request->record;
//            dd($request->all());
            $id = $request->id;

            $slugCheck = Franchise::where('slug', $request->record['slug']);

            if ($id) {
                $slugCheck->where('id', '!=', $id);
//                dd($slugCheck->first());
            }
            $slugCheck = $slugCheck->count();

            if ($slugCheck) {
                return redirect()->back()->with('error-message', 'Slug is already taken');
            }


            if ($request->hasFile('header_logo')) {

                $originName = $request->file('header_logo')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('header_logo')->getClientOriginalExtension();
                $fileName = 'header_logo-' . date('ymdhis') . '.' . $extension;

                $request->file('header_logo')->move(public_path('frontend/images/logo'), $fileName);
                $record['header_logo'] = $fileName;
            }
            if ($request->hasFile('footer_logo')) {
                $originName = $request->file('footer_logo')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('footer_logo')->getClientOriginalExtension();
                $fileName = 'footer_logo-' . date('ymdhis') . '.' . $extension;

                $request->file('footer_logo')->move(public_path('frontend/images/logo'), $fileName);
                $record['footer_logo'] = $fileName;
            }
            if ($request->hasFile('no_image_logo')) {
                $originName = $request->file('no_image_logo')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('no_image_logo')->getClientOriginalExtension();
                $fileName = 'no_image_logo-' . date('ymdhis') . '.' . $extension;

                $request->file('no_image_logo')->move(public_path('frontend/images/logo'), $fileName);
                $record['no_image_logo'] = $fileName;
            }
            if ($id) {
                $franchise = Franchise::find($id)->update($record);
                $outcome = "Franchise Updated Successfully";
            } else {
                $record['user_id'] = $request->user_id;

                $franchise = Franchise::create($record);
                $fId = $franchise->id;

                DB::table('users')->where('id', $request->user_id)->update(['franchise_id' => $fId]);

//                User::find($request->user_id)->update(['franchise_id', $fId]);
                $outcome = "Franchise Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function defaultForAll() {
        $table = request()->table;
        $id = request()->id;
        $record = DB::table($table)->find($id);
        if ($record->franchise_id != 0) {
            $franchise_id = 0;
        } else {
            $franchise_id = auth()->user()->franchise_id;
        }
        DB::table($table)->where('id', $id)->update(['franchise_id' => $franchise_id]);

        return redirect()->back()->with('outcome', 'Record Changed');
    }

    public function userFranchise() {
        $user = User::find(request()->user_id);
//        dd();
//        dd();
//        $franchises = franchiseList();
        $view = view('franchises.user_franchise', compact('user'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function userFranchiseStore(Request $request) {
//        dd($request->all());
        /*
         * user_franchises
         */
        User::find($request->user_id)->user_franchises()->detach();
        $franchiseStore = User::find($request->user_id)->user_franchises()->attach(request()->form_franchise_id);
        return redirect()->back()->with('outcome', 'Franchise assigned successfully');
    }

}
