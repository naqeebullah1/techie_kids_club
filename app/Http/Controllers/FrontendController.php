<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Category;
use Illuminate\Support\Facades\Mail;
use App\Mail\contactMail;
use App\Models\Contact;
use App\Models\Testimonial;
use App\Models\Team;
use App\Models\Service;
use App\Models\School;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\Admission;
use App\Models\AdmissionDetail;
use App\Models\FranchiseUser;
use App\Models\Franchise;
use Stripe;
use Auth;

class FrontendController extends Controller {

    public function index($franchise, $type = 'home') {
//        findBySlug(request()->franchise_id)
        $franchiseCheck = \App\Models\Franchise::whereSlug($franchise)->first();
        if (!$franchiseCheck) {
            abort(404);
        }
        $page = Page::with('pageDetails')
                ->franchiseCheck()
                ->whereSlug($type)
                ->first();

        $slider = new Banner;
        if ($type == "home") {
            $slider = Banner::where('active', 1)->first();
        }
//        dd($slider);
        $schools = School::where('status', 1)->get();
//dd($schools);
        return view('frontend.index', compact('slider', 'page', 'schools'));

//        if ($page->is_contact == 1):
//            return view('frontend/contact');
//        elseif ($page->is_front == 1):
//        else:
//            return view('frontend.other_pages', compact('page'));
//
//        endif;
    }

    public function gallery($slug) {
        $gallery = Category::with('images')->whereSlug($slug)->first();
        return view('frontend.gallery', compact('gallery'));
    }

    public function service($id) {
        $service = \App\Models\Service::find($id);
        return view('frontend.service', compact('service'));
    }

    public function teamMember($id) {
        $teamMember = \App\Models\Team::find($id);
        return view('frontend.team_member', compact('teamMember'));
    }

    public function serviceDetails($id) {
        $service = Service::find($id);
        return view('frontend.services.service_details', compact('service'));
    }

    public function serviceDetail($id) {
        $serviceDetail = \App\Models\Service::find($id);
        return view('frontend.service_detail', compact('serviceDetail'));
    }

    public function contact() {
        return view('frontend.contact');
    }

    public function sendMessage(Request $request) {
//dd($request->for_demo);
        if (isset($request->for_demo)) {
            request()->validate([
                'contact.first_name' => ['required'],
                'contact.last_name' => ['required'],
                'contact.email' => ['required'],
                'contact.phone' => ['required'],
                'contact.school_name' => ['required'],
                'contact.title' => ['required'],
                'contact.select_one' => ['required'],
                    ], [
                'contact.first_name.required' => 'First Name is required.',
                'contact.last_name.required' => 'Last Name is required.',
                'contact.email.required' => 'Email is required.',
                'contact.phone.required' => 'School Phone is required.',
                'contact.school_name.required' => 'School Name is required.',
                'contact.title.required' => 'Contact Title is required.',
                'contact.select_one.required' => 'Please Select One is required.',
            ]);
        } else {

            request()->validate([
                'contact.first_name' => ['required'],
                'contact.last_name' => ['required'],
                'contact.email' => ['required'],
                'contact.phone' => ['required'],
                'contact.message' => ['required'],
                    ], [
                'contact.first_name.required' => 'First Name is required.',
                'contact.last_name.required' => 'Last Name is required.',
                'contact.email.required' => 'Email is required.',
                'contact.phone.required' => 'Phone is required.',
                'contact.message.required' => 'Message is required.',
            ]);
        }


        try {
            DB::beginTransaction();

            $data = $request->contact;
            if ($request->service) {
                $services = json_encode($request->service);
                $data['services'] = $services;
            }

//            dd($data);
            Contact::create($data);


            $to = "support@techiekidsclub.com";//env('MAIL_FROM_ADDRESS');
            //$to = "solutioners9@gmail.com";
            $details = $request->contact;


                    $subject = "Techie Kids Iquiry";
                    
                    //$cc = "cyberuser1@techiekidsclub.com";
                 
                    
                    $emailHtml = view('emails/contact', compact('details'))->render();
                   $sent =  sendEmail($subject,$to,$emailHtml);
            /*$mail = Mail::to($to)
                    ->send(new contactMail($request->contact));*/

//   dd($to);
            $outcomeStatus = "";
            $outcomeMessage = "";
            if (Mail::failures()) {
                $outcomeStatus = "error";
                $outcomeMessage = "Some error occured, please try again.";
            } else {
                $outcomeStatus = "success";
                $outcomeMessage = "Thank you for contacting us. We will reach out to you soon.";
            }
            DB::commit();
            return redirect()->back()->with($outcomeStatus, $outcomeMessage)->withFragment('#contactsection');
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function check_email($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }

    public function findYourSchool() {
        $getSchools = School::select('state_id')
                ->whereHas('school_classes', function($q) {
                    //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                    return $q->whereNotNull('school_classes.start_on')->where('schools.status', 1);
                })
                ->groupBy('state_id')
                ->get();
        $statesWithData = $getSchools->pluck('state_id')->toArray();

        $states = \App\Models\State::whereIn('id', $statesWithData)
                ->orderBy('state', 'ASC')
                ->Dropdown()
                ->getAll()
                ->pluck('value', 'id')
                ->toArray();

        $states = ['' => 'Select State'] + $states;
        $cities = ['' => 'Select City'];

        if (request()->state_id || request()->city_id) {
            $schools = School::query();
            if (request()->state_id) {
                $schools = $schools->where('state_id', request()->state_id);
            }
            if (request()->city_id) {
                $schools = $schools->where('city_id', request()->city_id);
            }
            if (request()->school_name) {
                $schools = $schools->where('name', 'Like', '%' . request()->school_name . '%');
            }
            $schools = $schools->whereHas('school_classes', function($q) {
                //return $q->whereDate('school_classes.start_on', '>', date('Y-m-d'));
                return $q->whereNotNull('school_classes.start_on')->where('schools.status', 1);
            });
            $schools = $schools->get();

            return view('frontend.partials.card', compact('schools'));
        }



        return view('frontend.find_your_school', compact("states", 'cities'));
    }

    public function schoolDetails() {
        $id = request()->id;
        $school = School::find($id);
        return view('frontend.school_details', compact("school"));
    }

    public function classDetails() {
        $id = request()->id;
        $school = SchoolClass::find($id);
        return view('frontend.class_details', compact("school"));
    }

    public function login() {
        if (strstr(url()->previous(), "/class-details/")) {
            session(['classlink' => url()->previous()]);
        }
        return view('frontend.login');
    }

    public function updateCart() {
        $id = request()->id;
        if ($id) {
            $schoolClass = SchoolClass::find($id);
            $student = \App\Models\Student::find(request()->student_id);
            $new = [
                'school_logo' => asset('images/uploads/' . $schoolClass->school->image),
                'school_name' => $schoolClass->school->name,
                'title' => $schoolClass->title,
                'class_charges' => $schoolClass->class_charges,
                'end_date' => $schoolClass->end_on,
                'student_id' => $student->id,
                'student_name' => $student->first_name . ' ' . $student->last_name,
                'school_class_id' => $id,
                'school_id' => $schoolClass->school_id,
                'classroom_name' => request()->classroom_name,
                'duration_type' => $schoolClass->duration_type,
                'franchise' => $schoolClass->school->franchise_id
            ];

            $old = Session::get('cart_info');

            $cart = [];
            if ($old) {
                foreach ($old as $o):
                    $cart[] = $o;
                endforeach;
                $cart[] = $new;
            }
            if (empty($cart)) {
                $cart = [$new];
            }

            Session::put('cart_info', $cart);
        } else {
            $cart = Session::get('cart_info');
        }
        $view = view('frontend.cart', compact('cart'))
                ->render();
        return response()->json(['html' => $view, 'count' => count($cart)]);
    }

    public function checkout() {
        
        if(!auth()->user()) {
            return redirect()->back();
        }
        $proIdWithKey = request()->productId ? request()->productId : null;
        
        //Session::forget('selectedFranchise');
        //Session::forget('radioselectedFranchise');

        $proId = explode("-", $proIdWithKey)[0];

        if ($proId != null) {
           
            $selectFranchise = SchoolClass::find($proId)->franchise_id;
            Session::forget('selectedFranchise');
            Session::forget('radioselectedFranchise');
            Session::put('selectedFranchise', $selectFranchise);
            Session::put('radioselectedFranchise', $proIdWithKey);
            return redirect()->back();
        }
//                Session::forget('cart_info');
        Session::put('promos', null);
        $customer_stripe_id = "";
        //$customer_stripe_id = auth()->user()->stripe_customer_id;
//        dd(Session::get('selectedFranchise'));
        if (Session::get('selectedFranchise') && Session::get('selectedFranchise') != 1) {
            $customer_stripe_id = FranchiseUser::where('user_id', auth()->user()->id)
                    ->where('franchise_id', Session::get('selectedFranchise'))
                    ->first();
            if($customer_stripe_id){
                $customer_stripe_id=$customer_stripe_id->customer_id;
            }
//            dd(Session::get('selectedFranchise'));
        } else {
            //dump(auth()->user()->stripe_customer_id);
            if(isset(auth()->user()->stripe_customer_id)){
                $customer_stripe_id = auth()->user()->stripe_customer_id;
            }
            if($customer_stripe_id == "") {
                $customer_stripe_id = FranchiseUser::where('user_id', auth()->user()->id)
                    ->where('franchise_id', 1)
                    ->first();
                if($customer_stripe_id) {
                    $customer_stripe_id=$customer_stripe_id->customer_id;
                }
            
            }
        }
        $cardlist = '';
        if (isset($customer_stripe_id) && Session::get('selectedFranchise')) {
//            $stripeAPiKey = 'sk_test_51HyGh4B3WkD7XcJGzOo6cTQRB4f2D9QQhPUkYiAXHcqEmkf2npVre8PV9Z9MsxBufAyN5hczg7lqy9co3updar5000KiCSOmnq';
            
            
            $franchise = Franchise::find(Session::get('selectedFranchise'));
            $stripeAPiKey = $franchise->stripe_client;

            $stripe = new \Stripe\StripeClient($stripeAPiKey);
            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $cardlist = $list_cards['data'];
            } else {
                $cardlist = "";
            }
        }
// get credit card list from stripe


        return view('frontend.checkout', compact('cardlist'));
    }

    public function storeOrder(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();
            $data = Session::get('cart_info');
            $addmission = Admission::create(['parent_id' => auth()->user()->id]);

            foreach ($data as $d):
                $s = [
                    "class_charges" => $d['class_charges'],
                    "student_id" => $d['student_id'],
                    "school_class_id" => $d['school_class_id'],
                    "school_id" => $d['school_id'],
                    "admission_id" => $addmission->id,
                ];
                AdmissionDetail::create($s);
//                dump($s);
            endforeach;
//            exit;

            if (Mail::failures()) {
                $outcomeStatus = "error";
                $outcomeMessage = "Some Error Occured. Please try again.";
            } else {
                $outcomeStatus = "success";
//                $outcomeMessage = "Some Error Occured. Please try again.";
                $outcomeMessage = "Your admission process is started. We will reply soon. Thank you!";
            }
            Session::forget('cart_info');
            DB::commit();
            return redirect()->back()->with($outcomeStatus, $outcomeMessage);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function viewCart() {
//        Session::forget('cart_info');
        if (request()->isMethod('post')) {
//            dd(request()->all());
            Session::put('cart_info', request()->cart);
            Session::forget('selectedFranchise'); 
            Session::forget('radioselectedFranchise');
            return redirect()->back()->with('success', 'Cart updated successfully');
        }
        return view('frontend.view_cart');
    }

    public function thanks() {
        Session::forget('selectedFranchise'); 
        Session::forget('radioselectedFranchise');
        Session::forget('cart_info');
        return view('frontend.thanks');
    }

}
