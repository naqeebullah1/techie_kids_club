<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use App\Models\NewsletterDesign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function index()
    {
        $newsletters = Newsletter::leftJoin('users', 'newsletters.email', '=', 'users.email')
            ->select('newsletters.id', 'newsletters.email', 'users.id as parent_id', 'users.first_name as firstName', 'users.last_name as lastName')
            ->paginate(15);
        return view('admin.newsletters.index', compact('newsletters'));
    }

    public function store(Request $request)
    {
        if ($request->email) {
            Newsletter::where('email', $request->email)->where('status', 1)->delete();
            Newsletter::create(['email' => $request->email]);

            $franchise = getFranchise();

            $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . frontendBaseUrl() . '" title="logo" target="_blank">
                            <img width="220" src="' . asset('frontend/images/logo/' . $franchise->header_logo) . '" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">It\'s nice to meet you!</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            You have successfully subscribed to '.$franchise->name.' Newsletter
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>' . str_replace(" ", "", strtolower(url('front-franchise/' . $franchise->slug))) . '</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';

            $email = $request->email;

            $verify = Mail::send(array(), array(), function ($message) use ($html, $email) {
                $message->to($email)
                    ->subject("Newsletter Subscription")
                    ->setBody($html, 'text/html');
            });

            return redirect()->back()->with('success', "You Have Subscribed Successfully");
        }
    }

    public function unsub(Request $request)
    {
        $newsletter = Newsletter::where('email', 'LIKE', '%'.$request->email.'%')->first();
        if ($newsletter) {
            $newsletter->status = 0;
            $newsletter->update();
        }
        return 'You have successfully unsubscribed!';
    }

    public function designForm()
    {
        $newsletterDesign = NewsletterDesign::first();
        if (!$newsletterDesign)
            $newsletterDesign = new NewsletterDesign();
        return view('admin.newsletters.design', compact('newsletterDesign'));
    }

    public function designStore(Request $request)
    {
        $imageName = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $imageName = time() . '.' . $image->extension();

            $path = public_path('/frontend/images/newsletter');

            $request->image->move($path, $imageName);
        } else {
            $newsletter = NewsletterDesign::find(1);
            if ($newsletter != null && $newsletter->image != null)
                $imageName = $newsletter->image;
        }
        NewsletterDesign::truncate();

        NewsletterDesign::create([
            'title' => $request->title,
            'content' => $request->get('content'),
            'image' => $imageName
        ]);

        return redirect()->back();
    }
}
