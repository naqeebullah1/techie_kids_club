<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\NotificationViewer;
use App\Models\State;
use App\Models\School;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use App\Models\SchoolClass;

class NotificationsController extends Controller {

    public function index(Request $request) {
        $schools = Notification::select('notifications.*',
                 DB::raw('(Select GROUP_CONCAT(Distinct schools.name)
                  WHERE notification_viewers.school_id = schools.id and
                  notifications.id = notification_viewers.notification_id) as school_titles')
                )
                ->join('notification_viewers', 'notification_viewers.notification_id', 'notifications.id')
                ->join('schools', 'notification_viewers.school_id', 'schools.id')
                ->groupBy('notification_viewers.notification_id')
                ->where('notifications.franchise_id',auth()->user()->franchise_id)
                ->paginate(15);
        if ($request->ajax()) {
            $view = view('admin.notifications.table', compact('schools'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();

        return view('admin.notifications.index', compact('schools', 'roles'));
    }

    public function create($id = null) {
        $school = new Notification;
        if ($id) {
            $school = Notification::find($id);
        }
        /*
         * States with schools
         */
        $getSchools = School::select('state_id')->whereHas('school_classes', function($q) {
                    return $q->whereNotNull('school_classes.start_on');
                })->groupBy('state_id')->get();
        $statesWithData = $getSchools->pluck('state_id')->toArray();

        $states = \App\Models\State::whereIn('id', $statesWithData)->orderBy('state', 'ASC')->Dropdown()->getAll()->pluck('value', 'id')->toArray();
        $states = ['' => 'Select State'] + $states;

        if ($school->state_id) {
            $sId = $school->state_id;
        }
        $view = view('admin.notifications.form', compact('school', 'states'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
        try {

            DB::beginTransaction();

            $record = $request->record;
            $record['activation_date'] = date('Y-m-d', strtotime($record['activation_date']));

            $id = $request->id;


            if ($id) {
                $notification = Notification::find($id);
                if ($notification->files) {
                    $storeFiles = explode(',', $notification->files);
                }
            } else {
                $storeFiles = [];
            }


            if ($request->hasFile('files')) {
                //get filename with extension
                $files = $request->file('files');

                foreach ($files as $k => $file):
                    $filenamewithextension = $file->getClientOriginalName();
                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                    $extension = $file->getClientOriginalExtension();
                    $filenametostore = $filename . '-' . time() . '.' . $extension;
                    $file->move(public_path('user_files'), $filenametostore);

                    $storeFiles[] = $filenametostore;
                endforeach;
                $record['files'] = implode(',', $storeFiles);
            }

            if ($id) {
                $notification->viewers()->delete();

                $notification->update($record);
//                dd($request->school_class_ids);
                $lastId = $id;
                $outcome = "Notification Updated Successfully";
            } else {
                $notification = Notification::create($record);
                $lastId = $notification->id;
                $outcome = "Notification Created Successfully";
            }

            /*
             * Update Viewers
             */
            $classes = $request->school_class_ids;
            foreach ($classes as $class):
                $school = SchoolClass::select('school_id')->where('id', $class)->first();
                $schoolId = $school->school_id;
                $data['school_id'] = $schoolId;
                $data['notification_id'] = $lastId;
                $data['school_class_id'] = $class;
                NotificationViewer::create($data);
            endforeach;




            DB::commit();
            return redirect()->back()->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function removeFile($id) {
//     dd($id);
        $file = request()->file;
        $user = Notification::find($id);
        $userFiles = explode(',', $user->files);
        foreach ($userFiles as $k => $userFile):
            if ($userFile == $file) {
                unset($userFiles[$k]);
            }
        endforeach;
        if ($userFiles) {
            $userFiles = implode(',', $userFiles);
        } else {
            $userFiles = null;
        }
        $user->update(['files' => $userFiles]);
    }

    public function destroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $record = Notification::find($id);

            $record->viewers()->delete();
            $record->delete();
            $outcome = "Notification deleted successfully.";
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

}
