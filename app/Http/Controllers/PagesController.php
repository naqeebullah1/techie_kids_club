<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\PageDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\PageDetailSection;
use Illuminate\Support\Facades\Http;

class PagesController extends Controller {

   
    public function testApiRes() {
//        dd('yes');
//        $clientId = "e1438694-f30a-44e5-b1c8-65ef35d08ef0";
        $clientId = "2a963702-8428-42c6-956e-a5ea67bce796";
        $redirectURI = "http://localhost/techie_kids_club/test-api-redirect";
        $code = request()->code;
//        $clientSecret = "d1ieVcCzR7SOMOcxD9mZiQ";
        $clientSecret = "aSlhvz0AGdbZaCcQBY8GZA";
        $response = $this->getAccessToken($redirectURI, $clientId, $clientSecret, $code);
        /*
         * Save Access Token Information in DB
         * 
         */

        dd($response);
    }

    function getAccessToken($redirectURI, $clientId, $clientSecret, $code) {
        // Use cURL to get access token and refresh token
        $ch = curl_init();

        // Define base URL
        $base = 'https://authz.constantcontact.com/oauth2/default/v1/token';
        // Create full request URL
        $url = $base . '?code=' . $code . '&redirect_uri=' . $redirectURI . '&grant_type=authorization_code';
//dd($url);
        curl_setopt($ch, CURLOPT_URL, $url);

        // Set authorization header
        // Make string of "API_KEY:SECRET"
        $auth = $clientId . ':' . $clientSecret;
        // Base64 encode it
        $credentials = base64_encode($auth);
        // Create and set the Authorization header to use the encoded credentials, and set the Content-Type header
        $authorization = 'Authorization: Basic ' . $credentials;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization, 'Content-Type: application/x-www-form-urlencoded'));

        // Set method and to expect response
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Make the call
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function testApi() {
//        dd('');
//        $clientId = "e1438694-f30a-44e5-b1c8-65ef35d08ef0";
        $clientId = "2a963702-8428-42c6-956e-a5ea67bce796";
        $redirectURI = "http://localhost/techie_kids_club/test-api-redirect";
        $scope = "contact_data";
        $state = "235o250eddsdff";

        $url = $this->getAuthorizationURL($clientId, $redirectURI, $scope, $state);
        return $url;
//        return \Illuminate\Support\Facades\Redirect::to($url);
    }

    public function getAuthorizationURL($clientId, $redirectURI, $scope, $state) {
        $baseURL = "https://authz.constantcontact.com/oauth2/default/v1/authorize";
        $authURL = $baseURL . "?client_id=" . $clientId . "&scope=" . $scope . "+offline_access&response_type=code&state=" . $state . "&redirect_uri=" . $redirectURI;

        return $authURL;
    }

    public function dashboard() {
//        dump(auth()->user()->role);
        return view('pages/dashboard');
    }

    public function index($id = null) {
        if ($id) {
            $page = Page::find($id);
            return view('pages/edit', compact('page'));
        }
        $banners = Page::franchiseCheck()->paginate(10);
        return view('pages/index', compact('banners'));
    }

    public function pageDetails($pageId) {
        $banners = PageDetail::orderBy('sort_order')->where('page_id', $pageId)->paginate(10);
        return view('page_details/index', compact('banners', 'pageId'));
    }

    public function contactDetails($pageId) {

        $page = Page::find($pageId);
        return view('page_details/contact', compact('page'));
    }

    public function updateType(Request $request) {
        if ($request->pageid_front) {
            foreach ($request->pageid_front as $key => $value) {
                $id = $request->pageid_front[$key];
                DB::beginTransaction();
                $page = Page::find($id);
                $page->update([
                    'is_front' => $request->is_front_radio[$key],
                    'is_contact' => $request->is_contact_radio[$key],
                    'is_service' => $request->is_service_radio[$key],
                    'updated_by' => '',
                ]);
                DB::commit();
            }
            $outcome = 'Page settings updated.';
            return redirect('backend/pages')->with('outcome', $outcome);
        }
    }

    public function store(Request $request) {
//        dd($request->all());

        if (request()->contact) {
            $data = request()->validate([
                'contact.phone1' => ['required'],
                'contact.email' => ['required'],
                'contact.address1' => ['required'],
            ]);
        } else {
            $data = request()->validate([
                'page.title' => ['required'],
                'page.slug' => ['required'],
//                'page.keyword' => ['required'],
//                'page.description' => ['required'],
            ]);
        }
        try {
            DB::beginTransaction();
            $user = Auth::user();
            $active = 1;
            if (request()->contact) {
                $page = request()->contact;
            } else {
                $page = $request->page;
                $page['active'] = $active;
            }
            if ($request->id) {
                $page['updated_by'] = '';
                $banner = Page::find($request->id)->update($page);
                $outcome = 'Page has been Updated successfully.';
            } else {
                $banner = Page::create($page);
                $outcome = 'New Page has been added successfully.';
            }

//            $description = $user->displayName() . ' has added a new banner.';
//            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
            return redirect('backend/pages')->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function pageDetailCreate($pageId) {
        $page = new Page;

        return view('page_details/create', compact('pageId', 'page'));
    }

    public function pageDetailEdit($pageId) {
        $page = PageDetail::find($pageId);
        return view('page_details/edit', compact('pageId', 'page'));
    }

    public function pageDetailStore($pageId, Request $request) {
//dd($request->all());
        $rules = [
            'title' => ['required'],
            'section' => ['required'],
        ];

//        dd($request->all());
        if ($request->section_type == 22) {
            $rules = [];            
        }

        if ($request->section_type == 1) {
            $rules = [
                'section_type' => ['required'],
                'sec3.*.title' => ['required'],
                'sec3.*.image' => ['required'],
                'sec3.*.description' => ['required'],
            ];
        }
//        dd($request->two_sections);
//        if ($request->section_type == 2) {
//            $rules = [
//                'link' => ['required'],
//            ];
//        }
        $request->section_type;



        $noValidation = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
        if (!in_array($request->section_type, $noValidation)) {
            $data = request()->validate($rules);
        }

        try {
            DB::beginTransaction();
            $user = Auth::user();
            $sort_order = PageDetail::where('page_id', $pageId)->max('sort_order');
            $sort_order = $sort_order + 1;

            $active = 1;
            $data = [
                'page_id' => $pageId,
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'section_type' => $request->section_type,
                'section' => $request->section,
                'active' => $active,
                'sort_order' => $sort_order,
                'btn_title' => $request->btn_title,
                'btn_link' => $request->btn_link,
                'btn_icon' => $request->btn_icon,
            ];

            if ($request->hasFile('image')) {
                $res = app('App\Http\Controllers\BannerController')->resizeImagePost($request);
                $data['image'] = $res;
            }


            $banner = PageDetail::create($data);
            /*
             * 3 1 3 Section
             */
            if ($request->section_type == 5) {
                $sections = $request->section5;

                //dd($sections);
                foreach ($sections as $k => $page_detail_section):
//                        $page_detail_section = collect($page_detail_section);
                    $files = $_FILES['section5'];
                    $extension = explode('.', $files['name'][$k]['icon']);
                    $extension = end($extension);
                    $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                    $target_file = public_path('images/thumbnails/' . $fileName);
                    move_uploaded_file($files['tmp_name'][$k]['icon'], $target_file);
                    unset($page_detail_section['icon']);
                    $page_detail_section['icon'] = $fileName;
                    unset($page_detail_section['id']);
                    $sections[$k] = $page_detail_section;
                endforeach;
                //dd($sections);
                $banner->sections()->createMany($sections);
            }
            /*
             * 3 columns section
             */
//            dd($request->section_type);
            if ($request->section_type == 1 || $request->section_type == 19) {
                $sections = $request->sec3;
//                dd($sections);
                if ($request->section_type == 1) {
                    foreach ($sections as $k => $page_detail_section):
//                        $page_detail_section = collect($page_detail_section);
                        $files = $_FILES['sec3'];
                        $extension = explode('.', $files['name'][$k]['image']);
                        $extension = end($extension);
                        $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                        $target_file = public_path('images/thumbnails/' . $fileName);
                        move_uploaded_file($files['tmp_name'][$k]['image'], $target_file);
                        unset($page_detail_section['image']);
                        $page_detail_section['icon'] = $fileName;
                        unset($page_detail_section['id']);
                        $sections[$k] = $page_detail_section;
                    endforeach;
                }
                $banner->sections()->createMany($sections);
            }
            /*
             * 4 Sections design
             * Tabs Section
             */
            if ($request->section_type == 15 || $request->section_type == 16) {
                $sections = $request->section4;
                foreach ($sections as $k => $page_detail_section):
                    $files = $_FILES['section4'];
                    $extension = explode('.', $files['name'][$k]['image']);
                    $extension = end($extension);
                    if ($extension) {
                        $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                        $target_file = public_path('images/thumbnails/' . $fileName);
                        move_uploaded_file($files['tmp_name'][$k]['image'], $target_file);
                        unset($page_detail_section['image']);
                        $page_detail_section['icon'] = $fileName;
                    }
                    unset($page_detail_section['id']);
                    $sections[$k] = $page_detail_section;
                endforeach;
                $banner->sections()->createMany($sections);
            }

            DB::commit();
            $outcome = 'New section has been added successfully.';
            return redirect()->route('admin.page.details', $pageId)->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function pageDetailUpdate($pageId, Request $request) {

        $rules = [
            'title' => ['required'],
            'section' => ['required'],
        ];

        if ($request->section_type == 22) {
            $rules = [];
        }        
//        if ($request->section_type == 1) {
//            $rules = [
//                'section_type' => ['required'],
//                'sec3.*.title' => ['required'],
//                'sec3.*.title' => ['required'],
//                'sec3.*.icon' => ['required'],
//                'sec3.*.description' => ['required'],
//            ];
//        }
//        dd($request->section_type);
//        if ($request->section_type == 2) {
//            $rules = [
//                'section_type' => ['required'],
//                'two_sections.*.title' => ['required'],
//                'two_sections.*.description' => ['required'],
//            ];
//        }
//        
//         $request->section_type;
//        $noValidation = [5,6,7,8,9];
//        if (!in_array($request->section_type, $noValidation)) {
//            $data = request()->validate($rules);
//        }

        try {
            DB::beginTransaction();
            $user = Auth::user();
            $data = [
                'title' => $request->title,
                'sub_title' => $request->sub_title,
                'link' => $request->link,
                'section_type' => $request->section_type,
                'section' => $request->section,
                'updated_by' => '',
                'btn_title' => $request->btn_title,
                'btn_link' => $request->btn_link,
                'btn_icon' => $request->btn_icon,
            ];

//                dd($data);
            if ($request->hasFile('image')) {
                $res = app('App\Http\Controllers\BannerController')->resizeImagePost($request);
                $data['image'] = $res;
            }

            $banner = PageDetail::find($pageId);
            $red = $banner->page_id;

//            if ($request->section_type == 1 || $request->section_type == 5) {
            if ($request->section_type == 5) {
                $banner->sections()->delete();
                $sections = $request->section5;

                $banner->sections()->createMany($sections);
            }
            if ($request->section_type == 5) {

                $sections = $request->section5;

                foreach ($sections as $k => $page_detail_section):
                    $files = $_FILES['section5'];
                    if ($files['name'][$k]['icon'] != "") {
                        $extension = explode('.', $files['name'][$k]['icon']);
                        $extension = end($extension);
                        //                        dd($extension);
                        if ($extension) {
                            $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                            $target_file = public_path('images/thumbnails/' . $fileName);
                            move_uploaded_file($files['tmp_name'][$k]['icon'], $target_file);
                            unset($page_detail_section['icon']);
                            $page_detail_section['icon'] = $fileName;
                        }
                    }
                    $id = $page_detail_section['id'];
                    unset($page_detail_section['id']);
                    PageDetailSection::find($id)->update($page_detail_section);
                endforeach;
            }
            if ($request->section_type == 1 || $request->section_type == 19) {
                $sections = $request->sec3;
                foreach ($sections as $k => $page_detail_section):
                    $files = $_FILES['sec3'];
                    $extension = explode('.', $files['name'][$k]['image']);
                    $extension = end($extension);
//                        dd($extension);
                    if ($extension) {
                        $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                        $target_file = public_path('images/thumbnails/' . $fileName);
                        move_uploaded_file($files['tmp_name'][$k]['image'], $target_file);
                        unset($page_detail_section['image']);
                        $page_detail_section['icon'] = $fileName;
                    }

                    $id = $page_detail_section['id'];
                    unset($page_detail_section['id']);

                    PageDetailSection::find($id)->update($page_detail_section);
                endforeach;
            }
            /*
             * 4 Sections design
             * Tabs Section
             */

            if ($request->section_type == 15 || $request->section_type == 16) {
//                dd('yes');
                $sections = $request->section4;
//                dd($sections);
                foreach ($sections as $k => $page_detail_section):
                    if ($request->section_type == 15) {
                        $files = $_FILES['section4'];
                        $extension = explode('.', $files['name'][$k]['image']);
                        $extension = end($extension);
                        if ($extension) {
                            $fileName = date('ymdhis') . '-' . $k . '.' . $extension;
                            $target_file = public_path('images/thumbnails/' . $fileName);
                            move_uploaded_file($files['tmp_name'][$k]['image'], $target_file);
                            unset($page_detail_section['image']);
                            $page_detail_section['icon'] = $fileName;
                        }
                    }
                    $id = $page_detail_section['id'];
                    unset($page_detail_section['id']);
                    PageDetailSection::find($id)->update($page_detail_section);
                endforeach;
            }

            $banner->update($data);

            DB::commit();
            $outcome = 'Section has been updated successfully.';
            return redirect()->route('admin.page.details', $red)->with('outcome', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {
//        if (Gate::denies('banner.index', 'update')) {
//            abort(403);
//        }
        $outcome = '';
        try {
            DB::beginTransaction();
            $page = Page::find($id);
//            $page->pageDetails->delete();
            $page->delete();
            $outcome = "Banner deleted successfully.";

            // save audit
//            $user = Auth::user();
//            $description = $user->displayName() . ' has deleted Banner.';
//            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function pageDetailDestroy($id) {
//        if (Gate::denies('banner.index', 'update')) {
//            abort(403);
//        }
        $outcome = '';
        try {
            DB::beginTransaction();
            $page = PageDetail::find($id);
//            $page->pageDetails->delete();
            $page->delete();
            $outcome = "Section deleted successfully.";

            // save audit
//            $user = Auth::user();
//            $description = $user->displayName() . ' has deleted Banner.';
//            $user->auditTrails()->create(['description' => $description, 'menu_id' => 7]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function ContactsDestroy($id) {
        $outcome = '';
        try {
            DB::beginTransaction();
            $page = \App\Models\Contact::find($id);
//            $page->pageDetails->delete();
            $page->delete();
            $outcome = "Contact message deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('outcome', $outcome);
    }

    public function contacts() {

        $contacts = \App\Models\Contact::orderBy('id', 'desc')->paginate(10);

        return view('pages/contacts', compact('contacts'));
    }

}
