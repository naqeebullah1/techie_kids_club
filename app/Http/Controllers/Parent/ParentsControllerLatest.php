<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\AdmissionDetail;
use App\Models\AdmissionDetailTrasaction;
use App\Models\Admission;
use App\Models\School;
use App\Models\SchoolClass;
use Stripe;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use Mail;
use Illuminate\Support\Facades\Storage;
use App\Models\FranchiseUser;
use App\Models\Franchise;
use Illuminate\Database\Eloquent\Builder;
class ParentsController extends Controller {

    public function children(Request $request, $parentId = null) {
        $students = Student::where('user_id', $parentId)->paginate(15);
        if ($request->ajax()) {
            $view = view('parents.students.table', compact('students'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $classes = AdmissionDetail::select('school_classes.*', 'school_class_id', 'students.first_name as s_first_name', 'students.last_name as s_last_name')
                ->join('school_classes', 'school_classes.id', 'admission_details.school_class_id')
                ->join('students', 'students.id', 'admission_details.student_id')
                ->join('admissions', 'admissions.id', 'admission_details.admission_id')
                ->where('admissions.parent_id', $parentId)
                ->get();
//                ->toArray();
        $parent = User::find($parentId);
        return view('parents.students.index', compact('students', 'parent', 'classes'));
    }

//    childCreate
    public function childCreate($id = null) {
        $userId = auth()->user()->id;
        $student = new Student();

        if ($id) {
            $student = Student::find($id);
        }

        $view = view('parents.students.form', compact('student'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function admissionDetails($id) {

         if(auth()->user()->franchise_id != 1 && auth()->user()->role != 3) {
            $listSchools = School::where('franchise_id',auth()->user()->franchise_id)->pluck('id')->toArray();

        $admission = Admission::withoutGlobalScope(\App\Scopes\FranchiseScope::class)->where('id', $id)->first();
        $details = AdmissionDetail::withoutGlobalScope(\App\Scopes\FranchiseScope::class)->whereIn('school_id',$listSchools)->with('admission_detail_transaction')->where('admission_id', $id)->get();
        
         } else {
            $details = AdmissionDetail::with('admission_detail_transaction')->where('admission_id', $id)->get();
        //$transaction = AdmissionDetailTrasaction::where('admission_id', $id)->get();
        $admission = Admission::where('id', $id)->first();    
         }

        

        $view = view('parents.partials.add_details', compact('details', 'admission'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function childStore(Request $request) {

        $request->merge(['first_name' => $request['record']['first_name']]);
        $request->merge(['last_name' => $request['record']['last_name']]);
        $dob = date('Y-m-d', time());
        $request->merge(['dob' => $dob]);
        if ($request->id) {
            $valdidateData = Validator::make($request->all(), [
                        'first_name' => 'required | min:3',
                        'last_name' => 'required | min:2',
                        //'dob' => 'required',
                        'image' => 'nullable',
            ]);
        } else {
            $valdidateData = Validator::make($request->all(), [
                        'first_name' => 'required | min:3',
                        'last_name' => 'required | min:2',
                        'dob' => 'required',
                        'image' => 'nullable',
            ]);
        }
        if ($valdidateData->fails()) {
            return redirect()->back()->withErrors($valdidateData)->withInput()->withFragment('#children');
        }

        try {
            DB::beginTransaction();
            $city = $request->record;
            $id = $request->id;
            if ($request->hasFile('image')) {

                $originName = $request->file('image')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileName = date('ymdhis') . '.' . $extension;

                $request->file('image')->move(public_path('images/uploads'), $fileName);
                $city['image'] = $fileName;
            }
            $city['dob'] = date('Y-m-d', time());
            $city['user_id'] = auth()->user()->id;
            $city['allow_photo_release'] = $request->allow_photo_release;
            if ($id) {
                $city['updated_by'] = auth()->user()->id;
                $city = Student::find($id)->update($city);
                $outcome = "Student Updated Successfully";
            } else {
//                dd($city);
                $city = Student::create($city);
                $outcome = "Student Created Successfully";
            }

            DB::commit();
            return redirect()->back()->with('success', $outcome);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function dashboard() {
        $user = auth()->user();
//        $userRole = $user->ModelHasRoles->first()->role_id;
        $userRole = $user->role;
        if ($userRole == 3) {
            return view('parents.dashboard', compact('user'));
        }
        if ($userRole == 5) {
            return view('school_admin.dashboard', compact('user'));
        }
    }

    public function updateProfile(Request $request) {
//        dd($request->all());

        $valdidateData = Validator::make($request->all(), [
                    'first_name' => 'required | min:3',
                    'last_name' => 'required | min:2',
                    'email' => 'required | email | unique:users,email,' . Auth::user()->id,
                    'password' => 'nullable|min:8|confirmed',
                    'password_confirmation' => 'nullable',
                    'roles' => 'required',
                    'status' => '',
                    'phone1' => 'required',
                    'phone2' => '',
                    'address1' => '',
                    'address2' => '',
                    'city' => '',
                    'state' => '',
                    'zip' => '',
                    'check_privacy' => '',
                    'check_terms' => '',
                    'check_privacy_date' => '',
                    'check_terms_date' => '',
        ]);
        if ($valdidateData->fails()) {
            return redirect()->back()->withErrors($valdidateData)->withInput()->withFragment('#profile');
        }

        $user = auth()->user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone1 = $request->phone1;
        $user->phone2 = $request->phone2;

        if ($request->password) {
            $user->password = $request->password;
        }
        $user->save();
        return redirect()->back()->with('success', 'Profile updated successfully.');
    }

    public function deleteChild($id) {
        $student = Student::find($id);
//dd($student->admission_details);
        if ($student->admission_details->count()) {
            return redirect()->back()->with('error', 'This student is applied for admissions. So it can not be deleted.');
        }
        $student->delete();
        return redirect()->back()->with('success', 'Student deleted successfully');
    }

    public function cancelstripesubscription($id) {
//        $stripeAPiKey = "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";
//        if (env('STRIPE_API_KEY')) {
//            $stripeAPiKey = env('STRIPE_API_KEY');
//        }

        $franchise_id = AdmissionDetail::where(['stripe_subscription_id' => $id])->get()->first()->franchise_id;
        $franchise = Franchise::where('id',$franchise_id)->first();
        $stripeAPiKey = $franchise->stripe_client;

        Stripe\Stripe::setApiKey($stripeAPiKey);

        try {
            $subscriptionData = \Stripe\Subscription::retrieve($id);
            $subscriptionData->cancel();
            $subs = AdmissionDetail::where(['stripe_subscription_id' => $id])->get()->first();

            // Make sure you've got the Page model
            if ($subs) {
                $subs->status = 0;
                $subs->save();
            }

            return redirect()->back()->with('success', "Subscription cancelled sucessfully");
        }catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
            //echo json_encode(['error' => $api_error]);
            return redirect()->back()->with('error', $api_error);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
            //echo json_encode(['error' => $api_error]);
            return redirect()->back()->with('error', $api_error);
                } catch (Exception $e) {
                     $api_error = $e->getMessage();
            //echo json_encode(['error' => $api_error]);
            return redirect()->back()->with('error', $api_error);
                }
    }

    /* Add Credit Card */

    public function addcreditcard(Request $request) {
        
        // Get Card Data
        
         $jsonStr = file_get_contents('php://input');
          $jsonObj = json_decode($jsonStr);
          $paymentId = $jsonObj->pmtinfo->id;
          $franchiseId = $jsonObj->franchise;
          
          
        
        if($jsonObj->request_type == "create_customer_card") {
            //*****DO All code here*****//
            
            // We check franchise exists
            $franchise = Franchise::find($franchiseId);
            if($franchise){
                $stripeAPiKey = $franchise->stripe_client;
                Stripe\Stripe::setApiKey($stripeAPiKey);
                
                // We check if customer on stripe for the franchise exists or not
                
                // Check in FranchiseUser Table
                $customer_stripe_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',$franchiseId)->first();
                if($customer_stripe_id) {
                    $customer_stripe_id = $customer_stripe_id->customer_id;
                } elseif(!$customer_stripe_id && auth()->user()->franchise_id == $franchiseId && auth()->user()->stripe_customer_id != "") {
                // Next we check if customerId for stripe exist in user and user franchise id is equal to selected franchise from request
                    $customer_stripe_id = auth()->user()->stripe_customer_id;
                    $idInUser = true;
                    
                } else {
                        // We Create Customer as it is new
                        
                         try {
                            $customer = \Stripe\Customer::create([
                                        'name' => auth()->user()->first_name . " " . auth()->user()->last_name,
                                        'email' => auth()->user()->email
                            ]); 
                        
                                if ($customer) {
                                    // Update user with customer id    
                                    if(isset($idInUser)) {
                                        $user = User::find(auth()->user()->id);
                    
                                        // Make sure you've got the Page model
                                        if ($user) {
                                            $user->stripe_customer_id = $customer->id;
                                            $user->save();
                                        }    
                                    } else{
                                    // Check if user and franchise exist in FranchiseUser table
                    
                                    $franchiseUserExist = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',$franchiseId)->first();       
                                    //Updated Franchise User table with Customer Id
                                    if($franchiseUserExist){
                                        $franchiseUserExist->customer_id = $customer->id;
                                        $franchiseUserExist->save();
                                     } else {
                                        $franchiseUserData['user_id'] = auth()->user()->id;
                                        $franchiseUserData['franchise_id'] = $franchiseId;
                                        $franchiseUserData['customer_id'] = $customer->id;
                                       
                                        $franchiseUser = FranchiseUser::create($franchiseUserData);    
                                     }
                                    }
                            } // ENd of customer if
                            $customer_stripe_id = $customer->id;
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }
                    
                } // End of Else
                
                // We Attach Card to Customer AND CHECK FOR duplication of card
                if($customer_stripe_id) {
                    // Since we got the customer id to attach the card
                            // Get paymentMehtod info from request
                            $paymentMethod = \Stripe\PaymentMethod::retrieve($paymentId);
                        
                            // We get all Cards
                            $stripe = new \Stripe\StripeClient($stripeAPiKey);
                            $paymentMethods = $stripe->customers->allPaymentMethods(
                                    $customer_stripe_id, ['type' => 'card']
                            );

                           
                            
                            
                                 
                            	$fingerprints = [];
                                
                                 if ($paymentMethods->data) {
                                    foreach ($paymentMethods->data as $method)
                                    {
                                    	$fingerprints[] = $method->card->fingerprint . '-' . $method->card->last4 . '-' .$method->card->exp_year;
                                    }
                                 }
                               
                                // We check for finger print duplicate
                                if (in_array($paymentMethod->card->fingerprint . '-' . $paymentMethod->card->last4. '-' .$paymentMethod->card->exp_year, $fingerprints))
                                	{
                                	    // Card Already exist
                                		//echo "Duplication";
                                		//exit;
                                		$output = [
                                            'payment_id' => 'DUPLICATION'
                                        ];
                                        echo json_encode($output);
                                        
                                	} else {
                                	    // Card does not exist, attach
                                	   $stripe = new \Stripe\StripeClient($stripeAPiKey);
                                	   try {
                                        $attach_pmt = $stripe->paymentMethods->attach(
                                                $paymentId, ['customer' => $customer_stripe_id]
                                        );
                                        $output = [
                                            'payment_id' => base64_encode($paymentId)
                                        ];
                                        echo json_encode($output);
                                	   } catch (Stripe\Exception\InvalidRequestException $e) {
                                            $api_error = $e->getMessage();
                                            echo json_encode(['error' => $api_error]);
                                        } catch (Stripe\Exception\ApiConnectionException $e) {
                                            $api_error = $e->getMessage();
                                            echo json_encode(['error' => $api_error]);
                                        } catch (Exception $e) {
                                            $api_error = $e->getMessage();
                                            echo json_encode(['error' => $api_error]);
                                        }
                                        
                                	}
                    
                }
                
            }
     
            
        } else {
            // return error for invalid request
        }   
        exit;
        //dd($request->choosenFranchise);
        // Get list of existing cards
        //dd($request->choosenFranchise);
            $franchise = Franchise::find($request->choosenFranchise);
            $stripeAPiKey = $franchise->stripe_client;
        if(isset($request->choosenFranchise) && $request->choosenFranchise != ""){

            $customer_stripe_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',$request->choosenFranchise)->first();
            if($customer_stripe_id) {
                $customer_stripe_id = $customer_stripe_id->customer_id;
            } 
        } else {
            $customer_stripe_id = auth()->user()->stripe_customer_id;
            $franchise = Franchise::find(1);
            $stripeAPiKey = $franchise->stripe_client;
        }
        
        if (isset($customer_stripe_id) && $customer_stripe_id != "") {
            
        } else {
            // Create Customer on stripe
//            $stripeAPiKey = "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";
//            if (env('STRIPE_API_KEY')) {
//                $stripeAPiKey = env('STRIPE_API_KEY');
//            }
            

        }


        if ($customer_stripe_id) {
//            $stripeAPiKey = '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';

           

            $stripe = new \Stripe\StripeClient($stripeAPiKey);
            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $cardlist = $list_cards['data'];
            } else {
                
            }
        }
        $expiry = date("d/m/Y", strtotime("01/" . str_replace(" ", "", $request->expiry)));

        $cc_month = date("m", strtotime($expiry));
        $cc_year = date("Y", strtotime($expiry));
        if (isset($cardlist)) {
            $card_type = array();
            $card_month = array();
            $card_year = array();
            $card_last = array();
            $cc_last = substr($request->number, -4);
            foreach ($cardlist as $key => $card_data) {
                $card_type[] = strtolower($card_data['card']['brand']);
                $card_month[] = $card_data['card']['exp_month'];
                $card_year[] = $card_data['card']['exp_year'];
                $card_last[] = $card_data['card']['last4'];
            }



            if (in_array($cc_month, $card_month) && in_array($cc_year, $card_year) && in_array($cc_last, $card_last)) {
                // Card Already Exist redirect back
                return redirect()->back()->withErrors(['error' => 'Card already exist']);
            } else {
                // Add card to stripe
//                $stripeAPiKey = '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';


                $stripe = new \Stripe\StripeClient($stripeAPiKey);

                try {
                    $pmt_method = $stripe->paymentMethods->create([
                        'type' => 'card',
                        'card' => [
                            'number' => $request->number,
                            'exp_month' => $cc_month,
                            'exp_year' => $cc_year,
                            'cvc' => $request->cvc,
                        ],
                        'billing_details' => ['name' => $request->name],
                    ]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiErrorException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\CardException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                }
                try {
                    $intent = $stripe->setupIntents->create([
                        'payment_method_types' => ['card'],
                        'customer' => $customer_stripe_id,
                        'payment_method' => $pmt_method->id,
                            /* 'setup_future_usage' => 'off_session' */
                    ]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiErrorException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\CardException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                }
                try {
                    $attach_pmt = $stripe->paymentMethods->attach(
                            $pmt_method->id, ['customer' => $customer_stripe_id]
                    );
                    return redirect()->back()->with('success', 'Card added successfully');
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiErrorException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\CardException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                    return redirect()->back()->withErrors($api_error)->withInput();
                }
                //dd($attach_pmt);
            }
            //dd($request);
            //dd($card_type);
            //dd($cardlist);
        } else {
            // Add new Card
//              $stripeAPiKey = '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';

            

            $stripe = new \Stripe\StripeClient($stripeAPiKey);


            try {
                $pmt_method = $stripe->paymentMethods->create([
                    'type' => 'card',
                    'card' => [
                        'number' => $request->number,
                        'exp_month' => $cc_month,
                        'exp_year' => $cc_year,
                        'cvc' => $request->cvc,
                    ],
                    'billing_details' => ['name' => $request->name],
                ]);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiErrorException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\CardException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            }
            try {
                $intent = $stripe->setupIntents->create([
                    'payment_method_types' => ['card'],
                    'customer' => $customer_stripe_id,
                    'payment_method' => $pmt_method->id,
                        /* 'setup_future_usage' => 'off_session' */
                ]);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiErrorException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\CardException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            }
            try {
                $attach_pmt = $stripe->paymentMethods->attach(
                        $pmt_method->id, ['customer' => $customer_stripe_id]
                );
                return redirect()->back()->with('success', 'Card added successfully');
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiErrorException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\CardException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
                return redirect()->back()->withErrors($api_error)->withInput();
            }
        }
        //dd($request);
    }

    /* End Add Credit Card */
    /* List credit card Parent Dashboard */

    public function listcreditcard() {
        $cardlist = "";
        $megaCardList = [];
        // Get Franchises from db
        
        if(Session::get('selectedFranchise') && Session::get('selectedFranchise') != 1) {
        
        $franchises_users = FranchiseUser::where('user_id',auth()->user()->id)->where('customer_id','<>','')->get();

        if($franchises_users) {
            // Get Card List
            foreach($franchises_users as $c => $data) {
                $customer_stripe_id = $data->customer_id; 
                $franchise = Franchise::find($data->franchise_id);
                $stripeAPiKey = $franchise->stripe_client;
                $stripe = new \Stripe\StripeClient($stripeAPiKey);


            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $megaCardList[$c]['franchise'] = $franchise->name;
                $megaCardList[$c]['carddata'] = $list_cards['data'];
            } else {
                //$cardlist = "";
            }   
            }    
        }

       else if(Session::get('selectedFranchise')){

            $customer_stripe_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',Session::get('selectedFranchise'))->first();
            if($customer_stripe_id) {
                $customer_stripe_id = $customer_stripe_id->customer_id;
            }
        

        if (isset($customer_stripe_id) && $customer_stripe_id != "") {

//              $stripeAPiKey = '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';

            $franchise = Franchise::find(Session::get('selectedFranchise'));
            $stripeAPiKey = $franchise->stripe_client;

            $stripe = new \Stripe\StripeClient($stripeAPiKey);



            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $cardlist = $list_cards['data'];
            } else {
                $cardlist = "";
            }
        }
    }
    } else if(auth()->user()->stripe_customer_id != "") {
        $customer_stripe_id = auth()->user()->stripe_customer_id; 
        $franchises_users = "";
        if($customer_stripe_id != ""){
        $franchise = Franchise::find(1);
            $stripeAPiKey = $franchise->stripe_client;

            $stripe = new \Stripe\StripeClient($stripeAPiKey);



            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $cardlist = $list_cards['data'];
            } else {
                $cardlist = "";
            }
        }else {
             $cardlist = "";
        }
    } else {
        $customer_stripe_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',1)->first();
        if($customer_stripe_id) {
            $customer_stripe_id = $customer_stripe_id->customer_id;
        }
        $franchises_users = "";
        if($customer_stripe_id != ""){
        $franchise = Franchise::find(1);
            $stripeAPiKey = $franchise->stripe_client;

            $stripe = new \Stripe\StripeClient($stripeAPiKey);



            $list_cards = $stripe->customers->allPaymentMethods(
                    $customer_stripe_id, ['type' => 'card']
            );

            if (!empty($list_cards['data'])) {
                $cardlist = $list_cards['data'];
            } else {
                $cardlist = "";
            }
        }else {
             $cardlist = "";
        }
    }
        return view('parents.creditcard', compact('cardlist','franchises_users','megaCardList'));
    }

    /* End List credit card Parent Dashboard */

    /* Payment By Existing Card */

    public function stripepayPMT(Request $request) {

        $pmt_id = $request->pmt_id;
        if (!$pmt_id) {
            echo json_encode(['error' => 'Please choose credit card']);
            exit;
        }
        if (isset($request->cart_index)) {
            $cartIndex = $request->cart_index;
            Session::put('cart_index', $request->cart_index);
        }
        // else{
        //     $cartIndex = Session::get('cart_index');
        // }
        $cartIndex = Session::get('cart_index');

        //header('Content-Type: application/json');

        /* $jsonStr = file_get_contents('php://input');
          $jsonObj = json_decode($jsonStr);
          dd($jsonObj); */
//        $stripeAPiKey = "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";
//        if (env('STRIPE_API_KEY')) {
//            $stripeAPiKey = env('STRIPE_API_KEY');
//        }
//          $stripeAPiKey = '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';


        if(Session::get('selectedFranchise')){

            $customer_stripe_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',Session::get('selectedFranchise'))->first();
            if($customer_stripe_id) {
                $customer_stripe_id = $customer_stripe_id->customer_id;
            }  
        }
        $franchise = Franchise::find(Session::get('selectedFranchise'));
        $stripeAPiKey = $franchise->stripe_client;


        Stripe\Stripe::setApiKey($stripeAPiKey);
        $carts = Session::get('cart_info');

        $checkPromos = Session::get('promos');

        $cval = $carts[$cartIndex];


        $school_name = School::where(['id' => $cval['school_id']])->get()->first()['name'];
        $class_name = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['title'];

        //foreach($carts as $key => $cval) {

        $name = !empty($jsonObj->name) ? $jsonObj->name : '';

        $email = !empty($jsonObj->email) ? $jsonObj->email : '';
        $amount = !empty($jsonObj->amount) ? $jsonObj->amount : '';
        if (!isset($cval['duration_type']) || $cval['duration_type'] == "") {
            $cval['duration_type'] = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['duration_type'];
        }
        //$planInterval = "month";
        if (strtolower($cval['duration_type']) == "weekly") {
            $planInterval = "week";
        } elseif (strtolower($cval['duration_type']) == "monthly") {
            $planInterval = "month";
        }
        // Convert price to cents 

        $planName = $class_name . " - " . $school_name;



        //$cart = $carts[0];
        // Add customer to stripe 
        try {
            // Check if customer id exist
            if(Session::get('selectedFranchise') && Session::get('selectedFranchise') != 1){
                $customer_id = $customer_stripe_id;
                
            } else {
                $customer_id = User::where(['id' => auth()->user()->id])->get()->first()['stripe_customer_id'];
                
                if(!$customer_id) {
                     $customer_id = FranchiseUser::where('user_id',auth()->user()->id)->where('franchise_id',Session::get('selectedFranchise'))->first()['customer_id'];
                
                    
                }
            }
            
            if ($customer_id) {
               
                // retrieve customer
                $customer = \Stripe\Customer::retrieve($customer_id);
            }

            // else
            /* else {
              $customer = \Stripe\Customer::create([
              'name' => $name,
              'email' => $email
              ]);
              if ($customer) {
              // Update user with customer id
              $user = User::find(auth()->user()->id);

              // Make sure you've got the Page model
              if ($user) {
              $user->stripe_customer_id = $customer->id;
              $user->save();
              }
              }
              } */
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $api_error = $e->getMessage();
            echo json_encode(['error' => $api_error]);
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $api_error = $e->getMessage();
            echo json_encode(['error' => $api_error]);
        } catch (Exception $e) {
            $api_error = $e->getMessage();
            echo json_encode(['error' => $api_error]);
        }


        $class = \App\Models\SchoolClass::find($cval['school_class_id']);
        $endDate = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));



        /* if ($checkPromos) {
          $planPriceCents = subPromoAmount($checkPromos, $cval['class_charges']);
          $planPriceCents = round(($cval['class_charges'] - $planPriceCents) * 100);
          } else { */
        $planPriceCents = round($cval['class_charges'] * 100);
        //}


        $cancel_at = strtotime($endDate);
        if (empty($api_error) && $customer) {
            try {
                // Check if product id exist

                $price_id = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['stripe_product_id'];
                if ($price_id) {
                    // retrieve Price
                    $price = \Stripe\Price::retrieve($price_id);
                } else {
                    // Create price with subscription info and interval 
                    $price = \Stripe\Price::create([
                                'unit_amount' => $planPriceCents,
                                'currency' => 'USD',
                                'recurring' => ['interval' => $planInterval],
                                'product_data' => ['name' => $planName],
                    ]);
                    if ($price) {
                        // Update user with customer id    
                        $schoolclassprice = SchoolClass::find($cval['school_class_id']);

                        // Make sure you've got the Page model
                        if ($schoolclassprice) {
                            $schoolclassprice->stripe_product_id = $price->id;
                            $schoolclassprice->save();
                        }
                    }
                }
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            }

            if (empty($api_error) && $price) {
                // Create a new subscription 
                try {


                    if (isset($checkPromos['promo_code']) && $checkPromos['promo_code'] != "") {
                        $promoset = ['coupon' => $checkPromos['promo_code']];
                    } else {
                        $promoset = [];
                    }
                    $subscription = \Stripe\Subscription::create([
                                'customer' => $customer->id,
                                'items' => [[
                                'price' => $price->id,
                                    ]],
                                'proration_behavior' => 'none',
                                'default_payment_method' => $pmt_id,
                                'payment_behavior' => 'default_incomplete',
                                'cancel_at' => $cancel_at,
                                'expand' => ['latest_invoice.payment_intent'],
                                $promoset,
                                    //'coupon' => $checkPromos['promo_code'],
                    ]);
                } catch (Stripe\Exception\ApiErrorException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\CardException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }

                if (empty($api_error) && $subscription) {
                    /* $output = [
                      'subscriptionId' => $subscription->id,
                      'clientSecret' => $subscription->latest_invoice->payment_intent->client_secret,
                      'customerId' => $customer->id
                      ]; */
                    //dd($subscription);
//                    $stripeAPiKey = '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';

                    $franchise = Franchise::find(Session::get('selectedFranchise'));
                    $stripeAPiKey = $franchise->stripe_client;

                    $stripe = new \Stripe\StripeClient($stripeAPiKey);
//                    $stripe = new \Stripe\StripeClient('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');

                    try {

                        $result = $stripe->invoices->pay(
                                $subscription->latest_invoice->id, []
                        );
                        if (strtolower($result->status) == 'paid') {
                            $payment_intent_id = $result->payment_intent;
                            $paidCurrency = $result->currency;
                            $customer_name = $result->customer_name;
                            $customer_email = $result->customer_email;
                            $paidAmount = $result->amount_paid;
                            $paidAmount = ($paidAmount / 100);

                            $paidCurrency = $result->currency;
                            $payment_status = $result->status;


                            $created = date("Y-m-d H:i:s", $result->created);
                            $current_period_start = $cancel_at = $current_period_end = '';
                            if (!empty($subscription)) {
                                $created = date("Y-m-d H:i:s", $subscription->created);
                                $current_period_start = date("Y-m-d H:i:s", $subscription->current_period_start);
                                $current_period_end = date("Y-m-d H:i:s", $subscription->current_period_end);
                                $cancel_at = date("Y-m-d H:i:s", $subscription->cancel_at);
                            }
                            // Here rest logic
                            if (strtolower($cval['duration_type']) == "weekly") {
                                $planInterval = "week";
                            } elseif (strtolower($cval['duration_type']) == "monthly") {
                                $planInterval = "month";
                            }
                            $subscription_id = $subscription->id;
                            $customer_id = "";
                            if (Session::get('admission_id')) {
                                $addId = Session::get('admission_id');
                            } else {
                                $addmission = \App\Models\Admission::create([
                                            'parent_id' => auth()->user()->id,
                                ]);
                                $addId = $addmission->id;
                            }


                            $addDetails = [
                                "class_charges" => $cval['class_charges'],
                                "student_id" => $cval['student_id'],
                                "school_class_id" => $cval['school_class_id'],
                                "school_id" => $cval['school_id'],
                                "admission_id" => $addId,
                                "classroom_name" => $cval['classroom_name'],
                            ];

                            if (Session::get('promos')) {
                                $addDetails['discount'] = $cval['discount'];
                                $addDetails['promo_code_id'] = $cval['promo_id'];
                                $addDetails['percent_off'] = $cval['percent_off'];
                                $addDetails['flat_amount'] = $cval['flat_amount'];
                            }



                            $addDetails["stripe_subscription_id"] = $subscription_id;
                            $addDetails["stripe_customer_id"] = $customer_id;
                            $addDetails["stripe_payment_intent_id"] = $payment_intent_id;

                            $addDetails["paid_amount"] = $paidAmount;
                            $addDetails['paid_amount_currency'] = $paidCurrency;
                            $addDetails['plan_interval'] = $planInterval;
                            $addDetails['customer_name'] = $customer_name;
                            $addDetails['customer_email'] = $customer_email;
                            $addDetails['plan_period_start'] = $current_period_start;
                            $addDetails['plan_period_end'] = $current_period_end;
                            $addDetails['plan_cancel_at'] = $cancel_at;
                            $addDetails['admission_id'] = $addId;
                            $addDetails['status'] = 1;
                            $addDetails['payment_method_id'] = $pmt_id;
                            $addDetails['franchise_id'] = Session::get('selectedFranchise');



                            try {
                                $add = AdmissionDetail::create($addDetails);
                            } catch (\Exception $e) {
                                // do task when error
                                echo $e->getMessage();   // insert query
                            }


                            $output = [
                                'payment_id' => base64_encode($addId),
                                'cart_index' => $cartIndex
                            ];

                            if (count(Session::get('cart_info'))) {
                                Session::put('admission_id', $addId);
                            } else {
                                Session::put('admission_id', null);
                            }

                            /*
                             * UNSET INDEX FROM ARRAY
                             */

                            Session::put('cart_index', null);
                            unset($carts[$cartIndex]);

                            Session::put('cart_info', $carts);

                            /*
                             * UNSET PROMO CODES
                             */
                            if (count($carts) == 0) {
                                Session::put('promos', null);
                            }

                            // Send Email to teacher
                            $getTeacherId = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['user_id'];
                            $teacherData = User::where(['id' => $getTeacherId])->where(['status' => 1])->get()->first();
                            $student_name = $cval['student_name'];

                            $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            <strong>' . $student_name . '</strong> has been added to <strong>' . $class_name . '</strong> at <strong>' . $school_name . '</strong>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                            if (isset($teacherData) && !empty($teacherData)) {
                                Mail::send(array(), array(), function ($message) use ($html, $teacherData, $school_name, $student_name) {
                                    $message->to($teacherData['email'])
                                            ->subject("A new student " . $student_name . " has registered at " . $school_name)
                                            ->setBody($html, 'text/html');
                                });
                            }
                            // Send Email to RD

                            $getRDId = School::where(['id' => $cval['school_id']])->get()->first()['user_id'];
                            $RDData = User::where(['id' => $getRDId])->where(['status' => 1])->get()->first();
                            if (isset($RDData) && !empty($RDData)) {
                                Mail::send(array(), array(), function ($message) use ($html, $RDData, $school_name, $student_name) {
                                    $message->to($RDData['email'])
                                            ->subject("A new student " . $student_name . " has registered at " . $school_name)
                                            ->setBody($html, 'text/html');
                                });
                            }
                            // Send Email to Parent with order details
                            $account_url = url('/') . "/login";
                            $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Welcome!</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . auth()->user()->first_name . ', <br /><br /><span style="display:block;text-align:left;">
' . $cval["student_name"] . ' is registered to join Techie Kids Club classes at ' . $cval["school_name"] . '. <br />
Get ready to hear all about coding + robotics over dinner!<br />
Here\'s what you can expect:<br />
<ul style="list-style:square;text-align:left;">
 <li> Your child will join the class on the next regularly scheduled class day.</li>
<li> The enrichment coach and your child\'s classroom teacher will remind ' . $cval["student_name"] . ' that it\'s class day and get them to the right classroom.</li>
<li> Each week, you\'ll get an update on what\'s happening in class. Log into your <a href="' . $account_url . '" target="_blank">account</a> to see it. </li>
</ul></span>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                            $student_name = $cval['student_name'];
                            $user = User::where(['id' => Auth::user()->id])->first();
                            Mail::send(array(), array(), function ($message) use ($html, $user, $student_name) {
                                $message->to(Auth::user()->email)
                                        ->subject("$student_name is now enrolled in " . config('app.name') . " Team")
                                        ->setBody($html, 'text/html');
                            });

                            echo json_encode($output);
                        } else {
                            // trasaction failed
                            echo json_encode(['error' => 'Transaction has been failed!']);
                        }


                        //echo json_encode(['payment_id'=>$subscription->latest_invoice->id]);
                    } catch (Stripe\Exception\InvalidRequestException $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    } catch (Stripe\Exception\ApiConnectionException $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    }
                } else {

                    echo json_encode(['error' => $api_error]);
                }
            } else {

                echo json_encode(['error' => $api_error]);
            }
        } else {

            echo json_encode(['error' => $api_error]);
        }
    }

    /* End Of Payment By Existing Card */

    public function stripepay(Request $request) {
        if (isset($request->cart_index)) {
            $cartIndex = $request->cart_index;
            Session::put('cart_index', $request->cart_index);
        }
        // else{
        //     $cartIndex = Session::get('cart_index');
        // }
        $cartIndex = Session::get('cart_index');

        //header('Content-Type: application/json');
        $jsonStr = file_get_contents('php://input');
        $jsonObj = json_decode($jsonStr);

//        $stripeAPiKey = "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";
//        if (env('STRIPE_API_KEY')) {
//            $stripeAPiKey = env('STRIPE_API_KEY');
//        }

        $franchise = getFranchise();
        $stripeAPiKey = $franchise->stripe_client;





        Stripe\Stripe::setApiKey($stripeAPiKey);
        $carts = Session::get('cart_info');

        $checkPromos = Session::get('promos');

        $cval = $carts[$cartIndex];


        $school_name = School::where(['id' => $cval['school_id']])->get()->first()['name'];
        $class_name = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['title'];

        //foreach($carts as $key => $cval) {
        if ($jsonObj->request_type == 'create_customer_subscription') {
            $name = !empty($jsonObj->name) ? $jsonObj->name : '';

            $email = !empty($jsonObj->email) ? $jsonObj->email : '';
            $amount = !empty($jsonObj->amount) ? $jsonObj->amount : '';

            //$planInterval = "month";
            if (strtolower($cval['duration_type']) == "weekly") {
                $planInterval = "week";
            } elseif (strtolower($cval['duration_type']) == "monthly") {
                $planInterval = "month";
            }
            // Convert price to cents 

            $planName = $class_name . " - " . $school_name;



            //$cart = $carts[0];
            // Add customer to stripe 
            try {
                // Check if customer id exist
                $customer_id = User::where(['id' => auth()->user()->id])->get()->first()['stripe_customer_id'];
                if ($customer_id) {
                    // retrieve customer
                    $customer = \Stripe\Customer::retrieve($customer_id);
                }

                // else
                else {
                    $customer = \Stripe\Customer::create([
                                'name' => $name,
                                'email' => $email
                    ]);
                    if ($customer) {
                        // Update user with customer id    
                        $user = User::find(auth()->user()->id);

                        // Make sure you've got the Page model
                        if ($user) {
                            $user->stripe_customer_id = $customer->id;
                            $user->save();
                        }
                    }
                }
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                //echo json_encode(['error' => $api_error]);
            }


            $class = \App\Models\SchoolClass::find($cval['school_class_id']);
            $endDate = date('Y-m-d H:i:s', strtotime("2099-12-30 00:00:00"));



            /* if ($checkPromos) {
              $planPriceCents = subPromoAmount($checkPromos, $cval['class_charges']);
              $planPriceCents = round(($cval['class_charges'] - $planPriceCents) * 100);
              } else { */
            $planPriceCents = round($cval['class_charges'] * 100);
            //}


            $cancel_at = strtotime($endDate);
            if (empty($api_error) && $customer) {
                try {
                    // Check if product id exist

                    $price_id = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['stripe_product_id'];
                    if ($price_id) {
                        // retrieve Price
                        $price = \Stripe\Price::retrieve($price_id);
                    } else {
                        // Create price with subscription info and interval 
                        $price = \Stripe\Price::create([
                                    'unit_amount' => $planPriceCents,
                                    'currency' => 'USD',
                                    'recurring' => ['interval' => $planInterval],
                                    'product_data' => ['name' => $planName],
                        ]);
                        if ($price) {
                            // Update user with customer id    
                            $schoolclassprice = SchoolClass::find($cval['school_class_id']);

                            // Make sure you've got the Page model
                            if ($schoolclassprice) {
                                $schoolclassprice->stripe_product_id = $price->id;
                                $schoolclassprice->save();
                            }
                        }
                    }
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                }

                if (empty($api_error) && $price) {
                    // Create a new subscription 
                    try {


                        if (isset($checkPromos['promo_code']) && $checkPromos['promo_code'] != "") {
                            $promoset = ['coupon' => $checkPromos['promo_code']];
                        } else {
                            $promoset = [];
                        }
                        $subscription = \Stripe\Subscription::create([
                                    'customer' => $customer->id,
                                    'items' => [[
                                    'price' => $price->id,
                                        ]],
                                    'proration_behavior' => 'none',
                                    //'default_payment_method' => $pmtid,
                                    'payment_behavior' => 'default_incomplete',
                                    'cancel_at' => $cancel_at,
                                    'expand' => ['latest_invoice.payment_intent'],
                                    $promoset,
                                        //'coupon' => $checkPromos['promo_code'],
                        ]);
                    } catch (Stripe\Exception\InvalidRequestException $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    } catch (Stripe\Exception\ApiConnectionException $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        echo json_encode(['error' => $api_error]);
                    }

                    if (empty($api_error) && $subscription) {
                        $output = [
                            'subscriptionId' => $subscription->id,
                            'clientSecret' => $subscription->latest_invoice->payment_intent->client_secret,
                            'customerId' => $customer->id
                        ];

                        echo json_encode($output);
                    } else {

                        echo json_encode(['error' => $api_error]);
                    }
                } else {

                    echo json_encode(['error' => $api_error]);
                }
            } else {

                echo json_encode(['error' => $api_error]);
            }
        } elseif ($jsonObj->request_type == 'payment_insert') {


            $payment_intent = !empty($jsonObj->payment_intent) ? $jsonObj->payment_intent : '';
            $subscription_id = !empty($jsonObj->subscription_id) ? $jsonObj->subscription_id : '';
            /* $pmt_id = !empty($jsonObj->default_payment_method) ? $jsonObj->default_payment_method : ''; */
            $customer_id = !empty($jsonObj->customer_id) ? $jsonObj->customer_id : '';
            //$planInterval = "month";
            if (strtolower($cval['duration_type']) == "weekly") {
                $planInterval = "week";
            } elseif (strtolower($cval['duration_type']) == "monthly") {
                $planInterval = "month";
            }
            // Retrieve customer info 
            try {
                $customer = \Stripe\Customer::retrieve($customer_id);
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                echo json_encode(['error' => $api_error]);
            }

            // Check whether the charge was successful 
            if (!empty($payment_intent) && $payment_intent->status == 'succeeded') {


                // Retrieve subscription info 
                try {
                    $subscriptionData = \Stripe\Subscription::retrieve($subscription_id);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    //echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }

                $payment_intent_id = $payment_intent->id;
                $paidAmount = $payment_intent->amount;
                $paidAmount = ($paidAmount / 100);
                $paidCurrency = $payment_intent->currency;
                $payment_status = $payment_intent->status;


                $created = date("Y-m-d H:i:s", $payment_intent->created);
                $current_period_start = $cancel_at = $current_period_end = '';
                if (!empty($subscriptionData)) {
                    $created = date("Y-m-d H:i:s", $subscriptionData->created);
                    $current_period_start = date("Y-m-d H:i:s", $subscriptionData->current_period_start);
                    $current_period_end = date("Y-m-d H:i:s", $subscriptionData->current_period_end);
                    $cancel_at = date("Y-m-d H:i:s", $subscriptionData->cancel_at);
                }
                $pmt_id = 0;
                try {
                    $pmtmethod = \Stripe\Customer::allPaymentMethods(
                                    $customer_id, ['type' => 'card']
                    );
                    //dd($pmtmethod['data']);
                    /* $lastCardElement =  array_key_first($pmtmethod['data']);       

                      $pmtid = $pmtmethod['data'][$lastCardElement]['id']; */

                    if (count($pmtmethod['data']) > 0) {
                        $lastCardElement = array_key_first($pmtmethod['data']);

                        $pmt_id = $pmtmethod['data'][$lastCardElement]['id'];
                    } else {
                        $pmt_id = isset($pmtmethod['data']['id']) ? $pmtmethod['data']['id'] : $pmtmethod['id'];
                    }
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }
                try {
                    $stripeUpdate = \Stripe\Subscription::update(
                                    $subscription_id, ['default_payment_method' => $pmt_id]
                    );
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo json_encode(['error' => $api_error]);
                }
                //$pmt_id = $subscriptionData->default_payment_method;
                $customer_name = $customer_email = '';
                if (!empty($customer)) {
                    $customer_name = !empty($customer->name) ? $customer->name : '';
                    $customer_email = !empty($customer->email) ? $customer->email : '';

                    if (!empty($customer_name)) {
                        $name_arr = explode(' ', $customer_name);
                        $first_name = !empty($name_arr[0]) ? $name_arr[0] : '';
                        $last_name = !empty($name_arr[1]) ? $name_arr[1] : '';
                    }
                }

                // Check if any transaction data exists already with the same TXN ID 


                $payment_id = 0;
                if (!empty($prevPaymentID)) {
                    $payment_id = $prevPaymentID;
                } else {
                    
                }


//                $data = Session::get('cart_info');
                if (Session::get('admission_id')) {
                    $addId = Session::get('admission_id');
                } else {
                    $addmission = \App\Models\Admission::create([
                                'parent_id' => auth()->user()->id,
                    ]);
                    $addId = $addmission->id;
                }


                $addDetails = [
                    "class_charges" => $cval['class_charges'],
                    "student_id" => $cval['student_id'],
                    "school_class_id" => $cval['school_class_id'],
                    "school_id" => $cval['school_id'],
                    "admission_id" => $addId,
                    "classroom_name" => $cval['classroom_name'],
                ];

                if (Session::get('promos')) {
                    $addDetails['discount'] = $cval['discount'];
                    $addDetails['promo_code_id'] = $cval['promo_id'];
                    $addDetails['percent_off'] = $cval['percent_off'];
                    $addDetails['flat_amount'] = $cval['flat_amount'];
                }



                $addDetails["stripe_subscription_id"] = $subscription_id;
                $addDetails["stripe_customer_id"] = $customer_id;
                $addDetails["stripe_payment_intent_id"] = $payment_intent_id;
                $addDetails["paid_amount"] = $cval['class_charges'];
                $addDetails['paid_amount_currency'] = $paidCurrency;
                $addDetails['plan_interval'] = $planInterval;
                $addDetails['customer_name'] = $customer_name;
                $addDetails['customer_email'] = $customer_email;
                $addDetails['plan_period_start'] = $current_period_start;
                $addDetails['plan_period_end'] = $current_period_end;
                $addDetails['plan_cancel_at'] = $cancel_at;
                $addDetails['admission_id'] = $addId;
                $addDetails['status'] = 1;
                $addDetails['payment_method_id'] = $pmt_id;



                try {
                    $add = AdmissionDetail::create($addDetails);
                } catch (\Exception $e) {
                    // do task when error
                    echo $e->getMessage();   // insert query
                }


                $output = [
                    'payment_id' => base64_encode($addId),
                    'cart_index' => $cartIndex
                ];

                if (count(Session::get('cart_info'))) {
                    Session::put('admission_id', $addId);
                } else {
                    Session::put('admission_id', null);
                }

                /*
                 * UNSET INDEX FROM ARRAY
                 */

                Session::put('cart_index', null);
                unset($carts[$cartIndex]);

                Session::put('cart_info', $carts);

                /*
                 * UNSET PROMO CODES
                 */
                if (count($carts) == 0) {
                    Session::put('promos', null);
                }

                // Send Email to teacher
                $getTeacherId = SchoolClass::where(['id' => $cval['school_class_id']])->get()->first()['user_id'];
                $teacherData = User::where(['id' => $getTeacherId])->where(['status' => 1])->get()->first();
                $student_name = $cval['student_name'];

                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            <strong>' . $student_name . '</strong> has been added to <strong>' . $class_name . '</strong> at <strong>' . $school_name . '</strong>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                if (isset($teacherData) && !empty($teacherData)) {
                    Mail::send(array(), array(), function ($message) use ($html, $teacherData, $school_name, $student_name) {
                        $message->to($teacherData['email'])
                                ->subject("A new student " . $student_name . " has registered at " . $school_name)
                                ->setBody($html, 'text/html');
                    });
                }
                // Send Email to RD

                $getRDId = School::where(['id' => $cval['school_id']])->get()->first()['user_id'];
                $RDData = User::where(['id' => $getRDId])->where(['status' => 1])->get()->first();
                if (isset($RDData) && !empty($RDData)) {
                    Mail::send(array(), array(), function ($message) use ($html, $RDData, $school_name, $student_name) {
                        $message->to($RDData['email'])
                                ->subject("A new student " . $student_name . " has registered at " . $school_name)
                                ->setBody($html, 'text/html');
                    });
                }
                // Send Email to Parent with order details
                $account_url = url('/') . "/login";
                $html = '<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: Open Sans, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                          <a href="' . url('/') . '" title="logo" target="_blank">
                            <img width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:Rubik,sans-serif;">Welcome!</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            Hi ' . auth()->user()->first_name . ', <br /><br /><span style="display:block;text-align:left;">
' . $cval["student_name"] . ' is registered to join Techie Kids Club classes at ' . $cval["school_name"] . '. <br />
Get ready to hear all about coding + robotics over dinner!<br />
Here\'s what you can expect:<br />
<ul style="list-style:square;text-align:left;">
 <li> Your child will join the class on the next regularly scheduled class day.</li>
<li> The enrichment coach and your child\'s classroom teacher will remind ' . $cval["student_name"] . ' that it\'s class day and get them to the right classroom.</li>
<li> Each week, you\'ll get an update on what\'s happening in class. Log into your <a href="' . $account_url . '" target="_blank">account</a> to see it. </li>
</ul></span>
                                        </p>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>www.' . str_replace(" ", "", strtolower(config('app.name'))) . '.com</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
                $student_name = $cval['student_name'];
                $user = User::where(['id' => Auth::user()->id])->first();
                Mail::send(array(), array(), function ($message) use ($html, $user, $student_name) {
                    $message->to(Auth::user()->email)
                            ->subject("$student_name is now enrolled in " . config('app.name') . " Team")
                            ->setBody($html, 'text/html');
                });

                echo json_encode($output);
            } else {
                echo json_encode(['error' => 'Transaction has been failed!']);
            }
        }
        // }// end of loop cart
    }

    public function demandLibrary() {
        $banners = \App\Models\DemandLiberary::query();
        if (request()->ajax()) {
//            dd('');
            if (request()->type) {
                $search = request()->type;

                $banners = $banners->where(function($q) use($search) {
                    return $q->where('title', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%')
                                    ->orWhere('tags', 'like', '%' . $search . '%');
                });
            }
            $banners = $banners->paginate(1000000);
            $view = view('parents.partials.dl_card', compact('banners'))
                    ->render();
            return $view;
        }
        $banners = $banners->paginate(1000000);
        return view('parents.demand_liberary', compact('banners'));
    }

    public function notifications() {
        return view('parents.notifications');
    }

    public function notificationList() {
        $parent = auth()->user();
        $parentClasses = AdmissionDetail::select('school_class_id')->join('admissions', 'admissions.id', 'admission_details.admission_id')
                ->where('admissions.parent_id', $parent->id)
                ->groupBy('school_class_id')
                ->pluck('school_class_id')
                ->toArray();


        $notifications = \App\Models\NotificationViewer::whereHas('notification', function($q) {
                    return $q->whereDate('activation_date', '<=', date('Y-m-d'));
                })
                ->whereIn('school_class_id', $parentClasses)
                ->get();
//        dd($notifications);
        return view('parents.notification_list', compact('notifications'));
    }

    public function notificationDetail($id) {
        $notification = \App\Models\Notification::find($id);
//        dd($notification);

        return view('parents.notification_details', compact('notification'))->render();
    }

    public function applyPromo($code) {

        $date = date('Y-m-d');
        $promocode = \App\Models\PromoCode::where('promo_code', $code)
                ->whereDate('from_date', '<=', $date)
                ->whereDate('to_date', '>=', $date)
                ->first();
//        Session::put('promos', null);
//        return ['html' => view('frontend.partials.calc_order')
//                    ->render(), 'status' => 1];

        if (Session::get('promos')) {
            return ['status' => 0, 'message' => 'Promo Applied', 'promocode' => []];
        }

        if ($promocode) {
//            $st = subPromoAmount($promocode, request()->st);
            $cartItems = Session::get('cart_info');
            foreach ($cartItems as $k => $ci):
                $discounted = subPromoAmount($promocode, $ci['class_charges']);
                $cartItems[$k]['discount'] = $discounted;
                $cartItems[$k]['promo_id'] = $promocode->id;
                $cartItems[$k]['percent_off'] = $promocode->percent_off;
                $cartItems[$k]['flat_amount'] = $promocode->flat_amount;
            endforeach;
            Session::put('cart_info', $cartItems);
            Session::put('promos', $promocode);

            return ['html' => view('frontend.partials.calc_order')
                        ->render(), 'status' => 1];

            return ['status' => 1, 'promocode' => $promocode];
        }
        return ['status' => 0, 'message' => 'Invalid Promo Code', 'promocode' => []];
    }

    public function saveSubscription(Request $request) {

        // The library needs to be configured with your account's secret key.
// Ensure the key is kept out of any version control system you might be using.
//            $stripeAPiKey = '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';
       
       //get franchise id from admision detail
       
       $payload = file_get_contents('php://input');
       
       $subs_id = json_decode( $payload, FALSE )->data->object->subscription;
       
       $franchise_id = AdmissionDetail::where('stripe_subscription_id',$subs_id)->first();
       if($franchise_id) {
           $franchise_id = $franchise_id->franchise_id;
       } else {
           
           return false;
       }
       
       $stripe_end_point_secret = Franchise::find($franchise_id)->stripe_end_point_secret;
       $stripeAPiKey  = Franchise::find($franchise_id)->stripe_client;
       
       //Storage::disk('local')->put('file.txt',  $stripeAPiKey);
       
       //exit;
      
        /*$franchise = getFranchise();
        $stripeAPiKey = $franchise->stripe_client;*/
    //    $stripe = new \Stripe\StripeClient($stripeAPiKey);

// This is your Stripe CLI webhook secret for testing your endpoint locally.
//        $endpoint_secret = 'whsec_X1o9FmVGJ2yZRxcIfVfj5xHAoNWMXJaU';
       
       
        $endpoint_secret = $stripe_end_point_secret;

        
        
        //Storage::disk('local')->put('file.txt',  json_decode( $payload, FALSE )->data->object->subscription);
        
      
//exit;
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];


        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            echo "Invalid Payload";
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            echo "Invalid Signature";
            http_response_code(400);
            exit();
        }

// Handle the event
        switch ($event->type) {
            case 'invoice.paid':
                $invoice = $event->data->object;
            case 'invoice.payment_failed':
                $invoice = $event->data->object;
            case 'invoice.payment_succeeded':
                $invoice = $event->data->object;
            // ... handle other event types
            default:
                echo 'Received unknown event type ' . $event->type;
        }

        http_response_code(200);

        $data = [];
        $data['trasaction_id'] = $invoice->id;
        $data['amount_due'] = $invoice->amount_due / 100;
        $data['amount_paid'] = $invoice->amount_paid / 100;
        $data['charge_id'] = $invoice->charge;
        $data['customer'] = $invoice->customer;
        /* $data['customer_email']  = $invoice->customer_email;
          $data['customer_name']  = $invoice->customer_name; */
        $data['start_date'] = $invoice->period_start;
        $data['end_date'] = $invoice->period_end;
        $data['subscription'] = $invoice->subscription;
        $data['paid_status'] = $invoice->paid;
        $data['payment_intent'] = $invoice->payment_intent;
        $data['franchise_id'] = $franchise_id;
//Storage::disk('local')->put('file.txt',  $data);
// Get admission_detail_id from Admission Detail table using Subscription ID
        $admission_detail_id = AdmissionDetail::where('stripe_subscription_id', '=', $invoice->subscription)->first();
        if (isset($admission_detail_id->id)) {
            // Check if payment_intent already exist in admission detail against subscription id
            $payment_intent_id = AdmissionDetail::where('stripe_subscription_id', '=', $invoice->subscription)->first()->stripe_payment_intent_id;
            if ($payment_intent_id == $invoice->payment_intent) {
                //Do Nothing
            } else {

                $data['admission_detail_id'] = $admission_detail_id->id;
                //check if payment_intent already exist
                $transaction_id_exists = AdmissionDetailTrasaction::where('trasaction_id', '=', $invoice->id)->where('payment_intent', '=', $invoice->payment_intent)->first();
                if (isset($transaction_id_exists->trasaction_id) && $transaction_id_exists->trasaction_id != "") {
                    //Do Nothing    
                } else {
                    //Store Details in Transaction Table
                    $transaction = AdmissionDetailTrasaction::create($data);
                    //Storage::put('file.txt', $transaction);
                }
            }
        }
    }

    public function subscriptionpmtid(Request $request) {
        //            $stripeAPiKey = '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';
        $franchise = getFranchise();
        $stripeAPiKey = $franchise->stripe_client;
        $stripe = new \Stripe\StripeClient($stripeAPiKey);
        // Get All subscription id from admission details
        $admission_detail = AdmissionDetail::where('payment_method_id', '0')->orWhere('payment_method_id', '')->get()->toArray();

        foreach ($admission_detail as $k => $data) {
            $customer_id = $data['stripe_customer_id'];
            $subscription_id = $data['stripe_subscription_id'];
            $id = $data['id'];

            try {
                $pmtmethod = $stripe->customers->allPaymentMethods(
                        $customer_id, ['type' => 'card']
                );
                if (count($pmtmethod['data']) > 0) {
                    $lastCardElement = array_key_first($pmtmethod['data']);

                    $pmtid = $pmtmethod['data'][$lastCardElement]['id'];
                } else {
                    $pmtid = $pmtmethod['data']['id'] ? $pmtmethod['data']['id'] : $pmtmethod['id'];
                }
                //echo $pmtid."<br />";
                // Update Subscription PMT method id
                try {
                    $stripe->subscriptions->update(
                            $subscription_id, ['default_payment_method' => $pmtid]
                    );

                    AdmissionDetail::where('id', $id)->update(['payment_method_id' => $pmtid]);
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                }
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $api_error = $e->getMessage();
            } catch (Exception $e) {
                $api_error = $e->getMessage();
            }
        }
        //dd($admission_detail);
    //
    }

    /* Update Price in stripe from stripe products */

    public function update_price_stripe() {

        // Get products/price from stripe
        //            $stripeAPiKey = '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';
        $franchise = getFranchise();
        $stripeAPiKey = $franchise->stripe_client;
        $stripe = new \Stripe\StripeClient($stripeAPiKey);
        $price = $stripe->prices->all(['limit' => 10000]);
        // dd($price->data[0]['recurring']);
        foreach ($price->data as $k => $data) {
            $amount = $data['unit_amount'] / 100;
            if ($amount == 60) {
                $price_id = $data['id'];
                $product_id = $data['product'];
                $duration_type = $data['recurring']['interval'];
                $new_amount = 80 * 100;
                //Now create new Price for the product
                try {
                    $new_price = $stripe->prices->create([
                        'product' => $product_id,
                        'unit_amount' => $new_amount,
                        'currency' => 'usd',
                        'recurring' => ['interval' => $duration_type],
                        'active' => true
                    ]);
                    // Fetch Subscriptions that contain old price id

                    $old_price_subs = $stripe->subscriptions->all(['price' => $price_id, 'limit' => 100000]);

                    foreach ($old_price_subs as $ky => $subs_data) {
                        $sub_id = $subs_data['id'];
                        try {
                            // Attach New price with all subscriptions
                            $subs_updated = $stripe->subscriptions->update(
                                    $sub_id, [
                                'items' => [
                                        [
                                        'id' => $product_id,
                                        'price' => $new_price->id
                                    ],
                                ],
                                'proration_behavior' => 'none'
                                    ]
                            );
                        } catch (Stripe\Exception\InvalidRequestException $e) {
                            $api_error = $e->getMessage();
                            echo $api_error;
                            continue;
                        } catch (Stripe\Exception\ApiConnectionException $e) {
                            $api_error = $e->getMessage();
                            echo $api_error;
                            continue;
                        } catch (Exception $e) {
                            $api_error = $e->getMessage();
                            echo $api_error;
                            continue;
                        }
                    }
                } catch (Stripe\Exception\InvalidRequestException $e) {
                    $api_error = $e->getMessage();
                    echo $api_error;
                    continue;
                } catch (Stripe\Exception\ApiConnectionException $e) {
                    $api_error = $e->getMessage();
                    echo $api_error;
                    continue;
                } catch (Exception $e) {
                    $api_error = $e->getMessage();
                    echo $api_error;
                    continue;
                }
                // archieve old price
                $update_price = $stripe->prices->update($price_id, ['active' => false]);
                //echo $duration_type . "<br />";
            } else {
                continue;
            }
        }
        exit;
        return false; // Remove this
    }

    /* End Update Price in stripe */

    /* Update Price in stripe from database values */

    public function update_price() {

        return false; // Remove this
        // Get price from table
        $class_data = SchoolClass::whereNotNull('stripe_product_id')->where('class_charges', '>', '60')->skip(201)->take(50)->get();
        //dd($class_data);
        foreach ($class_data as $data) {
            $stripe_price_id = $data->stripe_product_id;
            $charges = round($data->class_charges * 100);
            $duration = strtolower($data->duration_type);
            if ($duration == "monthly") {
                $duration = "month";
            } else if ($duration == "weekly") {
                $duration = "week";
            } else if ($duration == "yearly") {
                $duration = "year";
            }
            // Now update at stripe
            //            $stripeAPiKey = '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';
            $franchise = getFranchise();
            $stripeAPiKey = $franchise->stripe_client;
            $stripe = new \Stripe\StripeClient($stripeAPiKey);
            $price = $stripe->prices->retrieve($stripe_price_id);


            if ($price && ($price->unit_amount != $charges)) {
                $product_id = $price->product;
                // create new price and set active
                $new_price = $stripe->prices->create([
                    'product' => $product_id,
                    'unit_amount' => $charges,
                    'currency' => 'usd',
                    'recurring' => ['interval' => $duration],
                    'active' => true
                ]);
                // Update school class with new price id
                $class_id = $data->id;
                $updatedPrice = SchoolClass::where('id', $class_id)->update(['stripe_product_id' => $new_price->id]);
                // Get All subscriptions against class id
                $admission_data = AdmissionDetail::where(['school_class_id' => $class_id])->where('stripe_subscription_id', '!=', '0')->get();
                // Update All subscriptions with new price id at stripe
                foreach ($admission_data as $a_data) {
                    //check if subscription exist in stripe
                    try {
                        $get_subs = $stripe->subscriptions->retrieve($a_data->stripe_subscription_id);
                        //echo "<pre>".var_dump($get_subs->items['data'][0]['id'])."</pre>";
                        $subs_item_id = $get_subs->items['data'][0]['id'];
                        if ($get_subs) {
                            // we need to update subscription item price


                            try {

                                $subs_updated = $stripe->subscriptions->update(
                                        $a_data->stripe_subscription_id, [
                                    'items' => [
                                            [
                                            'id' => $subs_item_id,
                                            'price' => $new_price->id
                                        ],
                                    ],
                                    'proration_behavior' => 'none'
                                        ]
                                );
                            } catch (Stripe\Exception\InvalidRequestException $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            } catch (Stripe\Exception\ApiConnectionException $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            } catch (Exception $e) {
                                $api_error = $e->getMessage();
                                echo $api_error;
                                continue;
                            }
                        }
                    } catch (Stripe\Exception\InvalidRequestException $e) {
                        $api_error = $e->getMessage();
                        echo $api_error;
                        continue;
                    } catch (Stripe\Exception\ApiConnectionException $e) {
                        $api_error = $e->getMessage();
                        echo $api_error;
                        continue;
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        echo $api_error;
                        continue;
                    }
                }
                $update_price = $stripe->prices->update($stripe_price_id, ['active' => false]);
            }
            //dd($price);
        }
        //dd($class_data);
    }

    /* End Update Price in stripe */
}
