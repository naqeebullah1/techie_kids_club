<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PromoCode;
use Illuminate\Support\Facades\DB;
use Stripe;
use Carbon\Carbon;

class PromoCodesController extends Controller {

    public function index(Request $request, $expired = false) {
        $schools = PromoCode::query();
        if ($expired) {
            $schools = $schools->whereDate('to_date', '<', date('Y-m-d'));
        } else {
            $schools = $schools->whereDate('to_date', '>', date('Y-m-d'));
        }
        $schools = $schools->paginate(15);
        if ($request->ajax()) {
            $view = view('promo_codes.table', compact('schools'))
                    ->render();
            return response()->json(['html' => $view]);
        }
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();

        return view('promo_codes.index', compact('schools', 'roles', 'expired'));
    }

    public function create($id = null) {
//        dd('');
        $school = new PromoCode();
        if ($id) {
            $school = PromoCode::find($id);
        }
        $view = view('promo_codes.form', compact('school'))
                ->render();
        return response()->json(['html' => $view]);
    }

    public function store(Request $request) {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $record = $request->record;
            $id = $request->id;
            $record['from_date'] = date('Y-m-d', strtotime($record['from_date']));
            $record['to_date'] = date('Y-m-d', strtotime($record['to_date']));

            $toDate = Carbon::parse($record['to_date']);
            $fromDate = Carbon::parse($record['from_date']);
            $monthDiff = $toDate->diffInMonths($fromDate);


            $outcome = "";
            $error = "";
            $api_error = "";
//            $stripeAPiKey='sk_test_51HyGh4B3WkD7XcJGzOo6cTQRB4f2D9QQhPUkYiAXHcqEmkf2npVre8PV9Z9MsxBufAyN5hczg7lqy9co3updar5000KiCSOmnq';

            $franchise = getFranchise();
            $stripeAPiKey = $franchise->stripe_client;

            $stripe = new \Stripe\StripeClient($stripeAPiKey);

            if ($id) {

                /* if($record['promo_type'] == 'flat') {
                  $amount = $record['flat_amount']*100;

                  try {
                  $coupon_res = $stripe->coupons->update($record['promo_code'],[
                  'amount_off' => (int) $amount,
                  'currency' => 'USD',
                  'duration' => 'repeating',
                  'duration_in_months' => $monthDiff
                  ]);
                  PromoCode::find($id)->update($record);
                  $outcome = "Promo Code Updated Successfully";
                  } catch (Exception $e) {
                  $api_error = $e->getMessage();
                  $error = json_encode(['error' => $api_error]);

                  }
                  catch (Stripe\Exception\InvalidRequestException $e) {
                  $api_error = $e->getMessage();
                  $error = json_encode(['error' => $api_error]);

                  }

                  } else {
                  $promo_type = 'percent_off';
                  $amount = $record['percent_off'];

                  try {
                  $coupon_res = $stripe->coupons->update($record['promo_code'],
                  ['percent_off' => $amount,
                  'duration' => 'repeating',
                  'duration_in_months' => $monthDiff
                  ]);

                  PromoCode::find($id)->update($record);
                  $outcome = "Promo Code Updated Successfully";
                  } catch (Exception $e) {
                  $api_error = $e->getMessage();
                  $error = json_encode(['error' => $api_error]);

                  }
                  catch (Stripe\Exception\InvalidRequestException $e) {
                  $api_error = $e->getMessage();
                  $error = json_encode(['error' => $api_error]);

                  }

                  } */
            } else {
                // Create Coupon in Stripe


                if ($record['promo_type'] == 'flat') {
                    $amount = $record['flat_amount'] * 100;

                    try {
                        $coupon_res = $stripe->coupons->create([
                            'id' => $record['promo_code'],
                            'amount_off' => (int) $amount,
                            'currency' => 'USD',
                            'duration' => 'repeating',
                            'duration_in_months' => $monthDiff
                        ]);
                        PromoCode::create($record);
                        $outcome = "Promo Code Created Successfully";
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        $error = json_encode(['error' => $api_error]);
                    } catch (Stripe\Exception\InvalidRequestException $e) {
                        $api_error = $e->getMessage();
                        $error = json_encode(['error' => $api_error]);
                    }
                } else {
                    $promo_type = 'percent_off';
                    $amount = $record['percent_off'];

                    try {
                        $coupon_res = $stripe->coupons->create([
                            'id' => $record['promo_code'],
                            'percent_off' => $amount,
                            'duration' => 'repeating',
                            'duration_in_months' => $monthDiff
                        ]);
                        PromoCode::create($record);
                        $outcome = "Promo Code Created Successfully";
                    } catch (Exception $e) {
                        $api_error = $e->getMessage();
                        $error = json_encode(['error' => $api_error]);
                    } catch (Stripe\Exception\InvalidRequestException $e) {
                        $api_error = $e->getMessage();
                        $error = json_encode(['error' => $api_error]);
                    }
                }
            }


            DB::commit();
            return redirect()->back()->with(['outcome' => $outcome, 'promoerror' => $api_error]);
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function destroy($id) {

        $outcome = '';
        $api_error = "";
//            $stripeAPiKey = 'sk_test_51HyGh4B3WkD7XcJGzOo6cTQRB4f2D9QQhPUkYiAXHcqEmkf2npVre8PV9Z9MsxBufAyN5hczg7lqy9co3updar5000KiCSOmnq';
        
        $franchise = getFranchise();
        $stripeAPiKey = $franchise->stripe_client;
        
        $stripe = new \Stripe\StripeClient($stripeAPiKey);

        try {
            DB::beginTransaction();
            $promo = PromoCode::find($id);

            $promoId = $promo->promo_code;

            try {
                $stripe->coupons->delete($promoId, []);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
                $error = json_encode(['error' => $api_error]);
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $api_error = $e->getMessage();
                $error = json_encode(['error' => $api_error]);
            }
            $promo->delete();
            $outcome = "Coupon deleted successfully.";

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with(['outcome' => $outcome, 'promoerror' => $api_error]);
    }

}
