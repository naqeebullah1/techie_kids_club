<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\School;

class SchoolAdminsController extends Controller {

    public function schoolDetails($schoolId) {
        $school = School::find($schoolId);
        $view = view('school_admin.partials.school_details', compact('school'))
                ->render();
        return $view;
    }

    public function getSchools() {
        $user = auth()->user();
        $view = view('school_admin.partials.card', compact('user'))
                ->render();
        return $view;
    }

    public function getClass($classId) {
        $students = \App\Models\AdmissionDetail::where('school_class_id', $classId)->get();
        $school = \App\Models\SchoolClass::find($classId);
        $view = view('school_admin.partials.class_details', compact('school','students'))
                ->render();
        return $view;
    }

}
