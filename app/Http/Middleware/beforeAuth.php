<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class beforeAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (Auth::check()) {
            $user = Auth::user()->ModelHasRoles->pluck('role_id')->toArray();
            if (in_array(3, $user)) {
                
            } elseif (in_array(5, $user)) {
                return redirect()->back()->with('error', 'You are not allowed');
                
            } else {
                return redirect('/backend/dashboard')->with('error', 'You are not allowed to access that location');
            }
        }
        return $next($request);
    }

}
