<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\FranchiseScope;

class Admission extends Model {

    use HasFactory;

    protected $guarded = [];

    public function details() {
        return $this->hasMany(AdmissionDetail::class);
    }

    public function admission_parent() {
        return $this->belongsTo(User::class, 'parent_id')->withoutGlobalScope(\App\Scopes\FranchiseScope::class);
    }

    protected static function booted() {
        if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
            static::addGlobalScope(new FranchiseScope($franchiseId));
        }
    }
    public function scopeRemoveFranchiseScope($query)
{
    return $query->withoutGlobalScope('franchise');
}

}
