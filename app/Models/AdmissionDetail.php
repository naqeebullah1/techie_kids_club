<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdmissionDetail extends Model {

    use HasFactory;

    protected $guarded = [];

    public function admission_class() {
        return $this->belongsTo(SchoolClass::class,'school_class_id');
    }
     public function admission() {
        return $this->belongsTo(Admission::class, 'admission_id');
    }
    public function school() {
        return $this->belongsTo(School::class);
    }
    public function student() {
        return $this->belongsTo(Student::class);
    }
    
    public function admission_detail_transaction() {
        return $this->HasMany(AdmissionDetailTrasaction::class,'admission_detail_id');
    }
    public function franchise() {
            return $this->belongsTo(Franchise::class,'franchise_id');
        }
}
