<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdmissionDetailTrasaction extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes;
    use HasFactory;

    protected $guarded = [];

    public function admission_detail_transaction() {
        return $this->belongsTo(AdmissionDetail::class,'admission_detail_id');
    }
    
}
