<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceSheet extends Model {

    use HasFactory;

    protected $guarded = [];

    public function attendance_sheet_lists() {
        return $this->hasMany(AttendanceSheetList::class);
    }

}
