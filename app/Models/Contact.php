<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\FranchiseScope;

class Contact extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            if ($user) {
                $franchiseId = $user->franchise_id;
            } else {
                $franchiseId = slugToID(request()->franchise_id);
            }
            $model->franchise_id = $franchiseId;
        });
    }

    protected static function booted() {
        if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
            static::addGlobalScope(new FranchiseScope($franchiseId));
        }
    }

}
