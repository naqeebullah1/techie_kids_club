<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Footer extends Model implements Auditable {

    use HasFactory,
        \OwenIt\Auditing\Auditable,
        SoftDeletes;

    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
        });
    }

    public function scopeFranchiseCheck($q) {

        if (auth()->user()) {
            if (auth()->user()->role == 3) {
//                dd('han');
                $franchiseId = [1];
            } else {
                $franchiseId = [auth()->user()->franchise_id];
            }
        } else {
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
//        dd($franchiseId);
        return $q->whereIn('franchise_id', $franchiseId);
    }

}
