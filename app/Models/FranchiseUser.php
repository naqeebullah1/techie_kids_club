<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FranchiseUser extends Model {

    use HasFactory;

    protected $guarded = [];
    protected $table = 'franchise_user';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function franchise() {
        return $this->belongsTo(Franchise::class);
    }

}
