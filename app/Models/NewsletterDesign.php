<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsletterDesign extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'content', 'image'];

    protected $table = 'newsletter_design';
}
