<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\FranchiseScope;

class Notification extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function city() {
        return $this->belongsTo(City::class);
    }

    public function school() {
        return $this->belongsTo(School::class);
    }

    public function viewers() {
        return $this->hasMany(NotificationViewer::class);
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
        });
    }

   /* protected static function booted() {
        if (auth()->user()) {
            
            $franchiseId = [auth()->user()->franchise_id];
            static::addGlobalScope(new FranchiseScope($franchiseId,'notifications.franchise_id'));
        }
    }*/

}
