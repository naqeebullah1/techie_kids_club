<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Franchise;
use App\Scopes\FranchiseScope;

class Page extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes;

    protected $guarded = [];

    use HasFactory;

    public function pageDetails() {
        return $this->hasMany(PageDetail::class)->orderBy('sort_order');
    }

    protected static function boot() {
        parent::boot();

        static::deleted(function ($invoice) {
            $invoice->pageDetails()->delete();
        });


        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;

            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

    public function scopeFranchiseCheck($q) {

        if (auth()->user()) {
           if (auth()->user()->role == 3) {
                $franchiseId = [1];
            } else {
                $franchiseId = [auth()->user()->franchise_id];
            }
        } else {
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
        $franchiseId[] = 0;
        return $q->whereIn('franchise_id', $franchiseId);
    }

}
