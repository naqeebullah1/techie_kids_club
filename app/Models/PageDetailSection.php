<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\FranchiseScope;

class PageDetailSection extends Model {

    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];

    //protected $fillable = ['title','icon','description','btn_title','btn_icon','btn_link'];

    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
        });
    }

    protected static function booted() {
        if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
            static::addGlobalScope(new FranchiseScope($franchiseId));
        }
    }

}
