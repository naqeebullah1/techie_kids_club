<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\FranchiseScope;

class PromoCode extends Model {

    use HasFactory;

    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
        });
    }

    protected static function booted() {
        if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
            static::addGlobalScope(new FranchiseScope($franchiseId));
        }
    }

}
