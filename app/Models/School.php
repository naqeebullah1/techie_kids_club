<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\FranchiseScope;

class School extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function city() {
        return $this->belongsTo(City::class);
    }
    public function franchise() {
        return $this->belongsTo(Franchise::class,'franchise_id');
    }

    public function regional_director() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function school_classes() {
        return $this->hasMany(SchoolClass::class);
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
            $model->created_by = $user->id;
            $model->updated_by = $user->id;

            if (request()->franchise_id) {
                $model->franchise_id = request()->form_franchise_id;
            }
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

    public function scopeGetList($query) {
        $roles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
        if (in_array(4, $roles)) {
            $query = $query->where('user_id', 4);
        }
        if (in_array(2, $roles)) {
            $query = $query->whereHas('school_classes', function($q) {
                return $q->where('user_id', 2);
            });
        }
        return $query;
    }

    protected static function booted() {

        if (auth()->user()) {
            if(auth()->user()->role==3){
                return true;
            }
            $franchiseId = [auth()->user()->franchise_id];
            
        } else {
            /*
             * Allow for parent auth or without auth
             */
            return true;
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
//        dd($franchiseId);
        static::addGlobalScope(new FranchiseScope($franchiseId));
    }

}
