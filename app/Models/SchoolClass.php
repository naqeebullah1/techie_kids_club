<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\FranchiseScope;

class SchoolClass extends Model implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes,
        HasFactory;

    protected $guarded = [];
    protected $casts = [
        'start_at' => 'date:h:i a',
    ];

    public function teacher() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function school() {
        return $this->belongsTo(School::class);
    }

    public function admission_details() {
        return $this->hasMany(AdmissionDetail::class);
    }

    public function scopeDropdown($query) {
        return $query->selectRaw('CONCAT(title,"  (", TIME_FORMAT(start_at, "%h:%i %p"),")") as value,id');
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

    protected static function booted() {
//        if (auth()->user()) {
//            $franchiseId = [auth()->user()->franchise_id];
//            $column = "school_classes.franchise_id";
//            static::addGlobalScope(new FranchiseScope($franchiseId, $column));
//        }

        if (auth()->user()) {
            if (auth()->user()->role == 3) {
                return true;
            }
            $franchiseId = [auth()->user()->franchise_id];
        } else {
            /*
             * Allow for parent auth or without auth
             */
            return true;
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
//        dd($franchiseId);
        static::addGlobalScope(new FranchiseScope($franchiseId, "school_classes.franchise_id"));
    }
    
        public function scopeRemoveFranchiseScope($query)
{
    return $query->withoutGlobalScope('franchise');
}
}
