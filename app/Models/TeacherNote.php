<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherNote extends Model {

    use HasFactory;

    protected $guarded = [];

    public function teacher_notes() {
        return $this->belongsToMany(User::class);
    }

}
