<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use App\Scopes\FranchiseScope;

class Testimonial extends Model implements Auditable {

    use HasFactory,
        SoftDeletes,
        \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $user = auth()->user();
            $model->franchise_id = $user->franchise_id;
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });

        static::updating(function($model) {
            $user = auth()->user();
            $model->updated_by = $user->id;
        });
    }

   protected static function booted() {
       if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
        } else {
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
        static::addGlobalScope(new FranchiseScope($franchiseId));
    }

}
