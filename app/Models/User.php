<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
use App\Scopes\FranchiseScope;

class User extends Authenticatable implements Auditable {

    use \OwenIt\Auditing\Auditable,
        SoftDeletes;
    use HasFactory,
        Notifiable,
        HasRoles;

    protected $fillable = [
        'password',
        'reset_token',
        'reset_token_expiry',
        'first_name',
        'last_name',
        'email',
        'status',
        'phone1',
        'phone2',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'created_by',
        'updated_by',
        'check_privacy',
        'check_terms',
        'check_privacy_date',
        'check_terms_date',
        'files',
        'franchise_id',
        'sms_subscribed',
    ];

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function ModelHasRoles() {
        return $this->hasMany('App\Models\ModelHasRole', 'model_id', 'id');
    }

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function admissions() {
        return $this->hasMany(Admission::class, 'parent_id');
    }

    public function schools() {
        return $this->hasMany(School::class, 'school_admin_id');
    }

    public function scopeGetList($query, $r) {
//        dd($a);
        return $query->selectRaw('CONCAT(users.last_name," - ",users.email) as value,users.id')
                        ->singleRole($r);
//        return $this->hasMany('App\Models\ModelHasRole', 'model_id', 'id');
    }

    public function scopeSingleRole($query, $r) {
        return $query->whereHas('ModelHasRoles', function($q) use($r) {
                    return $q->where('role_id', $r);
                });
    }

    public function getRoleAttribute() {
        return Session::get('role', null);
    }

    public function setRoleAttribute($value) {
        Session::put('role', $value);
    }

    public function franchise() {
        return $this->belongsTo(Franchise::class);
    }

    public function user_franchises() {
        return $this->belongsToMany(Franchise::class);
    }

    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            $user = auth()->user();
            if ($user) {
                $franchiseId = $user->franchise_id;
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            } else {
                $franchise = (request()->franchise_id)?request()->franchise_id:'techie-kids-club';
                $franchiseId = slugToID($franchise);
            }
            if (request()->user_franchise_id) {

            } else {
                $model->franchise_id = $franchiseId;
            }
        });

        static::updating(function($model) {
            $user = auth()->user();
            if ($user) {
                $model->updated_by = $user->id;
            }
        });
    }

    protected static function booted() {
        /*
         * No Scopes for Parents as zuber asked
         */
//        dd(request()->login_type);
        if (request()->login_type == 3) {
            return true;
        }

        if (auth()->user()) {
            $franchiseId = [auth()->user()->franchise_id];
        } else {
            $franchiseId = [1];
            if (request()->franchise_id) {
                $franchiseId = slugToID(request()->franchise_id);
                $franchiseId = [$franchiseId];
            }
        }
        static::addGlobalScope(new FranchiseScope($franchiseId, "users.franchise_id"));
    }

}
