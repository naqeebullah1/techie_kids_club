<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        if (\Schema::hasTable('franchises')) {
//        dd('naqeeb');
//            $mail = DB::table('franchises')->first();
            $mail = DB::table('franchises')->first();
            if ($mail) { //checking if table is not empty
                $config = array(
                    'driver' => 'smtp',
                    'host' => 'smtp.gmail.com',
                    'port' => 587,
                    'from' => array('address' => 'info@techiekidsclub.com', 'name' => 'khan'),
                    'encryption' => 'tls',
                    'username' => $mail->mail_email,
                    'password' => $mail->mail_password,
                    'sendmail' => '/usr/sbin/sendmail -bs',
                    'pretend' => false,
                );
                
                Config::set('mail', $config);
            }
        }
    }

}
