<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FranchiseScope implements Scope {

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    protected $franchise_id;
    protected $column;

    public function __construct($franchise_id, $column = null) {

        $this->franchise_id = $franchise_id;
        if ($column) {
            $this->column = $column;
        } else {
            $this->column = 'franchise_id';
        }
    }

//      public function apply(Builder $builder, Model $model) {
//        $franchises = $this->franchise_id;
//        $franchises[] = 0;
//        $builder->whereIn($this->column, $franchises);
//    }


    public function apply(Builder $builder, Model $model) {
        $franchises = $this->franchise_id;
        $franchises[] = 0;

        /*
         * These checks are for zuber@cyberclouds.com
         * 
         */
        if (auth()->user()) {
            /*
             * If Super admin is logged in or parent is logged in
             */
            if (auth()->user()->id != 1 && auth()->user()->role != 3) {
                $builder->whereIn($this->column, $franchises);
            }
        } else {
            $builder->whereIn($this->column, $franchises);
        }
    }

}
