<?php

namespace App\Traits;

use App\Models\User;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\BroadcastUser;
use App\Models\Audit;

trait CronJobTrait {

    /**
     * @param Request $request
     * @return $this|false|string
     */
    public function checkScheduledBroadcast() {
        $users = BroadcastUser::select('broadcast_users.id', 'broadcast_users.user_id', 'broadcasts.title', 'broadcasts.description', 'phone1', 'phone2', 'sms_subscribed', 'last_sent')
                ->join('broadcasts', 'broadcasts.id', 'broadcast_users.broadcast_id')
                ->join('users', 'users.id', 'broadcast_users.user_id')
                ->where('broadcast_users.status', 0)
                ->limit(1)
                ->get();
        $audit = [
            'event' => 'updated',
            'auditable_type' => 'App\Models\BroadcastUser',
            'auditable_id' => '',
            'old_values' => '[]',
            'new_values' => '',
        ];
        foreach ($users as $user):
            $currentDateTime = new DateTime();
            $storedDateTimeObj = new DateTime($user->last_sent);

            // Calculate the difference in days
            $interval = $currentDateTime->diff($storedDateTimeObj);

            $daysDifference = $interval->days;
            if ($user->sms_subscribed == 1 && ($daysDifference >= 7 || $user->last_sent == null)) {
                $broadcastUserId = $user->id;
                $phone = $user->phone1;
                if ($phone == "") {
                    $phone = $user->phone2;
                }

                $response = $this->conveyMessage($phone, $user->title, $user->description);
                $user->status = $response['status'];

                $userModel = User::find($user->user_id);
                $userModel->last_sent = now();
                $userModel->save();

                $updateBC = BroadcastUser::find($broadcastUserId)->update($response);

                $audit['auditable_id'] = $broadcastUserId;
                $audit['user_id'] = $user->user_id;
                $response['phone'] = $phone;

                $audit['new_values'] = json_encode($response);
                $audit = Audit::create($audit);
            }
        endforeach;
    }

    public function conveyMessage($phone, $title, $description) {
        // dd($phone);

        $phone = preg_replace('/[^\d]/', '', $phone);

        $notificationpagelink = url('/') . "/parent/notifications";

        $message = $title . "\n" . $description . "\n" . $notificationpagelink;
//        dd($message);
        //Curl

        $app_name = "TECHIE-KIDS";
        $utc_date = gmdate("m-d-Y");
        $token = strtoupper(md5("TECHIE-KIDSCyberCloudAWS" . $utc_date));
        $cc = '1';
        $phone = $cc . $phone;

        $client = new Client();
        $response = $client->request('POST', 'http://cybercloudaws.cyberclouds.info/api/TezzSMS/SendSMS', [
            'headers' => [
                'Content-Type' => 'application/json',

            ],
            'json' => [
                "ApplicationName" => $app_name,
                "Token" => $token,
                "Messages" => [
                    [
                        "PhoneNumber" => $phone,
                        "Message" => $message
                    ]
                ]
            ],
        ]);

        $response = $response->getBody()->getContents();
        // dd($phone);

//        $curl = curl_init();

        //change mobile number to db phone number

//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'http://cybercloudaws.cyberclouds.info/api/SMS/SendSMS',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => "{
//                        'ApplicationName': '$app_name',
//                        'Token': '$token',
//                        'PhoneNumber': '" . $phone . "',
//                        'Message': '$message',
//
//                    }",
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json'
//            ),
//        ));
//        $response = curl_exec($curl);
//        curl_close($curl);
        if (json_decode($response, true)['Messages'][0]['Success'] == true) {
            $status = 1;
        } else {
            $status = 0;
        }
        $data = [];
        $data['status'] = $status;
        $data['response'] = json_decode($response, true)['Messages'][0]['MessageID'];

        return $data;
    }

}
