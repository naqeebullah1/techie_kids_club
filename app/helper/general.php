<?php

use GuzzleHttp\Client;

function menus($type = "main header") {
    $menus = App\Models\Page::where('active', 1)
            ->where('header_type', $type)->orderby('id', 'ASC')
            ->franchiseCheck()
            ->get();
    return $menus;
}

function categories() {
    $menus = App\Models\Category::where('active', 1)->get();
    return $menus;
}

function sectionTypes() {
    return [
        '' => 'Select type',
        1 => '3 columns section',
        2 => 'Right Side Video',
        3 => 'Right Side Image',
        4 => 'Left Side Image',
        5 => '3 1 3 Section',
        6 => 'Gallery',
        7 => 'Testimonials Parents',
        8 => 'FAQ For Parents',
        10 => 'FAQ For Schools',
        9 => 'Schools',
        11 => 'Contact Us Form',
        12 => 'Our Team',
        13 => 'Banner Section',
        14 => 'Home Page Banner',
        15 => '4 Sections design',
        16 => 'Tabs Section',
        17 => 'Grid View Gallery',
        18 => 'Demo Class Form',
        19 => 'Demo Steps',
        20 => 'Testimonials Schools',
        21 => 'Video (Autoplay)',
        22 => 'Single Image Section'
    ];
}

function services($id = null) {
    $services = \App\Models\Service::where('status', 1)->get();
//    dd($services);
    return $services;
}

function siteDetails() {
    $services = \App\Models\Footer::franchiseCheck()->first();
    if ($services == null) {
        $services = new App\Models\Footer;
    }
//    dd($services);
    return $services;
}

function trimText($in) {
    $ch = 100;
    return strlen($in) > $ch ? substr($in, 0, $ch) . "..." : $in;
}

function weekDays() {
    return ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
}

function formatDate($date) {
    if ($date && $date != '0000-00-00') {
        return date('m/d/Y', strtotime($date));
    }
    return "";
}

function formatTime($date) {
    if ($date) {
        return date('h:i A', strtotime($date));
    }
}

function basicRoles() {
    return [1 => 'Admin', 2 => 'Teacher', 3 => 'Parents', 4 => 'Regional Director', 5 => 'School Admin'];
}

function schoolAdminRoles() {
    return [2 => 'Teacher', 3 => 'Parents', 4 => 'Regional Director'];
}

function getGallery() {
    return App\Models\Category::where('active', 1)->first();
}

function IDToSlug($franchiseId) {
    return App\Models\Franchise::find($franchiseId);
}

function findBySlug($franchiseId) {
    return App\Models\Franchise::whereSlug($franchiseId)->first();
}

function slugToID($franchise_slug) {
//    dd($franchise_slug);
    return App\Models\Franchise::where('slug', $franchise_slug)->first()->id;

    $franchise_slug = $franchiseId[0];
    if (auth()->user()) {
        return [$franchise_slug];
    } else {
        return [App\Models\Franchise::where('slug', $franchise_slug)->first()->id];
    }
}

function getFAQ($type) {
//    dd(App\Models\Question::where('faq_for', $type)->where('status', 1)->get());
    return App\Models\Question::where('faq_for', $type)->where('status', 1)->get();
}

function team() {
//    dd('naq');
    $director = App\Models\Team::where('is_active', 1)
            //->franchiseCheck()
            ->where('title', 'Like', '%director%')
            ->withoutGlobalScope(\App\Scopes\FranchiseScope::class)
            ->get();


//    dd($director);
    $coach = App\Models\Team::where('is_active', 1)
            //->franchiseCheck()
            //->where('title', 'Like', '%coach%')
            ->where('title', 'Like', '%teacher%')
            ->withoutGlobalScope(\App\Scopes\FranchiseScope::class)
            ->get();

    return ['director' => $director, 'coach' => $coach];
}

function testimonial_parents() {
    $testimonial_parents = App\Models\Testimonial::where('is_active', 1)->where('type', '1')->get()->toArray();

    return $testimonial_parents;
}

function testimonial_schools() {
    $testimonial_schools = App\Models\Testimonial::where('is_active', 1)->where('type', '2')->get()->toArray();

    return $testimonial_schools;
}

function searchInThese() {
    return [
        4 => 'Regional Manager',
        2 => 'Teachers',
        3 => 'Parents',
        's' => 'Students',
        'sc' => 'Schools',
        'c' => 'Classes',
    ];
}

function loginTypesBackend() {
    return [
        0 => 'Select Login Type',
        1 => 'Staff',
        2 => 'Teachers',
        4 => 'Regional Manager',
    ];
}

function loginTypesFrontend() {
    return [
        3 => 'Parent',
        5 => 'School Admin',
        4 => 'Regional Manager',
        2 => 'Teachers'
    ];
}

function auth_user_roles() {
    return auth()->user()->ModelHasRoles->pluck('role_id')->toArray();
}

function formatPromoAMount($promo) {
    if ($promo->promo_type == 'flat') {
        $pamount = '-$' . $promo->flat_amount;
    }
    if ($promo->promo_type == 'percent') {
        $pamount = '-' . $promo->percent_off . '%';
    }
    return $pamount;
}

function subPromoAmount($promo, $st) {
    if ($promo->promo_type == 'flat') {
        $pamount = $promo->flat_amount;
    }
    if ($promo->promo_type == 'percent') {
        $pamount = number_format(($st / 100) * $promo->percent_off, 2, '.', '');
    }
//    $pamount = number_format($st - $pamount, 2, '.', '');
    return $pamount;
}

function priceSettings() {
    return App\Models\PriceSetting::pluck('type', 'type')->toArray();
}

function franchiseLink() {
//    dump(request()->franchise_id);
if(!request()->franchise_id) {
    request()->franchise_id = "techie-kids-club";
}
    return 'front-franchise/' . request()->franchise_id;
}

function parentfranchiseLink() {
    if(!request()->franchise_id) {
    request()->franchise_id = "techie-kids-club";
}
    return 'parent/front-franchise/' . request()->franchise_id;
}

function schoolAdminfranchiseLink() {
    return 'school-admin/front-franchise/' . request()->franchise_id;
}

function replaceCodes($text) {
    $franchise = findBySlug(request()->franchise_id);
    $text = str_replace('##franchise_title##', $franchise->name, $text);
    $text = str_replace('##franchise_link##', url('front-franchise/' . $franchise->slug), $text);
    return $text;
}

function frontendBaseUrl($franchiseSlug = null) {
    if (!$franchiseSlug) {
        if (auth()->user()) {
            $franchise = IDToSlug(auth()->user()->franchise_id);
            $franchiseSlug = $franchise->slug;
        } else {
            $franchiseSlug = request()->franchise_id;
        }
    }
    return url('front-franchise/' . $franchiseSlug);
}

function franchiseList() {
    return \App\Models\Franchise::pluck('name', 'id')->toArray();
}

function getFranchise($id=null) {
    if (auth()->user()) {
        $franchise = IDToSlug(auth()->user()->franchise_id);
    } elseif($id) {
        
        $franchise = findBySlug($id);
        
    }else {
        $franchise = findBySlug(request()->franchise_id);
    }
    return $franchise;
}

function ClassStudentCount($id) {
    return  \App\Models\AdmissionDetail::where('school_class_id', $id)->count();
}

function sendOptIn($user) {
    $app_name = "TECHIE-KIDS";
    $utc_date = gmdate("m-d-Y");
    $token = strtoupper(md5("TECHIE-KIDSCyberCloudAWS" . $utc_date));
    $cc = '+1';
    $phone = preg_replace('/[^\d]/', '', $user->phone1);
    $phone = $cc . $phone;
    // Call API accordingly
    $client = new Client();
    $response = $client->request('POST', 'http://cybercloudaws.cyberclouds.info/api/TezzSMS/OptIn', [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'json' => [
            "ApplicationName" => $app_name,
            "Token" => $token,
            "PhoneNumber" => $phone,
            "FirstName" => $user->first_name,
            "LastName" => $user->last_name
        ],
    ]);

    return json_decode($response->getBody()->getContents());
}

function sendOptOut($user) {
    $app_name = "TECHIE-KIDS";
    $utc_date = gmdate("m-d-Y");
    $token = strtoupper(md5("TECHIE-KIDSCyberCloudAWS" . $utc_date));
    $cc = '+1';
    $phone = preg_replace('/[^\d]/', '', $user->phone1);
    $phone = $cc . $phone;
    // Call API accordingly
    $client = new Client();
    $response = $client->request('POST', 'http://cybercloudaws.cyberclouds.info/api/TezzSMS/OptOut', [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'json' => [
            "ApplicationName" => $app_name,
            "Token" => $token,
            "PhoneNumber" => $phone,
            "FirstName" => $user->first_name,
            "LastName" => $user->last_name
        ],
    ]);

    return json_decode($response->getBody()->getContents());
}
function sendEmail($subject,$email,$body,$cc=NULL){

    $url = 'http://cybercloudaws.cyberclouds.info/api/Email/SendEmail';
    $token = strtoupper(md5("TECHIE-KIDSCyberCloudAWS".date("m-d-Y")));
    $data = array(
        "ApplicationName" => "TECHIE-KIDS",
        "Token" => $token,
        "FromEmail" => "Techie Kids Club <no-reply@techiekidsclub.info>",
        "ToEmail" => [$email],
        "Subject" => $subject,
        "Body" => $body,
        "IsPassEmailBodyTemplate" => true
    );

    if($cc){
        $data["CcEmail"] = $cc; 
    }

    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
        CURLOPT_RETURNTRANSFER => true
    );

    $curl = curl_init();
    curl_setopt_array($curl, $options);
    $response = curl_exec($curl);

    // Check for errors and handle the response
    if (curl_errno($curl)) {
        return false;
    } else {
        $response = json_decode($response);
        return $response->Success;
    }

    curl_close($curl);
}
?>
