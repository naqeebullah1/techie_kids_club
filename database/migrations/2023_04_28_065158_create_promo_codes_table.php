<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoCodesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->id();
            $table->string('promo_code');
            $table->date('from_date');
            $table->date('to_date');
            $table->string('promo_type', 10);
            $table->float('percent_off')->nullable();
            $table->float('flat_amount')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('promo_codes');
    }

}
