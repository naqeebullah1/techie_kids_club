<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPromotionFieldsInAddmissionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admission_details', function (Blueprint $table) {
            $table->float('discount',6,2)->nullable();
            $table->foreignId('promo_code_id')->nullable();
            $table->float('percent_off',6,2)->nullable();
            $table->float('flat_amount',6,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admission_details', function (Blueprint $table) {
             $table->dropColumn('discount');
            $table->dropColumn('promo_code_id');
            $table->dropColumn('percent_off');
            $table->dropColumn('flat_amount');
        });
    }
}
