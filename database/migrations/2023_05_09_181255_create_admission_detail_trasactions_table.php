<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmissionDetailTrasactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_detail_trasactions', function (Blueprint $table) {
            $table->id();
            $table->string('trasaction_id');
            $table->integer('admission_detail_id')->unsigned();
            $table->string('charge_id');
            $table->string('customer');
            $table->string('subscription');
            $table->string('payment_intent');
            $table->string('start_date');
            $table->string('end_date');
            $table->float('amount_due');
            $table->float('amount_paid');
            $table->tinyInteger('paid_status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_detail_trasactions');
    }
}
