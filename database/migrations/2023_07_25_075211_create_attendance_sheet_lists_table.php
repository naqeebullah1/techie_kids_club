<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceSheetListsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attendance_sheet_lists', function (Blueprint $table) {
            $table->id();
            $table->integer('attendance_sheet_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->tinyInteger('is_present')->unsigned();
            $table->string('note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('attendance_sheet_lists');
    }

}
