<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdInBroadcastClassesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('broadcast_classes', function (Blueprint $table) {
            $table->foreignId('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('broadcast_classes', function (Blueprint $table) {
            $table->dropColumn('school_id');
            //
        });
    }

}
