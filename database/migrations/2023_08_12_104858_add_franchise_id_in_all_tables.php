<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFranchiseIdInAllTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $tables = [
            'admissions',
            'admission_details',
            'admission_detail_trasactions',
            'attendance_sheets',
            'attendance_sheet_lists',
            'banners',
            'broadcasts',
            'broadcast_classes',
            'broadcast_users',
            'categories',
            'category_galleries',
            'contacts',
            'demand_liberaries',
            'districts',
            'footers',
            'non_schedule_times',
            'notifications',
            'notification_viewers',
            'pages',
            'page_details',
            'page_detail_sections',
            'permissions',
            'price_settings',
            'promo_codes',
            'questions',
            'roles',
//            'role_has_permissions',
            'schools',
            'school_classes',
            'services',
            'settings',
            'students',
            'teacher_notes',
            'teacher_note_user',
            'teams',
            'testimonials',
            'users'
        ];
        foreach ($tables as $tableName):
            Schema::table($tableName, function (Blueprint $table) {
                $table->foreignId('franchise_id')->default(1);
            });
        endforeach;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $tables = [
            'admissions',
            'admission_details',
            'admission_detail_trasactions',
            'attendance_sheets',
            'attendance_sheet_lists',
            'banners',
            'broadcasts',
            'broadcast_classes',
            'broadcast_users',
            'categories',
            'category_galleries',
            'contacts',
            'demand_liberaries',
            'districts',
            'footers',
            'non_schedule_times',
            'notifications',
            'notification_viewers',
            'pages',
            'page_details',
            'page_detail_sections',
            'permissions',
            'price_settings',
            'promo_codes',
            'questions',
            'roles',
//            'role_has_permissions',
            'schools',
            'school_classes',
            'services',
            'settings',
            'students',
            'teacher_notes',
            'teacher_note_user',
            'teams',
            'testimonials',
            'users'
        ];
        foreach ($tables as $tableName):
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('franchise_id');
            });
        endforeach;
    }

}
