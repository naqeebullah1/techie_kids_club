<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdInNotificationViewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_viewers', function (Blueprint $table) {
             $table->foreignId('school_id');
        });
         Schema::table('notifications', function (Blueprint $table) {
            $table->dropColumn('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_viewers', function (Blueprint $table) {
             $table->dropColumn('school_id');
        });
         Schema::table('notifications', function (Blueprint $table) {
            $table->foreignId('school_id');
        });
    }
}
