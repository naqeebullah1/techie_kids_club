<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogoFieldsInFranchisesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('franchises', function (Blueprint $table) {
            $table->string('header_logo')->nullable();
            $table->string('footer_logo')->nullable();
            $table->string('no_image_logo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('header_logo');
            $table->dropColumn('footer_logo');
            $table->dropColumn('no_image_logo');
        });
    }

}
