<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripeAddionalFieldsInFranchisesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('franchises', function (Blueprint $table) {
            $table->string('stripe_publishable')->nullable();
            $table->string('stripe_end_point_secret')->nullable();
            $table->string('mail_email')->nullable();
            $table->string('mail_password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('franchises', function (Blueprint $table) {
            $table->dropColumn('stripe_publishable');
            $table->dropColumn('stripe_end_point_secret');
            $table->dropColumn('mail_email');
            $table->dropColumn('mail_password');
        });
    }

}
