<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Franchise;

class FranchiseTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     * php artisan db:seed --class=FranchiseTableSeeder
     */
    public function run() {
//        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
        $permissions = [
                [
                'name' => 'Techie Kids Club',
                'stripe_client' => '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',
                'stripe_end_point_secret' => 'whsec_X1o9FmVGJ2yZRxcIfVfj5xHAoNWMXJaU',
                'slug' => 'techie-kids-club',
                'user_id' => 1
            ],
        ];
        foreach ($permissions as $permission) {
            Franchise::create($permission);
        }
    }

}
