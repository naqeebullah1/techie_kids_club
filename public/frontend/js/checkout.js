// Get API Key
let STRIPE_PUBLISHABLE_KEY = document.currentScript.getAttribute('STRIPE_PUBLISHABLE_KEY');
let STRIPE_PAY_URL = document.currentScript.getAttribute('STRIPE_PAY_URL');

// Create an instance of the Stripe object and set your publishable API key
const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);

// Select subscription form element
const subscrFrm = document.querySelector("#subscrFrm");


// Attach an event handler to subscription form

subscrFrm.addEventListener("submit", handleSubscrSubmit);

let elements = stripe.elements();
var style = {
  base: {
    iconColor: '#666EE8',
    color: '#31325F',
    lineHeight: '40px',
    fontWeight: 300,
    fontFamily: 'Helvetica Neue',
    fontSize: '15px',

    '::placeholder': {
      color: '#CFD7E0',
    },
  },
};
//let cardNumberElement = elements.create('card', {style: style});


                
var cardNumberElement = elements.create('cardNumber', {
  style: style,
  placeholder: '',
});
var cardExpiryElement = elements.create('cardExpiry', {
  style: style,
  placeholder: 'MM/YY',
});

var cardCvcElement = elements.create('cardCvc', {
  style: style,
  placeholder: '',
});

cardNumberElement.mount('#cardnumber');

cardExpiryElement.mount('#expirationdate');

cardCvcElement.mount('#securitycode');

cardNumberElement.on('change', function (event) {
  
    if (event.brand) {
        setBrandIcon(event.brand);
    }
  
    displayError(event);
});

cardExpiryElement.on('change', function (event) {
    displayError(event);
});

cardCvcElement.on('change', function (event) {
    displayError(event);
});
//cardNumberElement.mount('#card-element');


var cardBrandToPfClass = {
    'visa': 'pf-visa',
  'mastercard': 'pf-mastercard',
  'amex': 'pf-american-express',
  'discover': 'pf-discover',
  'diners': 'pf-diners',
  'jcb': 'pf-jcb',
  'unknown': 'pf-credit-card',
}

function setBrandIcon(brand) {
    var brandIconElement = document.getElementById('brand-icon');
  var pfClass = 'pf-credit-card';
  if (brand in cardBrandToPfClass) {
    pfClass = cardBrandToPfClass[brand];
  }
  for (var i = brandIconElement.classList.length - 1; i >= 0; i--) {
    brandIconElement.classList.remove(brandIconElement.classList[i]);
  }
  brandIconElement.classList.add('pf');
  brandIconElement.classList.add(pfClass);
}

function displayError(event) {
    if (typeof event.error.message !== 'undefined') {
        showMessage(event.error.message,2);
    } else if (typeof event.error !== 'undefined') {
        showMessage(event.error,2);
    }
    document.querySelector("#addSubCard").disabled = false;
                            document.querySelector("#addSubCard").innerHTML = "Add Card";
}

async function handleSubscrSubmit(e) {
    e.preventDefault();
    document.querySelector("#addSubCard").disabled = true;
    document.querySelector("#addSubCard").innerHTML = "Please wait....";
       /* var options = {
        address_zip: document.getElementById('postal-code').value,
      };*/
  
    setLoading(true);

    let franchise_id = $("#choosenFranchise").val();
    
    let customer_name = $("#name").val();


    let cardinfo = stripe.createPaymentMethod({
    type: 'card',
    card: cardNumberElement,
    billing_details: {
      name: customer_name,
    },
  })
  .then(function(result) {
    // Handle result.error or result.paymentMethod
    var pmtid = result.paymentMethod;
     fetch(STRIPE_PAY_URL, {
        method: "POST",
        headers: {"Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": $('input[name="_token"]').val()},
        body: JSON.stringify({request_type: 'create_customer_card',pmtinfo:pmtid, name: customer_name,franchise:franchise_id
        }),
    })
            .then(response => response.json())
            .then(data => {
                if (data.payment_id == "DUPLICATION") {
                   
                            cardNumberElement.clear();
                            cardExpiryElement.clear();
                            cardCvcElement.clear();
                            $("#name").val('');
                             //console.log(data.success.message);
                             showMessage("Card already exists, please try another card",2);
                             document.querySelector("#addSubCard").disabled = false;
                            document.querySelector("#addSubCard").innerHTML = "Add Card";
                             
                    
                } else if(data.payment_id) {
                        cardNumberElement.clear();
                        cardExpiryElement.clear();
                        cardCvcElement.clear();
                        $("#name").val('');
                        showMessage("Card added successfully",1);
                        document.querySelector("#addSubCard").disabled = false;
                        document.querySelector("#addSubCard").innerHTML = "Add Card";
                        location.reload();  
                } else {
                   
                    
                    cardNumberElement.clear();
                    cardExpiryElement.clear();
                    cardCvcElement.clear();
                    $("#name").val('');
                    console.log(data.error.message);
                    showMessage(data.error.message,2);
                    document.querySelector("#addSubCard").disabled = false;
                document.querySelector("#addSubCard").innerHTML = "Add Card";
                    
                }
                
                
                            setLoading(false);
            })
            .catch(console.error); 
  });
            
}


// Display message
function showMessage(messageText,status) {
    var messageContainer = "";
    console.log(status);
    if(status == 1) {
         messageContainer = document.querySelector("#paymentSuccessResponse");
    }
    else {
         messageContainer = document.querySelector("#paymentErrorResponse");
    }

    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;

    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 15000);
}

// Show a spinner on payment submission
function setLoading(isLoading) {
   /*if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("#submitBtn").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#buttonText").classList.add("hidden");
    } else {
        // Enable the button and hide spinner
        document.querySelector("#submitBtn").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#buttonText").classList.remove("hidden");
    }*/
}
