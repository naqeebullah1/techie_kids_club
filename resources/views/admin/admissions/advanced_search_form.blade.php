<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('advanced-search')">{{ __('Advanced Search') }}
                    <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = "display:none";

            if (request()->has('first_name')) {
                $display = "display:block";
            }
            ?>

            <div class="card-body" style="{{ $display }}" id="advanced-search">
                <form method="get" id="searchform" accept-charset="utf-8"
                      action="{{url('/')}}/backend/admissions">
                    <div class="row">
                        <div class="col-lg-2">
                            <input type="text" name="first_name" class="form-control" placeholder="Parent First Name"
                                   value="<?php if($request->first_name){ echo $request->first_name; } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="last_name" class="form-control" placeholder="Parent Last Name"
                                   value="<?php if($request->last_name){ echo $request->last_name; } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="phone" class="form-control" placeholder="Phone"
                                   value="<?php if($request->phone){ echo $request->phone; } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="email" name="email" class="form-control" placeholder="Email"
                                   value="<?php if($request->email){ echo $request->email; } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="from_date" class=" searchfield form-control datepicker" placeholder="From Date" id="from_date" 
                            value="<?php if($request->from_date){ echo $request->from_date; } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="to_date" class=" searchfield form-control datepicker" placeholder="To Date" id="to_date"
                            value="<?php if($request->to_date){ echo $request->to_date; } else { echo ''; } ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <?php $resetUrl = url('/')."/backend/admissions"; ?>
                            <input type="button" name="reset" class="btn btn-success" style="margin-top:25px;"
                                   value="Reset" onClick="window.location.assign('{{$resetUrl}}')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;"
                                   value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function loadClasses() {
        // alert('1');
        var $url = "{{ route('admin.class-dd') }}";
        var sId = $('#schoolname').val();
        var classId = "<?php echo old('class_id') ?>";
        if (classId) {
            classId = "<?php echo old('class_id') ?>";
        } else {
            classId = "";
        }

        $url = $url + "?school_id=" + sId + "&class_id=" + classId;
        if (sId != "") {
            $('#class_id').html('<option value="">Select Class</option>');
            $.get($url, function (data) {
                $('#class_id').html(data.html);
            });
        } else {
            $('#class_id').html('<option value="">Select Class</option>');
        }
    }

    function loadChild() {
        // alert('1');
        var $url = "{{ route('admin.child-dd') }}";
        var sId = $('#parent_id').val();
        var childId = "<?php echo old('child_id') ?>";
        if (childId) {
            childId = "<?php echo old('child_id') ?>";
        } else {
            childId = "";
        }
        $url = $url + "?parent_id=" + sId + "&child_id=" + childId;
        if (sId != "") {
            $('#child_id').html('<option value="">Select Child</option>');
            $.get($url, function (data) {
                $('#child_id').html(data.html);
            });
        } else {
            $('#child_id').html('<option value="">Select Class</option>');
        }
    }

    $('#reset-btn').click(function (e) {
        e.preventDefault();
        window.location.href = '/backend/admissions'
    });

</script>
<?php
if (old('parent_id')) {
    echo "<script>loadChild()</script>";
}
if (old('school_id')) {
    echo "<script>loadClasses()</script>";
}

?>
