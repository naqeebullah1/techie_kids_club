
<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>ID</th>
            <th>Parent Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Next due date</th>
            <th>Amount</th>
            <th>Total Classes</th>
            <th>Details</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @forelse($admissions as $k => $purchase)
        <?php
        $details = $purchase->details;
        $parent = $purchase->admission_parent;
        ?>
        <tr>
            <td width="10px"><?= $purchase->id ?></td>
            <td><?= ($parent)?ucwords($parent->first_name.' '.$parent->last_name):'' ?></td>
            <td><?= ($parent)?$parent->phone1:'' ?></td>
            <td><?= ($parent)?$parent->email:'' ?></td>
            <td>
                <?php $planstarted = isset($details[0])?$details[0]->plan_period_start:$purchase->created_at; ?>
                <?= date('m/d/Y', strtotime('+1 month +1 day', strtotime($planstarted))) ?>
            </td>

            <td>$<?= $details->sum(function($detail) {
    return ($detail->paid_amount>=1 )?$detail->paid_amount:$detail->class_charges - $detail->discount;
}); ?></td>
            <td><?= $details->count() ?></td>
            <td>
                <a href="<?= url('parent/add-details/' . $purchase->id) ?>" onclick="return getDetails(this)" class="btn btn-info btn-sm btn-small">View</a>
            </td>
            <td>
                <?php
                $cc = 'badge-success';
                if ($purchase->status == 'Pending') {
                    $cc = 'badge-warning ';
                }
                ?>
                <span class="badge <?= $cc ?>"><?= $purchase->status ?></span>
            </td>

        </tr>
        @empty
        <tr>
            <td colspan="100%">No Record Found</td>
        </tr>
        @endforelse
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $admissions->links() }}
