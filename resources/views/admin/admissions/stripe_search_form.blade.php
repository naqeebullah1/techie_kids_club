<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Fetch Stripe Payment') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
           <?php
            $display = "display:none";
            
            if(old('school_id') != "") {
                $display="display:block";        
            }
           ?>
            
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <form method="post" id="searchform" accept-charset="utf-8" action="{{route('admin.admission.search_stripe')}}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-2">
                            <select name="school_id" id="schoolname" class="form-control" onChange='loadClasses()' required>
                                <option value="">Select School</option>
                                @foreach($schools as $school)
                                
                                <option <?php if(old('school_id') == $school->id){ echo 'selected'; } else { echo ''; } ?> value="{{ @$school->id }}">{{ $school->name }}</option>
                                
                                @endforeach
                            </select>
                                 
                        </div>
                        <div class="col-lg-2">
                           <select name="class_id" id="class_id" class="form-control" required>
                                <option value="">Select Class</option>
                                
                            </select>   
                        </div>
                        <div class="col-lg-2">
                            <select name="parent_id" id="parent_id" class="form-control" onChange='loadChild()' required>
                                <option value="">Select Parent</option>
                                @foreach($parents as $parent)
                                
                                <option <?php if(old('parent_id') == $parent->id){ echo 'selected'; } else { echo ''; } ?> value="{{ @$parent->id }}">{{ $parent->last_name }} - {{ $parent->email }}</option>
                                
                                @endforeach
                            </select>  
                                
                        </div>
                        <div class="col-lg-2">
                            <select name="child_id" id="child_id" class="form-control">
                                <option value="">Select Child</option>
                                
                            </select> 

                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="classroom_name" class="form-control" placeholder="Classroom Name" required value="<?php if(old('classroom_name')){ echo old('classroom_name'); } else { echo ''; } ?>">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" name="subscription_id" class="form-control" placeholder="Stripe Subscription ID" required value="<?php if(old('subscription_id')){ echo old('subscription_id'); } else { echo ''; } ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-success" style="margin-top:25px;" value="Reset" onClick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;" value="Fetch Payment Status" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
function loadClasses() {
       // alert('1');
       var $url = "{{ route('admin.class-dd') }}";
        var sId = $('#schoolname').val();
         var classId = "<?php echo old('class_id') ?>";
        if(classId) {
            classId = "<?php echo old('class_id') ?>";
        } else {
            classId = "";
        }
        
        $url = $url + "?school_id=" + sId + "&class_id="+classId;
        if(sId != "") {
            $('#class_id').html('<option value="">Select Class</option>');  
            $.get($url, function (data) {
                $('#class_id').html(data.html);
            });
        } else {
            $('#class_id').html('<option value="">Select Class</option>');   
        }
    }  
    
    function loadChild() {
       // alert('1');
       var $url = "{{ route('admin.child-dd') }}";
        var sId = $('#parent_id').val();
        var childId = "<?php echo old('child_id') ?>";
        if(childId) {
            childId = "<?php echo old('child_id') ?>";
        } else {
            childId = "";
        }
        $url = $url + "?parent_id=" + sId + "&child_id=" + childId;
        if(sId != "") {
            $('#child_id').html('<option value="">Select Child</option>');  
            $.get($url, function (data) {
                $('#child_id').html(data.html);
            });
        } else {
            $('#child_id').html('<option value="">Select Class</option>');   
        }
    }  

</script>
<?php
if(old('parent_id')) {
                echo "<script>loadChild()</script>";
            }
if(old('school_id')) {
                echo "<script>loadClasses()</script>";
            }
            
?>
