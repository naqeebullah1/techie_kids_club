<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit Broadcast') }}
                    @else
                    {{ __('Create Broadcast') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" onsubmit="return validateForm()" enctype="multipart/form-data" action="{{ route('admin.broadcasts.store') }}">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-lg-3">
                            <label for="all-schools">
                                <input
                                    type="checkbox" id="all-schools" name="all-schools" value="1" onchange="getClasses()"> All Schools
                            </label>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-3">
                            <input type="hidden" value="<?= request()->id ?>" name="id">
                            <label>State</label>
                            {{ Form::select('record[state_id]',$states,$school->state_id,['onchange'=>"citiesDD(1)",'required','class'=>'select2','id'=>'state_id']) }}
                        </div>
                        <div class="col-lg-3">
                            <?php
                            $cities = [];
                            $schools = [];
                            if ($school->city_id) {
                                $cities = App\Models\School::select('cities.city', 'cities.id')->join('cities', 'cities.id', 'schools.city_id')
                                                ->where('state_id', $school->state_id)
                                                ->pluck('city', 'id')->toArray();
                                $schools = App\Models\School::select('name', 'id')
                                                ->where('city_id', $school->city_id)
                                                ->pluck('name', 'id')->toArray();
                            }
                            ?>
                            <label>City</label>
                            {{ Form::select('record[city_id]',[''=>'Select City']+$cities,$school->city_id,['required','class'=>'select2','onchange'=>'schoolDD();getClasses();','id'=>'city_id']) }}
                        </div>
                        <div class="col-lg-6">
                            <label>School [showing schools that contain classes with admissions]</label>
                            {{ Form::select('school_id[]',$schools,$school->school_id,['onchange'=>'getClasses()', 'multiple','class'=>'select2','id'=>'school_id']) }}
                        </div>
                    </div>
                    <div class="row school-classes-container">
                        @if($school->school_id)
                        <?php
                        $viewers = $school->viewers->pluck('school_class_id')->toArray();
                        $classes = \App\Models\SchoolClass::where('school_id', $school->school_id)->select('title', 'id')->get();

                        $schoolHtml = '<div class="mt-2 mb-2 col-12" style="font-weight:bold;">Select Classes</div>';

                        foreach ($classes as $schoolClass):
                            $checked = '';
                            if (in_array($schoolClass->id, $viewers)) {
                                $checked = 'checked';
                            }

                            $schoolHtml .= '<div class="col-2">'
                                    . '<label>'
                                    . '<input ' . $checked . ' name="school_class_ids[]" type="checkbox" value="' . $schoolClass->id . '"> ' . $schoolClass->title
                                    . '</label>'
                                    . '</div>';
                        endforeach;
                        echo $schoolHtml;
                        ?>
                        @endif

                    </div>

                    <div class="row mt-3">
                        <div class="col-12">
                            <label>Title <small class="text-primary">[Limit: 20 Characters]</small></label>
                            <input type="text" class="form-control" name="record[title]" maxlength="20" required>
                        </div>
                        <div class="col-12">
                            <label>Description <small class="text-primary">[Limit: 27 Characters]</small></label>
                            <textarea class="form-control" id="form-description" name="record[description]" maxlength="27" required></textarea>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>

                        <button type="submit" class="mt-2 btn btn-success" onclick="return submitform101();this.disabled = true; this.value = 'Sending SMS…';">Send Message</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function schoolDD() {
        var $url = "<?= url('schools-parents-dd') ?>";
        var cId = $('#city_id').val();

        $url = $url + "?city_id=" + cId;

        if (cId != "") {
            $('#school_id').html('<option value="">Select School</option>');
            $.get($url, function (data) {
                $('#school_id').html(data);
            });
        } else {
            $('#school_id').html('<option value="">Select School</option>');
        }
    }
    @if(request()->id && $school->state_id == null && $school->city_id == null)
        setTimeout(function () {
            $('#all-schools').prop('checked', true);
            getClasses();
        }, 1000);
    @endif
    $('#all-schools').on('change', function () {
        if($('#all-schools').is(':checked')) {
            $('#school_id').attr('disabled', true);
            $('#city_id').attr('disabled', true);
            $('#state_id').attr('disabled', true);
        } else {
            $('#school_id').attr('disabled', false);
            $('#city_id').attr('disabled', false);
            $('#state_id').attr('disabled', false);
        }
    });
    function getClasses() {
        var $url = "<?= url('school-classes-parents-dd') ?>";
        var cId = $('#school_id').val();
        $url = $url + "?school_id=" + cId + "&is_dd=1&city_id=" + $('#city_id').val();

        if($('#all-schools').is(':checked')) {
            $url += '&all_schools=1';
            $.get($url, function (data) {
                $('.school-classes-container').html(data);
                $('.ajax-select2').select2();
            });
        } else {
            $('.school-classes-container').html('');
        }

        if ($('#city_id').val() != "") {
            $.get($url, function (data) {
                $('.school-classes-container').html(data);
                $('.ajax-select2').select2();
            });
        } else {
            $('.school-classes-container').html('');
        }
    }
    function citiesDD() {
         var $url = "<?php echo url('/').'/front-franchise/'. App\Models\Franchise::where('id',auth()->user()->franchise_id)->first()->slug; ?>/cities-dd";
        var sId = $('#state_id').val();

        $url = $url + "?state_id=" + sId + "&city_id=&ignore_without_schools=1";
        if (sId != "") {
            $('#city_id').html('<option value="">Select City</option>');
            $.get($url, function (data) {
                $('#city_id').html(data.html);
            });
        } else {
            $('#city_id').html('<option value="">Select City</option>');
        }
    }


    function submitform101() {
//        alert($('#school_class_ids').val());
        if ($('#school_class_ids').val() == '') {
            alert("Please select atleast one class");
            return false;
        }

    }
</script>
