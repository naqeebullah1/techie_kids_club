@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Broadcasts</h1>
    <div class="clearfix"></div>

    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>
    @can('schools-edit')
    <a onclick="event.preventDefault();loadForm(this)" class="btn btn-success float-right" href="<?= url('backend/broadcasts-create') ?>">Create Broadcast</a>
    @endcan

    <div class="clearfix"></div>
</div>
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row table-responsive" id="no-more-tables">
                    @include('admin.broadcasts.table')
                </div>

            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
<script>

    function isValidUrl(str) {
        const pattern = new RegExp(
                '^([a-zA-Z]+:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', // fragment locator
                'i'
                );
        return pattern.test(str);
    }

//console.log(isValidUrl('https://codingbeautydev.com')); // true
//console.log(isValidUrl('codingbeautydev.co')); // true
//console.log(isValidUrl('https://codingbeautydev.com')); // false
    function validateForm() {
//        alert();
        var descriptionString = $('#form-description').val().split(" ");
//        console.log(descriptionString);
        $c = false;
        for (var i = 0; i < descriptionString.length; i++) {
            var eachValue = descriptionString[i];
            $c = isValidUrl(eachValue);

        }
        if ($c) {
            alert("URL Not Allowed");
            return false;
        } else {
            return true;
        }

    }
</script>
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif

@endsection