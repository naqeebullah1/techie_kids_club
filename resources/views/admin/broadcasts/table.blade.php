<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>State</th>
            <th>City</th>
            <th>School</th>
            <th>Classes</th>
            <th>SMS Sent</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schools as $school)
        <tr>
            <td>
                ِ<?= $school->title ?>َ
            </td>
            <td>
                <?= $school->description ?>
            </td>
            <td>
                <?= $school->state === null ? 'All States' : $school->state->state ?>
            </td>
            <td>
                <?= $school->city === null ? 'All States' : $school->city->city ?>
            </td>
            <td>
               <ul>
                <?php
                    echo "<li>" . str_replace ("," , "</li><li>" , $school->school_titles) . "</li>";
                ?>
                </ul>
            </td>
            <td>
                <ul>
                <?php
                    echo "<li>" . str_replace ("," , "</li><li>" , $school->class_title) . "</li>";
                ?>
                </ul>
            </td>
            <td>
                <?php
                    $sms_total = $school->success_sent + $school->failed_sent;
                ?>
                <?= $school->success_sent ?> / <?= $sms_total ?>
            </td>
            <!--<td>
                <?= $school->failed_sent ?>
            </td>-->

        </tr>
        @endforeach


    </tbody>
</table>

    {{ $schools->links() }}
