<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>City</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cities as $city)
        <tr>
            <td>
                <?= $city->city ?>
            </td>
            <td>
                @can('cities-edit')

                <a href="{{ url('backend/change-status/cities/'.$city->id,$city->status) }}">

                    <span class="span-status {{ $city->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $city->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>
                @else
                <span class="span-status {{ $city->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $city->status == 1 ? __('Active') : __('Blocked') }}
                </span>
                @endcan

            </td>
            <td>
                 @can('cities-edit')
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/cities-create/' . $city->state_code . '/' . $city->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.cities.destroy', $city->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                 @endcan
            </td>
        </tr>
        @endforeach
        @can('page.edit')
        <tr>
            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $cities->links() }}
