<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>District</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($districts as $district)
        <tr>
            <td>
                <?= $district->name ?>
            </td>
            
            <td>               
                @can('states-edit')

                <a href="{{ url('backend/change-status/districts/'.$district->id,$district->status) }}">

                    <span class="span-status {{ $district->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $district->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>
                @else
                    <span class="span-status {{ $district->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $district->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                @endcan
            </td>
            <td>
                @can('states-edit')
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/districts/create/' . $district->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.districts.destroy', $district->id) }}'); deleteConfirm('destroy', 'Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $districts->links() }}
