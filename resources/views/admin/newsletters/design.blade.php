@extends('master')
@section('content')
    <div class="mb-2">
        <h1 class="float-left">Newsletter Design</h1>
        <a class="btn btn-default m-4 pull-right" href="{{route('admin.newsletter.index')}}">&leftarrow;Go back</a>
        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    Newsletter Design Details
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.newsletter.designStore') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <label class="col-12">Title</label>
                            <input type="text" class="form-control"
                                   value="@if($newsletterDesign->title) {{ $newsletterDesign->title }} @endif"
                                   name="title"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <label>Content</label>
                            <textarea class="form-control ckeditor"
                                      name="content">@if($newsletterDesign->content)
                                    {{$newsletterDesign->content}}
                                @endif</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <label>Image</label>
                            <input type="file" accept="image/png, image/gif, image/jpeg"
                                   class="dropify @error('image')  @enderror"
                                   @if($newsletterDesign->image)
                                       data-default-file="{{ asset('/frontend/images/newsletter/'.$newsletterDesign->image) }}"
                                   @endif name="image"
                            value="@if($newsletterDesign->image){{ asset('/frontend/images/newsletter/'.$newsletterDesign->image) }}@endif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12 text-right">
                            <button type="submit" class="mt-2 btn btn-success">
                                @if($newsletterDesign==null)
                                    Add
                                @else
                                    Update
                                @endif
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
