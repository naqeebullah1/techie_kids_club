@extends('master')
@section('content')
    <div class="mb-2">
        <h1 class="float-left">Newsletters</h1>
        <a class="btn btn-default float-right m-4" href="{{route('admin.newsletter.design')}}">Newsletter Design</a>
        <div class="clearfix"></div>

        <table class="sortable table table-bordered table-small" id="newsletter-table">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @forelse($newsletters as $newsletter)
                <tr>
                    <td width="10px">{{ucfirst($newsletter->firstName)}}</td>
                    <td width="10px">{{ucfirst($newsletter->lastName)}}</td>
                    <td width="10px">
                        @if($newsletter->parent_id && App\Models\User::find($newsletter->parent_id)->hasRole(3))
                            <a style="font-weight:bold;" href="{{url('/')}}/parent/children/{{$newsletter->parent_id}}?linked=1"><i class="fa fa-link"></i> <?= $newsletter->email ?></a>
                        @else
                                <?= $newsletter->email ?>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%">No Record Found</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <form
            id="destroy"
            method="post" id="destroy"
            action="">
            @csrf
            @method('DELETE')
        </form>
        {{ $newsletters->links() }}

{{--        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">--}}
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">

        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>


        @include('partials.loadmorejs')
        @endsection
        @section('style')
            <style>
                .dcare__btn {
                    align-self: center;
                }
                div.dt-buttons {
                    float: right !important;
                }
                div.dt-buttons button {
                    background-color: #0e90d2;
                }
            </style>
        @endsection

        @section('script')
            @if(Session::has('outcome'))
                <script>
                    $(function () {
                        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
                    });
                </script>

            @endif
            <script>
                function removePD() {
                    $('.purchase-order-details').html('');
                    $('.purchase-orders').removeClass('d-none');
                }

                function getDetails($this) {
                    $.get($this, function (data) {
                        $('.purchase-order-details').html(data.html);
                        $('.purchase-orders').addClass('d-none');
                        $('.dcare__btn').attr('style', '');
                        $('.dcare__btn').addClass('btn btn-primary');
                    });
                    return false;

                }
            </script>
                <script>
                    var table = $('#newsletter-table').DataTable({
                        dom: 'B',
                        buttons: [
                            {"extend": 'excel', "text":'Export Excel',"className": 'btn', 'css': "float: right;"}
                        ],
                    });
                </script>
    </div>
@endsection
