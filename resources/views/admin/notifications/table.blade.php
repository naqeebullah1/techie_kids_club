<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>State</th>
            <th>City</th>
            <th>School</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schools as $school)
        <tr>
            <td>
                ِ<?= $school->title ?>َ
            </td>
            <td>
                <?= $school->description ?>
            </td>
            <td>
                <?= $school->state === null ? 'All States' : $school->state->state ?>
            </td>
            <td>
                <?= $school->city === null ? 'All States' : $school->city->city ?>
            </td>
            <td>
                <ul>
                    <?php
                    echo "<li>" . str_replace(",", "</li><li>", $school->school_titles) . "</li>";
                    ?>
                </ul>
            </td>

            <td>
                @can('notifications-edit')
                <a href="{{ url('backend/change-status/notifications/'.$school->id,$school->status) }}">

                    <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>

                @else
                <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                </span>
                @endcan
            </td>



            <td nowrap>
                @can('notifications-edit')
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/notifications-create/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.notifications.destroy', $school->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach
        @can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>
        @endcan
    </tbody>
</table>
<form
    id="destroy"
    method="post" id="destroy"
    action="">
    @csrf
    @method('DELETE')
</form>
@if(count($schools->items()) > $schools->perPage())
    {{ $schools->links() }}
@endif
