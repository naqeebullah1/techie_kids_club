<h4 class="font-weight-bold">Schools Classes</h4>
<div style="padding:10px;background: white">

    <table class="sortable table table-striped table-custom table-small">
        <thead>
            <tr>
                <th>School</th>
                <th>Title</th>
                <th>Teacher</th>
                <th>Charges</th>
                <th>Duration Of Class</th>
                <th>Timimg Of Class</th>
                <th>Status</th>
               
            </tr>
        </thead>
        <tbody>
            @forelse($classes as $city)
            <tr>
                <td>
                    <?= $city->school ?>
                </td>
                <td>
                    <?= $city->title ?>
                </td>
                <td>
                    <?= $city->teacher ?>
                </td>
                <td>
                    $<?= $city->class_charges ?>
                </td>
                <td>
                    <b>From: </b><?= formatDate($city->start_on) ?><br>
                    <b>To: </b> <?= formatDate($city->end_on) ?>
                </td>
                <td>
                    <b>From: </b><?= formatTime($city->start_at) ?><br>
                    <b>To: </b> <?= formatTime($city->end_at) ?>
                </td>
                <td>
                    <span class="span-status {{ $city->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $city->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
</td>            
                     
              
            </tr>
            @empty
            <tr>
                <td colspan="100%" class="text-danger text-center">No record exists</td>
            </tr>
            @endforelse
            
        </tbody>
    </table>

    
    {{ $classes->links()->with('type','school-classes') }}

</div>
