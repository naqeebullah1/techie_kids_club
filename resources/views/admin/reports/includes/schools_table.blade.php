<h4 class="font-weight-bold">Schools</h4>
<div style="padding:10px;background: white">

    <table class="sortable table table-striped table-custom table-small">
    <thead>
        <tr>
            <th>Logo</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Contact</th>
            <th>State</th>
            <th>City</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($schools as $school)
        <tr>
            <td>
                <img src="<?= asset('images/uploads/' . $school->image) ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" width="50px">

            </td>
            <td>
                <?= $school->name ?>
            </td>
            <td>
                <?= $school->address ?>
            </td>
            <td>
                <?= $school->email ?>
            </td>
            <td>
                <?= $school->contact ?>
            </td>
            <td>
                <?= $school->state ?>
            </td>
            <td>
                <?= $school->city ?>
            </td>
            <td>
                <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                </span>
            </td>
            <td>
                
                @can('school-classes-list')
                <?php 
                    if(in_array("Teacher",auth()->user()->roles()->pluck('name')->toArray())) { ?>
                        <a href="<?= url('/teacher/school-classes/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-success">
                    Classes
                </a>
                    <?php } else { ?>
                        <a href="<?= url('/backend/school-classes/' . $school->id) ?>" class="btn btn-small btn-sm btn-success">
                            Classes
                        </a>
                    <?php }
                    //var_dump(auth()->user()->roles()->pluck('name')->toArray());
                ?>
                
                @endcan
                
            </td>

        </tr>
            @empty
            <tr>
                <td colspan="100%" class="text-danger text-center">No record exists</td>
            </tr>
            @endforelse
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $schools->links()->with('type','schools') }}
</div>
