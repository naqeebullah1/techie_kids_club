@extends('master')
@section('title','Dashboard')
@section('content')
<style>
    .nav-item{
        border: 1px solid #069d4d;
    }
    .main-container > div{
        background-color: white;
    }
    .main-container > div h4{
        padding: 20px 0 0 20px !important;
    }
</style>
<div>
    <h1 class=" col-12">Search</h1>
    <div class="col-12 mt-2 mb-3 p-0">
        <ul class="nav nav-pills nav-fill"></ul>
    </div>

    @include('admin.reports.search_all_filters')
    <div class="clearfix"></div>
</div>

<div class="main-container"></div>

@include('partials.loadmorejs')
@endsection
@section('script')
<script>
    function printTable() {
        window.location = $('#searchform').attr('action') + '?' + keywords() + '&print=1';
        return false;
    }
    function keywords() {

        return 'keyword=' + $('#keyword').val() + '&only_this=' + $('#only_this').val();
    }
    $(function () {
        $(document).on('submit', '#searchform', function () {
            var url = $(this).attr('action') + '?' + keywords();
            callAjax(url);
            return false;
        });
        $(document).on('click', 'a.page-link', function () {
            var url = $(this).attr('href') + '&' + keywords();
            callAjax(url);
            return false;
        });
    });
    function callAjax(url) {
        $.ajax({
            type: "get",
            url: url,
            success: function (data) {
                $('.' + data.type + '-container').html(data.html);
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
        return false;
    }
</script>
@endsection
@section('style')
<style>
    .table-custom th{
        border-top: 0 !important;
    }
</style>
@endsection
