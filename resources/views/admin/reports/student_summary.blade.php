@extends('master')
@section('content')

<div class="mb-2">
    <h1 class="float-left">Student Summary Report</h1>
    <div class="clearfix"></div>
</div>

<div class="record-form"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card mb-2">
            <div class="card-header bg-light mb-3">
                <h4 class="font-weight-bold m-0">
                    Filters 
                    <!--<span class="fa fa-filter"></span>-->
                </h4>
            </div>
            <div class="card-body">
                <form action="" onsubmit="return loadSchedule(this)" method="post" >
                    <div class="row">
                        <div class="col-3">
                            {{ Form::select('district_id',[''=>"Select District"]+$districts->toArray(),null,['class'=>'select2','required']) }}
                        </div>
                        <div class="col-1">
                            <button class="btn btn-success" type="submit">
                                Search
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="card">
            <!--            <div class="card-header">
                            <div class="card-title pull-left">Districts</div>
                        </div>-->
            <div class="card-body">
                <div class="row">
                    <!--CODE HERE-->
                    <div class="col-12 table-responsive" id='calendar'></div>
                </div>

            </div>
        </div>
    </div>
</div>


@include('partials.loadmorejs')
@endsection
@section('script')
<script src='<?= asset('event_calendar/dist/index.global.js') ?>'></script>

<script>

                    function loadSchedule($this) {
                        var $url = '<?= url("backend/reports/students-summary-report") ?>';

                        $.ajax({
                            type: 'get',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                $('#calendar').html($data);
                            }});

                        return false;
                    }

</script>

@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
@endsection