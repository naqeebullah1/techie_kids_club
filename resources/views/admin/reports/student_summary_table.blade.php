<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>RD</th>
            <th>School</th>
            <th>Teacher</th>
            @foreach($uniqueClasses as $uniqueClass)
            <th nowrap><?= $uniqueClass ?></th>
            @endforeach
            <th>Total Students</th>
        </tr>
    </thead>
    <tbody>

        @foreach($records as $teachers)
        @foreach($teachers as $record)
        
        <tr>
            <td><?= $record['rd'] ?></td>
            <td><?= $record['school_name'] ?></td>
            <td><?= $record['teacher_name'] ?></td>

            <?php
            $schoolClasses = $record['classes'];
            $total = 0;
            ?>
            @foreach($uniqueClasses as $id=>$uniqueClass)
            <td>
                <?php
                if (isset($schoolClasses[$uniqueClass])) {
                    echo $schoolClasses[$uniqueClass];
                    $total = $total + $schoolClasses[$uniqueClass];
                } else {
                    echo 'X';
                }
                ?>
            </td>
            @endforeach
            <td><?= $total ?></td>
        </tr>
        @endforeach
        @endforeach
    </tbody>
</table>
