@extends('master')
@section('content')
<style>
    .graph-container{
        background: #f8f8f8;
        margin-bottom: 10px; 
        border: 1px solid #ccc;
    }
    .rd-details h4{
        font-weight: bold;
    }
    .rd-details{
        background: #f8f8f8;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 10px;
        margin-left: 10px;
        border: 1px solid #ccc;
    }
</style>
<div class="mb-2">
    <h1 class="float-left">Teacher Summary</h1>
    <div class="clearfix"></div>
</div>

<div class="record-form"></div>

<div class="row">
    <div class="col-md-12">
        <div class="card mb-2">
            <div class="card-header bg-light mb-3">
                <h4 class="font-weight-bold m-0">
                    Filters 
                </h4>
            </div>
            <div class="card-body">
                <form action="" onsubmit="return barChart()" method="post" >
                    <div class="row">
                        <div class="col-3">
                            {{ Form::select('district_id',[''=>"Select District"]+$districts->toArray(),null,['id'=>'district_id','class'=>'select2','required']) }}
                        </div>
                        <div class="col-1">
                            <button class="btn btn-success" type="submit">
                                Search
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="card">
            <!--            <div class="card-header">
                            <div class="card-title pull-left">Districts</div>
                        </div>-->
            <div class="card-body">
                <div class="row" id='barchart-container'></div>
            </div>
        </div>
    </div>
</div>


@include('partials.loadmorejs')
@endsection
@section('script')
<script src='<?= asset('event_calendar/dist/index.global.js') ?>'></script>

<script>

                    function loadSchedule($this) {
                        var $url = '<?= url("backend/reports/students-summary-report") ?>';

                        $.ajax({
                            type: 'get',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                $('#calendar').html($data);
                            }});

                        return false;
                    }

</script>

@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>
    function barChart() {
        var $url = "<?= url('backend/teacher-summary-gr') ?>?district_id=" + $('#district_id').val();
        $.get($url, function ($records) {
           $('#barchart-container').html($records);
        });
        return false;
    }


</script>

@endsection