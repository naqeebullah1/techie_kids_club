<table class="table table-sm table-small table-bordered table-striped">
    <thead>
        <tr>
            <th>District</th>
            <th>RD</th>
            <th>Teacher</th>
            <th>Total Students</th>
        </tr>
    </thead>
    <tbody>
        @foreach($classes as $class)
        <tr>
            <td><?= $class->district_name ?></td>
            <td><?= $class->d_first_name.' '. $class->d_last_name ?></td>
            <td><?= $class->t_first_name.' '. $class->t_last_name ?></td>
            <td><?= $class->class_students ?></td>
        </tr>
        @endforeach
    </tbody>
</table>
