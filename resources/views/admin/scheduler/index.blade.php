@extends('master')
@section('content')
    <style>
        .datepicker-dropdown { z-index: 1001 !important; }
        .timepicker-popover{
            z-index: 1002 !important;
        }
        .fc-prev-button,.fc-today-button,.fc-next-button{
            display: none !important;
        }
        .fc-list-event-time {
            display: none;
        }
    </style>
    <div class="mb-2">
        <h1 class="float-left">Scheduler Screen</h1>
        <div class="clearfix"></div>
    </div>


    <div class="record-form"></div>


    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-light mb-3">
                    <h4 class="font-weight-bold m-0">
                        Filters
                        <!--<span class="fa fa-filter"></span>-->
                    </h4>
                </div>
                <div class="card-body">
                    <form action="" id="filter-form" onsubmit="return loadSchedule(this)" method="post" >
                        @csrf
                        <div class="row mb-2">
                            <div class="col-sm-12 col-md-4 col-lg-3">
                                {{ Form::select('filter[district_id]',[''=>"Select District"]+$districts->toArray(),null,['id'=>'district_id','onchange'=>'getTeachers()','class'=>'select2','required']) }}
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-2">
                                {{ Form::text('filter[month_year]',null,['class'=>'form-control monthPicker','placeholder'=>'Select Month/Year','required']) }}
                            </div>
                            <?php
                            $roleId = Auth::user()->role;
                            ?>
                            @if($roleId==2)
                                {{ Form::hidden('teacher_id[]',auth()->user()->id,['id'=>'teacher_id']) }}
                            @else
                                <div class="col-sm-12 col-md-4">
                                    {{ Form::select('teacher_id[]',[],null,['class'=>'select2 teacher_id','id'=>'teacher_id','required','multiple','data-placeholder'=>'Select Teachers']) }}
                                </div>
                            @endif
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-1">
                                <button class="btn btn-success btn-block" type="submit">
                                    Search
                                </button>
                            </div>
                            @if($roleId !=2)
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                                    <button class="btn btn-primary btn-block" id="addnotes" type="button">
                                        Add Appointment
                                    </button>
                                </div> 
                            @endif
                        </div>
                    </form>
                    <div class="notes-new-form"></div>
                    <h4 style="font-weight:bold;margin:30px 0px 10px 0px;">Legends
                        <br class="d-md-none d-sm-block">
                        <i data-toggle="tooltip" data-placement="top" title="" class="rounded-circle ml-2 mr-2" style="background: #3788d8;" data-original-title="Schedule"></i>
                        <i class="rounded-circle bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Schedule with Note"></i>
                        <i class="rounded-circle bg-primary mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Teacher Appointment"></i>
                    </h4>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!--CODE HERE-->
                        <div class="col-12" id='calendar'>
                            <!--<h4 class="text-danger">Apply above filters and click on Search button</h4>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('partials.loadmorejs')
@endsection
@section('script')
    <script src='<?= asset('event_calendar/dist/index.global.js') ?>'></script>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            $(".monthPicker").datepicker({
                format: "mm/yyyy",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });
            $(document).on('click', '.fc-listMonth-button', function (e) {
                $('.fc-list-event-title a').each(function (index, item) {
                    var href = $(item).attr('href');
                    $(item).attr('href', 'javascript:void(0)');
                    $(item).attr('data-href', href);
                });
            });
            $(document).on('click', '.fc-list-event-title a', function ($a) {

                var url = $(this).attr('data-href');
                var teacherId = $('#teacher_id').val();
                url += '&teacher_id=' + teacherId;
                $.get('<?= url('backend/get-notes-form') ?>/' + url, function ($html) {

                    if ($html.ref == '#side-container') {
//                                    alert('Allah');
                        $("#side-container .form").html($html.html);
                        $("#side-container").animate({right: '0'});

                        $('#note_date').datepicker({
                            autoclose: true
                        });
                        $('#from_time').timepicker();
                        $('#to_time').timepicker();
                    }
                    if ($html.ref == '.notes-new-form') {
                        $(".notes-new-form").html($html.html);
                        $('#school_id').select2();
                        $('#school_class_id').select2();

                        $('html, body').animate({
                            scrollTop: $(".notes-new-form").offset().top
                        }, 1000);
                    }

                });
                return false;
            });
            $(document).on('click', 'a.fc-event', function ($a) {

                var url = $(this).attr('href');
                var teacherId = $('#teacher_id').val();
                url += '&teacher_id=' + teacherId;
                $.get('<?= url('backend/get-notes-form') ?>/' + url, function ($html) {

                    if ($html.ref == '#side-container') {
//                                    alert('Allah');
                        $("#side-container .form").html($html.html);
                        $("#side-container").animate({right: '0'});

                        $('#note_date').datepicker({
                            autoclose: true
                        });
                        $('#from_time').timepicker();
                        $('#to_time').timepicker();
                    }
                    if ($html.ref == '.notes-new-form') {
                        $(".notes-new-form").html($html.html);
                        $('#school_id').select2();
                        $('#school_class_id').select2();

                        $('html, body').animate({
                            scrollTop: $(".notes-new-form").offset().top
                        }, 1000);
                    }

                });
                return false;
            });
        });

        function getNoteForm() {
            var url = $('#teacher_id').val();

//                        var url = url.join();

//                        if (url.length != 1) {
//                            $(".notes-new-form").html('');
//                            return false;
//                        }
            $.get('<?= url('backend/notes-form') ?>?teacher_id=' + url, function ($html) {
                $("#side-container .form").html($html);
                $('.select2').select2();
                $("#side-container").animate({right: '0'});

                $('#note_date').datepicker({
                    autoclose: true
                });
                $('#from_time').timepicker();
                $('#to_time').timepicker();
            });
            return false;
        }
        function getTeachers() {
            var url = $('#district_id').val();

            $.get('<?= url('backend/get-teacher') ?>?district_id=' + url, function ($html) {
                $(".teacher_id").html($html);
                $('.teacher_id').select2();
            });
            return false;
        }
        function loadClasses() {
            var $schoolId = $('#school_id').val();
            $.get('<?= url('load-school-classes') ?>?school_id=' + $schoolId + '&teacher_id=' + $('#teacher_id').val(), function ($html) {
                $('#school_class_id').html($html);
                $('#school_class_id').select2();
                $('#note_date').datepicker({
                    autoclose: true
                });

//                            $(".notes-new-form").html($html);
//                            $('.select2').select2();
            });
        }
        function hideSliderContainer() {
            $("#side-container .form").html('');
            $("#side-container").animate({right: '-400'});
        }
        function convertTo24Hour(time12H) {
            var [startTime, endTime] = time12H.split('-');

            function convertSingleTime(time) {
                var [hours, minutes] = time.trim().split(':');
                hours = parseInt(hours);

                if (time.includes('PM') && hours !== 12) {
                    hours += 12;
                } else if (time.includes('AM') && hours === 12) {
                    hours = 0;
                }

                return hours.toString().padStart(2, '0') + ':' + minutes;
            }

            var convertedStartTime = convertSingleTime(startTime);
            var convertedEndTime = convertSingleTime(endTime);

            return convertedStartTime + '-' + convertedEndTime;
        }
        function loadSchedule($this) {
    var $url = '<?= url("backend/scheduler-data") ?>';
    $.ajax({
        type: 'post',
        url: $url,
        data: $($this).serialize(),
        success: function ($data) {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(
                calendarEl,
                {
                    events: $data.events,
                    dayMaxEvents: $data.dayMaxEvents,
                    initialDate: $data.initialDate,
                    selectable: false,
                    headerToolbar: $data.headerToolbar,
                    eventOrder: function (a, b) {
                        // your eventOrder logic
                    }
                }
            );

            calendar.render();

            // If you have dynamic event sources, use refetchEvents here
             calendar.refetchEvents();

            $('.fc-event-title').each(function () {
                $(this).attr('data-content', $(this).html());
                $(this).attr('data-trigger', 'hover');
                $(this).popover();
            });

            $('.fc-more-link').on('click', function (e) {
                var offset = $(this).offset();
                setTimeout(function () {
                    $('.fc-more-popover').show();
                    $('.fc-more-popover').offset({ left: offset.left, top: offset.top })
                }, 100)
            });
        }
    });
            $(".notes-new-form").html('');
            //getNoteForm();
            return false;
        }

        $("#addnotes").on('click', function () {
            getNoteForm();
        });

        $(document).on('change', '#from_time,#to_time', function () {
            var from_time = $(this).val();
//                        alert(getTwentyFourHourTime(from_time));
            if (getTwentyFourHourTime(from_time) < 9 || getTwentyFourHourTime(from_time) > 17) {
//                            alert(getTwentyFourHourTime(from_time));

                if (getTwentyFourHourTime(from_time) < 9) {
                    $(this).val("09:00 AM");
                }
//                            alert(getTwentyFourHourTime(from_time));
                if (getTwentyFourHourTime(from_time) > 17) {
                    $(this).val("05:00 PM");
                }
            }
        });

        function getTwentyFourHourTime(amPmString) {
            var d = new Date("1/1/2023 " + amPmString);
            return d.getHours() + '.' + d.getMinutes();
        }
        function saveNotes($this) {

            var $url = '<?= url("backend/notes-store") ?>';
            $.ajax({
                type: 'post',
                url: $url,
                data: $($this).serialize(),
                success: function ($data) {
                    if ($data.success == 1) {
                        $.toaster({priority: 'success', title: 'Success', message: $data.message});
                        hideSliderContainer();
                        $('.notes-new-form').html('');
                        loadSchedule('#filter-form');
                    }
                }});
            return false;

        }
        function deleteNote($id) {
            var teacherId = $('input[name=teacher_ids]').val();
            var date = $('input[name="notes[note_date]"]').val();
            var start_at = $('input[name=start_at]').val();
            var end_at = $('input[name=end_at]').val();
            var note_type = $('input[name="notes[note_type]"]').val();
            var $url = '<?= url("backend/notes-delete") ?>';
            $url += "?teacherId=" + teacherId + "&date=" + date
                + "&start_at=" + start_at + "&end_at=" + end_at
                + "&note_type=" + note_type;
            $.ajax({
                type: 'post',
                url: $url,
                data: {_token: "{{ csrf_token() }}",
                    id: $id},
                success: function ($data) {
                    if ($data.success == 1) {
                        $.toaster({priority: 'success', title: 'Success', message: $data.message});
                        hideSliderContainer();
                        $('.notes-new-form').html('');
                        loadSchedule('#filter-form');
                    }
                }});
            return false;

        }
        $(".teacher_id").select2({
            minimumResultsForSearch: -1,
            placeholder: function () {
                $(this).data('placeholder');
            }
        });
        $(".teacher_id").on('change', function () {
            $(".notes-new-form").html('');
        });

        //                    $(document).on('hover', '.fc-event-title', function () {
        //                        console.log('yes');
        //                    })

        function confirmRequest() {
//                        return true;
            if (confirm('Are You Sure?')) {
                $('.remove-required').removeAttr('required');
                $('#is_delete').val(1);
                return true;
            }
            return false;
        }
    </script>

    @if(Session::has('outcome'))
        <script>
            $(function () {
                $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
            });
        </script>
    @endif
@endsection
