@extends('master')
@section('content')
<style>
    .datepicker-dropdown { z-index: 1001 !important; }
    .timepicker-popover{
        z-index: 1002 !important;
    }
    .fc-prev-button,.fc-today-button,.fc-next-button{
        display: none !important;
    }

</style>
<div class="mb-2">
    <h1 class="float-left">Scheduler Screen</h1>
    <div class="clearfix"></div>
</div>


<div class="record-form"></div>


<div class="row">
    <div class="col-md-12">
        <div class="card mb-2">
            <div class="card-header bg-light mb-3">
                <h4 class="font-weight-bold m-0">
                    Filters 
                    <!--<span class="fa fa-filter"></span>-->
                </h4>
            </div>
            <div class="card-body">
                <form action="" id="filter-form" onsubmit="return loadSchedule(this)" method="post" >
                    @csrf
                    <div class="row mb-2">
                        <div class="col-3">
                            {{ Form::select('filter[district_id]',[''=>"Select District"]+$districts->toArray(),null,['id'=>'district_id','onchange'=>'getTeachers()','class'=>'select2','required']) }}
                        </div>
                        <div class="col-2">
                            {{ Form::text('filter[month_year]',null,['class'=>'form-control monthPicker','placeholder'=>'Select Month/Year','required']) }}
                        </div>
                        <?php
                        $roleId = Auth::user()->role;
                        ?>
                        @if($roleId==2)
                        {{ Form::hidden('teacher_id[]',auth()->user()->id,['id'=>'teacher_id']) }}
                        @else
                        <div class="col-4">
                            {{ Form::select('teacher_id[]',[],null,['class'=>'select2 teacher_id','id'=>'teacher_id','required','multiple','data-placeholder'=>'Select Teachers']) }}
                        </div>
                        @endif
                        <div class="col-1">
                            <button class="btn btn-success" type="submit">
                                Search
                            </button>
                        </div>
                        @if($roleId !=2)
                        <div class="col-2">
                            <button class="btn btn-primary" id="addnotes" type="button">
                                Add Appointment
                            </button>
                        </div>
                        @endif
                    </div>
                </form>
                <div class="notes-new-form"></div>
                <h4 style="font-weight:bold;margin:30px 0px 10px 0px;">Legends 
                    <br class="d-md-none d-sm-block">
                    <i data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Schedule"></i>
                    <i class="rounded-circle bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Schedule with Note"></i>
                    <i class="rounded-circle bg-primary mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Teacher Appointment"></i>
                </h4>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!--CODE HERE-->
                    <div class="col-12" id='calendar'>
                        <!--<h4 class="text-danger">Apply above filters and click on Search button</h4>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('partials.loadmorejs')
@endsection
@section('script')
<script src='<?= asset('event_calendar/dist/index.global.js') ?>'></script>

<script>
                    document.addEventListener('DOMContentLoaded', function () {
                        $(".monthPicker").datepicker({
                            format: "mm/yyyy",
                            viewMode: "months",
                            minViewMode: "months",
                            autoclose: true
                        });
                        $(document).on('click', 'a.fc-event', function ($a) {

                            var url = $(this).attr('href');
                            $.get('<?= url('backend/get-notes-form') ?>/' + url, function ($html) {

                                if ($html.ref == '#side-container') {
//                                    alert('Allah');
                                    $("#side-container .form").html($html.html);
                                    $("#side-container").animate({right: '0'});

                                    $('#note_date').datepicker({
                                        autoclose: true
                                    });
                                    $('#from_time').timepicker();
                                    $('#to_time').timepicker();
                                }
                                if ($html.ref == '.notes-new-form') {
                                    $(".notes-new-form").html($html.html);
                                    $('#school_id').select2();
                                    $('#school_class_id').select2();

                                    $('html, body').animate({
                                        scrollTop: $(".notes-new-form").offset().top
                                    }, 1000);
                                }

                            });
                            return false;
                        });
                    });

                    function getNoteForm() {
                        var url = $('#teacher_id').val();
                       
//                        var url = url.join();

//                        if (url.length != 1) {
//                            $(".notes-new-form").html('');
//                            return false;
//                        }
                        $.get('<?= url('backend/notes-form') ?>?teacher_id=' + url, function ($html) {
                            $("#side-container .form").html($html);
                            $('.select2').select2();
                            $("#side-container").animate({right: '0'});

                            $('#note_date').datepicker({
                                autoclose: true
                            });
                            $('#from_time').timepicker();
                            $('#to_time').timepicker();
                        });
                        return false;
                    }
                    function getTeachers() {
                        var url = $('#district_id').val();

                        $.get('<?= url('backend/get-teacher') ?>?district_id=' + url, function ($html) {
                            $(".teacher_id").html($html);
                            $('.teacher_id').select2();
                        });
                        return false;
                    }
                    function loadClasses() {
                        var $schoolId = $('#school_id').val();
                        $.get('<?= url('load-school-classes') ?>?school_id=' + $schoolId + '&teacher_id=' + $('#teacher_id').val(), function ($html) {
                            $('#school_class_id').html($html);
                            $('#school_class_id').select2();
                            $('#note_date').datepicker({
                                autoclose: true
                            });

//                            $(".notes-new-form").html($html);
//                            $('.select2').select2();
                        });
                    }
                    function hideSliderContainer() {
                        $("#side-container .form").html('');
                        $("#side-container").animate({right: '-400'});
                    }
                    function loadSchedule($this) {
                        var $url = '<?= url("backend/scheduler-data") ?>';
                        $.ajax({
                            type: 'post',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                var calendarEl = document.getElementById('calendar');
                                var calendar = new FullCalendar.Calendar(calendarEl, $data);
                                calendar.render();
//                                alert(.length);
                                $('.fc-event-title').each(function () {
                                    $(this).attr('data-content', $(this).html());
                                    $(this).attr('data-trigger', 'hover');
                                    $(this).popover();
//                                    $(this).attr('title', $(this).html());
//                                    $(this).attr('data-placement', 'top');
//                                    $(this).tooltip();
                                });


                            }});
                        $(".notes-new-form").html('');
                        //getNoteForm();
                        return false;
                    }
                    $("#addnotes").on('click', function () {
                        getNoteForm();
                    });
                    function saveNotes($this) {
                        var $url = '<?= url("backend/notes-store") ?>';
                        $.ajax({
                            type: 'post',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                if ($data.success == 1) {
                                    $.toaster({priority: 'success', title: 'Success', message: $data.message});
                                    hideSliderContainer();
                                    $('.notes-new-form').html('');
                                    loadSchedule('#filter-form');
                                }
                            }});
                        return false;

                    }
                     function deleteNote($id) {
                        var $url = '<?= url("backend/notes-delete") ?>';
                        $.ajax({
                            type: 'post',
                            url: $url,
                            data: { _token:"{{ csrf_token() }}",
 id:$id},
                            success: function ($data) {
                                if ($data.success == 1) {
                                    $.toaster({priority: 'success', title: 'Success', message: $data.message});
                                    hideSliderContainer();
                                    $('.notes-new-form').html('');
                                    loadSchedule('#filter-form');
                                }
                            }});
                        return false;

                    }
                    $(".teacher_id").select2({
                        minimumResultsForSearch: -1,
                        placeholder: function () {
                            $(this).data('placeholder');
                        }
                    });
                    $(".teacher_id").on('change', function () {
                        $(".notes-new-form").html('');
                    });

//                    $(document).on('hover', '.fc-event-title', function () {
//                        console.log('yes');
//                    })


</script>

@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
@endsection