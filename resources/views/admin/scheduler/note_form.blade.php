<form onsubmit="return saveNotes(this)" method="post">
    @csrf
     <?php
   //  echo $tId
   if(!$tId) {
    $teacher_name = $note->teacher_notes->toArray()[0];
   }

    ?>
    @if($note->id)
    <h4 class="font-weight-bold">Update Appointment</h4>
    @else
    <h4 class="font-weight-bold">Create Appointment</h4>
    @endif

    <div class="form-group d-none">
        {{ Form::hidden('id',$note->id) }}
        @if($tId != null)
            {{ Form::hidden('teacher_ids',$tId) }}
        @else
            {{ Form::hidden('teacher_ids',$teacherIds) }}
        @endif
        {{ Form::hidden('notes[note_type]','appointment') }}
    </div>
    <?php
    $disabled = '';
    if (auth()->user()->role == 2) {
        $disabled = 'disabled';
    }
    ?>
   <div class="form-group">
        <label>Teacher</label>

        <input type="text" disabled="" style="color:#333 !important" class="form-control" value="<?= $teacher_name['first_name']. ' ' . $teacher_name['last_name'] ?>">
    </div>
    <div class="form-group">
        <label>Date</label>
        <input <?= $disabled ?> type="text"  name="notes[note_date]" id="note_date" class="form-control" value="<?= formatDate($note->note_date) ?>">
    </div>
    <div class="form-group">
        <label>From Time</label>
        <input <?= $disabled ?> type="text"  name="notes[start_at]" id="from_time" class="form-control" value="<?= formatTime($note->start_at) ?>">
    </div>
    <div class="form-group">
        <label>To Time</label>
        <input <?= $disabled ?> type="text"  name="notes[end_at]" id="to_time" class="form-control" value="<?= formatTime($note->end_at) ?>">
    </div>
    <div class="form-group">
        <label>Subject</label>
        <input <?= $disabled ?> type="text" required class="form-control" value="<?= $note->subject ?>" name="notes[subject]">
    </div>
    <div class="form-group">
        <label>Notes</label>
        <textarea <?= $disabled ?> required="" class="form-control" name="notes[note]"><?= $note->note ?></textarea>
    </div>
    @if(auth()->user()->role != 2)
    <div class="form-group text-right">
         @if($note->id)
    <button class="btn btn-danger" type="button" onClick="deleteNote({{$note->id}})">Delete</button>
    @endif
        <button class="btn btn-success">Save</button>
    </div>
    @endif
</form>
