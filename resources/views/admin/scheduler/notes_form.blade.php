<form onsubmit="return saveNotes(this)" method="post">
    @csrf
    <?php
    $teacher = $schoolClass->teacher;
    ?>
    <div class="form-group d-flex">
        <h4 class="font-weight-bold flex-grow-1">Schedule Details</h4>
        @unless(auth()->user()->hasRole('Teacher'))
            <input name="delete_schedule" type="submit" onclick="return confirmRequest();" value="Delete Schedule" class="btn btn-danger btn-sm btn-small align-self-center">
        @endunless
    </div>
    <div class="form-group">
        <label class="mb-0">Teacher</label>
        {{ Form::hidden('id',$note->id) }}
        <input type="hidden" name="teacher_ids" value="<?= $teacher->id ?>">
        <input type="hidden" name="is_delete" value="0" id="is_delete">
        <input type="hidden" name="notes[note_type]" value="note">
        <input type="text" disabled="" style="color:#333 !important" class="form-control" value="<?= $teacher->first_name . ' ' . $teacher->last_name ?>">
    </div>
    <div class="form-group">
        <label class="mb-0">School</label>
        <input type="hidden" name="notes[school_id]" value="<?= $schoolClass->school->id ?>" >
        <input type="text" disabled="" style="color:#333 !important" class="form-control" value="<?= $schoolClass->school->name ?>" >
        <small class="text-primary d-block mt-1 ml-1">Address: {{$schoolClass->school->address}}<br /> {{$schoolClass->school->city->city}}, {{$schoolClass->school->city->state_code}}</small>
    </div>
    <div class="form-group">
        <label class="mb-0">Class</label>
        <input type="hidden" value="<?= $schoolClass->id ?>" name="notes[school_class_id]">
        <input type="text" class="form-control" style="color:#333 !important" readonly="" value="<?= $schoolClass->title ?>">
    </div>
    <div class="form-group">
        <label class="mb-0">Date</label>
        <input type="text" readonly="" style="color:#333 !important" name="notes[note_date]" class="form-control" value="<?= formatDate(request()->date) ?>">
    </div>
    <div class="d-flex">
        <div class="form-group mr-1">
            <label class="mb-0">From Time</label>
            <input type="text" readonly="" name="start_at" style="color:#333 !important" class="form-control" value="<?= request()->start_at ?>">
        </div>
        <div class="form-group">
            <label class="mb-0">to Time</label>
            <input type="text" readonly="" name="end_at" style="color:#333 !important" class="form-control" value="<?= request()->end_at ?>">
        </div>
    </div>
    <?php
    $disabled = '';
    if (auth()->user()->role == 2) {
        $disabled = 'disabled';
    }
    ?>
    <div class="form-group">
        <label class="mb-0">Subject</label>

        <input <?= $disabled ?>  type="text"  required class="remove-required form-control" value="<?= $note->subject ?>" name="notes[subject]">
    </div>
    <div class="form-group">
        <label class="mb-0">Notes</label>
        <textarea <?= $disabled ?> required="" class="remove-required form-control" name="notes[note]"><?= $note->note ?></textarea>
    </div>

    <div class="form-group d-flex ">
        <?php
//        dump(request()->school_id);
        $urlData = "backend/attendance_sheets/index?class_id=" . request()->class_id . '&date=' . request()->date . '&school_id=' . $schoolClass->school->id;

        if (auth()->user()->role == 2) {
            $urlData = "teacher/attendance_sheets/index?class_id=" . request()->class_id . '&date=' . request()->date . '&school_id=' . $schoolClass->school->id;
        }
        ?>
        <a href="<?= url($urlData) ?>">Go to Roster</a>

        <div class="form-group text-right flex-grow-1">
            @if(Auth::user()->role!=2)
            @if($note->id)
            <button class="btn btn-danger" type="button" onClick="deleteNote({{$note->id}})">Delete</button>
            @endif

            <button class="btn btn-success">Save</button>

            @endif
        </div>
    </div>
</form>
