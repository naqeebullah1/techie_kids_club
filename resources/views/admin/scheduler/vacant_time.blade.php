@extends('master')
@section('content')
<style>
    .fc-prev-button,.fc-today-button,.fc-next-button{
        display: none !important;
    }

</style>
<div class="mb-2">
    <h1 class="float-left">Teacher Vacant Time</h1>
    <div class="clearfix"></div>
</div>
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-2">
            <div class="card-header bg-light mb-3">
                <h4 class="font-weight-bold m-0">
                    Filters 
                    <!--<span class="fa fa-filter"></span>-->
                </h4>
            </div>
            <div class="card-body">
                <form action="" onsubmit="return loadSchedule(this)" method="post" >
                    @csrf
                    <div class="row">
                        <div class="col-3">
                            {{ Form::select('filter[district_id]',[''=>"Select District"]+$districts->toArray(),null,['id'=>'district_id','onchange'=>'getTeachers()','class'=>'select2','required']) }}
                        </div>
                        <div class="col-3">
                            {{ Form::text('filter[month_year]',null,['class'=>'form-control monthPicker','placeholder'=>'Select Month/Year','required']) }}
                        </div>
                        <?php
                        $roleId = Auth::user()->role;
                        ?>
                        @if($roleId==2)
                        {{ Form::hidden('teacher_id[]',auth()->user()->id,['id'=>'teacher_id']) }}
                        @else
                        <div class="col">
                            {{ Form::select('teacher_id[]',[''=>'Select Teacher'],null,['class'=>'select2 teacher_id','required']) }}
                        </div>
                        @endif
                        <div class="col-1">
                            <button class="btn btn-success" type="submit">
                                Search
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!--CODE HERE-->
                    <div class="col-12" id='calendar'></div>
                </div>

            </div>
        </div>
    </div>
</div>


@include('partials.loadmorejs')
@endsection
@section('script')
<script src='<?= asset('event_calendar/dist/index.global.js') ?>'></script>

<script>
                    document.addEventListener('DOMContentLoaded', function () {
                        $(".monthPicker").datepicker({
                            format: "mm/yyyy",
                            viewMode: "months",
                            minViewMode: "months",
                            autoclose: true
                        });

                    });

                    function hideSliderContainer() {
                        $("#side-container .form").html('');
                        $("#side-container").animate({right: '-400'});
                    }
                    function loadSchedule($this) {
                        var $url = '<?= url("backend/load-vacant-time") ?>';
                        $.ajax({
                            type: 'post',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                var calendarEl = document.getElementById('calendar');
                                var calendar = new FullCalendar.Calendar(calendarEl, $data);
                                calendar.render();
                            }});

                        return false;
                    }

                    function saveNotes($this) {
                        var $url = '<?= url("backend/notes-store") ?>';
                        $.ajax({
                            type: 'post',
                            url: $url,
                            data: $($this).serialize(),
                            success: function ($data) {
                                if ($data.success == 1) {
                                    $.toaster({priority: 'success', title: 'Success', message: $data.message});
//                                    hideSliderContainer();
                                }
//                                var calendarEl = document.getElementById('calendar');
//                                var calendar = new FullCalendar.Calendar(calendarEl, $data);
//                                calendar.render();
                            }});
                        return false;

                    }
                    function getTeachers() {
                        var url = $('#district_id').val();
                        $.get('<?= url('backend/get-teacher') ?>?district_id=' + url, function ($html) {
                            $(".teacher_id").html($html);
                            $('.teacher_id').select2();
                        });
                        return false;
                    }

</script>

@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
@endsection