<div class="d-flex">
    <h4 style="flex-grow: 1" class="mb-2">Class Students</h4>
    <a style="align-self: center" class="btn btn-primary mr-1" href="?print=1&class_id=<?= request()->id ?>">Export</a>
    <a style="align-self: center" class="btn btn-danger" href="javascript:" onclick="removeDetail()">Close</a>
</div>
<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Class</th>
            <?php
            $roles = auth_user_roles();
            ?>
            @if(!in_array(2,$roles))
            <th>Parent</th>
            <th>Email</th>
            <th>Phone</th>
            @endif
            <th>Allow Photo Release</th>
        </tr>
    </thead>
    <tbody>

        @foreach($students as $city)
       
        <tr>
            <td>

                <img src="<?= asset('images/uploads/' . $city->student->image) ?>" width="100px" onerror="this.src='{{asset('frontend/images')}}/no-image.png'">

            </td>
            <td>
                <?= $city->student->first_name ?>
            </td>
            <td>
                <?= $city->student->last_name ?>
            </td>
            <td>
                <?php echo ($city->admission_class->title) ?>
            </td>

            @if(!in_array(2,$roles) && !empty($city->student->user))
            <td>

                <?= $city->student->user->first_name . ' ' . $city->student->user->last_name ?>
            </td>
            <td>
                <?= $city->student->user->email ?>
            </td>
            <td>
                <?= $city->student->user->phone1 ?>
            </td>
            @else
            <td colspan="3">&nbsp;</td>
            @endif 
            <td>
                <?= ($city->student->allow_photo_release)?"<span class='font-weight-bold text-success'>YES</span>":"<span class='font-weight-bold text-danger'>NO</span>"?>
            </td>
        </tr>
        @endforeach
        @can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan
    </tbody>
</table>
