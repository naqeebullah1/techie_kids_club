<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->class_id)
                    {{ __('Edit Class') }}
                    @else
                    {{ __('Create Class') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.school.classes.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4">
                            <label>Class Title</label>
                            <input value="{{ $class->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $school->id }}" type="hidden" class="form-control" name="record[school_id]"/>
                            <input value="{{ $school->franchise_id }}" type="hidden" class="form-control" name="record[franchise_id]"/>
                            <input value="{{ $class->title }}" type="text" class="form-control" name="record[title]" required/>
                        </div>
                        <div class="col-lg-4">
                            <label>Teacher</label>
                            <select required  name="record[user_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$teachers,'value'=>$class->user_id,'label'=>'Teacher'])
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Payment Type</label>
                            <?php
                            $dt = $class->duration_type;
                            if (!$dt) {
                                $dt = 'monthly';
                            }
//                            dump($dt)
                            ?>
                            {{ Form::select('record[duration_type]',priceSettings(),$dt,['onchange'=>'getAmount()','id'=>'payment_type',"class"=>"select2"]) }}
                        </div>

                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-3">
                            <label>Class Charges ($)</label>
                            <?php
                            $cc = App\Models\PriceSetting::where('type', 'monthly')->first();
                            ?>
                            <input required value="{{ $class->class_charges?$class->class_charges:$cc->amount }}" id="class_charges" type="text" class="form-control" name="record[class_charges]" />
                        </div>
                        <div class="col-lg-3">
                            <label>Start Date</label>
                            <input required value="{{ formatDate($class->start_on) }}" type="text" class="datepicker form-control" name="record[start_on]"/>
                        </div>
                        <!--<div class="col-lg-3">
                            <label>End Date</label>
                            <input required value="{{ formatDate($class->end_on) }}" type="text" class="datepicker form-control" name="record[end_on]"/>
                        </div>-->
                        <div class="col-lg-3">
                            <label>Start Time</label>
                            <input required value="{{ formatTime($class->start_at) }}" type="text" class="timepicker form-control startat" name="record[start_at]"/>
                        </div>
                        <div class="col-lg-3">
                            <label>Duration (minutes)</label>
                            <?php
                            $duration = "";
                            if ($class->end_at) {
                                $start_time = strtotime($class->start_at);
                                $end_time = strtotime($class->end_at);
                                $duration = round(abs($end_time - $start_time) / 60, 2);
                            }
                            ?>
                            <input required value="{{ $duration?$duration:45 }}" type="text" class="form-control" name="record[duration]"/>
                        </div>
                        <!--<div class="col-lg-4">
                            <label>End Time</label>
                            <input required value="{{ formatTime($class->end_at) }}" type="text" class="timepicker form-control endat" name="record[end_at]"/>
                        </div>-->
                    </div>

                    <div class="row mt-2">
                        <label class="col-12 mb-3">Week Days</label>
                        <?php
                        $weekDays = weekDays();
                        $offdays = $class->weekly_off_days;
//                        dump($offdays);

                        if ($offdays != "") {
                            $offdays = explode(',', $offdays);
                        } else {
                            $offdays = [];
                        }
                        ?>
                        @foreach($weekDays as $i=>$v)
                        <div class="col">
                            <label>

                                <input <?= (in_array($i, $offdays) && !empty($offdays)) ? 'checked' : '' ?> type="checkbox" name="record[weekly_off_days][]" value="<?= $i ?>"> <?= $v ?>
                            </label>
                        </div>
                        @endforeach
                    </div>

                    <div class="row">
                        <div class="col text-right">
                            <br/>
                            <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                            @if(request()->class_id)
                            <button type="submit" class="mt-2 btn btn-success">Update</button>
                            @else
                            <button type="submit" class="mt-2 btn btn-success">Add</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function getAmount() {
        $.get("<?= url('backend/get-pd') ?>/" + $('#payment_type').val(), function (e) {
            $('#class_charges').val(e.amount);
        });
    }
</script>