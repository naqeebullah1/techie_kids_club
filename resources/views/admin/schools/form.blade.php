<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit School') }}
                    @else
                    {{ __('Create School') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.schools.store') }}">
                    @csrf
                    <div class="row mb-2">
                        @if(auth()->user()->id==1)
                        <div class="col-lg">
                            <label>Franchise</label>
                            {{ Form::select('form_franchise_id',['Select']+franchiseList(),$school->franchise_id,['class'=>'select2']) }}
                        </div>
                        @endif
                        <div class="col-lg">
                            <label>District</label>
                            {{ Form::select('record[district_id]',['Select']+$districts,$school->district_id,['class'=>'select2']) }}
                        </div>
                        <div class="col-lg">
                            <label>Regional Manager</label>
                            <select name="record[user_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$managers,'value'=>$school->user_id])
                            </select>
                        </div>
                        <div class="col-lg">
                            <label>School Admin</label>
                            {{ Form::select('record[school_admin_id]',[''=>'Select']+$schoolAdmins->toArray(),$school->school_admin_id,['class'=>'select2']) }}

                        </div>

                    </div>
                    <div class="row mb-2">

                        <div class="col-lg-4">
                            <label>School Name</label>
                            <input value="{{ $school->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $school->name }}" type="text" class="form-control" name="record[name]"/>

                        </div>
                        <div class="col-lg-4">
                            <label>Contact</label>
                            <input value="{{ $school->contact }}" type="text" class="form-control" name="record[contact]"/>
                        </div>
                        <div class="col-lg-4">
                            <label>Email</label>
                            <input value="{{ $school->email }}" type="email" class="form-control" name="record[email]"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <label>State</label>
                            <select onchange="citiesGetDD(1)" id="states_id" name="record[state_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$states,'value'=>$school->state_id])
                            </select>

                        </div>
                        <div class="col-lg-4">
                            <label>City</label>
                            <select id="cities_id" name="record[city_id]" class="select2">
                                @include('partials.dropdowns',['options'=>$cities,'value'=>$school->city_id])
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Address</label>
                            <input value="{{ $school->address }}" type="text" class="form-control" name="record[address]"/>
                        </div>

                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label>Logo</label>
                            <input type="file" name="image" data-default-file="<?= ($school->image) ? asset('images/uploads/' . $school->image) : '' ?>" class="dropify">
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    function citiesGetDD(isChanged = null) {
        var $url = "{{ url('backend/cities-dd') }}";
        var sId = '{{$school->state_id}}';
        var cId = '{{$school->city_id}}';
        if (isChanged) {
            sId = $('#states_id').val();
            cId = '';
        }
        $url = $url + "?state_id=" + sId + "&city_id=" + cId;

        $.get($url, function (data) {
            $('#cities_id').html(data.html);
//            $('.select2').select2();
        });
    }
</script>