<div class="clearfix"></div>
<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Advance Search') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = "display:none";

            if (request()->search) {
                $display = "display:block";
            }
            ?>

            <div class="card-body" style="{{ $display }}" id="searchfields">
                <?php 
                    $segment = 'backend';
                    if(auth()->user()->role == 2){
                        $segment = 'teacher';
                    }
                ?>
                <form method="post" id="searchform" accept-charset="utf-8" action='<?php echo url("$segment/schools") ?>'>
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-4">
                            <?php
//                            dump(request()->all());
                            ?>
                            <select name="state_id" id="state_id" class="form-control select2" onChange='loadCities()'>
                                <option value="">Select State</option>
                                @foreach($states as $state)
                                <option <?php
                                if (request()->state_id == $state->id) {
                                    echo 'selected';
                                } else {
                                    echo '';
                                }
                                ?> value="{{ $state->id }}">{{ $state->state }}</option>

                                @endforeach
                            </select>

                        </div>
                        <div class="col-lg-4">
                            <select name="city_id" id="city_id" class="form-control select2">
                                <option value="">Select City</option>
                            </select>   
                        </div>

                        <div class="col-lg-4">
                            <input type="text" name="school_name" class="form-control" placeholder="School Name" id="school_name" value="<?php
                            if (request()->school_name) {
                                echo request()->school_name;
                            } else {
                                echo '';
                            }
                            ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-success" style="margin-top:25px;" value="Reset" onClick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;" value="Search" id="searchBnt">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function loadCities() {
        var $url = "<?php echo url('/').'/front-franchise/'. App\Models\Franchise::where('id',auth()->user()->franchise_id)->first()->slug; ?>/cities-dd";

        var stateId = $('#state_id').val();
        var cityId = '{{ request()->city_id }}';

        if (stateId == '') {
            stateId = "";
        }

        $url = $url + "?state_id=" + stateId+"&city_id="+cityId;
        if (stateId != "") {
            $('#city_id').html('<option value="">Select City</option>');
            $.get($url, function (data) {
                $('#city_id').html(data.html);
            });
        } else {
            $('#city_id').html('<option value="">Select City</option>');
        }
    }
    loadCities();
</script>