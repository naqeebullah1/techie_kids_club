@extends('layouts.front_theme')
@section('title','Forgot Password')
@section('content')


<div class="ht__bradcaump__area">
    <div class="ht__bradcaump__container">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">

                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
                            <span class="brd-separetor"><img src="{{asset('frontend/images/icons/brad.png')}}" alt="separator images"></span>
                            <span class="breadcrumb-item active">Forgot Password</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

<section class="section-padding--xs bg-pngimage--2">

    <div class="row justify-content-center">
        <div class="col-xs-11 col-sm-11 col-md-8 col-lg-5 p-0" style="border: 1px solid #ccc;box-shadow: 1px 1px 20px rgb(0 0 0 / 15%);">


            <div class="tab-content p-4">

                <div class="tab-pane fade show active">
                    @include('partials.alerts')
                    <div class="login-wrap p-0">
                        <h3 class="mb-4 text-center">Please provide your registered email</h3>
                        <form id="form-login" class="signin-form" role="form" action="{{url('front-franchise/'.request()->franchise_id)}}/recoverPassword" method="POST" autocomplete="off">
                            @csrf
                            <input type="hidden" name="franchise_id" value="{{ request()->franchise_id }}" />
                            <input type="hidden" name="request_from" value="parent" />
                            <div class="form-group animate__animated animate__backInLeft @error('email'){{'has-error'}} @enderror">
                                <input type="email" name="email" placeholder="Provide Your Email" required class="form-control @error('email'){{'error'}} @enderror" autocomplete="off">
                            </div>
                            @error('email')
                            <label id="email-error" class="error" for="email">{{$message}}</label>
                            @enderror
                            <div class="form-group">
                                <button type="submit" id="submitForm" class="form-control btn btn-primary submit px-3">Recover Password</button>
                            </div>
                            <div class="form-group d-md-flex">
                                <div class="w-100 text-md-right">
                                    <a href="{{url(franchiseLink())}}/parentlogin" class="text-info m-l-10">Back To Login</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection