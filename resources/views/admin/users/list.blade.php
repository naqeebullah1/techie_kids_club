@extends('master')
@section('title','Dashboard')
@section('content')
<style>
    .nav-item{
        border: 1px solid #069d4d;
    }
</style>
<div>
    <h1 class=" col-12">List Users</h1>
    <div class="col-12 mt-2 mb-3 p-0">
        <ul class="nav nav-pills nav-fill">

            <?php
            $authRoles = auth()->user()->ModelHasRoles->pluck('role_id')->toArray();

            $basicRoles = basicRoles();
            if (in_array(5, $authRoles)):
                $basicRoles = schoolAdminRoles();
            endif;
            ?>


            @foreach($basicRoles as $id=>$rr)

            <?php if (strtolower($rr) != "parents") { ?>
                <li class="nav-item p-0">
                    <!--active-->
                    <a class="nav-link btn-outline-success no-border-radius @if($roleId==$id) active @endif" aria-current="page" href="<?= url('backend/users-list/' . $id) ?>"><?= $rr ?></a>
                </li>
            <?php } ?>
            @endforeach
        </ul>
    </div>



    @include('admin.users.partials.user_search_form')
    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>
    @can('user-create')
    <a href="{{route('admin.users.create')}}" class="btn btn-success float-right" role="button">Create User</a>
    @endcan	
    <div class="clearfix"></div>
</div>
@include('admin.users.partials.loop.list')    

@include('partials.loadmorejs')
@endsection
