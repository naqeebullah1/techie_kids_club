@extends('master')
@section('title','Dashboard')
@section('content')
<style>
    .nav-item{
        border: 1px solid #069d4d;
    }
</style>
<div>
    <h1 class=" col-12">List Parents</h1>
    <div class="form-container"></div>

    @include('admin.users.partials.parent_search_form', compact('states'))
    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>
    <!-- @can('user-create')
     <a href="{{route('admin.users.create')}}" class="btn btn-success float-right" role="button">Create User</a>
     @endcan	-->
    <div class="clearfix"></div>
</div>
@include('admin.users.partials.loop.parentlist')

@include('partials.loadmorejs')
<script>
    function getFranchises(ref) {
//        alert(ref.href);
        $.get('{{url("backend/assign-franchise-to-user")}}/' + ref, function ($data) {
            $('.form-container').html($data.html);
            $('.dynamic-select').select2();
        });
        $('html, body').animate({
            scrollTop: $(".form-container").offset().top
        }, 1000);
        return false;
    }
    function removeForm() {
        $('.form-container').html('');
    }
</script>

@endsection
