<div class="clearfix"></div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <h4 class="bold" style="padding:0px 10px;font-size:18px;"><a class="toggler" href="#" onClick="toggleSearch('searchfields')">{{ __('Advanced Search') }}<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
            <?php
            $display = 'display:none;';
            $requestData = [
                "first_name" => null,
                "last_name" => null,
                "email" => null,
                "childfname" => null,
                "from_date" => null,
                "to_date" => null,
                "status" => null,
            ];
            if (Request::all()) {
                $requestData = Request::all();
                $isData = 1;
            }
            if (isset($isData)) {
                $display = '';
            }

//            dump($requestData);exit;
            extract($requestData, EXTR_PREFIX_ALL, "ex");
            ?>
            <div class="card-body" style="{{ $display }}" id="searchfields">
                <form method="post" id="searchform" accept-charset="utf-8" action="{{route('admin.users.parentlist')}}">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="sort_field" class="sort_field" value="">
                        <input type="hidden" name="sort_order" class="sort_order" value="">
                        <div class="col-lg-3">
                            <input autofocus="" type="text" name="first_name" value="{{ @$ex_first_name }}" class=" searchfield form-control" placeholder="Parent First Name" id="account-name">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" name="last_name" value="{{ @$ex_last_name }}" class=" searchfield form-control" placeholder="Parent Last Name" id="account-name">
                        </div>
                        <div class="col-lg-3">
                            <input type="text" name="email" class="searchfield form-control" placeholder="Parent Email" id="email" value="{{ @$ex_email }}">
                        </div>
                           <div class="col-lg-3">
                            <input type="text" name="childfname" value="{{ @$ex_childfname }}" class=" searchfield form-control" placeholder="Child First Name" id="child-fname">
                        </div>
                    </div>
                    <div class="row mt-2">

                        <div class="col-lg-4">
                            <select name="status" class="form-control" >
                                <option value="">Select Parent Status</option>
                                <option value="1" <?= (@$ex_status) ? 'selected' : '' ?> >Active</option>
                                <option value="0" <?= (@$ex_status == '0') ? 'selected' : '' ?>>Inactive</option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="from_date" value="{{ @$ex_from_date }}" class=" searchfield form-control datepicker" placeholder="From Date" id="from_date">
                        </div>
                        <div class="col-lg-4">
                            <input type="text" name="to_date" value="{{ @$ex_to_date }}" class=" searchfield form-control datepicker" placeholder="To Date" id="to_date">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-4">
                            <?php
//                            dump(request()->all());
                            ?>
                            <select name="state_id" id="state_id" class="form-control select2" onchange="citiesDD(1)">
                                <option value="">Select State</option>
                                @foreach($states as $state)
                                    <option <?php
                                                if (request()->state_id == $state->id) {
                                                    echo 'selected';
                                                } else {
                                                    echo '';
                                                }
                                                ?> value="{{ $state->id }}">{{ $state->state }}</option>

                                @endforeach
                            </select>

                        </div>
                        <div class="col-lg-4">
                            <select name="city_id" id="city_id" class="form-control select2" onchange="getSchools()">
                                <option value="">Select City</option>
                            </select>
                        </div>

                        <div class="col-lg-4">
                            <select name="school_id" id="school_id" class="form-control select2" onchange="getClasses()">
                                <option value="">Select School</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="reset" name="reset" class="btn btn-success" style="margin-top:25px;" value="Reset" onclick="resetForm('searchform')">
                            <input type="submit" name="search" class="btn btn-primary" style="margin-top:25px;" value="Search" id="searchBnt">
                            <input type="submit" name="export" class="btn btn-primary" style="margin-top:25px;" value="Export">
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    if ($('#state_id').val()) {
        citiesDD();
    }
    function citiesDD() {
        var $url = "<?php echo url('/').'/front-franchise/'. App\Models\Franchise::where('id',auth()->user()->franchise_id)->first()->slug; ?>/cities-dd";

        var sId = $('#state_id').val();

        @if(request()->city_id)
            $url = $url + "?state_id=" + sId + "&city_id={{request()->city_id}}&ignore_without_schools=1";
        @else
            $url = $url + "?state_id=" + sId + "&ignore_without_schools=1";
        @endif
        if (sId != "") {
            $('#city_id').html('<option value="">Select City</option>');
            $.get($url, function (data) {
                $('#city_id').html(data.html);
                @if(request()->city_id)
                    getSchools();
                @endif
            });
        } else {
            $('#city_id').html('<option value="">Select City</option>');
        }
    }

    function getSchools() {
        var $url = "<?php echo url('/').'/front-franchise/'. App\Models\Franchise::where('id',auth()->user()->franchise_id)->first()->slug; ?>/schools-dd";
        var cId = $('#city_id').val();
        @if(request()->school_id)
            $url = $url + "?city_id=" +cId + "&school_id={{request()->school_id}}";
        @else
            $url = $url + "?city_id=" +cId;
        @endif

        if ($('#city_id').val() != "") {
            $.get($url, function (data) {
                $('#school_id').html(data);
                $('.ajax-select2').select2();
            });
        } else {
            $('.school_id').html('');
        }
    }

    function getClasses() {
        var $url = "<?= url('school-classes-parents-dd') ?>";
        var cId = $('#school_id').val();
        $url = $url + "?school_id=" + cId + "&is_dd=1&city_id=" + $('#city_id').val();

        if ($('#city_id').val() != "") {
            $.get($url, function (data) {
                $('.school-classes-container').html(data);
                $('.ajax-select2').select2();
            });
        } else {
            $('.school-classes-container').html('');
        }
    }
</script>
