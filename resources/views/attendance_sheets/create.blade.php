@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Attendance Sheets</h1>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
</div>
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form onsubmit="return getStudents()" id="filter-form">
                    <div class="row">
                        <div class="col-4">
                            @csrf
                            {{ Form::select('school_id',[''=>'Select School']+$schools,'',['onchange'=>"loadClasses()",'id'=>'school_id','class'=>'select2']) }}

                        </div>
                        <div class="col-4">
                            {{ Form::select('school_class_id',[''=>'Select Class'],'',['id'=>'school_class_id','class'=>'select2']) }}
                        </div>
                        <div class="col-4">
                            {{ Form::submit('Search',['class'=>'btn btn-primary']) }}
                        </div>

                    </div>


                </form>
                <div class="row">
                    <div class="col-12 ajax-container" >

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
<script>

    function loadClasses() {
        var $schoolId = $('#school_id').val();
        $.get('<?= url('load-school-classes') ?>?school_id=' + $schoolId + '&teacher_id=', function ($html) {
            $('#school_class_id').html($html);
            $('#school_class_id').select2();
        });
    }
    function getStudents() {
        var form = '#filter-form';
        //sort-form
        var url = $(form).attr('action');
        var request = $.ajax({
            url: url,
            type: "POST",
            data: $(form).serialize(),
            dataType: "json"
        });
        request.done(function (msg) {
            $('.ajax-container').html(msg.html);
            $('.datepicker').datepicker({
                autoclose:true
            });
        });
        request.fail(function (jqXHR, textStatus) {
            $.toaster({
                priority: 'error',
                title: 'Fail',
                message: "Request failed: " + textStatus
            });
        });
        return false;
    }
    function changeCheckBox($this) {
        if ($($this).prop('checked')) {
            $($this).siblings('.checkbox-field').val(1);

        } else {
            $($this).siblings('.checkbox-field').val(0);
        }
    }

</script>

@endsection