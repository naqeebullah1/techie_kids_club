<div class="d-flex">
    <h3 class="flex-grow-1">Student's attendance detail on - <b><?= formatDate($attendance->date) ?></b></h3>
    <a class="btn btn-success align-self-center" href="javascript:;" onclick="return closeDetail()">Back</a>
</div>

<table class="table table-bordered table-sm table-small table-striped">
    <thead>
    <tr>
        <th>Student</th>
        <th>Is Present</th>
        <th>Notes</th>
    </tr>
    </thead>
    <tbody>
    @foreach($students as $key=>$student)
        <tr>
            <td>
                    <?php
                    $sd = $student->student;
                    ?>
                    <?= $sd->first_name . ' ' . $sd->last_name ?>
            </td>
            <td>
                    <?= ($student->is_present) ? '<span class="text-success font-weight-bold">Yes</span>' : '<span class="text-danger font-weight-bold">No</span>' ?>
            </td>
            <td>{{$student->note}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


