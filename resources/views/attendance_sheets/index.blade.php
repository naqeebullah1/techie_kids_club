@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Attendance Sheet Report</h1>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
</div>
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form onsubmit="return getStudents()" id="filter-form">
                    <div class="row">
                        <div class="col-3">
                            @csrf
                            {{ Form::select('school_id',[''=>'Select School']+$schools,request()->school_id,['onchange'=>"loadClasses()",'id'=>'school_id','class'=>'select2']) }}
                        </div>
                        <div class="col-3">
                            <?php
                            $classes=[];
                            if(request()->school_id){
                                $classes= \App\Models\SchoolClass::where('school_id',request()->school_id)->select('id','title')->pluck('title','id')->toArray();
                            }
                            ?>
                            {{ Form::select('school_class_id',[''=>'Select Class']+$classes,request()->class_id,['id'=>'school_class_id','class'=>'select2']) }}
                        </div>
                        <div class="col-3">
                            {{ Form::text('from_date',formatDate(request()->date),['class'=>'datepicker form-control','placeholder'=>'From Date']) }}
                        </div>
                        <div class="col-3">
                            {{ Form::text('to_date',formatDate(request()->date),['class'=>'datepicker form-control','placeholder'=>'To Date']) }}
                        </div>
                        <div class="col-12 text-right">
                            {{ Form::submit('Search',['class'=>'btn btn-primary']) }}
                        </div>

                    </div>


                </form>
                <div class="row">
                    <div class="col-12 ajax-container" >
                        @if(request()->class_id && request()->isMethod('get') && $findRecord==null)
                        <h4 class="text-danger">No Data Found</h4>
                        @endif
                    </div>
                    <div class="col-12 ajax-details-container" ></div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    });
</script>
@endif
@if($findRecord)
<script>
    var url="{{ (auth()->user()->role == 2)?url('teacher/attendance_sheets/detail/'.$findRecord->id):url('backend/attendance_sheets/detail/'.$findRecord->id) }}";
     $.get(url, function ($res) {
            $('.ajax-container').addClass('d-none');
            $('.ajax-details-container').html($res.html);

        });
</script>
@endif

<script>

    function loadClasses() {
        var $schoolId = $('#school_id').val();
        $.get('<?= url('load-school-classes') ?>?school_id=' + $schoolId + '&teacher_id=', function ($html) {
            $('#school_class_id').html($html);
            $('#school_class_id').select2();
        });
    }
    function getStudents() {
        var form = '#filter-form';
        //sort-form
        var url = $(form).attr('action');
        var request = $.ajax({
            url: url,
            type: "POST",
            data: $(form).serialize(),
            dataType: "json"
        });
        request.done(function (msg) {
            $('.ajax-container').html(msg.html);
            $('.datepicker').datepicker({
                autoclose: true
            });
        });
        request.fail(function (jqXHR, textStatus) {
            $.toaster({
                priority: 'error',
                title: 'Fail',
                message: "Request failed: " + textStatus
            });
        });
        return false;
    }
    function getDetails($this) {
        var url = $($this).attr('href');
        $.get(url, function ($res) {
            $('.ajax-container').addClass('d-none');
            $('.ajax-details-container').html($res.html);
        });
        return false;
    }
    function closeDetail() {
        $('.ajax-container').removeClass('d-none');
        $('.ajax-details-container').html('');
        return false;
    }

</script>

@endsection