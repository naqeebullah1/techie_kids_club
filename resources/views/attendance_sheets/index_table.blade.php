<h3 class="">Student's attendance date wise</h3>
<table class="table table-bordered table-sm table-small table-striped">
    <thead>
        <tr>
            <th>Date</th>
            <th >Lesson</th>
            <th >Present</th>
            <th >Absent</th>
        </tr>
    </thead>
    <tbody>
        @foreach($students as $key=>$student)
        <?php
        $attendance = $student->attendance_sheet_lists;
        ?>
        <tr>
            <td>
                <?php
                $route = 'backend/attendance_sheets/detail/' . $student->id;
                if (auth()->user()->role == 2) {
                    $route = 'teacher/attendance_sheets/detail/' . $student->id;
                }
                ?>
                <a class="btn btn-success btn-small" href="<?= url($route) ?>" onclick="return getDetails(this)">
                    <?= formatDate($student->date) ?>
                </a>
            </td>
            <td><?= $student->lesson ?></td>
            <td class="text-success font-weight-bold">
                <?php
                $present = $attendance->where('is_present', 1)->count();
                echo $present;
                ?>
            </td>
            <td class="text-danger font-weight-bold"><?php
                $present = $attendance->where('is_present', 0)->count();
                echo $present;
                ?></td>
        </tr>
        @endforeach
    </tbody>
</table>


