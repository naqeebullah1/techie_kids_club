<div class="p-3 mt-3" style="background: #f0f8ff;">
    <b class="text-danger">Last Entry: <?= ($lastEntry) ? formatDate($lastEntry->date) : '--' ?></b>
    <?php
    $route = 'admin.attendance_sheets.store';
    if (auth()->user()->role == 2) {
    $route = 'teacher.attendance_sheets.store';
        
    }
    ?>
    <form action="{{ route($route) }}" method="post">
        @csrf
        <div class='row'>
            <div class="col-4">
                <input type="hidden" value="<?= request()->school_class_id ?>" name="attendance[school_class_id]">
                <label>Select Date</label>
                <input type="text" required=""  class="datepicker mr-2 form-control"  name="attendance[date]">
            </div>
            <div class="col">
                <label>Lesson</label>
                <input type="text" class="form-control" required="" name="attendance[lesson]">
            </div>
        </div>
        <h4>Class Students</h4>
        <table class="table table-bordered table-sm table-small table-striped table-secondary">
            <thead>
                <tr>
                    <th>Student</th>
                    <th width='10px' nowrap>Is Present</th>
                    <th>Note</th>
                </tr>
            </thead>
            <tbody>
                @foreach($students as $key=>$student)
                <tr>
                    <td><?= $student->student_name ?></td>
                    <td>
                        <input type="hidden"  name="students[{{$key}}][student_id]" value="<?= $student->id ?>">
                        <input type="hidden"  name="students[{{$key}}][is_present]" class="checkbox-field" value="0">
                        <input type="checkbox" onclick="changeCheckBox(this)">
                    </td>
                    <td>
                        <input type="input" class="form-control"  name="students[{{$key}}][note]">
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-right mt-2">
            <input type="submit" value="Create" class="btn btn-primary">
        </div>
    </form>
</div>

