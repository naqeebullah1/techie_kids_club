@extends('master')

@section('content')
<style>
    .editable-clear-x{
        display: none !important;
    }
    .form-control{
        padding:.6rem 1rem !important;
    }
    .table-responsive th,
    .table-responsive td {
        white-space: nowrap;
        border: 1px solid #ebedf2 !important;

    }
    .modal-lg{
        width: 1250px !important;
    }
</style>

<div class="d-flex">
    <h1 class="flex-grow-1 float-left">Demand Library</h1>
    @can('demand-liberaries-edit')
    <a href="{{ route('admin.demand_liberaries.create') }}" class="btn btn-success align-self-center" role="button">Create New</a>
    @endcan
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row justify-content-end">
                    <div class="col-3">
                        <input type="text" class="form-control p-4" placeholder="Search for keywords" id="search-dl" name="search">
                    </div>
                </div>
                <div class="row table-responsive dl-container">
                    @include('demand_liberaries.table')
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                &nbsp;
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <label>Link</label>
                <iframe width="100%" height="200px" src=""  allowfullscreen></iframe>
                <label>Description</label>
                <div class="desc"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif
<script type="text/javascript">
    function getMore($this) {
        var url = $($this).attr('href');
        $('.dl-container').load(url);
        return false;
    }
    function makeSlug($value) {
//        alert($value);
        var $slug = $value.toLowerCase().replace(' ', '-');
        $('#slug').val($slug);
    }
    function setUrl($url) {
        $('form#destroy').attr('action', $url);
    }
    function setDetails($id) {
        $.get("{{url('get-liberary-details')}}/" + $id, function (data) {
            $('.modal-body iframe').attr('src', data.link);
            $('.modal-body .desc').html(data.description);
        });
        return false;


    }

    $(function () {
        $('#myModal').on('hidden.bs.modal', function () {
            $('.modal-body iframe').attr('src', '');
        });
        //setup before functions
        var typingTimer;     //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 seconds for example
        var $input = $('#search-dl');

//on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

//on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

//user is "finished typing," do something
        function doneTyping() {
            var $val = $('#search-dl').val();
            $val = $val.split(' ').join('%20');
            $('.dl-container').load("<?= url()->full() ?>?type=" + $val);
        }

    })
</script>
@endsection