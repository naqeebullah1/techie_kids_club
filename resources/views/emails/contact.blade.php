<?php if(isset($details['select_one']) && $details['select_one'] != "") { ?>
<html>
    <head>
        <title>Techie Kids Club School Inquiry -  <?php $details['school_name'] ?> </title>
    </head>
    <body>
        <a href="' . url('/') . '" title="logo" target="_blank">
                            <img style="margin: auto;display: block;margin-bottom:10px;" width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="5" cellspacing="0">
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Contact Name:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;"><?php echo $details['first_name'] . " " .$details['last_name']  ?></td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Contact Title:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;"><?php echo $details['title'] ?></td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">School Name:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['school_name'] }}</td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">School Phone:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['phone'] }}</td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">E-mail:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['email'] }}</td>
            </tr>
             <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Contact me to schedule Demo Classes at my school:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['select_one'] }}</td>
            </tr>
        </table>
    </body>
</html>

<?php } else { ?>
<html>
    <head>
        <title>Techie Kids Club Inquiry - ' . $first_name . '</title>
    </head>
    <body>
        <a href="' . url('/') . '" title="logo" target="_blank">
                            <img style="margin: auto;display: block;margin-bottom:10px;" width="220" src="https://techiekidsclub.com/frontend/images/logo/techie_kids_logo.png" title="logo" alt="logo">
                          </a>
        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="5" cellspacing="0">
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Name:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;"><?php echo $details['first_name'] . " " .$details['last_name']  ?></td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">E-mail:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['email'] }}</td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Phone:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['phone'] }}</td>
            </tr>
            <tr style="height: 32px;">
                <th align="right" style="width:150px; padding-right:5px;">Message:</th>
                <td align="left" style="padding-left:5px; line-height: 20px;">{{ $details['message'] }}</td>
            </tr>
            
        </table>
    </body>
</html>
<?php } ?>