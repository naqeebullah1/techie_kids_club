<?php
$franchise = IDToSlug(auth()->user()->franchise_id);
//dd($franchise);
//            Techie Kids Club &#8211; Coding and Robotics for Kids Preschool &#8211; Third Grade
?>
<div class="ajax-loader" style="background:white;display:none;padding:10px;">
    <img src="{{asset('img/loading.gif')}}" style="display:block;margin:auto;" />
</div>

</div>
<!-- End of Content Container -->
</div>
<!-- END PAGE CONTENT -->
<!-- START COPYRIGHT -->
<!-- START CONTAINER FLUID -->
<!-- START CONTAINER FLUID -->
<div class=" container   container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <p class="small no-margin pull-left sm-pull-reset">
            <span class="hint-text">Copyright &copy; {{@date('Y')}} </span>
            <span class="font-montserrat">{{$franchise->name}}</span>.
            <span class="hint-text">All rights reserved. </span>
        </p>
        {{--<p class="small no-margin pull-right sm-pull-reset">
            <a href="https://www.cyberclouds.com">Cyber Clouds</a> <span class="hint-text"> made with Love</span>
              </p>--}}
        <div class="clearfix"></div>
    </div>
</div>
<!-- END COPYRIGHT -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->