<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit Franchise') }}
                    @else
                    {{ __('Create Franchise') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.franchises.store') }}">
                    @csrf
                    <div class="row mb-2">
                        @if(request()->id)
<!--                        <div class="col-lg">
                            <label>Admin</label>
                            {{ Form::select('user_id',[''=>'Select']+$schoolAdmins->toArray(),$school->user_id,['class'=>'select2']) }}
                        </div>-->
                        @endif
                        <div class="col-lg">
                            <label>Franchise Name</label>
                            <input value="{{ $school->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $school->name }}" type="text" class="form-control" onchange="nametoslug(this.value)" name="record[name]"/>
                        </div>
                        <div class="col-lg">
                            <label>Franchise Slug</label>
                            <input value="{{ $school->slug }}" type="text" class="form-control" id="slug" name="record[slug]"/>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg-8">
                            <label>Stripe Client Secret Key</label>
                            <input value="{{ $school->stripe_client }}" type="text" class="form-control" name="record[stripe_client]"/>
                        </div>
                        <div class="col-lg-4">
                            <label>Stripe Publishable</label>
                            <input value="{{ $school->stripe_publishable }}" type="text" class="form-control" name="record[stripe_publishable]"/>
                        </div>
                        <div class="col-lg-4">
                            <label>Stripe End Point Secret</label>
                            <input value="{{ $school->stripe_end_point_secret }}" type="text" class="form-control" name="record[stripe_end_point_secret]"/>
                        </div>
                    </div>
                    <div class="row mb-2 d-none">
                        <div class="col">
                            <label>Mailing Email Address</label>
                            <input value="{{ $school->mail_email }}" type="text" class="form-control" name="record[mail_email]"/>
                        </div>
                        <div class="col">
                            <label>Mailing Email Password</label>
                            <input value="{{ $school->mail_password }}" type="text" class="form-control" name="record[mail_password]"/>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-4">
                            <label>Header Logo</label>
                            <input type="file" name="header_logo" data-default-file="<?= ($school->header_logo) ? asset('frontend/images/logo/' . $school->header_logo) : '' ?>" class="dropify">
                        </div>
                        <div class="col-4">
                            <label>Footer Logo</label>
                            <input type="file" name="footer_logo" data-default-file="<?= ($school->footer_logo) ? asset('frontend/images/logo/' . $school->footer_logo) : '' ?>" class="dropify">
                        </div>
                        <div class="col-4">
                            <label>Favicon</label>
                            <input type="file" name="no_image_logo" data-default-file="<?= ($school->no_image_logo) ? asset('frontend/images/logo/' . $school->no_image_logo) : '' ?>" class="dropify">
                        </div>
                    </div>



                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>


    function nametoslug(Text) {
        $('#slug').val(Text.toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, ""));
    }
</script>