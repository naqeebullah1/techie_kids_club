@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Franchises</h1>
    <div class="clearfix"></div>

    
    @can('schools-edit')
    <a onclick="event.preventDefault();loadForm(this)" class="btn btn-success float-right" href="<?= url('backend/franchises-create') ?>">Create Franchise</a>
    @endcan

    <div class="clearfix"></div>
</div>
<!--include('admin.schools.search')-->
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row table-responsive" id="no-more-tables">
                    @include('franchises.table')
                </div>

            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif
@if(Session::has('error-message'))
<script>
    $(function () {
        $.toaster({priority: 'error', title: 'Error', message: "{{Session::get('error-message')}}"});
    })

</script>
@endif

@endsection