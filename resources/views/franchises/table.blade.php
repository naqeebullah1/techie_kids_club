<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Name</th>
            <th>User</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schools as $school)
        <tr>
            <td>
                <?= $school->name ?>
            </td>
            <td>
                <?= ($school->user_id)?$users[$school->user_id]:'<span class="font-weight-bold text-danger">No Admin Assigned</span>' ?>
            </td>

            <td nowrap>
                <a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/franchises-create/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $schools->links() }}
