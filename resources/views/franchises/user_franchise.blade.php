<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                   Modify User Franchise
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.user.franchise.store') }}">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-lg-4">
                            <label>User</label>
                            <input value="{{ request()->user_id }}" type="hidden" class="form-control" name="user_id"/>
                            <input value="{{ $user->first_name.' '.$user->last_name }}" style="color:#666" type="text" class="form-control"  readonly=""/>
                        </div>
                        <div class="col-lg">
                            <label>Franchise</label>
                            {{ Form::select('form_franchise_id[]',['Select']+franchiseList(),$user->user_franchises->pluck('id')->toArray(),['multiple','class'=>'dynamic-select']) }}
                        </div>
                    </div>
                    
                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        <button type="submit" class="mt-2 btn btn-success">Assign</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>