<?php
$st=0;
?>

@forelse($cart as $c)

<?php
$st=$c['class_charges']+$st;
?>
<div class=" cartbox__items">
    <div class="cartbox__item">
        <div class="cartbox__item__thumb">
            <a href="<?= asset('frontend/class-details/'.$c['school_class_id']) ?>">
                <img src="<?= $c['school_logo'] ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'">
            </a>
        </div>
        <div class="cartbox__item__content">
            <h5><a href="<?= asset('frontend/class-details/'.$c['school_class_id']) ?>"><?=$c['title']?></a></h5>
            <p><span class="font-weight-bold">School:</span> <span><?=$c['school_name']?></span></p>
            <p><span class="font-weight-bold">Student Name:</span> <span><?=$c['student_name']?></span></p>
            <p><span class="font-weight-bold">Classroom Name:</span> <span><?=$c['classroom_name']?></span></p>
            
            <span class="price">$<?=$c['class_charges']?></span>
        </div>
    </div>                     
</div>
@empty
<p class="text-danger">EMPTY</p>
@endforelse

<div class="cartbox__total">
    <ul>
        <li><span class="cartbox__total__title">Subtotal</span><span class="price">$<?=$st?></span></li>
        <!--<li class="shipping-charge"><span class="cartbox__total__title">Shipping Charge</span><span class="price">$05</span></li>-->
        <li class="grandtotal">Total<span class="price">$<?=$st?></span></li>
    </ul>
</div>
<div class="cartbox__buttons">
    <a class="dcare__btn" href="{{ url(franchiseLink()) }}/view-cart"><span>View cart</span></a>
    <a class="dcare__btn" href="{{ url(franchiseLink()) }}/checkout"><span>Checkout</span></a>
</div>
