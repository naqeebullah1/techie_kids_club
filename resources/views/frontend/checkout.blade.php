@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'Checkout']])
<!--<script src="https://js.stripe.com/v3/"></script>-->
<!--<script src="{{asset('frontend/js/checkout.js')}}" STRIPE_PAY_URL="{{url('parent/stripepay')}}" STRIPE_PUBLISHABLE_KEY="pk_live_51HyGh4B3WkD7XcJGGYMsUEIh1yNeIJZUxSQtmqi7qOMHWn9w31bFjqHkK3eADqnraBI7OhcysyZhj0StX9U6yV8b00THERnQeb" defer></script>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.1.2/css/paymentfont.min.css" rel="stylesheet" type="text/css" />


<style>
    .accordion-head {
        /*color: #fff;
        display: block;
        text-align: left;
        background: #FFE472;
        color: #222;
        padding: 20px;*/
    }

    .accordion-head:after {
        content: "\f078";
        font-family: 'FontAwesome';
        font-weight: 900;
        float: right;
    }
    #card-element {
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 54px;
        width: 100%;
        background: white;
    }

    #card-element form {
        width: 30vw;
        min-width: 500px;
        align-self: center;
        box-shadow: 0px 0px 0px 0.5px rgba(50, 50, 93, 0.1),
            0px 2px 5px 0px rgba(50, 50, 93, 0.1), 0px 1px 1.5px 0px rgba(0, 0, 0, 0.07);
        border-radius: 7px;
        padding: 40px;
    }

    #card-element input {
        border-radius: 6px;
        margin-bottom: 6px;
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 44px;
        font-size: 16px;
        width: 100%;
        background: white;
    }

    .accordion-head.collapsed:after {
        content: "\f077";
    }

    #card-info label {
        position: relative;
        color: #8898AA;
        font-weight: 300;
        height: 40px;
        line-height: 40px;
        margin-left: 0px;
        display: flex;
        flex-direction: row;
    }

    #card-info .group label:not(:last-child) {
        border-bottom: 1px solid #F0F5FA;
    }

    #card-info label > span {
        /*width: 120px;
        text-align: right;
        margin-right: 30px;*/
        width: 100px;
    }

    #card-info .field {
        background: transparent;
        font-weight: 300;
        border: 0;
        color: #31325F;
        outline: none;
        flex: 1;
        padding-right: 10px;
        padding-left: 10px;
        cursor: text;
    }
    .bordered-colored {
        background: #fff;   
        border: 1px solid #ccc;
        flex: auto;
        position:relative;
    }

    #card-info .field::-webkit-input-placeholder {
        color: #CFD7E0;
    }

    #card-info .field::-moz-placeholder {
        color: #CFD7E0;
    }

    #card-info .brand {
        width: 30px;
        position: absolute;
        right: 10px;
        top: 0px;
    }
    .list-credit-cards .cardblock {
        background:#f8f8f8;   
    }
   
</style>
<section class="htc__checkout bg--white section-padding--lg">
    <!-- Checkout Section Start-->
    <div class="checkout-section">
        <div class="container">
            <div class="row">
                @if(Session::has('success'))
                <p class="text-success" style="font-weight: bold;font-size: 32px;"><?= Session::get('success') ?></p>
                @endif

                @if(Session::has('error'))
                <p class="text-danger" style="font-weight: bold;font-size: 32px;"><?= Session::get('error') ?></p>
                @endif
                @if(session('cart_info'))
                <p class="alert alert-danger hidden" style="width: 100%;margin-left: 15px;margin-right: 15px;" id="paymentResponse"></p>

                <div class="col-lg-12 col-12 mb-30">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <!-- Checkout Accordion Start -->
                            <div id="checkout-accordion">

                                <!-- Checkout Method -->
                                <div class="single-accordion">
                                    <form id="subscriptionFrm" method="POST">
                                        @csrf()
                                        <?php
                                        if (auth()->user()->first_name == "" || auth()->user()->last_name == "" || auth()->user()->email == "") {
                                            $iscollapsed = "";
                                            $isShow = " show";
                                        } else {
                                            $iscollapsed = " collapsed";
                                            $isShow = " hide";
                                        }
                                        ?>
                                        <a class="accordion-head <?php echo $iscollapsed; ?>" data-toggle="collapse" data-parent="#checkout-accordion" href="#checkout-method">Step 1. Billing Information</a>
                                        <div class="clearfix"></div>
                                        <div id="checkout-method" class="collapse <?php echo $isShow; ?>">
                                            <div class="checkout-method accordion-body fix">
                                                <div class="d-flex form-group">
                                                    <div class="mr-2" style="flex-grow: 1">
                                                        <label>First NAME</label>
                                                        <input type="hidden" id="charges" required="" autofocus="">
                                                        <input value="<?= auth()->user()->first_name ?>" type="text" id="first_name" class="form-control" placeholder="First Name" required="" autofocus="">
                                                    </div>

                                                    <div style="flex-grow: 1">
                                                        <label>Last NAME</label>
                                                        <input type="text" value="<?= auth()->user()->last_name ?>" id="last_name" class="form-control" placeholder="Last name" required="" autofocus="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>EMAIL</label>
                                                    <input value="<?= auth()->user()->email ?>" type="email" id="email" class="form-control" placeholder="Enter email" required="">
                                                </div>

                                                <!-- Form submit button -->
                                                <!--<button id="submitBtn" class="btn btn-success">-->
                                                <!--    <div class="spinner hidden" id="spinner"></div>-->
                                                <!--    <span id="buttonText">Proceed</span>-->
                                                <!--</button>-->


                                            </div>
                                        </div><!-- Billing Info Accordian end -->
                                        <!-- Card Info Accordian Start -->
                                        <a class="accordion-head" data-toggle="collapse" data-parent="#checkout-accordion" href="#card-info" style="margin-top:10px">Step 2. Card Information</a>
                                        <div class="clearfix"></div>
                                        <div id="card-info" class="collapse show" >
                                            <div class="checkout-method accordion-body fix">
                                                <div class="row form-group">
                                                    <div class="col-lg-12">
                                                        
                                                        @if(Session::get('selectedFranchise') and Session::get('radioselectedFranchise'))
                                                        @if($cardlist == "")
                                                        <p class="float-left text-danger">You have not added any card to Stripe yet </p> <a class="float-right btn btn-warning btn-sm" style="font-weight:bold;font-size:12px;" href="{{ url('parent/'.franchiseLink()) }}/creditcard">
                                                        Add New Card</a>
                                                        <div class="clearfix"></div>
                                                        <small class=""><strong>Note: </strong>We do not store any of your credit card info, these are listed from Stripe</small>
                                                        <br />
                                                        @endif

                                                        @if(isset($cardlist) && $cardlist != "")
                                                        <p class="float-left text-primary">Choose Your Credit Card To Pay From</p> <a class="float-right btn btn-warning btn-sm" style="font-weight:bold;font-size:12px;" href="{{url('parent/'.franchiseLink())}}/creditcard">Add New Card</a>
                                                        <div class="clearfix"></div>
                                                        <small class=""><strong>Note: </strong>We do not store any of your credit card info, these are listed from Stripe</small>
                                                        <br />
                                                        <div class="list-credit-cards row" id="list-cards">
                                                            
                                                             @foreach($cardlist as $key => $card)
                                                                <div class="col-lg-6">
                                                                <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                <input type="radio" value="{{$card['id']}}" name="pmt_id" class="radio-card-select" id="radio-card-select-{{$key}}" />
                                                                <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                    <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                </span>
                                                                <span style="display:inline-block;margin-left: 35px;text-align: right;font-size: 12px;">{{date('M',strtotime($card['card']['exp_month']))}}-{{$card['card']['exp_year']}}</span>
                                                                <br />
                                                                <span style="padding-left: 25px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                 
                                                                 </label>
                                                                 </div>
                                                            @endforeach 
                                                            
                                                        </div>
                                                        <br />
                                                        @endif 
                                                        @else
                                                        <p class="alert alert-info">Please choose the product to load credit cards</p>
                                                        @endif
                                                    </div>
                                                    <!--<div id="card-element">
                                                        
                                                    </div>-->

                                                    <!--<div class="col-lg-12">
                                                        <p class="text-info" style="margin-bottom:10px;"><strong>Please provide your credit/debit card details</strong></p>
                                                        <label>

                                                            <span>Card Number</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-number-element" class="field"></div>

                                                                <span class="brand">
                                                                    <i class="pf pf-credit-card" id="brand-icon"></i>
                                                                </span>
                                                            </div>
                                                        </label>
                                                    </div>


                                                    <div class="col-lg-7">
                                                        <label>
                                                            <span>Expiry Date</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-expiry-element" class="field"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <label>
                                                            <span style="width: 36px;">CVC</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-cvc-element" class="field"></div>
                                                            </div>    
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label>
                                                            <span>Postal Code</span>
                                                            <div class="bordered-colored">
                                                                <input id="postal-code" maxlength="6" name="postal_code" class="field" style="width: 100%;height: 40px;" required/>
                                                            </div>
                                                        </label>
                                                    </div>-->


                                                </div>
                                                <input name="cart_index" type="hidden" value="" id="cart_index">
                                            </div>
                                        </div>    
                                        <!-- Card Info Accordian End -->
                                    </form>
                                </div>
                            </div><!-- Checkout Accordion Start -->
                        </div>
                        <!-- Order Details -->
                        <div class="col-lg-6 col-12 ">

                            <div class="order-details-wrapper">
                                <h2>Step 3. Checkout Your Order</h2>
                                <div class="order-details disabled">
                                    <!--<form action="#">-->
                                    @include('frontend.partials.calc_order')

                                    <!--</form>-->
                                </div>
                            </div>

                        </div>                    
                    </div>
                </div>

                @else
                <p class="text-danger text-center" style="font-weight: bold;font-size: 32px;margin:auto;">Cart is empty</p>

                @endif
            </div>
        </div>
    </div><!-- Checkout Section End-->             
</section>  
@endsection
@section('style')
<style>
    .order-details{
        background: white;
        border: 1px solid #f1f2f3;
    }
    .order-details.disabled,#checkout-method.disabled{
        /*pointer-events: none !important;*/
        background: #f1f2f3; 
    }
</style>
@endsection
@section('script')
<script>
function setIndex($index) {
    $('#cart_index').val($index);
}
</script>
<script>

    function addToCart($url) {
        $url = $url + '?student_id=' + $('#child').val() + '&classroom_name=' + $('#classroom_name').val();
        $.get($url, function (data) {
            $('.cart-container').html(data.html);
            $('#item-count').html(data.count);
        });
        toastr.success('Item added to cart');
        return false;
    }
    function enableBtn(id) {
        var allowbtn = $("#" + id).attr('class').split(' ')[1];
        var smash = atob($("#"+id).attr('smash'));
        $("#charges").attr('value',smash);
        $("#charges").val(smash);
        if ($("#" + id).is(':checked')) {
            $("#" + allowbtn).attr('disabled', false);
            //$("#" + allowbtn).attr('form', 'subscriptionFrm');
        } else {
            $("#" + allowbtn).attr('disabled', true);
            $("#" + allowbtn).removeAttr('form');
        }
    }
    function setFranchise(id) {
        var currentUrl = window.location.href;
        var url =  currentUrl + '?productId=' + id;
        window.location.href=url;
    }
    function searchPC() {

        var pc = $('#promocode').val();
        $(".checkmark-cart").prop('checked',false);
        $.get('<?= url('parent/apply-promocode') ?>/' + pc + '?st=' + $('#sub_total').val(), function ($data) {
            
            if ($data.status == 1) {
                
                $('.order-details').html($data.html);
                return true;


                var $promoAMount = '';
                if ($data.promocode.promo_type == 'flat') {
                    var $promoAMount = '$' + $data.promocode.flat_amount;
                }
                if ($data.promocode.promo_type == 'percent') {
                    var $promoAMount = $data.promocode.percent_off + '%';
                }
                var $html = '<p class="strong">Applied Promo code: </p>\
                               <p class="strong float-right text-center" style="padding-left: 20px;">' + $promoAMount + '<button class="btn btn-danger mr-1 resetpromo" type="button" onclick="window.location.reload()" style="line-height: unset;height: auto;"><i class="fa fa-trash"></i></button></p>';
               
                $('.promo-container').html($html);
                $('.sub-total-container').html("$" + $data.st);
                $('#sub_total').val($data.st);
            } else {
                $.alert({
                    title: 'Alert!',
                    content: $data.message,
                });
            }
        });
    }
    $("input[type=radio][name=pmt_id]").on("change",function(){
             
    });
    $("body").on("click",".btn-pay",function(){
        
       var btnid = $(this).attr('id');
      //console.log($("#subscriptionFrm").serialize());
      //return false;
       let cart_index = document.getElementById("cart_index").value;
        $('#pay-'+cart_index).attr('disabled','disabled');
        $('#pay-'+cart_index).css('display','none');
        $('.btn-pay').attr('disabled','disabled');
        $(".dummy-"+cart_index).css("display","block");
    
     
       $.ajax({
          type: "POST",
          url: "{{url('parent/stripepayPMT')}}",
          data:$("#subscriptionFrm").serialize(),
          success: function(data)
            {   
                
                var data = JSON.parse(data);
                
                if(data.payment_id) {
                    $('#pay-'+data.cart_index).replaceWith('<span class="fa fa-check" style="font-size: 24px;color: green;margin-left:35px;"></span>');
                            $(".dummy-"+data.cart_index).css("display","none");
                            addToCart('https://techiekidsclub.com/update-cart');
                            
                            if ($(".cart-check").find(".btn-pay").length <= 0){ 
                                
                                window.location.href = 'https://techiekidsclub.com/thanks';
                            } else {
                                $('.btn-pay').attr('disabled',false);
                            }
                } else {
                     $('#pay-'+cart_index).css('display','block');
                    $("#pay-"+cart_index).attr('disabled',false);
                    $("#checkmark-"+cart_index).prop("checked",false);
                    $(".checkmark-cart").prop("checked",false);
                    $('.btn-pay').attr('disabled','false');
                    $(".dummy-"+cart_index).css("display","none");
                    
                    showMessage(data.error);
                }
                //alert(data);
                //console.log(data);
            }
        });
    
    });
    function showMessage(messageText) {
    const messageContainer = document.querySelector("#paymentResponse");

    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;

    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 15000);
        var targetPosition = $('#paymentResponse').offset().top;

        $('html, body').animate({
                scrollTop: targetPosition
        }, 1000); // Adjust the duration as needed
}
</script>

@endsection

