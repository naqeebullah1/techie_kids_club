@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'Class Details']])
<style>
    .service__icon{
        background-size: cover !important;
        background-position: center !important;
    }
</style>
<section class="dcare__courses__area bg-image--1 pb-0">
    <div class="container">
        <a href="{{url('/')}}/frontend/school-details/{{$school->school->id}}" class="dcare__btn float-right"><span>&laquo; Go Back</span></a>
        <div class="clearfix"></div>
        <div class="row ">
            <!-- Start Single Courses -->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="class__list bg-white" style="min-height: 500px">
                    <div class="courses__thumb">
                        <a href="#">
                            <img src="<?= asset('images/uploads/' . $school->school->image) ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" alt="class images" style="width:auto;max-height:400px;">
                        </a>
                    </div>
                    <div class="courses__inner">
                        <div class="courses__wrap">
                            <!--<div class="courses__type"></div>-->
                            <div class="courses__content">
                                <div class="class__title mb-3" >
                                    <h4 class="d-flex" style="width: 100%">
                                        <a style="flex-grow: 1" href="javascript:"><?= $school->title ?></a>
                                        Price  :<span style="color:#fe5629;">   $<?= $school->class_charges ?></span>
                                    </h4>

                                </div>

                                <ul class="school-details">
                                    <li class="mb-1"><i class="fa fa-building mr-2" style="font-size: 20px;"></i> <?= $school->school->name ?></li>
                                    <!--<li class="mb-1"><i class="fa fa-calendar mr-2" style="font-size: 20px;"></i> <?= formatDate($school->start_on) ?> - <?= formatDate($school->end_on) ?></li>-->
                                    <li class="mb-1"><i class="fa fa-clock-o mr-2" style="font-size: 20px;"></i> <?= formatTime($school->start_at) ?> - <?= formatTime($school->end_at) ?></li>
                                </ul>
                                <?php if ($school->weekly_off_days) { ?>
                                    <h5 class="mt-3 font-weight-bold mb-2">Payment</h5>
                                    <p><strong><?= ucfirst($school->duration_type) ?></strong></p>
                                    <h5 class="mt-3 font-weight-bold mb-2">Week Days</h5>
                                    <?php
                                    $weekly_off_days = $school->weekly_off_days;
                                    if ($weekly_off_days) {
                                        $weekly_off_days = explode(',', $weekly_off_days);
                                        if ($weekly_off_days) {
                                            echo "<div style='padding-left:15px;'><ul style='list-style: circle;' class='d-flex'>";
                                            foreach ($weekly_off_days as $day):
                                                echo '<li style="width:60px;">' . weekDays()[$day] . '</li>';
                                            endforeach;
                                            echo "</ul></div>";
                                        }else {
                                            echo "<p>No Days</p>";
                                        }
                                    } else {
                                        echo "<p>No Days</p>";
                                    }
                                }
                                ?>

                                <!--<h5 class="mt-3 font-weight-bold mb-2">Description</h5>-->
<!--                                <p>
                                    Lor error sit volupta aclaud antium, toe ape sriam, ab illnv ritatis et quasi jhaie zanfin archite tbeatae viliq.Lorem ipsum dolor sit ameveritatis evoluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consemagnam aliquam quaerat voluptatem.
                                </p>-->
                                @if(Auth::user())
                                <?php if (empty(Auth::user()->students->toArray())) { ?>
                                    <div class="mt-2 mb-2">
                                        <p class="text-info"><strong>In order to purchase this class, please add your child first</strong></p>
                                        <a href="{{url('/')}}/parent/dashboard" class="dcare__btn btn-sm btn-info">Add Child</a>
                                    </div>
                                <?php } else { ?>
                                    <div class="mt-2 mb-2">

                                        <select id="child" class="form-control">
                                            <option value="">Select Child</option>
                                            @foreach(Auth::user()->students as $student)
                                            <option value="<?= $student->id ?>"><?= $student->first_name . ' ' . $student->last_name ?></option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mt-2 mb-2">
                                        <input required type="text" name="classroom_name" id="classroom_name" class="form-control" placeholder="Enter classroom name">
                                        <label>* The classroom where your child typically is during the day</label>
                                    </div>
                                    <div class="">

                                        <a class="dcare__btn btn-sm" href="<?= url('front-franchise/'.request()->franchise_id.'/update-cart/' . $school->id) ?>" onclick="return addToCart(this)">
                                            <i class="fa fa-cart-arrow-down" style="font-size: 20px;margin-right: 5px"></i>
                                            Add To Cart
                                        </a>
                                    </div>
                                <?php } ?>
                                @else
                                <p class="text-danger font-weight-bold">Please login to enroll in this class</p>
                                <a href="{{url('front-franchise/'.request()->franchise_id)}}/parentlogin" class="dcare__btn btn-success"><span>Login Now</span></a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

@endsection
@section('script')
<script>

    function addToCart($url) {
        if ($('#child').val() == '') {
            $.alert({
                title: 'Alert!',
                content: 'Please select child',
            });
            return false;
        }
        if ($('#classroom_name').val() == '') {
            $.alert({
                title: 'Alert!',
                content: 'Please enter classroom name',
            });
            return false;
        }
        $url = $url + '?student_id=' + $('#child').val() + '&classroom_name=' + $('#classroom_name').val();
        $.get($url, function (data) {
            $('.cart-container').html(data.html);
            $('#item-count').html(data.count);
        });
        toastr.success('Item added to cart');
        return false;
    }
</script>
@endsection
