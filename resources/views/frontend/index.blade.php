@extends('layouts.front_theme')
@section('content')
<?php
$sections = [];
if ($page) {
    $sections = $page->pageDetails;
//    dd($sections);
}
$banner_loaded = 0;
?>
@foreach($sections as $section)

@include('frontend.section_designs.'.$section->section_type)
<?php
if($section->section_type == 13) {
    $banner_loaded = 1;
}
?>
@endforeach
<?php $newsletter = \App\Models\NewsletterDesign::first() ?>
@if($newsletter != null)
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700;900&display=swap" rel="stylesheet">
    <style>

        .modal-dialog {
            max-width: 100%;
            margin: auto;
        }
        .modal-content {
            background-image: url("{{asset('/frontend/images/newsletter/') . '/' . $newsletter->image}}");
            background-position: center;
            background-size: cover;
            top: 50px;
            background-repeat: no-repeat;
            margin: auto;
            width: 100%;
            height: auto;
            border-radius: 15px;
            box-shadow: 0px 0px 5px #747272;
            max-width: 1000px;
        }
        .newsletter-content {
            padding-left: 66px;
            margin-top: 135px;
        }
        .newsletter-content .close {
            font-size: 2.5rem !important;
            margin-top: -135px !important;
        }
        .newsletter-content h2 {
            color: #00a7ff;
            font-size: 50px;
            font-weight: 700;
            font-family: 'Poppins', sans-serif;
            filter: drop-shadow(5.142px 5.142px 4.5px #000000);
        }
        .newsletter-content .subheading {
            color: #ff9000;
            font-size: 30px;
            font-weight: 900;
            font-style: italic;
            font-family: 'Poppins', sans-serif;
            text-transform: uppercase;
            margin-top: 10px;
        }
        .newsletter-content .middle-text {
            color: #454545;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Poppins', sans-serif;
            margin-top: 10px;
            letter-spacing: 1px;
            text-align: justify;
            text-justify: inter-word;
            margin-left: 3px;
        }
        .newsletter-content .bottom-content {
            color: #000000;
            font-size: 25px;
            font-weight: 600;
            font-family: 'Poppins', sans-serif;
            margin-top: 10px;
            font-style: italic;
            margin-bottom: 15px;
            letter-spacing: 1px;
        }
        .modal-dialog input[name=email]{
            width: 350px;
            border-radius: 0;
            color: #08a7e2;
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            height: 35px;
        }
        .modal-dialog .submit-newsletter {
            margin-bottom: 4px;
            height: 33px;
            cursor: pointer;
        }
        @media (max-width:600px) {
            .newsletter-content {
                background: rgba(255,255,255,0.6);
                    padding-left: 10px;
                    padding-right: 10px;
                    margin-top:35px;
                }
                .newsletter-content .close {
                    margin-top: 0px !important;
                }
                #newsletterModal {
                    overflow-y:scroll !important;
                }
        }
    </style>
@endif
<!-- Modal -->
<div class="modal" id="newsletterModal" tabindex="999" role="dialog" aria-labelledby="#newsletterModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{url(franchiseLink()) . '/newsletters'}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="newsletter-content"><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
                                {!! $newsletter->content !!}
                                <input class="form-control d-inline" name="email" placeholder="Enter Your Email" type="email" />
                                <button style="background: transparent;border: none;margin-left: -10px;" type="submit"><img class="submit-newsletter d-inline" src="{{asset('frontend/images/submit.png')}}"/></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    @if(request()->path() == 'front-franchise/techie-kids-club')
        if(!sessionStorage.getItem('newsletterPopupClosed') && !localStorage.getItem('newsletterSubscribed'))
            setTimeout(function () {
                $('#newsletterModal').show();
            }, 3000)
    @endif
    $('.close').click(function (e) {
        $('#newsletterModal').hide();
        sessionStorage.setItem('newsletterPopupClosed', true);
    });

    $('.submit-newsletter').click(function () {
        localStorage.setItem('newsletterSubscribed', true);
    });

    function changeTab($this) {

        var $tab = $($this).attr('href');

        $('.custom-tabs .nav-link').removeClass('active');
        $('.tab-container').addClass('d-none');
        $($this).addClass('active');
        $($tab).removeClass('d-none');
        return false;
    }
</script>
@endsection
