@extends('layouts.front_theme')
@section('content')
<?php
$franchise = findBySlug(request()->franchise_id);
?>
<style>
    .nav-link{
        border-radius: 0 !important;
    }
    .btn-outline-primary:not([disabled]):not(.disabled).active{
        box-shadow: none !important;
    }
    label.error{
        color:red;
    }
</style>
<div class="ht__bradcaump__area">
    <div class="ht__bradcaump__container">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">

                        <nav class="bradcaump-inner">
                            <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
                            <span class="brd-separetor"><img src="{{asset('frontend/images/icons/brad.png')}}" alt="separator images"></span>
                            <span class="breadcrumb-item active">Login / Register</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-padding--xs bg-pngimage--2">

    <div class="row justify-content-center">
        <div class="col-xs-11 col-sm-11 col-md-8 col-lg-5 p-0" style="border: 1px solid #ccc;box-shadow: 1px 1px 20px rgb(0 0 0 / 15%);">
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                {{$error}}
            </div>
            @endforeach
            @if(Session::has('error'))

            <div class="alert alert-danger">
                {{Session::get('error')}}
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
                @if(Session::has('messaging_status'))
                    <div class="alert alert-primary">
                        {{Session::get('messaging_status')}}
                    </div>
                @endif

            <!-- Pills navs -->
            <ul class="nav nav-pills nav-justified mb-3" id="ex1" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link btn  btn-outline-primary active" id="tab-login" data-mdb-toggle="pill" href="#pills-login" role="tab"
                       aria-controls="pills-login" aria-selected="true">Login</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link btn  btn-outline-primary" id="tab-register" data-mdb-toggle="pill" href="#pills-register" role="tab"
                       aria-controls="pills-register" aria-selected="false">Register</a>
                </li>
            </ul>
            <!-- Pills navs -->

            <!-- Pills content -->
            <div class="tab-content p-4">

                <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="tab-login">

                    <form action="{{url('/franchise/'.request()->franchise_id)}}/dologin" id="login-form" method="post">
                        <input type="hidden" name="franchise_id" value="{{ request()->franchise_id }}" />
                        @csrf
                        <div class="form-outline mb-2">
                            <label class="form-label" for="loginName">Login As</label>
                            {{ Form::select('login_type',loginTypesFrontend(),'',['class'=>'form-control custom-dd']) }}
                        </div>
                        <!-- Email input -->
                        <div class="form-outline mb-2">
                            <label class="form-label" for="loginName">Email</label>
                            <input name="email" required=""  type="email" id="loginName" class="form-control" />
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-2">
                            <label class="form-label" for="loginPassword">Password</label>
                            <div class="controls" style="position:relative">
                                <input name="password" type="password" id="loginPassword" class="form-control" />
                                <span toggle="#loginPassword" class="fa fa-fw fa-eye field-icon toggle-password" style="position: absolute;display: inline;right: 5px;
                                      top: 35%;"></span></div>
                        </div>
                        <div class="mb-4">
                            <label class="font-weight-bold">Please write "<?php echo ($franchise->name) ?>" in green box</label>
                            <div class="d-flex ">
                                <input name="red_box" type="text" style="color:black !important;background:#FEDAD0 !important;" class="form-control mr-2 bg-danger">
                                <input name="green_box" type="text" style="color:black !important; background:#D2F1EB !important;" class="form-control bg-success">
                            </div>
                            @error('green_box')
                            <label id="green_box-error" class="text-danger error" for="green_box">{{$message}}</label>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label class="" for="allow_photo_release">
                                <input type="checkbox" id="allow_photo_release" class="" name="allow_photo_release" value="1">
                                <strong>I agree to allow photos of my child(ren) to be used in promotional materials for Techie Kids Club (<a target="_blank" href="{{url('/')}}/privacy-policy">Privacy Policy</a>)</strong></label> <br /><span class="text-info">* Techie Kids Club will never share or sell your data or images</span>
                        </div>

                        <!-- 2 column grid layout -->
                        <div class="row mb-2">
                            <!-- Checkbox -->
                            <!--                            <div class="col-md-6 d-flex justify-content-center">
                                                            <div class="form-check mb-3 mb-md-0">
                                                                <input class="form-check-input" type="checkbox" value="" id="loginCheck" checked />
                                                                <label class="form-check-label" for="loginCheck"> Remember me </label>
                                                            </div>
                                                        </div>-->

                            <!-- Simple link -->
                            <!--                            <div class="col-md-6 d-flex justify-content-center">
                                                            <a href="#!">Forgot password?</a>
                                                        </div>-->
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-block mb-2">Sign in</button>
                        <div class="form-group d-md-flex">
                            <div class="w-100 text-md-right">
                                <a href="<?= url(franchiseLink()) ?>/forgotPassword" class="text-info m-l-10">Forgot Password?</a>
                            </div>
                        </div>
                        <!-- Register buttons -->
                        <!--                        <div class="text-center">
                                                    <p>Not a member? <a href="#!">Register</a></p>
                                                </div>-->
                    </form>
                </div>
                <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="tab-register">
                    <form action="{{route('register-parent',request()->franchise_id)}}" enctype="multipart/form-data" id="register-form" method="post">
                        @csrf
                        <!-- Name input -->
                        <div class="row mb-2">
                            <div class="col-lg-6 pr-1">
                                <div class="form-outline">
                                    <label class="form-label" for="registerName">First Name</label>
                                    <input type="hidden" name="roles[]" value="3" class="form-control" />
                                    <input type="hidden" name="is_parent" value="1" class="form-control" />
                                    <input  type="text" required name="first_name" value="{{old('first_name')}}" id="registerName" class="form-control" />
                                </div>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <div class="form-outline">
                                    <label class="form-label" for="registerName">Last Name</label>
                                    <input type="text" name="last_name" required value="{{old('last_name')}}" id="last_name" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <!-- Email input -->
                        <div class="row mb-2">
                            <div class="col-lg-6 pr-1">
                                <div class="form-outline">
                                    <label class="form-label" for="registerEmail">Email</label>
                                    <input type="email" name="email" required value="{{old('email')}}"  id="r-email" class="form-control" />
                                </div>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <div class="form-outline">
                                    <label class="form-label" for="registerEmail">Confirm Email</label>
                                    <input type="email" name="email_confirmation" required value="{{old('email_confirmation')}}"  id="registerEmail" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <!-- Phone input -->
                        <div class="row mb-2">
                            <div class="col-lg-6 pr-1">
                                <div class="form-outline">
                                    <label class="form-label" for="phone1">Phone</label>
                                    <input type="tel" name="phone1" value="{{old('phone1')}}" pattern="^\(\d{3}\) \d{3}-\d{4}?$" required id="phone1" class="form-control phone" />
                                </div>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <div class="form-outline">
                                    <label class="form-label" for="phone2">Alternate Phone</label>
                                    <input type="tel" name="phone2" value="{{old('phone2')}}" pattern="^\(\d{3}\) \d{3}-\d{4}?$"  id="phone2" class="form-control phone" />
                                </div>
                            </div>
                        </div>


                        <div class="form-outline mb-2 mt-3 d-flex">
                            <h5 style="font-weight: bold;flex-grow: 1;">Child(ren) Information</h5>
                            <button class="btn btn-success btn-small btn-sm" type="button" onclick="addmorechild();"  style="font-size: 14px;"><i class="fa fa-plus"></i> Add More</button>
                        </div>

                        <div class="child-container">
                            <?php
                            $i = 0;
                            $students = [new \App\Models\Student];
                            if (old('students')) {
                                $students = old('students');
                                $i = 1;
                            }
                            ?>
                            @foreach($students as $student)
                            <div class="child row pt-2 mb-1" style="background: #f8f8f8;">
                                <div class="col-12 text-right">
                                    <a onclick="removeEle(this)" style="font-size: 20px;" href="javascript:;"><i class="fa fa-trash"></i></a>
                                </div>
                                <div class="col-6">
                                    <div class="form-outline mb-2">
                                        <label class="form-label">First Name</label>
                                        <input type="text" value="<?= $student['first_name'] ?>" name="students[0][first_name]" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-outline mb-2">
                                        <label class="form-label">Last Name</label>
                                        <input type="text" value="<?= $student['last_name'] ?>" name="students[0][last_name]" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label class="form-label">Profile Picture</label>
                                    <div class=" form-outline mb-2 pp">
                                        <input type="file" name="students[0][image]" class="dropify" />
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="form-outline my-2 mt-4">
                            <label class="" for="check_sms_service">
                                <input type="checkbox" id="check_sms_service" class="" name="check_sms_service" value="1" />
                                <strong>Check here to <strong>Opt-In SMS</strong> service <?= ucwords($franchise->name)?></label>
                        </div>
                        <div class="form-outline mb-2 mt-4">
                            <label class="" for="check_privacy">
                                <input type="checkbox" id="check_privacy" class="" name="check_privacy" value="1" />
                                <strong>Check here to indicate that you have read and agree to the <?= ucwords($franchise->name)?> <a target="_blank" href="{{url('/front-franchise/'.request()->franchise_id)}}/privacy-policy">Privacy Policy</a></strong></label>
                        </div>
                        <div class="form-outline mb-2">
                            <label class="" for="check_terms">
                                <input type="checkbox" id="check_terms" class="" name="check_terms" value="1" />
                                <strong>Check here to indicate that you have read and agree to the <?= ucwords($franchise->name)?> <a target="_blank" href="{{url('/front-franchise/'.request()->franchise_id)}}/terms-conditions">Terms & Conditions</a></strong></label>
                        </div>


                        <!-- Password input -->
                        <!--                        <div class="form-outline mb-2">
                                                    <label class="form-label" for="registerPassword">Password</label>
                                                    <input type="password" id="registerPassword" class="form-control" />
                                                </div>-->

                        <!-- Repeat Password input -->
                        <!--                        <div class="form-outline mb-2">
                                                    <label class="form-label" for="registerRepeatPassword">Repeat password</label>
                                                    <input type="password" id="registerRepeatPassword" class="form-control" />
                                                </div>-->
                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-block mb-3">Sign Up</button>
                    </form>

                </div>
            </div>
            <!-- Pills content -->
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    var i = 1;
    function addmorechild() {
        var cloneEle = $('.child-container .row:first').clone();
        cloneEle.find('input').each(function () {
            this.value = '';
            this.name = this.name.replace('[0]', '[' + i + ']');
        });
        cloneEle.find('.pp').html('<input type="file" name="students[' + i + '][image]" class="dropify" />');

        $('.child-container').append(cloneEle);
        $('.dropify').dropify();
        i++;
    }
    function removeEle($this) {
        if ($('.child-container .row').length == 1) {
            alert('Atleast 1 child must be added');
            return false;
        }
        $($this).parents('.child').remove();
    }
    $(document).ready(function () {
        $('.dropify').dropify();
        $('#register-form').submit(function () {

            $("#register-form").validate({
                rules: {
                    first_name: {
                        required: true,
                    },
                    last_name: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    email_confirmation: {
                        required: true,
                        equalTo: "#r-email"
                    }
                }/*,
                 messages: {
                 login: {
                 required: "To pole jest wymagane!"
                 }
                 }*/
            })
            //return false;
        });
    });
    $('.nav-link').click(function (e) {
        e.preventDefault();

        $('.nav-link').removeClass('active');
        $('.tab-pane').removeClass('active');
        $('.tab-pane').removeClass('show');

        var $target = $(this).attr('href');
        $(this).addClass('active');
        $($target).addClass('active');
        $($target).addClass('show');

//        alert($(this).attr('href'));
    });
</script>
@endsection
