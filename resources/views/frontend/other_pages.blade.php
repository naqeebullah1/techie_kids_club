@extends('layouts.front_theme')
@section('content')
<style>
    .elementor-element.elementor-element-11c0b1c > .elementor-container{
        min-height: 300px !important;
    }
    .elementor-heading-title.elementor-size-default{
        text-align: center;
    }
    .elementor-section.elementor-top-section .elementor-background-overlay{
        background-color: #1E3143;
        background-image: url("<?= asset('public/frontend/wp-content/uploads/2022/11/close-up-of-security-camera-on-wall-security-system-concept.jpg') ?>");
        background-position: center center;
        background-repeat: repeat;
        background-size: cover;
        opacity: 0.1;
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
    }
</style>
<?php
$service = new App\Models\Service;
$siteDetail = siteDetails();
?>
@include('frontend.services.banner',['banner'=>['title'=>$page->title,'bc'=>$page->title]])

<?php
$sections = $page->pageDetails;
?>

@foreach ($sections as $section)
@if ($section->section_type == 1)
@include('frontend/section_designs/three_columns', [
'sections' => $section->sections,
])
@elseif($section->section_type == 3)
@include('frontend/section_designs/left_sided_image_dark', [
'section' => $section,
])
@elseif($section->section_type == 2)
@include('frontend/section_designs/two_columns', ['section' => $section])
@elseif($section->section_type == 4)
@include('frontend/section_designs/left_sided_image', [
'section' => $section,
])
@elseif($section->section_type == 5)
@include('frontend/section_designs/info_section', [
'section' => $section,
])
@endif
@endforeach

@endsection
