<ul>
    <li class="d-flex">
        <p class="strong" style="width:80%">product</p>
        <p class="strong text-center" >Amount</p>
    </li>
    <?php
    $st = 0;

    $promo = Session::get('promos');
    $count=0;
    
    ?>

    @foreach(session('cart_info') as $i=>$item)
    <?php
        $discounted = number_format($item['class_charges'],2);
        ?>
        @if($promo)
        <?php
            $discounted = number_format($item['class_charges'] - $item['discount'], 2);
            //$st =  '111'.$discounted;
            ?>
        <!--<div style="border-top:1px solid #ddd;padding-top: 10px">
            <p style="width:80%">
                <strong>Applied Promo Code:</strong> 
            </p>
            <p class="text-center">-$<?php //$item['discount'] ?></p>
        </div>-->
        <!--<div>
            <p style="width:80%">
                <strong>Total</strong> 
            </p>
            
            <p class="text-center">$<?= $discounted ?></p>
        </div>-->
        @else
        <?php
       // $st =  $item['class_charges'];
        
        ?>
        @endif
       
    <li <?php if(Session::get('radioselectedFranchise') != null && Session::get('radioselectedFranchise') == $item['school_class_id']."-".$count) { echo "style='border:3px solid #28a745 !important'"; } else { echo "style='border-bottom:5px solid #e9e9e9 !important'"; } ?>>
        <div style="min-height:100px;">
            
            <p style="width:80%"><input type="radio" name="selectedProduct" <?php if(Session::get('radioselectedFranchise') != null && Session::get('radioselectedFranchise') == $item['school_class_id']."-".$count) { echo  "checked"; } ?> id="franchise{{$item['school_class_id']}}-{{$count}}" value="{{$item['school_class_id']}}-{{$count}}" onChange="setFranchise('{{$item['school_class_id']}}-{{$count}}')"/><strong>School:</strong> <?= $item['school_name'] ?><br /> <strong>Class:</strong> <?= $item['title'] ?> <br /> <strong>Student:</strong> <?= $item['student_name'] ?><br /> <strong>Classroom:</strong> <?= $item['classroom_name'] ?><br /><strong>Franchise:</strong> <?= App\Models\Franchise::find($item['franchise'])->name ?></p>
            
            @if(Session::get('radioselectedFranchise') != null && Session::get('radioselectedFranchise') == $item['school_class_id']."-".$count)
                 @if($promo)
                 <?php  $st = $discounted; ?>
                 @else
                 <?php $st = $item['class_charges']; ?>
                 @endif
            @endif
            @if($promo && Session::get('radioselectedFranchise') != null && Session::get('radioselectedFranchise') == $item['school_class_id']."-".$count)
                
                <p class="text-center"><del>$<?= $item['class_charges'] ?></del><br /><span class="text text-danger" style="font-size:16px;font-weight:bold">$<?= $discounted ?></span><br /><span class="badge badge-warning">-$<?php echo $item['discount'] ?></span></p>
                @else
                <p class="text-center">$<?= $item['class_charges'] ?></p>
                
                @endif
        </div>
        

        <?php if(Session::get('radioselectedFranchise') != null && Session::get('radioselectedFranchise') == $item['school_class_id']."-".$count) { ?>
        <div class="cart-check text-info">
            <label class="float-left" style="width:75%">
                <input type="checkbox" value="1" id="checkmark-<?= $i ?>" class="checkmark-cart pay-<?= $i ?>" onChange="enableBtn(id)" smash="<?php echo base64_encode($discounted); ?>"/>
                &nbsp;By clicking here you agree that you will be charged <strong>$<?= $discounted ?> {{strtolower(@$item['duration_type']?$item['duration_type']:'')}}</strong>.
            </label>
            <button  disabled="disabled" id="pay-<?= $i ?>" onclick="setIndex({{$i}})" class="btn btn-success btn-small btn-sm float-right btn-pay">Pay Now</button>
            <button disabled="disabled"  style="display:none;" class="dummy-<?= $i ?> btn btn-success btn-small btn-sm float-right">Please wait....</button>
        </div>
        <?php } ?>
    </li>
    <?php  $count++; ?>
    @endforeach
    @if(Session::get('selectedFranchise') == 1 && Session::get('radioselectedFranchise') != null)
    <li class="promo-container">
        <?php
        $promo = Session::get('promos');
        ?>
        @if($promo)
        <div class="float-left"><p style="width:100%" class="strong">Applied Promo code &nbsp;<small class="badge badge-info" style="letter-spacing:1px;font-size:16px;font-family:calibri;padding:2px 10px"><?php echo $promo['promo_code']; ?></small></p> </div>

        <p class="strong float-right text-center" style="padding-left: 20px;">
            <button class="btn btn-sm btn-danger mr-1 resetpromo" type="button" onclick="window.location.reload()" style="line-height: unset;height: auto;"><i class="fa fa-trash"></i></button>
        </p>
        @else
        <div class="d-flex">
            <input type="text" class="form-control mr-1 col-lg-7" id="promocode" placeholder="Promo Code">
            <button class="dcare__btn" type="button" onclick="searchPC()" id="search-pc" style="line-height: unset;height: auto;">Apply</button>
        </div>
        @endif
    </li>
    @endif
        @if(Session::get('selectedFranchise') == 1 && Session::get('radioselectedFranchise') != null)
         @if($promo)
         <li>
        <p class="strong">Discount</p>
        <p class="strong float-right text-center sub-total-container" style="padding-left: 20px;">
        <?php
//                                                $st = subPromoAmount($promo, $st);
            echo formatPromoAMount($promo);
            ?>
            </p>
            </li>
            @endif
            <li>
            <input type="hidden" id="sub_total" value="<?= number_format($st, 2) ?>">
        <p class="strong">order total</p>
        <p class="strong float-right text-center sub-total-container" style="padding-left: 20px;">
            $<?= number_format($st, 2) ?>
        </p>
    </li>
    @endif
</ul>