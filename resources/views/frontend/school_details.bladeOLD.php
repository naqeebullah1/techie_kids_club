@extends('layouts.front_theme')
@section('content')
@include('frontend.partials.banner',['data'=>['title'=>'School Details']])
<style>
    .service__icon{
        background-size: cover !important;
        background-position: center !important;
    }
</style>
<section class="dcare__courses__area bg-image--1 pb-0">
    <div class="container">
        <div class="row ">
            <!-- Start Single Courses -->
            <div class=" col-lg-12 col-md-12 col-sm-12">
                <div class="class__list bg-white">
                    <div class="courses__thumb">
                        <a href="#">
                            <img src="<?= asset('images/uploads/' . $school->image) ?>" alt="class images" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" style="width:auto;max-height:400px;">
                        </a>
                    </div>
                    <div class="courses__inner">
                        <div class="courses__wrap">
                            <!--<div class="courses__type">Course Type : <span>Basic</span></div>-->
                            <div class="courses__content">
                                <div class="class__title">
                                    <h4><a href="javascript:"><?= $school->name ?></a></h4>
                                </div>
                                <ul class="school-details">
                                    <li><i class="fa fa-phone mr-2"></i> 
                                    <?php
                                    
                                    $format_phone =
                                        substr($school->contact, -10, -7) . "-" .
                                        substr($school->contact, -7, -4) . "-" .
                                        substr($school->contact, -4);
                                        echo "<a href='tel:$format_phone'>".$format_phone."</a>";
                                    ?>
                                    </li>
                                    <li><i class="fa fa-envelope mr-2"></i> <a href="mailto:{{$school->email}}"><?= $school->email ?></a></li>
                                    <li><i class="fa fa-map-marker mr-2"></i> <?= $school->address ?>, <?=$school->city->city?>, <?=$school->state->state?></li>
                                </ul>
                                <!--<h5 class="mt-3 font-weight-bold mb-2 text-info" id="scroll-class" style="text-align: center;margin-top: 50px !important;font-size: 22px;"><a href="javascript:" style="color:#264499;">Scroll Down to Select Class <i class="fa fa-angle-down"></i></a></h5>-->
                                <!--<p>
                                    Lor error sit volupta aclaud antium, toe ape sriam, ab illnv ritatis et quasi jhaie zanfin archite tbeatae viliq.Lorem ipsum dolor sit ameveritatis evoluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consemagnam aliquam quaerat voluptatem.
                                </p>-->

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- Start Our Service Area -->
<section class="junior__service bg-image--1" id="classes-gird">
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1>School Classes</h1>
            </div>
        </div>
        <div class="row">
            <!-- Start Single Service -->
            <?php
            $classes = $school->school_classes->where('status',1);
//            dd($classes);
            ?>
            @foreach($classes as $c=>$subSection)
            <?php
                if($school->image == "") {
                    $school_image = asset('frontend/images')."/logo-noimage.png";
                } else {
                    $school_image = asset('/images/uploads/' . $school->image);
                }
            ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12" style="margin-top: 50px;">
                <div class="service bg--white border__color wow fadeInUp">
                    <div class="service__icon" style="background-image: url('<?= $school_image ?>')">
                        <!--<img src="" alt="icon images">-->
                    </div>
                    <div class="service__details">
                        <h6>
                            <a href="<?=url('frontend/class-details/'.$subSection->id)?>"><?= $subSection->title ?></a>
                            <!--<b>Start From: </b> <?= formatDate($subSection->start_on) ?>--> 
                            
                             <?php if($subSection->weekly_off_days) { ?>
                                <b>Week Days: </b>
                                <?php
                                $weekly_off_days = $subSection->weekly_off_days;
                                if ($weekly_off_days) {
                                    $weekly_off_days = explode(',', $weekly_off_days);
                                    if ($weekly_off_days) {
                                        echo "";
                                        foreach ($weekly_off_days as $day):
                                            echo '<span>' . weekDays()[$day] . '</span>&nbsp;&nbsp;';
                                        endforeach;
                                        echo "";
                                    }else {
                                        echo "<p>No Days Found</p>";
                                    }
                                } else {
                                    echo "<p>No Days Found</p>";
                                }
                                }
                                ?>
                                <br />
                                <p>&nbsp;</p>
                            <b>Timing: </b> 
                            <?= formatTime($subSection->start_at) ?> - <?= formatTime($subSection->end_at) ?> 

                        </h6>
                        <div class="service__btn">
                            <a class="dcare__btn--2" href="<?=url('frontend/class-details/'.$subSection->id)?>">Select Class <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Single Service -->
        </div>
    </div>
</section>
<!-- End Our Service Area -->

@endsection
@section('script')
<script>
    $("#scroll-class").on("click",function(){
        $('html, body').animate({
                scrollTop: $("#classes-gird").offset().top
            }, 500); 
    });
</script>
@endsection
