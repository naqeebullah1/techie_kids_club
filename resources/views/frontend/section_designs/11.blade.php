<!-- End Bradcaump area -->
<?php
$siteDetail = siteDetails();
?>
<section class="page__contact bg--white pb--50 pt--50">
    <div class="container">
        <a id="contactsection"></a>
        <div class="row">
            <div class="col-lg-5">
                <div class="section__title text-center">
                    <h2 class="title__line">Support</h2>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="address location">
                            <div class="ct__icon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="address__inner">
                                <h2>Support Hours</h2>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporquaerat voluptatem.</p>-->
                                <ul>
                                    <li><?= $siteDetail->hours ?> <br><br></li>
                                </ul>
                            </div>
                        </div>  
                    </div>
                    <div class="col-12">
                        <div class="address email">
                            <div class="ct__icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="address__inner">
                                <h2>E-mail Address</h2>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporquaerat voluptatem.</p>-->
                                <ul>
                                    <!--<li><a href="mailto:+08097-654321">juniorhomeschool.@email.com</a></li>-->
                                    <li><a href="mailto:<?= $siteDetail->email ?>"><?= $siteDetail->email ?></a><br><br></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="address phone">
                            <div class="ct__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="address__inner">
                                <h2>Phone Number</h2>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporquaerat voluptatem.</p>-->
                                <ul>
                                    <li><a href="tell:<?= $siteDetail->contact ?>"><?= $siteDetail->contact ?></a></li>
                                    <li><a href="tell:<?= $siteDetail->contact2 ?>"><?= $siteDetail->contact2 ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="section__title text-center">
                    <h2 class="title__line">How can we help?</h2>
                    <?= replaceCodes($siteDetail->description) ?>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunte magna aliquaet, consectetempora incidunt</p>-->
                </div>
                <section class="contact__box section-padding--lg bg-image--27">
                    <?php $style = ""; ?>
                    @if(session()->has('success'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('success')}}
                    </div>
                    <?php $style = "display:none;" ?>        
                    @endif
                    <div class="contact-form-wrap" style="<?php echo $style; ?>">
                        <form action="<?= route('sendMessage', request()->franchise_id) ?>" method="post">
                            @if($errors->any())
                            <?= implode('', $errors->all('<div class="alert alert-danger">:message</div>')) ?>
                            @endif

                            @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                            @endif

                            @csrf
                            <?php
                            $f = $l = $e = $p = $m = '';
                            if (old('contact')) {
                                $cont = old('contact');
                                $f = $cont['first_name'];
                                $l = $cont['last_name'];
                                $e = $cont['email'];
                                $p = $cont['phone'];
                                $m = $cont['message'];
                            }
                            ?>
                            <div class="single-contact-form name">
                                <input type="text" name="contact[first_name]" value="<?= $f ?>" placeholder="First Name*" required>
                                <input type="text" name="contact[last_name]" value="<?= $l ?>" placeholder="Last Name*" required>
                            </div>
                            <div class="single-contact-form name">
                                <input type="email" value="<?= $e ?>" name="contact[email]" placeholder="E-mail*" required>
                                <input type="text" name="contact[phone]" value="<?= $p ?>" placeholder="Phone*" pattern='^\(\d{3}\) \d{3}-\d{4}?$' required class="phone">
                            </div>
                            <div class="single-contact-form message">
                                <textarea name="contact[message]" required placeholder="How can we help?"><?= $m ?></textarea>
                            </div>
                            <div class="contact-btn">
                                <button type="submit" class="dcare__btn">submit</button>
                            </div>
                        </form>
                    </div> 
                    <div class="form-output">
                        <p class="form-messege"></p>
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>
