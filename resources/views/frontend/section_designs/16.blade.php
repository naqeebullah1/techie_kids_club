<!--Tabs Section-->
<?php
$sectionTabs = $section->sections;

?>
<style>
.tab-container ul {
    list-style:square;
}
</style>

<!--4 Sections design-->
<section class="dcare__team__area pb--100 bg--white bg-image--21" style="padding-top:20px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">{{replaceCodes($section->title)}}</h2>
                    <p>{{replaceCodes($section->sub_title)}}</p>
                </div>
            </div>
        </div>
        
        <div class="row mt--40">
            <div class="col-lg-12">
                <ul class="nav nav-tabs custom-tabs">
                    <?php $k = 1; ?>
                    @foreach($sectionTabs as $key=>$sectionTab)
                    <li class="nav-item">
                        <a class="nav-link @if($k ==1) active @endif" onclick="return changeTab(this)" aria-current="page" href="#tab<?php echo $k; ?>">{{replaceCodes($sectionTab->title)}}</a>
                    </li>
                    <?php $k++; ?>
                    @endforeach
                    
                </ul>
            </div>
            <div class="col-lg-12">
                <?php $j = 1; ?>
                   @foreach($sectionTabs as $key=>$sectionTab)
                <div id="tab<?php echo $j; ?>" class=" tab-container @if($j !=1) d-none @endif" style="padding:10px;"><?= replaceCodes($sectionTab->description) ?></div>
                <?php $j++; ?>
                    @endforeach

            </div>
        </div>
    </div>
</section>
<!-- End Team Area -->
