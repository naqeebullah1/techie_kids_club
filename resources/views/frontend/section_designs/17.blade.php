<!--Grid View Gallery-->
<!-- Start Our Gallery Area -->
<?php
$gallery = getGallery();
?>
<div class="junior__gallery__area gallery-page-one gallery__masonry__activation gallery--3 bg-image--25 section-padding--lg">
    <div class="container">
       <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line">{{replaceCodes($section->title)}}</h2>
                    <p>{{replaceCodes($section->sub_title)}}</p>
                </div>
            </div>
        </div>
       <!-- <div class="row d-none">
            <div class="col-sm-12">
                <div class="gallery__menu">
                    <button data-filter="*"  class="is-checked">All</button>
                    <button data-filter=".cat--1">Learning</button>
                    <button data-filter=".cat--2">Games</button>
                    <button data-filter=".cat--3">Event</button>
                </div>
            </div>
        </div>-->
        <div class="row galler__wrap masonry__wrap mt--80">
            <!-- Start Single Gallery -->
            <?php
            $images = $gallery->images;
            ?>
            @foreach($images as $image)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 gallery__item cat--2">
                <div class="gallery">
                    <div class="gallery__thumb">
                        <a href="<?= asset('images/thumbnails/' . $image->image) ?>" data-lightbox="grportimg">
                            <img src="<?= asset('images/thumbnails/' . $image->image) ?>" alt="gallery images">
                        </a>
                    </div>
                    <div class="gallery__hover__inner">
                        <div class="gallery__hover__action">
                            <ul class="gallery__zoom">
                                <li><a href="<?= asset('images/thumbnails/' . $image->image) ?>" data-lightbox="grportimg" data-title=""><i class="fa fa-search"></i></a></li>
                                <!--<li><a href="gallery-details.html"><i class="fa fa-link"></i></a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>	
            </div>
            @endforeach
            	
           
        </div>	
    </div>
</div>
<!-- End Our Gallery Area -->