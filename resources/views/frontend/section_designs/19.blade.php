<!--Demo Steps-->
<!-- Start Our Service Area -->
<section class="junior__service bg-white pb--100 mt--50">
    <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center pb--70">
                        <h1 class=""><?= replaceCodes($section->title) ?></h1>
                    </div>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="row">
            <!-- Start Single Service -->
            <?php
            $subSections = $section->sections;
            ?>
            @foreach($subSections as $c=>$subSection)
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="service bg--white border__color demosteps border__color--<?=$c+1?> wow fadeInUp">
                    <!--<div class="service__icon">
                        <img src="<?= asset('/images/thumbnails/'.$subSection->icon) ?>" alt="icon images">
                    </div>-->
                    <div class="service__details" style="padding-top:30px;">
                        <h4><?=replaceCodes($subSection->title)?></h4>
                        <p style="margin-top:10px;">
                            
                                <span class="content_show"><?=replaceCodes($subSection->description)?></span>
                                <!--Why Coding +<br /> Robotics For Kids?-->
                            
                        </p>
                        @if($subSection->btn_title && $subSection->btn_link)
                        <div class="service__btn">
                            <a class="dcare__btn--2" href="<?=$subSection->btn_link;?>"><?=$subSection->btn_title;?> <i class="fa <?=$subSection->btn_icon;?>"></i></a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Single Service -->
        </div>
         @if($section->btn_title != "" && $section->btn_link != "")
                    <div class="wel__btn" style="margin-top:40px;display: flex;">
                        <a class="dcare__btn" href="<?=$section->btn_link;?>" style="margin:auto;"><?=$section->btn_title; ?> <i class="fa <?=$section->btn_icon?>"></i></a>
                    </div>
                    @endif
    </div>
</section>

<!-- End Our Service Area -->