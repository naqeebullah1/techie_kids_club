
<!-- Start Welcame Area -->
<section class="junior__welcome__area section-padding--xs bg-pngimage--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__line"><?= replaceCodes($section->title) ?></h2>
                    <p><strong><?= replaceCodes($section->sub_title) ?></strong></p>
                </div>
            </div>
        </div>
        <div class="row jn__welcome__wrapper align-items-center">
            <div class="col-md-12 col-lg-6 col-sm-12">
                <?= replaceCodes($section->section) ?>
                @if($section->btn_title != "" && $section->btn_link != "")
                    <div class="wel__btn" style="margin-top:20px;">
                        <a class="dcare__btn" href="<?=$section->btn_link;?>"><?=$section->btn_title; ?> <i class="fa <?=$section->btn_icon?>"></i></a>
                    </div>
                    @endif
            </div>
            <div class="col-md-12 col-lg-6 col-sm-12 md-mt-40 sm-mt-40">
                <div class="jnr__Welcome__thumb">
                    <img src="<?=asset('images/thumbnails/'.$section->image)?>">
                    <a class="play__btn" href="<?=$section->link?>?rel=0"><i class="fa fa-play"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Welcame Area -->
