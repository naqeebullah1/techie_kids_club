<?php 
    $testimonial_schools = testimonial_schools();
?>

<!-- Start Testimonial Area -->
<section class="junior__testimonial__area bg-image--10 section-padding--lg">
    <div class="container">
        <div class="section__title text-center white--title">
            <h2 class="title__line"><?=replaceCodes($section->title)?></h2>
        </div>
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="testimonial__container">
                    <div class="testimonial-carousel">
                        
                        <?php foreach($testimonial_schools as $testimonial): ?>
                        <div class="testimonial__bg">
                            <!-- Start Single Testimonial -->
                            <div class="testimonial text-center">
                                <div class="testimonial__inner">
                                    <div class="test__icon">
                                        <img src="<?= asset('/frontend') ?>/images/testimonial/icon/1.png" alt="icon images">
                                    </div>
                                    <div class="client__details" style="min-height: 380px !important;">
                                        <p>{{$testimonial['description']}}</p>
                                        <div class="client__info">
                                            <img src="{{asset('frontend/images/testimonial')}}/{{$testimonial['link']}}" style="width:120px;display:block;margin:auto;margin-bottom:10px;" onerror="this.src='{{asset('frontend/images/testimonial')}}/no-image.png'" />
                                            <h6>{{$testimonial['parent_name']}}</h6>
                                            <span>{{$testimonial['school']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Testimonial -->
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonial Area -->
