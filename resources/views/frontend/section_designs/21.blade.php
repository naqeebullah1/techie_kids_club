  <section class="u-clearfix u-palette-4-base u-section-10" id="sec-4577">
    <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
      <div class="u-clearfix u-expanded-width u-layout-wrap u-layout-wrap-1">
        <div class="u-layout">
          <div class="u-layout-row">

            <div class="video">
              <div id="player"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
     var tag = document.createElement("script");
  tag.src = "https://www.youtube.com/iframe_api";
  document.head.appendChild(tag);
  // here
  var player1;

  function onYouTubeIframeAPIReady() {
    player1 = new YT.Player("player", {
      height: "400",
      width: "100%",
      videoId: "{{ $section->link }}",
      playerVars: {
        controls: 1,
        loop: 1
      },
      events: {
        onReady: function(e) {
          e.target.mute();
          videoAutoControl(".video", e.target, 0.5);
        },
        // here
        onStateChange: onPlayerStateChange
      }
    });
  }

  // here
  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
      player1.seekTo(0);
      player1.playVideo();
    }
  }

  function videoAutoControl(selector, video, videoTogglePct) {
    var videoHeightPct = $(selector).height() / $(window).height();
    var offsetIn = (1 - videoTogglePct * videoHeightPct) * 100;
    var offsetOut = -(videoTogglePct * videoHeightPct) * 100;

    function inView(video) {
      video.playVideo();
    }

    function outView(video) {
      video.pauseVideo();
    }
    $(selector).waypoint(
      function(direction) {
        if (direction == "down") inView(video);
        // 可視範圍往下；影片從下往上進入可視範圍，播放
        else outView(video); // 影片離開，暫停
      }, {
        offset: offsetIn + "%"
      }
    );
    $(selector).waypoint(
      function(direction) {
        if (direction == "up") inView(video);
        // 可視範圍往上；影片從上往下進入可視範圍，播放
        else outView(video); // 影片離開，暫停
      }, {
        offset: offsetOut + "%"
      }
    );
  }

  </script>
