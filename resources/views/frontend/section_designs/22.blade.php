<div class="ht__bradcaump__area">
    <div class="ht__bradcaump__container">
        <div class="ht_image_wrapper" style="background-position: center;background-size: cover;background-image:url('<?=asset('images/thumbnails/'.$section->image)?>')">
        <!--<img src="<?=asset('images/thumbnails/'.$section->image)?>" class="ht_banner_image" alt="{{$section->title}}">-->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="slider__activation" style="margin-top:10%">
                        <!-- Start Single Slide -->
                        @php $section->title = rtrim($section->title); @endphp
                        @php $section->sub_title = rtrim($section->sub_title); @endphp
                        <div class="slide">
                            <div class="slide__inner">
                                <?php if($section->title) { ?>
                                <h1><?=replaceCodes($section->title)?></h1>
                                <?php } ?>
                            <?php if($section->sub_title) { ?>
                            <div class="slider__text">
                                    <h2><?=replaceCodes($section->sub_title)?></h2>                                
                            </div>
                              <?php } ?>                                
                        </div>
                        </div>
                        
                        <!-- End Single Slide -->
                    </div>
                    </div>
                    </div>
            </div>
        </div>
       
    </div>
</div>