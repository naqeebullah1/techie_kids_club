
<!-- Start Welcame Area -->
<section class="junior__welcome__area welcome--2 section-padding--lg">
    <div class="container">
        <div class="row jn__welcome__wrapper align-items-center">
            <div class="col-md-12 col-lg-4 col-sm-12">
                <div class="">
                    <img src="<?= asset('images/thumbnails/'.$section->image) ?>" alt="images">
                    <!-- <a class="play__btn" href="https://www.youtube.com/watch?v=MCJEkZtqlBk"><i class="fa fa-play"></i></a> -->
                </div>
            </div>
            <div class="col-md-12 col-lg-8 col-sm-12 md-mt-40 sm-mt-40">
                <div class="welcome__juniro__inner">
                    <h3><?=replaceCodes($section->title) ?></h3>
                    <p>
                        <?=replaceCodes($section->section) ?></p>
                    @if($section->btn_title != "" && $section->btn_link != "")
                    <div class="wel__btn" style="margin-top:20px;">
                        <a class="dcare__btn" href="<?=$section->btn_link;?>"><?=$section->btn_title; ?> <i class="fa <?=$section->btn_icon?>"></i></a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Welcame Area -->