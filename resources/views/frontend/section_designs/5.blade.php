<?php 
$sub=$section->sections;

?>
<!-- Start Choose Us Area -->
<section class="dcare__choose__us__area section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section__title text-center">
                    <h2 class="title__line"><?= replaceCodes($section->title) ?></h2>
                    <p>
                        <!--We start with coding, but we don’t end there! Our curriculum reinforces important life skills, essential to success in kindergarten and beyond.-->
                        <?= replaceCodes($section->sub_title) ?>

                    </p>
                </div>
            </div>
        </div>
        <div class="row mt--40">
            <!-- Start Single Choose Option -->
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="dacre__choose__option">
                    <!-- Start Single Choose -->
                    <div class="choose">
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[0]->title)?></a></h4>
                            <p><?=  replaceCodes($sub[0]->description)?></p>
                        </div>
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[0]->icon) ?>">
                        </div>
                    </div>
                    
                    <div class="choose">
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[1]->title)?></a></h4>
                            <p><?=replaceCodes($sub[1]->description)?></p>
                        </div>
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[1]->icon) ?>">
                        </div>
                    </div>
                    <div class="choose">
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[2]->title)?></a></h4>
                            <p><?=replaceCodes($sub[2]->description)?></p>
                        </div>
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[2]->icon) ?>">
                        </div>
                    </div>
                    <!-- End Single Choose -->
                   
                    <!-- End Single Choose -->
                </div>
            </div>
            <!-- End Single Choose Option -->
            <!-- Start Single Choose Option -->
            <div class="col-lg-4 col-md-6 col-sm-12 d-block d-lg-block d-md-none">
                <div class="dacre__choose__option">
                    <div class="choose__big__img">
                        <img src="<?=asset('images/thumbnails/'.$section->image)?>" alt="choose images">
                    </div>
                </div>
            </div>
            <!-- End Single Choose Option -->
            <!-- Start Single Choose Option -->
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="dacre__choose__option text__align--left">
                    <!-- Start Single Choose -->
                    <div class="choose">
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[3]->icon) ?>">
                        </div>
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[3]->title)?></a></h4>
                            <p>
                                <?=replaceCodes($sub[3]->description)?>
                            </p>
                        </div>
                    </div>
                    <div class="choose">
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[4]->icon) ?>">
                        </div>
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[4]->title)?></a></h4>
                            <p>
                                <?=replaceCodes($sub[4]->description)?>
                            </p>
                        </div>
                    </div>
                    <div class="choose">
                        <div class="choose__icon">
                            <img src="<?= asset('/images/thumbnails/'.$sub[5]->icon) ?>">
                        </div>
                        <div class="choose__inner">
                            <h4><a href="javascript:"><?=replaceCodes($sub[5]->title)?></a></h4>
                            <p>
                                <?=replaceCodes($sub[5]->description)?>
                            </p>
                        </div>
                    </div>
                    
                    <!-- End Single Choose -->
                </div>
            </div>
            <!-- End Single Choose Option -->
        </div>
    </div>
</section>
<!-- End Choose Us Area -->