<?php 
    $testimonial_parents = testimonial_parents();
?>

<!-- Start Testimonial Area -->
<section class="junior__testimonial__area bg-image--10 section-padding--lg">
    <div class="container">
        <div class="section__title text-center white--title">
            <h2 class="title__line"><?=replaceCodes($section->title)?></h2>
        </div>
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="testimonial__container">
                    <div class="testimonial-carousel">
                        
                        <?php foreach($testimonial_parents as $testimonial): ?>
                        <div class="testimonial__bg">
                            <!-- Start Single Testimonial -->
                            <div class="testimonial text-center">
                                <div class="testimonial__inner">
                                    <div class="test__icon">
                                        <img src="<?= asset('/frontend') ?>/images/testimonial/icon/1.png" alt="icon images">
                                    </div>
                                    <div class="client__details">
                                        <p>{{$testimonial['description']}}</p>
                                        <div class="client__info">
                                            <h6>{{$testimonial['parent_name']}}, {{$testimonial['relation_with_child']}}</h6>
                                            <span>{{$testimonial['school']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Testimonial -->
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Testimonial Area -->
