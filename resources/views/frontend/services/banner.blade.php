
<section style="background-color: #1e3143" class="elementor-section elementor-top-section elementor-element elementor-element-11c0b1c elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle" data-id="11c0b1c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-background-overlay"></div>
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-776c10e" data-id="776c10e" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-5936da7 elementor-widget elementor-widget-heading" data-id="5936da7" data-element_type="widget" data-widget_type="heading.default">
                    <div class="elementor-widget-container">
                        <h2 style="color: white" class="elementor-heading-title elementor-size-default"><?= $banner['title'] ?></h2>		
                    </div>
                </div>
                <div class="elementor-element elementor-element-31a69e3 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="31a69e3" data-element_type="widget" data-widget_type="icon-list.default">
                    <div class="elementor-widget-container">
                        <ul class="elementor-icon-list-items elementor-inline-items">
                            <li class="elementor-icon-list-item elementor-inline-item">
                                <span style="color: white" class="elementor-icon-list-text">Home</span>
                            </li>
                            <li class="elementor-icon-list-item elementor-inline-item">
                                <span style="color: white" class="elementor-icon-list-text">/ <?= $banner['bc'] ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>