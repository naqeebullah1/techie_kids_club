<div class="header p-r-0" style="background: linear-gradient(to right, #eee 10%,#1e3143);">
    <div class="header-inner" style="height: 90px">
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu text-white" data-toggle="horizontal-menu"></a>
        <div class="">
            <div class="brand inline no-border d-sm-inline-block">
                <img style="width: 160px;" src="{{asset('public/frontend/images/logo/techie_kids_logo.png')}}" 
                     alt="{{config('app.name')}}" 
                     data-src="{{asset('public/frontend/images/logo/techie_kids_logo.png')}}" 
                     data-src-retina="{{asset('public/frontend/images/logo/techie_kids_logo.png')}}" class="img-fluid">
            </div>
        </div>
        <div class="d-flex align-items-center">
            <!-- START User Info-->

            <div class="dropdown pull-right">
                <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="pull-left p-r-10 fs-14 font-heading d-lg-inline-block text-white">
                        <span class="semi-bold">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>
                    </div>
                </button>
                <?php
                $user = Auth::user();
                $role = $user->ModelHasRoles->pluck('role_id')->toArray();
                $authRole = $user->role;
                ?>
                @if($authRole==2)
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <!-- <a href="{{url('/backend/user-profile')}}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a> -->
                    <a href="{{url('/')}}/teacher/change-password" class="dropdown-item"><i class="pg-settings_small"></i> Change Password</a>
                    <a href="javascript:" onClick="event.preventDefault();document.getElementById('logout-form').submit()" class="clearfix bg-master-lighter dropdown-item">
                        <form action="{{route('teacher.logout')}}" method="POST" style="display:none;" id="logout-form">
                            @csrf
                            @method('POST')
                        </form>
                        <span class="pull-left">Logout</span>
                        <span class="pull-right"><i class="pg-power"></i></span>
                    </a>
                </div>
                @else
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <!-- <a href="{{url('/backend/user-profile')}}" class="dropdown-item"><i class="fa fa-user"></i> Profile</a> -->
                    <a href="{{url('/')}}/backend/change-password" class="dropdown-item"><i class="pg-settings_small"></i> Change Password</a>
                    <a href="javascript:" onClick="event.preventDefault();document.getElementById('logout-form').submit()" class="clearfix bg-master-lighter dropdown-item">
                        <form action="{{route('admin.logout')}}" method="POST" style="display:none;" id="logout-form">
                            @csrf
                            @method('POST')
                        </form>
                        <span class="pull-left">Logout</span>
                        <span class="pull-right"><i class="pg-power"></i></span>
                    </a>
                </div>
                @endif
            </div>
            <!-- END User Info-->
        </div>
    </div>
    @auth
    <div class="bg-white">
        <div class="container">
            <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="">
                <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-close" data-toggle="horizontal-menu"></a>

                @if($authRole==2)
                <ul>
                    <li class="@if(in_array('dashboard',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/dashboard">Dashboard</a>
                    </li>

                    <li class="@if(in_array('schools',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/teacher/schools">Schools</a>
                    </li>                    
                    <li class="@if(in_array('demand-liberaries',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/teacher/demand-liberaries">Demand Library</a>
                    </li>

                    <li class="@if(in_array('reports',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/teacher/reports/search-all">Search</a>
                    </li>
                </ul>
                @else
                <ul>
                    <li class="@if(in_array('dashboard',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/dashboard">Dashboard</a>
                    </li>
                    <li class="@if(in_array('scheduler',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/scheduler">Scheduler</a>
                    </li>
                    <li class="@if(in_array('teacher-gr',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/teacher-gr">Graph</a>
                    </li>
                    <li class="@if(in_array('vacant-time',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/vacant-time">Vacant Time</a>
                    </li>
                    <li class="@if(in_array('students-summary-report',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/reports/students-summary-report">Student Summary</a>
                    </li>
                    @canany(['user-list','role-list'])
                    <li class="@if(in_array('users',Request::segments()) || in_array('users-list',Request::segments()) || in_array('roles',Request::segments())) {{'active'}} @endif">
                        <a class="nav-link dropdown-toggle"
                           id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                           href="javascript:">Users</a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                            @can('user-list')
                            <a class="nav-link" href="{{ route('admin.users.index') }}">Manage Users</a>
                            @endcan
                            @can('role-list')
                            <a class="nav-link" href="{{ route('admin.roles.index') }}">Manage Roles</a>
                            @endcan
                        </div>
                    </li> 
                    @endcanany

                    @canany(['user-list','role-list'])
                    <li class="@if(in_array('children',Request::segments()) || in_array('parent-list',Request::segments())) {{'active'}} @endif">
                        <a href="{{ route('admin.users.parentlist') }}">Parents</a>

                    </li> 
                    @endcanany

                    @can('banner-list')
                    <li class="@if(in_array('banners',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/banners">Banners</a>
                    </li>
                    @endcan
                    @can('page-list')
                    <li class="@if(in_array('pages',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/pages">Pages</a>
                    </li>
                    @endcan
                    @can('testimonials-list')
                    <li class="@if(in_array('teams',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/teams">Team</a>
                    </li>
                    <li class="@if(in_array('testimonials',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/testimonials">Testimonial</a>
                    </li>
                    @endcan
                    @can('gallery-list')
                    <li class="@if(in_array('categories',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/categories">Gallery</a>
                    </li> 
                    @endcan

                    @can('schools-list')
                    <li class="@if(in_array('schools',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/schools">Schools</a>
                    </li>                    
                    @endcan                    
                    @can('admissions-list')
                    <li class="@if(in_array('admissions',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/backend/admissions')}}">Admissions</a>
                    </li>    
                    @endcan
                    @can('faq-list')
                    <li class="@if(in_array('questions',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/questions">FAQ</a>
                    </li>
                    @endcan
                    @can('faq-list')
                    <li class="@if(in_array('questions',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/promo-codes">Promo Codes</a>
                    </li>
                    @endcan
                    @can('notifications-list')
                    <li class="@if(in_array('notifications',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/notifications">Notifications</a>
                    </li>
                    @endcan

                    @can('broadcasts-list')
                    <li class="@if(in_array('broadcasts',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/broadcasts">Broadcasts</a>
                    </li>
                    @endcan

                    <li class="@if(in_array('reports',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/reports/search-all">Search</a>
                    </li>
                    @can('setting-list')
                    <li class="@if(in_array('contacts',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/contacts">Contacts</a>
                    </li>
                    <li class="@if(in_array('footers',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/footers">Site Detail</a>
                    </li>
                    @endcan
                    @can('states-list')
                    <li class="@if(in_array('states',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/states">States</a>
                    </li>
                    @endcan
                    @can('states-list')
                    <li class="@if(in_array('states',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/districts">Districts</a>
                    </li>
                    @endcan
                    @can('demand-liberaries-list')
                    <li class="@if(in_array('demand-liberaries',Request::segments())) {{'active'}} @endif">
                        <a href="{{url('/')}}/backend/demand-liberaries">Demand Library</a>
                    </li>
                    @endcan
                </ul>

                @endif
            </div>
        </div>
    </div>
    @endauth
</div>

<div class="page-container ">
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div id="side-container">
            <div class="row">
                <div class="col-12 text-right side-container-header">
                    <a id="close-slide-container" onclick="hideSliderContainer()" class="text-danger" href="javascript:;">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-10 form">

                </div>
            </div>
        </div>
        <div class="content sm-gutter">

            <!-- START BREADCRUMBS -->
            <div class="bg-white">
                <div class="container">

                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item"><a href="{{url('/')}}/backend/dashboard">Home</a></li>
                        <?php
                        $link = URL::to('/');
                        $allSegments = Request::segments();
                        // dd($allSegments);
                        ?>
                        @for($i = 1; $i <= count(Request::segments()); $i++)
                        @if($i < count(Request::segments()) & $i > 0)
                        <?php
                        if (Request::segment($i) == 'edit') {
                            continue;
                        }
                        if (is_numeric(Request::segment($i))) {
                            continue;
                        }

                        $link .= "/" . Request::segment($i);

                        if ($link != URL::to('/') . "/backend") {
                            if ($i != 3 && is_int(intval(ucwords(str_replace('-', ' ', Request::segment($i)))))) {
                                $s2 = str_replace('-', ' ', Request::segment($i));
                                ?>
                                <li class="breadcrumb-item"><a href="<?= $link ?>">{{ ucwords($s2)}}</a> </li>
                                <?php
                            }
                        }
                        ?>
                        @else  
                        <?php
                        $s2 = str_replace('_', ' ', str_replace('-', ' ', Request::segment($i)));
                        ?>
                        <li class="breadcrumb-item active"> {{  ucwords($s2) }}</li>
                        @endif
                        @endfor  
                    </ol>
                </div>
            </div>
            <!-- END BREADCRUMBS -->
            <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
                @include('partials.alerts')