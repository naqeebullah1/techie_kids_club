<?php
$siteDetail = siteDetails();
$franchise = findBySlug(request()->franchise_id);
if(!$franchise) {
    $franchise = App\Models\Franchise::find(1)->first();
}
?>
<!-- Footer Area -->
<footer id="footer" class="footer-area footer--2">
    <div class="footer__wrapper bg-image--10 section-padding--lg">
        <div class="container">
            <div class="row">
                <!-- Start Single Widget -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="footer__widget">
                        <div class="ft__logo">
                            <a href="<?= url('/') ?>">
                                <img src="<?= asset('/frontend/images/logo/' . $franchise->footer_logo) ?>" alt="Techi Kids">
                                <!--<img src="<?= asset('/frontend') ?>/images/logo/techie_kids_logo_white.png" alt="Techi Kids">-->
                            </a>
                            <p style="color:#fff;">
                                <?= ucwords($franchise->name) ?> is an in-school enrichment program that provides coding + robotics for kids in preschool through third grade. Families, preschools, and childcare centers use the program to build reading, math, and logic skills critical for success in school. </p>
                        </div>
                        <div class="ftr__details">
                        </div>
                        <div class="ftr__address__inner">
                            <div class="footer__social__icon">
                                <ul class="dacre__social__link--2 d-flex justify-content-start">
                                    <li class=""><a target="_blank" href="<?= ($siteDetail->fb_link) ? ($siteDetail->fb_link) : '#' ?>"><i class="fa fa-facebook"></i></a></li>
                                    <li class=""><a target="_blank" href="<?= ($siteDetail->twitter_link) ? ($siteDetail->twitter_link) : '#' ?>"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a target="_blank" href="<?= ($siteDetail->insta_link) ? ($siteDetail->insta_link) : '#' ?>"><i class="fa fa-instagram"></i></a></li>
                                    <li><a target="_blank" href="<?= ($siteDetail->insta_link) ? ($siteDetail->youtube_link) : '#' ?>"><i class="fa fa-youtube-play"></i></a></li>
<!--                                    <li class="vimeo"><a href="https://vimeo.com/"><i class="fa fa-vimeo"></i></a></li>
                                    <li class="pinterest"><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget -->

                <!-- Start Single Wedget -->
                <div class="col-lg-1 col-md-6 col-sm-12 md-mt-40 sm-mt-40 hide-mobile"></div>

                <div class="col-lg-3 col-md-6 col-sm-12 md-mt-40 sm-mt-40">
                    <div class="footer__widget">
                        <h4>For Families</h4>
                        <div class="footer__innner">
                            <div class="ftr__latest__post">
                                <ul class="ftr__catrgory">
                                    <li><a href="<?= url(franchiseLink()) ?>/page/classes">- About Our Classes</a></li>
                                    <li><a href="<?= url(franchiseLink()) ?>/find-your-school">- Find Your School</a></li>
                                    <li><a href="<?= url(franchiseLink()) ?>/page/faqs">- FAQs</a></li>

                                </ul>
                            </div>
                        </div>
                        <h4 style="margin-top:10px">For Schools</h4>
                        <div class="footer__innner">
                            <div class="ftr__latest__post">
                                <ul class="ftr__catrgory">
                                    <li><a href="<?= url(franchiseLink()) ?>/page/school-partnerships">School Partnerships</a></li>
                                    <li><a href="<?= url(franchiseLink()) ?>/page/schedule-a-demo">Schedule Demo</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Wedget -->
                <!-- Start Single Widget -->
                <div class="col-lg-4 col-md-6 col-sm-12 md-mt-40 sm-mt-40">
                    <div class="footer__widget">
                        <h4>Customer Support</h4>
                        <div class="footer__innner">
                            <div class="dcare__twit__wrap">
                                <!-- Start Single -->
                                <div class="dcare__twit d-flex">
                                    <div class="dcare__twit__details">
                                        <p><?= $siteDetail->description ?></p>
                                        <p><i class="fa fa-clock-o"></i> <?= $siteDetail->hours ?></p>
                                        <p><i class="fa fa-phone"></i> <a href="tel:<?= $siteDetail->contact ?>"><?= $siteDetail->contact ?></a></p>
                                        <p><i class="fa fa-envelope"></i><a href="mailto:<?= $siteDetail->email ?>"> <?= $siteDetail->email ?></a></p>
                                    </div>
                                </div>
                                <!-- End Single -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget -->
            </div>
        </div>
        <div class="ft__bottom__images--1 wow flipInX" data-wow-delay="0.6s">
            <img src="<?= asset('/frontend') ?>/images/banner/mid-img/ft.png" alt="footer images">
        </div>
        <div class="ft__bottom__images--2 wow fadeInRight" data-wow-delay="0.6s">
            <img src="<?= asset('/frontend') ?>/images/banner/mid-img/ft-2.png" alt="footer images">
        </div>
    </div>

    <div class="copyright  bg--theme">
        <div class="container">
            <div class="row align-items-center copyright__wrapper justify-content-center">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="coppy__right__inner text-center">
                        <div class="row">
                            <div class="col-lg-4">
                                <p class="float-left"><i class="fa fa-copyright"></i><?php echo date('Y'); ?> All Right Reserved.<a href="{{url('/front-franchise/'.$franchise->slug)}}"> <?= ucwords($franchise->name) ?></a></p>
                            </div>
                            <div class="col-lg-4">
                                <p style="font-size:16px;"><strong>
                                        <a href="{{url('/front-franchise/'.request()->franchise_id.'/privacy-policy')}}">Privacy Policy</a> |
                                        <a href="{{url('/front-franchise/'.request()->franchise_id.'/terms-conditions')}}">Terms & Conditions</a>
                                    </strong>
                                </p>
                            </div>
                            <div class="col-lg-4">
                                <p class="float-right">Developed By: <a href="https://www.cyberclouds.co" target="_blank">Cyberclouds</a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- //Footer Area -->
