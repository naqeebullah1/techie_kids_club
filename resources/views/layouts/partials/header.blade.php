<!-- Header -->
<?php
$siteDetail = siteDetails();
$franchise = findBySlug(request()->franchise_id);
if(!$franchise) {
    $franchise = App\Models\Franchise::find(1)->first();
}
?>
<header id="header" class="jnr__header header--one clearfix">
    <!-- Start Header Top Area -->
    <div class="junior__header__top">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-6 col-sm-12">
                    <div class="jun__header__top__left">
                        <ul class="top__address d-flex justify-content-start align-items-center flex-wrap flex-lg-nowrap flex-md-nowrap">
                            <li><i class="fa fa-envelope"></i><a href="mailto:<?= $siteDetail->email ?>"><?= $siteDetail->email ?></a></li>
                            <li><i class="fa fa-phone"></i><span>Contact Now :</span><a href="tel:<?= $siteDetail->contact ?>"><?= $siteDetail->contact ?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-5 col-lg-6 col-sm-12">
                    <div class="jun__header__top__right">
                        <ul class="accounting d-flex justify-content-lg-end justify-content-md-end justify-content-start align-items-center">
                            <?php
                            $menus = menus('upper header');
                            ?>
                            @foreach($menus as $menu)
                            <li><a class="" href="<?= url(franchiseLink()) ?>/page/{{ $menu->slug }}"><?= $menu->title ?></a></li>
                            @endforeach
                            <li>
                                @if(Auth::check())
                                <a  href="<?= url(parentfranchiseLink()) ?>/dashboard"><i class="fa fa-dashboard"></i> Dashbord</a>
                                @else
                                <a class="login-trigger" href="<?= url(franchiseLink()) ?>/parentlogin"><i class="fa fa-lock"></i> Login / Register</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Start Mainmenu Area -->
    <div class="mainmenu__wrapper bg__cat--1 poss-relative header_top_line sticky__header">
        <div class="container">
            <div class="row d-none d-lg-flex">
                <div class="col-sm-4 col-md-6 col-lg-2 order-1 order-lg-1">
                    <div class="logo">
                        <a href="<?= asset('/') ?>">
                            <img src="<?= asset('/frontend/images/logo/'.$franchise->header_logo) ?>" alt="Techie Kids Club">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2 col-lg-9 order-3 order-lg-2">
                    <div class="mainmenu__wrap">
                        <nav class="mainmenu__nav">
                            <ul class="mainmenu">

                                <?php
                                $isactive = "";
                                $menus = menus();

                                if (Request::segment(1) == "/" || Request::segment(1) == "") {
                                    $isactive = " active";
                                }
                                ?>
                                @foreach($menus as $menu)
                                <?php
                                if (Request::segment(2) == $menu->slug || Request::segment(1) == $menu->slug) {
                                    $isactive = " active";
                                } else {
                                    $isactive = "";
                                }
                                $url = url(franchiseLink() . '/page/' . $menu->slug);

                                if ($menu->id == 3):
                                    $url = url(franchiseLink());
                                    if (Request::segment(1) == "")
                                        $isactive = " active";
                                endif;
                                if ($menu->id == 12):
                                    $url = url(franchiseLink() . '/find-your-school');

                                endif;
                                ?>
                                <li class="drop <?= $isactive; ?>"><a href="<?= $url ?>"><?= $menu->title ?></a></li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                    <div class="shopbox d-flex justify-content-end align-items-center">
                        <a class="minicart-trigger" href="#">
                            <i class="fa fa-shopping-basket"></i>
                        </a>
                        <span id="item-count"><?= (session('cart_info')) ? count(session('cart_info')) : 0 ?></span>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu -->
            <div class="mobile-menu d-block d-lg-none">
                <div class="logo">
                    <a href="<?= url('/') ?>"><img src="<?= asset('/frontend') ?>/images/logo/techie_kids_logo.png" alt="logo"></a>
                </div>
                <a class="minicart-trigger" href="#">
                    <i class="fa fa-shopping-basket"></i>
                </a>
            </div>
            <!-- Mobile Menu -->
        </div>
    </div>
    <!-- End Mainmenu Area -->
</header>
<!-- //Header -->