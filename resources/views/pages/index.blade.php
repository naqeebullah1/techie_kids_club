@extends('master')

@section('content')
<style>
    .editable-clear-x{
        display: none !important;
    }
    .form-control{
        padding:.6rem 1rem !important;
    }
    .table-responsive th,
    .table-responsive td {
        white-space: nowrap;
        border: 1px solid #ebedf2 !important;
    }
    .modal-lg{
        width: 1250px !important;
    }
</style>
@can('page-edit')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">{{ __('Create New Page') }}</div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin.pages.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <label class="col-12 mb-2">Header Type</label>
                            <label>
                                <input type="radio" value="main header" name="page[header_type]"/>
                                Main Header
                            </label>
                            <label>
                                <input type="radio" value="upper header" name="page[header_type]"/>
                                Upper Header
                            </label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label>Title</label>
                            <input type="text" onchange="makeSlug(this.value)" class="form-control" name="page[title]"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label>Slug</label>
                            <input type="text" class="form-control" id="slug" name="page[slug]"/>
                        </div>
                        <div class="col-lg-12 col-md-6 col-sm-12 col-12">
                            <label>Page Keyword</label>
                            <input type="text" class="form-control" id="keyword" name="page[meta_keyword]"/>
                        </div>
                        <div class="col-lg-12 col-md-6 col-sm-12 col-12">
                            <label>Page Description</label>
                            <textarea class="form-control" id="description" name="page[meta_description]"></textarea>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                    <button type="submit" class="mt-2 pull-right btn btn-success">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endcan

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">{{ __('Pages') }}</div>
            </div>
            <div class="card-body">
                <div class="row table-responsive">

                    <form action="{{ route('admin.pages.updatetype') }}" method="POST">
                        @csrf
                        <table class="sortable table table-bordered draggable">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Header Type</th>
                                    <th>Status</th>
                                    <th>Is Front Page</th>
                                    <th>Is Contact</th>
                                    <th>Is Service</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $userId = auth()->user()->id;
                                ?>
                                @foreach($banners as $k=>$b)
                                <tr>
                                    <td>
                                        <?= $b->title ?>
                                    </td>
                                    <td>
                                        <?= $b->slug ?>
                                    </td>
                                    <td>
                                        <?= ucwords($b->header_type) ?>
                                    </td>
                                    <td>
                                        <a href="{{ url('backend/change-status/pages/'.$b->id,$b->active) }}">

                                            <span class="span-status {{ $b->active == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                                                {{ $b->active == 1 ? __('Active') : __('Blocked') }}
                                            </span>
                                        </a>
                                    </td>
                                    <td>
                                        @if($b->is_front == 0)
                                        @php
                                        $value = 0;
                                        $checked = "";
                                        @endphp
                                        @else    
                                        @php
                                        $value = 1;
                                        $checked = "checked='checked'";
                                        @endphp
                                        @endif
                                        <input type="hidden"  name="pageid_front[]" value="{{$b->id}}" />
                                        <input type="hidden"  name="is_front_radio[]" value="{{$b->is_front}}" id="frontradio-{{$b->id}}" class="allFrontVal" />
                                        <input type="radio"  name="is_front[]" value="{{$b->is_front}}" {{$checked}} class="frontradio-{{$b->id}}" onChange="setFrontVal(this)" />
                                    </td>
                                    <td>
                                        @if($b->is_contact == 0)
                                        @php
                                        $value = 0;
                                        $checked = "";
                                        @endphp
                                        @else    
                                        @php
                                        $value = 1;
                                        $checked = "checked='checked'";
                                        @endphp
                                        @endif
                                        <input type="hidden"  name="pageid_contact[]" value="{{$b->id}}" />
                                        <input type="hidden"  name="is_contact_radio[]" value="{{$b->is_contact}}" id="contactradio-{{$b->id}}" class="allContactVal" />
                                        <input type="radio"  name="is_contact[]" value="{{$b->is_contact}}" {{$checked}} class="contactradio-{{$b->id}}" onChange="setContactVal(this)" />
                                    </td>
                                    <td>
                                        @if($b->is_service == 0)
                                        @php
                                        $value = 0;
                                        $checked = "";
                                        @endphp
                                        @else    
                                        @php
                                        $value = 1;
                                        $checked = "checked='checked'";
                                        @endphp
                                        @endif
                                        <input type="hidden"  name="pageid_service[]" value="{{$b->id}}" />
                                        <input type="hidden"  name="is_service_radio[]" value="{{$b->is_service}}" id="serviceradio-{{$b->id}}" class="service-values" />
                                        <input type="radio"  name="is_service[]" value="{{$b->is_service}}" {{$checked}} class="serviceradio-{{$b->id}}" onChange="setServiceVal(this)" />
                                    </td>
                                    <td class="sortable-handle">
                                        @if($userId==1)
                                        <!--<a href="{{ route('admin.default_for_all',['pages',$b->id]) }}" class="btn btn-sm btn-small btn-complete">-->
                                        @if($b->franchise_id)
                                            <!--Default-->
                                        @else
                                            <!--Remove Default-->
                                        @endif
                                        <!--</a>-->
                                        @endif


                                        @if($b->is_contact == 1)
                                        @can('setting.edit')
                                        <a href="{{ route('admin.contact.details',$b->id) }}" class="btn btn-sm btn-small btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @endcan
                                        @elseif($b->is_service == 1)
                                        <a href="{{ route('admin.services.index') }}" class="btn btn-sm btn-small btn-warning">
                                            <i class="fa fa-info"></i>
                                        </a>
                                        @else
                                        <a href="{{ route('admin.page.details',$b->id) }}" class="btn btn-sm btn-small btn-warning">
                                            <i class="fa fa-info"></i>
                                        </a>

                                        @endif
                                        
                                         @if($userId==1 || $b->franchise_id !=0)
                                        @can('page-edit')
                                        @if($b->is_service != 1 && $b->is_contact !=1)
                                        <a href="{{ url('backend/pages/edit/'.$b->id) }}" class="btn btn-sm btn-small btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @endif
                                        @endcan

                                        @can('page-delete')
                                        <button type="submit"
                                                onclick="event.preventDefault(); setUrl('{{ route('admin.pages.destroy', $b->id) }}'); deleteConfirm('destroy', 'Associated data with this page will also be deleted, Are you sure?')"

                                                class="btn btn-sm btn-small btn-icon btn-round btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endcan
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @can('page.edit')
                                <tr>

                                    <td colspan="6">
                                        <input type="submit" value="Update" class="btn btn-sm btn-small btn-success m-auto d-block"/>
                                    </td>

                                </tr>    
                                @endcan
                            </tbody>
                        </table>
                    </form>
                    <form 
                        id="destroy"
                        method="post" id="destroy" 
                        action="">
                        @csrf
                        @method('DELETE')
                    </form>
                    <div class="mt-2">
                        {{ $banners->links() }}

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@if(Session::has('outcome'))
<script>
    $(function () {
    $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif
<script type="text/javascript">
            function makeSlug($value){
            var $slug = $value.trim().toLowerCase().replaceAll(' ', '-');
            $('#slug').val($slug);
            }
    function setUrl($url){
    $('form#destroy').attr('action', $url);
    }
    function setFrontVal(e) {
    $(".allFrontVal").attr('value', '0');
    $("#" + e.className).attr('value', '1');
    }
    function setContactVal(e) {
    $(".allContactVal").attr('value', '0');
    $("#" + e.className).attr('value', '1');
    }
    function setServiceVal(e) {

    $(".service-values").attr('value', '0');
    $("#" + e.className).attr('value', '1');
    }
</script>
@endsection