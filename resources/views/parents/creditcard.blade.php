@extends('layouts.front_theme')
@section('content')

<!--<link href="{{asset('frontend/css/skeuocard.reset.css')}}" rel="stylesheet"/>
<link href="{{asset('frontend/css/skeuocard.css')}}" rel="stylesheet"/>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.1.2/css/paymentfont.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/css/card.css')}}" rel="stylesheet"/>
<script src="https://js.stripe.com/v3/"></script>
<?php 
if(Session::get('selectedFranchise')) {
    $franchiseCurrent = Session::get('selectedFranchise');
} else {
    $franchiseCurrent = 1;
}

// Get Franchise Publishable Key

$pub_key = App\Models\Franchise::where('id',$franchiseCurrent)->first()->stripe_publishable;
if(!$pub_key) {
    $urlRoot = url('/');
    Session::put('error','Something went wrong, try again later');
    header("location:$urlRoot");
    exit;
}
?>
<script src="{{asset('frontend/js/checkout.js')}}" STRIPE_PAY_URL="{{url('parent/add-card')}}" STRIPE_PUBLISHABLE_KEY="{{$pub_key}}" defer></script>

<style>
.form-container {
    padding:20px 20px 20px 20px !important;
    background: rgb(173,173,173) !important;
    background: linear-gradient(29deg, rgba(173,173,173,1) 0%, rgba(214,214,214,1) 23%, rgba(208,208,208,1) 48%, rgba(231,229,229,1) 65%, rgba(237,237,237,1) 100%) !important;
    border-radius: 10px;
    box-shadow: 1px 1px 4px #999;
    max-width:600px !important;
    margin:auto;
    margin-bottom: 30px !important;
}
#card-element {
        border-radius: 4px 4px 0 0 ;
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 54px;
        width: 100%;
        background: white;
    }

    #card-element form {
        width: 30vw;
        min-width: 500px;
        align-self: center;
        box-shadow: 0px 0px 0px 0.5px rgba(50, 50, 93, 0.1),
            0px 2px 5px 0px rgba(50, 50, 93, 0.1), 0px 1px 1.5px 0px rgba(0, 0, 0, 0.07);
        border-radius: 7px;
        padding: 40px;
    }

    #card-element input {
        border-radius: 6px;
        margin-bottom: 6px;
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 44px;
        font-size: 16px;
        width: 100%;
        background: white;
    }

    .accordion-head.collapsed:after {
        content: "\f077";
    }

    #card-info label {
        position: relative;
        color: #000000;
        font-weight: 300;
        height: 20px;
        line-height: 15px;
        margin-left: 0px;
        display: flex;
        flex-direction: row;
    }

    #card-info .group label:not(:last-child) {
        border-bottom: 1px solid #F0F5FA;
    }

    #card-info label > span {
        /*width: 120px;
        text-align: right;
        margin-right: 30px;*/
        width: 100px;
    }

    #card-info .field {
        background: transparent;
        font-weight: 300;
        border: 0;
        color: #31325F;
        outline: none;
        flex: 1;
        padding-right: 10px;
        padding-left: 10px;
        cursor: text;
    }
    .bordered-colored {
        background: #fff;   
        border: 1px solid #ccc;
        flex: auto;
        position:relative;
        padding-left: 10px !important;
    }

    #card-info .field::-webkit-input-placeholder {
        color: #CFD7E0;
    }

    #card-info .field::-moz-placeholder {
        color: #CFD7E0;
    }

    #card-info .brand {
        width: 30px;
        position: absolute;
        right: 10px;
        top: 10px;
    }
    .card-number-element > input.InputElement.is-empty.Input.Input--empty {
        padding-left: 10px !important;
        background-color:#fff !important;
    }
    .ellipsis {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
        .list-credit-cards .cardblock {
        background: #f8f8f8;
    }
</style>
<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2><i class="fa fa-credit-card"> Credit Cards</i></h2>
                    <small style="color:#fff;"><strong>Note: </strong>We do not store any of your credit card info, these are listed from stripe</small>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="cards-container" class="dl-container dcare__courses__area pt-3 bg--white">
    <div class="container">
    <div class="row form-group">
      <?php
    if(isset($franchises_users) && $franchises_users != "" && count($franchises_users) <= 0 && !Session::get('selectedFranchise')) { ?>
        <div class="col-lg-12">
            <div class="container preload">
                <h2 class="heading text-center mb-3 mt-3" style="font-variant:small-caps;">You cannot add any card yet, Please click <a href="{{url(franchiseLink())}}/find-your-school">HERE</a> to select school class first</h2>
            </div>
        </div>
    <?php }
     else {
 ?>
                                                    <div class="col-lg-12">
                                                       
                                                        <!-- START FORM -->
       <h2 class="heading text-center mb-3" style="font-variant:small-caps;">Add New Card</h2>
          <div class="form-container" id="card-info">
               @if(Session::has('success'))
                <div class="col-12">        
                <p class="alert alert-success" style=""><?= Session::get('success') ?></p>
                </div>
                @endif

                @if(Session::has('error'))
                 <div class="col-12"> 
                    <p class="alert alert-danger" style=";"><?= Session::get('error') ?></p>
                </div>
                @endif
                
              <p class="text-info text-center" style="margin-bottom:10px;margin-bottom: 20px;font-size: 18px;color: #000 !important;font-variant:small-caps;"><strong>Please provide your credit/debit card details</strong></p>
                    
                                                        @if(count($errors) > 0)
                                                          @foreach ($errors->all() as $error)
                                                          
                                                            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="max-width: 350px;margin: auto;margin-bottom: 10px;padding: 5px 10px;">
                                                              <strong>{{ $error }}</strong> 
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.2rem 1rem;">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                            </div>

                                                          @endforeach
                                                        @endif
                                                        <div id="paymentSuccessResponse" class="alert alert-success hidden"></div>
                                                        <div id="paymentErrorResponse" class="alert alert-danger hidden"></div>
              <form id="subscrFrm">
                  @csrf()
            
        <?php 
            $collect = collect($franchises_users)->pluck('franchise_id')->toArray();
            
        ?>
            @if(isset($franchises_users) && $franchises_users != "" && count($franchises_users) > 0 && Session::get('selectedFranchise') != 1 && in_array(Session::get('selectedFranchise'),$collect))
            <div class="field-container">
                <label>Franchise Name</label>
                <select required name="choosenFranchise" id="choosenFranchise" class="form-field form-control">
                    <option value="">Select Franchise</option>
                    @foreach($franchises_users as $key => $data)
                    <option <?php if(Session::get('selectedFranchise') == $data->franchise_id){echo "Selected"; } ?> value="{{$data->franchise_id}}">{{$data->franchise->name}}</option>
                    @endforeach
                </select>
            </div>
            @elseif(Session::get('selectedFranchise') && Session::get('selectedFranchise') != 1)
            <div class="field-container">
                <label>Franchise Name</label>
                <?php $franchise_name = App\Models\Franchise::find(Session::get('selectedFranchise'))->name; ?>
                <input type="text" value="{{$franchise_name}}" disabled class="form-field" />
            </div>
            <input type="hidden" class="form-field" name="choosenFranchise" id="choosenFranchise" value="{{Session::get('selectedFranchise')}}" />
            @else
            <input type="hidden" class="form-field" name="choosenFranchise" id="choosenFranchise" value="1" />
            @endif
        <div class="field-container row">  
            <div class="col-lg-8">
                <label for="cardnumber">Card Number</label>
                <!--<input id="cardnumber" type="text" pattern="[0-9 ]*" class="form-field" inputmode="numeric" name="number">-->
                <div class="bordered-colored">
                <div id="cardnumber" class="card-number-element" class="form-field field"></div>
                
                <span class="brand">
                                            <i class="pf pf-credit-card" id="brand-icon"></i>
                </span>
                </div>
            </div>
            <div class="col-lg-4">
            <label for="expirationdate">Expiration</label>
            <!--<input id="expirationdate" type="text" pattern="[0-9/0-9]*" class="form-field" inputmode="numeric" name="expiry">-->
            <div class="bordered-colored">
            <div id="expirationdate" class="form-field field"></div>
             </div>                   
                                
            </div>
            <!--<svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
            </svg>-->
        </div>
        <div class="field-container row">
            
            <div class="col-lg-8">
            <label for="name">Name</label>
            <input id="name" maxlength="20" pattern="[a-z A-Z]*" type="text" class="form-field" name="name">
            </div>
            <div class="col-lg-4">
            <label for="securitycode">Security Code</label>
            <!--<input id="securitycode" type="text" pattern="[0-9]*" class="form-field" inputmode="numeric" name="cvc">-->
            <div class="bordered-colored">
                <div id="securitycode" class="form-field field"></div> 
            </div>
            </div>
            
        </div>
        
        
        <div class="text-center">
            <button id="addSubCard" form="subscrFrm" class="btn-pay btn btn-primary btn-md" style="font-weight: bold;width: 100%;margin-top:10px;">Add Card </button>
          </div>
        </form>
    </div>
<div class="clear clear-fix clearfix"></div>
       <!-- <div class="form-container active">
          <form id="addcredit" method="POST" action="{{url('parent/add-card') }}">
          
            
                @csrf()
              <input placeholder="Card number" type="tel" name="number">
                <input placeholder="Full name" type="text" name="name">
                <input placeholder="MM/YY" type="tel" name="expiry">
                <input placeholder="CVC" type="number" name="cvc">
          <div class="text-center">
            <input type="submit" class="btn btn-primary btn-md" value="Add Card" style="width: 320px;" />
          </div>
        </form>
        <!-- END FORM
                                                    </div>-->
                                                    </div>
                                                    
                                                    <?php if(isset($megaCardList)  && count($megaCardList) > 0 && Session::get('selectedFranchise') != 1) { 
                                                        foreach($megaCardList as $k => $cardData) {
                                                        ?>
                                                        <div class="col-lg-12">
                                                         <p class="text-success text-center" style="margin: auto;margin-top:20px;font-variant:small-caps;font-size:22px;color:#000 !important;padding: 20px 0px 20px 0px;background: #f8f8f8;line-height: 22px;max-width: 1000px;"><strong>{{$cardData['franchise']}} - List of Your Credit Cards at Stripe</strong></p>
                                                           @if($cardData['carddata'] == "")
                                                            <p class="text-success text-center" style="margin-top:20px;"><strong>No Record Found</strong></p>
                                                            @endif
                                                            @if(isset($cardData['carddata']))
                                                            
                                                            <div class="list-credit-cards row" id="list-cards" style="background: wheat;padding: 10px 5px;max-width: 1000px !important;margin: auto;margin-top: 10px;">
                                                                
                                                                @foreach($cardData['carddata'] as $key => $card)
                                                                    <div class="col-lg-3">
                                                                    <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                    <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                        <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                    </span>
                                                                    
                                                                    <span style="display:inline-block;margin-left: 90px;text-align: right;font-size: 12px;">{{date('M',strtotime($card['card']['exp_month']))}}-{{$card['card']['exp_year']}}</span>
                                                                    <br />
                                                                    <span style="padding-left: 9px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                     
                                                                     </label>
                                                                     </div>
                                                                @endforeach
                                                                
                                                            </div>
                                                            <br />
                                                            @endif
                                                            <?php } ?>
                                                        </div>
                                                    <?php } else { ?>
                                                    <div class="col-lg-12">
                                                     <p class="text-success text-center" style="margin: auto;margin-top:20px;font-variant:small-caps;font-size:22px;color:#000 !important;padding: 20px 0px 20px 0px;background: #f8f8f8;line-height: 22px;max-width: 1000px;"><strong>Techie Kids Club - List of Your Credit Cards at Stripe</strong></p>
                                                       @if($cardlist == "")
                                                        <p class="text-success text-center" style="margin-top:20px;"><strong>No Record Found</strong></p>
                                                        @endif
                                                        @if(isset($cardlist) && $cardlist != "")
                                                        
                                                        <div class="list-credit-cards row" id="list-cards" style="background: wheat;padding: 10px 5px;max-width: 1000px !important;margin: auto;margin-top: 10px;">
                                                            
                                                            @foreach($cardlist as $key => $card)
                                                            
                                                                <div class="col-lg-3">
                                                                <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                    <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                </span>
                                                                <span style="display:inline-block;margin-left: 90px;text-align: right;font-size: 12px;">{{date('M',strtotime($card['card']['exp_month']))}}-{{$card['card']['exp_year']}}</span>
                                                                <br />
                                                                <span style="padding-left: 9px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                 
                                                                 </label>
                                                                 </div>
                                                            @endforeach
                                                            
                                                        </div>
                                                        <br />
                                                        @endif
                                                       
                                                        <?php } ?>
                                                    </div>
                                                    <!--<div id="card-element">
                                                        
                                                    </div>-->

                                                    <!--<div class="col-lg-12">
                                                        <p class="text-info" style="margin-bottom:10px;"><strong>Please provide your credit/debit card details</strong></p>
                                                        <label>

                                                            <span>Card Number</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-number-element" class="field"></div>

                                                                <span class="brand">
                                                                    <i class="pf pf-credit-card" id="brand-icon"></i>
                                                                </span>
                                                            </div>
                                                        </label>
                                                    </div>


                                                    <div class="col-lg-7">
                                                        <label>
                                                            <span>Expiry Date</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-expiry-element" class="field"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <label>
                                                            <span style="width: 36px;">CVC</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-cvc-element" class="field"></div>
                                                            </div>    
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label>
                                                            <span>Postal Code</span>
                                                            <div class="bordered-colored">
                                                                <input id="postal-code" maxlength="6" name="postal_code" class="field" style="width: 100%;height: 40px;" />
                                                            </div>
                                                        </label>
                                                    </div>-->

                                                    <?php } ?>
                                                </div>
                                                </div>
    
</section>
<!-- End Subscribe Area -->
@endsection
@section('style')
<style>
    .p-center .pagination{
        justify-content: center !important;
    }
    .d-content{
        cursor: pointer;
    }
</style>

@endsection


