@extends('layouts.front_theme')
@section('content')

<link href="{{asset('frontend/css/skeuocard.reset.css')}}" rel="stylesheet"/>
<link href="{{asset('frontend/css/skeuocard.css')}}" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.1.2/css/paymentfont.min.css" rel="stylesheet" type="text/css" />
<style>
    .ellipsis {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
     #card-element {
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 54px;
        width: 100%;
        background: white;
    }

    #card-element form {
        width: 30vw;
        min-width: 500px;
        align-self: center;
        box-shadow: 0px 0px 0px 0.5px rgba(50, 50, 93, 0.1),
            0px 2px 5px 0px rgba(50, 50, 93, 0.1), 0px 1px 1.5px 0px rgba(0, 0, 0, 0.07);
        border-radius: 7px;
        padding: 40px;
    }

    #card-element input {
        border-radius: 6px;
        margin-bottom: 6px;
        padding: 12px;
        border: 1px solid rgba(50, 50, 93, 0.1);
        height: 44px;
        font-size: 16px;
        width: 100%;
        background: white;
    }

    .accordion-head.collapsed:after {
        content: "\f077";
    }

    #cards-container label {
        position: relative;
        color: #8898AA;
        font-weight: 300;
        height: 40px;
        line-height: 40px;
        margin-left: 0px;
        display: flex;
        flex-direction: row;
    }

    #cards-container.group label:not(:last-child) {
        border-bottom: 1px solid #F0F5FA;
    }

    #cards-container label > span {
        /*width: 120px;
        text-align: right;
        margin-right: 30px;*/
        width: 100px;
    }

    #cards-container.field {
        background: transparent;
        font-weight: 300;
        border: 0;
        color: #31325F;
        outline: none;
        flex: 1;
        padding-right: 10px;
        padding-left: 10px;
        cursor: text;
    }
    .bordered-colored {
        background: #fff;   
        border: 1px solid #ccc;
        flex: auto;
        position:relative;
    }

    #cards-container.field::-webkit-input-placeholder {
        color: #CFD7E0;
    }

    #cards-container.field::-moz-placeholder {
        color: #CFD7E0;
    }

    #cards-container.brand {
        width: 30px;
        position: absolute;
        right: 10px;
        top: 0px;
    }
     
        input {
            width: 200px;
            margin: 10px auto;
            display: block;
        }
        .list-credit-cards .cardblock {
        background: #f8f8f8;
    }
</style>
<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2><i class="fa fa-credit-card"> Credit Cards</i></h2>
                    <small style="color:#fff;"><strong>Note: </strong>We do not store any of your credit card info, these are listed from stripe</small>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="cards-container" class="dl-container dcare__courses__area section-padding--lg bg--white">
    <div class="container">
    <div class="row form-group">
                                                    <div class="col-lg-6">
                                                        <h3 class="heading text-center">Add New Card</h3>
                                                        @if(count($errors) > 0)
                                                          @foreach ($errors->all() as $error)
                                                            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="max-width: 350px;margin: auto;margin-bottom: 10px;padding: 5px 10px;">
                                                              <strong>{{ $error }}</strong> 
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.2rem 1rem;">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                            </div>

                                                          @endforeach
                                                        @endif
                                                       
                                                        <!-- START FORM -->
        <p class="text-info text-center" style="margin-bottom:10px;"><strong>Please provide your credit/debit card details</strong></p>
          <form id="addcredit" method="POST" action="{{url('parent/add-card') }}" style="display:none;">
          <div class="credit-card-input no-js" style="margin:auto;" id="skeuocard">
            <p class="no-support-warning">
              Either you have Javascript disabled, or you're using an unsupported browser, amigo! That's why you're seeing this old-school credit card input form instead of a fancy new Skeuocard. On the other hand, at least you know it gracefully degrades...
            </p>
            
                @csrf()
            <label for="cc_type">Card Type</label>
            <select name="cc_type" >
              <option value="">...</option>
              <option value="visa">Visa</option>
              <option value="discover">Discover</option>
              <option value="mastercard">MasterCard</option>
              <option value="maestro">Maestro</option>
              <option value="jcb">JCB</option>
              <option value="unionpay">China UnionPay</option>
              <option value="amex">American Express</option>
              <option value="dinersclubintl">Diners Club</option>
            </select>
            <label for="cc_number">Card Number</label>
            <input type="text" name="cc_number"  id="cc_number" placeholder="**** **** **** ****" maxlength="19" size="19">
            <label for="cc_exp_month">Expiration Month</label>
            <input type="text" name="cc_exp_month" id="cc_exp_month"  placeholder="00">
            <label for="cc_exp_year">Expiration Year</label>
            <input type="text" name="cc_exp_year" id="cc_exp_year"  placeholder="00">
            <label for="cc_name">Cardholder's Name</label>
            <input type="text" name="cc_name" id="cc_name"  placeholder="John Doe">
            <label for="cc_cvc">Card Validation Code</label>
            <input type="text" name="cc_cvc" id="cc_cvc" placeholder="123"  maxlength="3" size="3">
          </div>
          <div class="text-center">
            <input type="submit" class="btn btn-primary btn-md" value="Add Card" style="width: 320px;" />
          </div>
        </form>
        <!-- END FORM -->
                                                    </div>
                                                    
                                                    <div class="col-lg-6">
                                                        @if(isset($cardlist))
                                                        <p class="text-success text-center" style="margin-top:20px;"><strong>List of Your Credit Cards at Stripe</strong></p>
                                                        <div class="list-credit-cards row" id="list-cards" style="margin-top:10px;background: wheat;padding: 10px 5px;">
                                                            
                                                            @foreach($cardlist as $key => $card)
                                                                <div class="col-lg-6">
                                                                <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                    <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                </span>
                                                                <span style="display:inline-block;margin-left: 35px;text-align: right;font-size: 12px;">{{$card['card']['exp_month']}}-{{$card['card']['exp_year']}}</span>
                                                                <br />
                                                                <span style="padding-left: 9px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                 
                                                                 </label>
                                                                 </div>
                                                            @endforeach
                                                            
                                                        </div>
                                                        <br />
                                                        @endif
                                                        
                                                    </div>
                                                    <!--<div id="card-element">
                                                        
                                                    </div>-->

                                                    <!--<div class="col-lg-12">
                                                        <p class="text-info" style="margin-bottom:10px;"><strong>Please provide your credit/debit card details</strong></p>
                                                        <label>

                                                            <span>Card Number</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-number-element" class="field"></div>

                                                                <span class="brand">
                                                                    <i class="pf pf-credit-card" id="brand-icon"></i>
                                                                </span>
                                                            </div>
                                                        </label>
                                                    </div>


                                                    <div class="col-lg-7">
                                                        <label>
                                                            <span>Expiry Date</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-expiry-element" class="field"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <label>
                                                            <span style="width: 36px;">CVC</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-cvc-element" class="field"></div>
                                                            </div>    
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label>
                                                            <span>Postal Code</span>
                                                            <div class="bordered-colored">
                                                                <input id="postal-code" maxlength="6" name="postal_code" class="field" style="width: 100%;height: 40px;" />
                                                            </div>
                                                        </label>
                                                    </div>-->


                                                </div>
                                                </div>
    
</section>
<!-- End Subscribe Area -->
@endsection
@section('style')
<style>
    .p-center .pagination{
        justify-content: center !important;
    }
    .d-content{
        cursor: pointer;
    }
</style>
@endsection


