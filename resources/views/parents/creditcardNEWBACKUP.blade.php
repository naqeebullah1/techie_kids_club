@extends('layouts.front_theme')
@section('content')

<!--<link href="{{asset('frontend/css/skeuocard.reset.css')}}" rel="stylesheet"/>
<link href="{{asset('frontend/css/skeuocard.css')}}" rel="stylesheet"/>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.1.2/css/paymentfont.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/css/card.css')}}" rel="stylesheet"/>


<style>
    .ellipsis {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
        .list-credit-cards .cardblock {
        background: #f8f8f8;
    }
</style>
<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2><i class="fa fa-credit-card"> Credit Cards</i></h2>
                    <small style="color:#fff;"><strong>Note: </strong>We do not store any of your credit card info, these are listed from stripe</small>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="cards-container" class="dl-container dcare__courses__area pt-3 bg--white">
    <div class="container">
    <div class="row form-group">
      <?php
    if(isset($franchises_users) && $franchises_users != "" && count($franchises_users) <= 0 && !Session::get('selectedFranchise')) { ?>
        <div class="col-lg-12">
            <div class="container preload">
                <h2 class="heading text-center mb-3 mt-3" style="font-variant:small-caps;">You cannot add any card yet, Please click <a href="{{url(franchiseLink())}}/find-your-school">HERE</a> to select school class first</h2>
            </div>
        </div>
    <?php }
     else {
 ?>
        <div class="col-lg-6">
            <div class="container preload">
            <h3 class="heading text-center mb-3" style="font-variant:small-caps;">Add New Card</h3>
            <div class="creditcard m-auto" >
                
            <div class="front">
                <div id="ccsingle"></div>
                <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front">
                        <g id="CardBackground">
                            <g id="Page-1_1_">
                                <g id="amex_1_">
                                    <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                            C0,17.9,17.9,0,40,0z" />
                                </g>
                            </g>
                            <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                        </g>
                        <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4"></text>
                        <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6"></text>
                        <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                        <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                        <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">card number</text>
                        <g>
                            <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9"></text>
                            <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                            <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                            <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                        </g>
                        <g id="cchip">
                            <g>
                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                        c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                            </g>
                            <g>
                                <g><rect x="82" y="70" class="st12" width="1.5" height="60" /></g>
                                <g><rect x="167.4" y="70" class="st12" width="1.5" height="60" /></g>
                                <g>
                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                            c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                            C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                            c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                            c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                </g>
                                <g><rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" /></g>
                                <g><rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" /></g>
                                <g><rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" /></g>
                                <g><rect x="142" y="117.9" class="st12" width="26.2" height="1.5" /></g>
                            </g>
                        </g>
                    </g>
                    <g id="Back"></g>
                </svg>
            </div>
            <div class="back">
                <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front"><line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" /></g>
                    <g id="Back">
                        <g id="Page-1_2_">
                            <g id="amex_2_"><path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                        C0,17.9,17.9,0,40,0z" /></g>
                        </g>
                        <rect y="61.6" class="st2" width="750" height="78" />
                        <g>
                            <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                    C707.1,246.4,704.4,249.1,701.1,249.1z" />
                            <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                            <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                            <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                        </g>
                        <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7"></text>
                        <g class="st8"><text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text></g>
                        <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                        <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                        <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13"></text>
                    </g>
                </svg>
            </div>
        </div>
            </div>
        </div>
                                                    <div class="col-lg-6">
                                                       
                                                        <!-- START FORM -->
       
          <div class="form-container">
               @if(Session::has('success'))
                <div class="col-12">        
                <p class="alert alert-success" style=""><?= Session::get('success') ?></p>
                </div>
                @endif

                @if(Session::has('error'))
                 <div class="col-12"> 
                    <p class="alert alert-danger" style=";"><?= Session::get('error') ?></p>
                </div>
                @endif
              <p class="text-info text-center" style="margin-bottom:10px;margin-bottom: 10px;font-size: 18px;color: #000 !important;font-variant:small-caps;"><strong>Please provide your credit/debit card details</strong></p>
                    
                                                        @if(count($errors) > 0)
                                                          @foreach ($errors->all() as $error)
                                                          
                                                            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="max-width: 350px;margin: auto;margin-bottom: 10px;padding: 5px 10px;">
                                                              <strong>{{ $error }}</strong> 
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 0.2rem 1rem;">
                                                                <span aria-hidden="true">&times;</span>
                                                              </button>
                                                            </div>

                                                          @endforeach
                                                        @endif
              <form id="addcredit" method="POST" action="{{url('parent/add-card') }}">
                  @csrf()
            
        <?php 
            $collect = collect($franchises_users)->pluck('franchise_id')->toArray();
            
        ?>
            @if(isset($franchises_users) && $franchises_users != "" && count($franchises_users) > 0 && Session::get('selectedFranchise') != 1 && in_array(Session::get('selectedFranchise'),$collect))
            <div class="field-container">
                <label>Franchise Name</label>
                <select required name="choosenFranchise" class="form-field form-control">
                    <option value="">Select Franchise</option>
                    @foreach($franchises_users as $key => $data)
                    <option <?php if(Session::get('selectedFranchise') == $data->franchise_id){echo "Selected"; } ?> value="{{$data->franchise_id}}">{{$data->franchise->name}}</option>
                    @endforeach
                </select>
            </div>
            @elseif(Session::get('selectedFranchise') && Session::get('selectedFranchise') != 1)
            <div class="field-container">
                <label>Franchise Name</label>
                <?php $franchise_name = App\Models\Franchise::find(Session::get('selectedFranchise'))->name; ?>
                <input type="text" value="{{$franchise_name}}" disabled class="form-field" />
            </div>
            <input type="hidden" class="form-field" name="choosenFranchise" value="{{Session::get('selectedFranchise')}}" />
            @else
            <input type="hidden" class="form-field" name="choosenFranchise" value="1" />
            @endif
        <div class="field-container">    
            <label for="cardnumber">Card Number</label>
            <input id="cardnumber" type="text" pattern="[0-9 ]*" class="form-field" inputmode="numeric" name="number">
            <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
            </svg>
        </div>
        <div class="field-container row">
            <div class="col-lg-6">
            <label for="expirationdate">Expiration (mm/yy)</label>
            <input id="expirationdate" type="text" pattern="[0-9/0-9]*" class="form-field" inputmode="numeric" name="expiry">
            </div>
            <div class="col-lg-6">
            <label for="securitycode">Security Code</label>
            <input id="securitycode" type="text" pattern="[0-9]*" class="form-field" inputmode="numeric" name="cvc">    
            </div>
        </div>
        
        <div class="field-container">
            <label for="name">Name</label>
            <input id="name" maxlength="20" pattern="[a-z A-Z]*" type="text" class="form-field" name="name">
        </div>
        <div class="text-center">
            <input type="submit" class="btn btn-primary btn-md" value="Add Card" style="width: 100%;font-weight: bold;" />
          </div>
        </form>
    </div>
<div class="clear clear-fix clearfix"></div>
       <!-- <div class="form-container active">
          <form id="addcredit" method="POST" action="{{url('parent/add-card') }}">
          
            
                @csrf()
              <input placeholder="Card number" type="tel" name="number">
                <input placeholder="Full name" type="text" name="name">
                <input placeholder="MM/YY" type="tel" name="expiry">
                <input placeholder="CVC" type="number" name="cvc">
          <div class="text-center">
            <input type="submit" class="btn btn-primary btn-md" value="Add Card" style="width: 320px;" />
          </div>
        </form>
        <!-- END FORM
                                                    </div>-->
                                                    </div>
                                                    
                                                    <?php if(isset($megaCardList)  && count($megaCardList) > 0 && Session::get('selectedFranchise') != 1) { 
                                                        foreach($megaCardList as $k => $cardData) {
                                                        ?>
                                                        <div class="col-lg-12">
                                                         <p class="text-success text-center" style="margin: auto;margin-top:20px;font-variant:small-caps;font-size:22px;color:#000 !important;padding: 20px 0px 20px 0px;background: #f8f8f8;line-height: 22px;max-width: 1000px;"><strong>{{$cardData['franchise']}} - List of Your Credit Cards at Stripe</strong></p>
                                                           @if($cardData['carddata'] == "")
                                                            <p class="text-success text-center" style="margin-top:20px;"><strong>No Record Found</strong></p>
                                                            @endif
                                                            @if(isset($cardData['carddata']))
                                                            
                                                            <div class="list-credit-cards row" id="list-cards" style="background: wheat;padding: 10px 5px;max-width: 1000px !important;margin: auto;margin-top: 10px;">
                                                                
                                                                @foreach($cardData['carddata'] as $key => $card)
                                                                    <div class="col-lg-3">
                                                                    <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                    <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                        <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                    </span>
                                                                    <span style="display:inline-block;margin-left: 90px;text-align: right;font-size: 12px;">{{ date('m',strtotime($card['card']['exp_month']))}}-{{$card['card']['exp_year']}}</span>
                                                                    <br />
                                                                    <span style="padding-left: 9px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                     
                                                                     </label>
                                                                     </div>
                                                                @endforeach
                                                                
                                                            </div>
                                                            <br />
                                                            @endif
                                                            <?php } ?>
                                                        </div>
                                                    <?php } else { ?>
                                                    <div class="col-lg-12">
                                                     <p class="text-success text-center" style="margin: auto;margin-top:20px;font-variant:small-caps;font-size:22px;color:#000 !important;padding: 20px 0px 20px 0px;background: #f8f8f8;line-height: 22px;max-width: 1000px;"><strong>List of Your Credit Cards at Stripe</strong></p>
                                                       @if($cardlist == "")
                                                        <p class="text-success text-center" style="margin-top:20px;"><strong>No Record Found</strong></p>
                                                        @endif
                                                        @if(isset($cardlist) && $cardlist != "")
                                                        
                                                        <div class="list-credit-cards row" id="list-cards" style="background: wheat;padding: 10px 5px;max-width: 1000px !important;margin: auto;margin-top: 10px;">
                                                            
                                                            @foreach($cardlist as $key => $card)
                                                                <div class="col-lg-3">
                                                                <label class="cardblock" style="display:block;height:auto;line-height:20px;padding: 10px 10px;" for="radio-card-select-{{$key}}">
                                                                <span class="brand" style="position:relative;right:0px;margin-left:5px;">
                                                                    <i class="pf pf-{{strtolower($card['card']['brand'])}}" style="color:#0059bd;"></i>
                                                                </span>
                                                                <span style="display:inline-block;margin-left: 90px;text-align: right;font-size: 12px;">{{ date('m',strtotime($card['card']['exp_month']))}}-{{$card['card']['exp_year']}}</span>
                                                                <br />
                                                                <span style="padding-left: 9px;font-size: 18px;font-weight:bold;"> xxxx xxxx xxxx {{$card['card']['last4']}}</span> <br /> 
                                                                 
                                                                 </label>
                                                                 </div>
                                                            @endforeach
                                                            
                                                        </div>
                                                        <br />
                                                        @endif
                                                       
                                                        <?php } ?>
                                                    </div>
                                                    <!--<div id="card-element">
                                                        
                                                    </div>-->

                                                    <!--<div class="col-lg-12">
                                                        <p class="text-info" style="margin-bottom:10px;"><strong>Please provide your credit/debit card details</strong></p>
                                                        <label>

                                                            <span>Card Number</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-number-element" class="field"></div>

                                                                <span class="brand">
                                                                    <i class="pf pf-credit-card" id="brand-icon"></i>
                                                                </span>
                                                            </div>
                                                        </label>
                                                    </div>


                                                    <div class="col-lg-7">
                                                        <label>
                                                            <span>Expiry Date</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-expiry-element" class="field"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <label>
                                                            <span style="width: 36px;">CVC</span>
                                                            <div class="bordered-colored">
                                                                <div id="card-cvc-element" class="field"></div>
                                                            </div>    
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label>
                                                            <span>Postal Code</span>
                                                            <div class="bordered-colored">
                                                                <input id="postal-code" maxlength="6" name="postal_code" class="field" style="width: 100%;height: 40px;" />
                                                            </div>
                                                        </label>
                                                    </div>-->

                                                    <?php } ?>
                                                </div>
                                                </div>
    
</section>
<!-- End Subscribe Area -->
@endsection
@section('style')
<style>
    .p-center .pagination{
        justify-content: center !important;
    }
    .d-content{
        cursor: pointer;
    }
</style>

@endsection


