<style>
    .other-link{
        
    }
</style>
<section id="schools-container" class="dl-container dcare__courses__area section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="courses">
                    <div class="courses__inner">
                        <div class="courses__wrap">
                            <div class="courses__content">
                                <div class="d-flex mb-2">
                                    <h4 class="" style="flex-grow: 1;">
                                        <?= ucfirst($notification->title) ?>
                                    </h4>
                                    <a class="dcare__btn" href="javascript:;" onclick="event.preventDefault();getNotifications()" 
                                       style="line-height: 35px;height: 35px;">Back</a>
                                </div>


                                <!--                                <h4 class="mb-2">
                                                                    Description
                                                                </h4>-->
                                <p><?= $notification->description ?></p>

                                @if($notification->files)
                                <?php
                                $links = $notification->files;
                                $links = explode(',', $links);
                                ?>
                                <h4><i class="fa fa-paperclip"></i> Attachments</h4>
                                <div>
                                    @foreach($links as $link)

                                    <div class="mb-2">
                                        @if (in_array($extension = pathinfo($link, PATHINFO_EXTENSION), ['jpg', 'jpeg','png', 'bmp']))
                                        <a href="{{ url('user_files/'.$link) }}" target="_blank">
                                            <img src="{{ asset('user_files/'.$link) }}" style="width:100px;">
                                        </a>
                                        @else
                                        <a href="{{ url('user_files/'.$link) }}" target="_blank" class="other-link">
                                            <?= $link ?>
                                        </a>
                                        @endif
                                    </div>
                                    @endforeach
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
