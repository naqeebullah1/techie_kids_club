<div class="container">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-lg-12">
            <div class="subscribe__inner">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>School</th>
                            <th>Class</th>
                            <th>Description</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notifications as  $notifi)
                        <?php
                        $notification = $notifi->notification;
                        $schoolClass = $notifi->school_class;
                        ?>
                        <tr>
                            <td><?= $notification->title ?></td>
                            <td><?= $schoolClass->school->name ?></td>
                            <td>
                                <?= $schoolClass->title ?>
                            </td>
                            <td> 
                                <?= mb_strimwidth($notification->description, 0, 100, "..."); ?>
                            </td>
                            <td><a href="<?= asset('parent/notification-details/' . $notifi->notification_id) ?>" onclick="return getNotificationDetails(this)" class="btn btn-link"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>