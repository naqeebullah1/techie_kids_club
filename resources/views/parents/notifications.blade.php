@extends('layouts.front_theme')
@section('content')
<style>
    .ellipsis {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
</style>
<!-- Start Subscribe Area -->
<section class="bcare__subscribe bg-image--7 subscrive--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-lg-12">
                <div class="subscribe__inner">
                    <h2><i class="fa fa-bell"> Notifications</i></h2>


                </div>
            </div>
        </div>
    </div>
</section>
<section id="schools-container" class="dl-container dcare__courses__area section-padding--lg bg--white"></section>

<!-- End Subscribe Area -->
@endsection
@section('style')
<style>
    .p-center .pagination{
        justify-content: center !important;
    }
    .d-content{
        cursor: pointer;
    }
</style>
@endsection
@section('script')
<script>
    function getNotifications() {
        var url = '{{url("parent/notification-list")}}';
        $('.dl-container').load(url);

    }
    $(function () {
        getNotifications();
    });
    function getNotificationDetails($this) {
        var url = $this.href;
        $('.dl-container').load(url);
        return false;
    }
</script>
@endsection
