<div class="d-flex">
    
    <h2 class="mb-2" style="flex-grow: 1;">Purchase Details</h2>
    <a class="dcare__btn" href="javascript:" onclick="removePD()" style="line-height: 35px;height: 35px;">Back</a>
</div>
<style>
.gradient-custom {
/* fallback for old browsers */
background: #cd9cf2;

/* Chrome 10-25, Safari 5.1-6 */
background: -webkit-linear-gradient(to top left, rgba(205, 156, 242, 1), rgba(246, 243, 255, 1));

/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
background: linear-gradient(to top left, rgba(205, 156, 242, 1), rgba(246, 243, 255, 1))
}
</style>
<section class="h-100" style="overflow-x:hidden">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-lg-12 col-xl-12">
        <div class="card" style="border-radius:0px;width:100%">
          <div class="card-header">
            <h5 class="text-info mb-0">Below are your purchase details. You can also cancel any subscription any time.</h5>
          </div>
          <div class="card-body p-4">
            <div class="d-flex justify-content-between align-items-center mb-4">
              <h3 class="text-muted mb-0">Order Number : <?php echo $admission['id']; ?> </h3>
              <h3 class="text-muted mb-0">Order Date : <?php echo date('M d,Y',strtotime($admission['created_at'])); ?> </h3>
            </div>
            <?php $total_amount = 0; $count = 0;?>
            <?php 

            ?>
            @foreach($details as $dd)
            <?php
                if($count %2 == 0) {
                    $bgcolor = "#f8f8f8";
                } else {
                    $bgcolor = "#fdfdfd";
                }
                if($dd->status == 0) {
                    $bgcolor = "#fdd8db";
                }
            ?>
            <div class="card shadow-0 mb-4" style="width:100%;border-radius:0px;background-color:<?php echo $bgcolor; ?>">
              <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Logo</th>
                        <th colspan="2" >School</th>
                        <th >Class</th>
                        <th colspan="2">Student</th>
                        <th>Franchise</th>
                        <th>Class Name</th>
                    </tr>
                    <tr>
                        <td><img src="<?= asset('images/uploads/' . $dd->school->image) ?>" width="80px" style="border:1px solid #f8f8f8;text-align:center;" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'"></td>
                        <td colspan="2">{{$dd->school->name }}</td>
                        <td><?php if(isset($dd->admission_class->title)) { ?>
                            {{ $dd->admission_class->title }}</td>
                            <?php } ?></td>
                        <td colspan="2">{{$dd->student->first_name.' '.$dd->student->last_name }}</td>
                        <td>{{$dd->franchise->name}}</td>
                        <!--<td><img src="<?= asset('images/uploads/' . $dd->student->image) ?>" width="100px"></td>-->
                
                        
                        <td>{{$dd->classroom_name }}</td>
                    </tr>
                    <?php if($dd->payment_method_id != "") { ?>
                    <tr>
                        <th>Transaction Id</th>
                        <th>Status</th>
                        <th colspan="2">Next Payment Date</th>
                        <th>Price($)</th>
                        <th>Discount ($)</th>
                        <th>Total ($)</th>
                        
                    </tr>
                    <tr>
                        <td>{{$dd->stripe_payment_intent_id?$dd->stripe_payment_intent_id:'- -'}}</td>
                        <td><h4><span class="badge badge-success">PAID</span></h4></td>
                        <td colspan="2"><?php echo date('M d,Y', strtotime('+1 day ', strtotime($dd->plan_period_end))) ?></td>
                        <td style="text-align:right;">{{$dd->class_charges}}</td>
                        <td style="text-align:right;">{{($dd->discount)?$dd->discount:0}}</td>
                        <td style="text-align:right;">{{number_format(($dd->class_charges - $dd->discount),2)}}</td>
                        <td>
                            <?php if($dd->status == 0) { ?>
                            <button class="btn btn-danger" disabled>Subscription Cancelled</button>
                            <?php }  else { ?>
                            <!--<a href="javascript:" class="btn btn-danger">Cancel Subscription</a>
                                DO THIS LATER
                            -->
                            <?php if(auth()->user()->role == 3 || auth()->user()->franchise->id == 1 || auth()->user()->franchise->id == $dd->franchise_id) { ?>
                           
                            <a href="{{url('/')}}/parent/cancelsubscription/{{$dd->stripe_subscription_id}}" class="btn btn-danger">Cancel Subscription</a>
                            <?php 
                                    } 
                                } ?>
                            </td>
                    </tr>
                    @if(!empty($dd['admission_detail_transaction'][0]))
                    <tr>
                            <th colspan='5'><h3 class="text-warning">Recurring Payment Details</h3></th>
                        </tr>
                        <tr>
                        <th>Transaction Id</th>
                        <th>Status</th>
                        <th colspan="2">Next Payment Date</th>
                        <th colspan="2">Amount Due($)</th>
                        <th >Amount Paid($)</th>
                    </tr>
                    @endif
                    @php
                    $prev_transid = "";
                    @endphp
                    @foreach($dd['admission_detail_transaction'] as $transdata)
                        @php
                            if($transdata->payment_intent == $prev_transid)
                            continue;
                        @endphp
                        <tr>
                        <td>{{$transdata->payment_intent}}</td>
                        <td>{!!($transdata->paid_status == 1)?'<h4><span class="badge badge-success">PAID</span></h4>':'<h4><span class="badge badge-danger">FAILED</span></h4>'!!}</td>
                        <td colspan="2"><?php echo date('M d,Y', strtotime('+1 day ', $transdata->end_date)) ?></td>
                        <td colspan="2" style="text-align:right;">{{number_format(($transdata->amount_due),2)}}</td>
                        <td style="text-align:right;">{{number_format(($transdata->amount_paid),2)}}</td>
                        </tr>
                        @php
                            $prev_transid = $transdata->payment_intent;
                            
                        @endphp
                    @endforeach
                    <?php } else {
                     ?>
                    <tr>
                        <th colspan="4" style="text-align:right">Amount($): {{$dd->paid_amount}}</th>
                        <td>
                            <?php if($dd->status == 0) { ?>
                            <button class="btn btn-danger" disabled>Subscription Cancelled</button>
                            <?php }  else { 
                                ?>
                                <?php if(auth()->user()->role == 3 || auth()->user()->franchise->id == 1 || auth()->user()->franchise->id == $dd->franchise_id) { ?>
                            <a href="javascript:" class="btn btn-danger" onClick="cancelManualSubscription('{{$dd->admission_class->title}}','{{ siteDetails()->contact }}')">Cancel Subscription</a>
                            <?php } } ?>
                            </td>
                    </tr>
                    <?php } 
                        $total_amount = $total_amount + ($dd->paid_amount);
                        $count++;
                    ?>
                    </table>
              
                
              </div>
            </div>
            @endforeach
          </div>
          <div class="card-footer border-0 px-4 py-5"
            style="background-color: #F78932; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
            <h5 class="d-flex align-items-center justify-content-end text-white text-uppercase mb-0">Total: <span class="h2 mb-0 ms-2 ml-2" style="color:#fff">${{$total_amount}}</span></h5>
          </div>
        </div>
      </div>
    </div>
</section>