<div class="z-index-0 active" id="first">
    <div class="icon big">
        <i class="fa fa-dashboard fa-4x"></i>
    </div>

    <h1>Welcome, <?= $user->first_name . ' ' . $user->last_name ?></h1>
    <?php if($user->franchise_id != 1) { ?>
    	<h3 class="text-info mt-2">You are connected to  <strong class="text-success">"<?php echo $user->franchise()->first()->name; ?>"</strong> Franchise</h3>
    <?php } ?>
      <!--<p class="dashboard-info" style="margin-top:20px;font-size:18px;text-align:center;">Your are currently viewing your account page. Here you will be able to view or update your <a href="#">child profile</a>, view <a href="#">recent purchases </a> and edit your <a href="#">profile</a></p>-->
</div>