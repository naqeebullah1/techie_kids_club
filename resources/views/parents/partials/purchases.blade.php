<style>
    .popover-body table td, .popover-body table th {
        border: 1px solid #ccc !important;
        padding:10px;
    }
    .popover{
        max-width: 100%; /* Max Width of the popover (depending on the container!) */
    }
</style>
<div class="z-index-0" id="third">
    <div class="col-12 purchase-order-details"></div>
    <div class="col-12 purchase-orders">
        <div class="d-flex">
            <h2 class="mb-2" style="flex-grow: 1;">Purchases</h2>
        </div>
        <?php
                $purchases = $user->admissions;
             // dd($purchases->toArray());
                ?>
        <?php if(empty($purchases->toArray())) { ?>
        <h3 style="text-align:center;margin-top:20px;" class="text-info">No Record Found.</h3>
        <?php } else { ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <!--<th>Next due date</th>-->
                    <th>Total Amount</th>
                    <!--<th>Payment</th>-->
                    <th>Total Classes</th>
                    <th>Details</th>
                    <th>Status</th>
                    <!--<th>Action</th>-->
                </tr>
            </thead>
            <tbody>

                @forelse($purchases as $purchase)
                <?php
                $details = $purchase->details;
//                $tc=$details
//                dump();
                ?>
                <tr>
                    <td width="10px"><?= $purchase->id ?></td>
                    <!--<td>
                        <?= date('m/d/Y', strtotime('+1 month ', strtotime($purchase->created_at))) ?>
                    </td>-->

                    <td>$<?= $details->sum(function($detail) {
    return $detail->class_charges - $detail->discount;
}); ?></td>
                    <td><?= $details->count() ?></td>
                    <td>
                        <a href="<?= url('parent/add-details/' . $purchase->id) ?>" onclick="return getDetails(this)" class="btn btn-info btn-sm btn-small" id="view-purchase-detail-<?php echo $purchase->id ?>">View</a>
                    </td>
                    <td>
                        <?php
                        $cc = 'badge-success';
                        if ($purchase->status == 'Pending') {
                            $cc = 'badge-warning ';
                        }
                        ?>
                        <span class="badge <?= $cc ?>"><?= $purchase->status ?></span>
                    </td>

                </tr>
                @empty
                <tr>
                    <td colspan="100%">No Record Found</td>
                </tr>
                @endforelse
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>
