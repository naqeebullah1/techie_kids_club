<div class="z-index-0 pt-5" id="eight">
    <?php if($user->sms_subscribed == 1) { ?>
    	<h3 class="text-info mt-5">You have subscribed to SMS service, Click below of turn off the SMS notifications</h3>
        <a href="<?= url('parent/front-franchise/' . request()->franchise_id .'/sms-subscription?status=0') ?>" class="btn btn-danger mt-5"> Unsubscribed</a>
    <?php } else { ?>
    <h3 class="text-info mt-5">You haven't subscribed to SMS service, Click below to start getting notifications via SMS</h3>
    <a href="<?= url('parent/front-franchise/' . request()->franchise_id .'/sms-subscription?status=1') ?>" class="btn btn-danger mt-5"> Subscribed</a>
    <?php } ?>
</div>
