<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>School</th>
            <th>Class </th>
            <th>Student</th>
            <th>Charges</th>
            <th>Duration Of Class</th>
            <th>Timimg Of Class</th>
      </tr>
    </thead>
    <tbody>
        @foreach($classes as $city)
        <tr>
            <td>
                <?= $city->school->name ?>
            </td>
            <td>
                <?= $city->title ?>
            </td>
            <td>
                <?= $city->s_first_name . ' ' . $city->s_last_name ?>
            </td>
            <td>
                $<?= $city->class_charges ?>
            </td>
            <td>
                <b>From: </b><?= formatDate($city->start_on) ?><br>
                <!--<b>To: </b> <?= formatDate($city->end_on) ?>-->
            </td>
            <td>
                <b>From: </b><?= formatTime($city->start_at) ?><br>
                <b>To: </b> <?= formatTime($city->end_at) ?>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>                                          

