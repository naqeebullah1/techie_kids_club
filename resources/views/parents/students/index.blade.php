@extends('master')
@section('content')
    <?php if(isset($_GET['linked'])){ ?>
     <div class="container grid-wrapper p-3 mt-2">
        <div class="grid-header">
            <h2>Parent Details</h2>
            <div class="row d-md-flex mb-md-2 p-md-1">
                <div class="col-3">
                    <h4 class="small bold m-0">Name</h4>
                </div>
                <div class="col-3">
                    <h4 class="small bold m-0">Email</h4>
                </div>
                <div class="col-3">
                    <h4 class="small bold m-0">Phone1</h4>
                </div>
                <div class="col-3">
                    <h4 class="small bold m-0">Franchise</h4>
                </div>
    
            </div>
            <div class="grid-body">
                <div class="row mb-md-0 p-md-1 mb-3 p-1">
                    <div class="col-3">
                        <p class="small bold m-0">{{ $parent->first_name . ' ' . $parent->last_name }}</p>
                    </div>
                    <div class="col-3">
                        <p class="small bold m-0">{{ $parent->email }}</p>
                    </div>
                    <div class="col-3">
                        <p class="small bold m-0">{{$parent->phone1}}</p>
                    </div>
                    <div class="col-3">
                        <p class="small bold m-0">{{$parent->franchise->name}}</p>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
    <?php } ?>
    <div class="mb-2">
        <h1 class="float-left">Children ({{ $parent->first_name . ' ' . $parent->last_name }})</h1>
        <div class="clearfix"></div>

        <div class="pull-left">
            <b>Legends: </b>
            <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title=""
               class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
            <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip"
               data-placement="top" title="" data-original-title="Inactive"></i>
        </div>
        @can('cities-edit')
            <!--<a onclick="event.preventDefault();loadForm(this)" class="btn btn-success float-right" href="<?= url('parent/children/create/' . request()->user_id) ?>">Create Child</a>-->
        @endcan
        <div class="clearfix"></div>
    </div>

    <div class="record-form"></div>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row table-responsive" id="no-more-tables">
                        @include('parents.students.table')
                    </div>

                    <h4 class="font-weight-bold mt-5">Related School Classes</h4>
                    <div class="row table-responsive" id="no-more-tables">
                        @include('parents.partials.student_classes')
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('partials.loadmorejs')
@endsection
@section('script')
    @if(Session::has('outcome'))
        <script>
            $(function () {
                $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
            })

        </script>
    @endif
@endsection
