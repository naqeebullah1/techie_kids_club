<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if(request()->id)
                    {{ __('Edit Promo Code') }}
                    @else
                    {{ __('Create Promo Code') }}
                    @endif
                </div>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.promocodes.store') }}">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-lg-6">
                            <label>Code</label>
                            <input value="{{ $school->id }}" type="hidden" class="form-control" name="id"/>
                            <input value="{{ $school->promo_code }}" type="text" class="form-control" name="record[promo_code]"/>
                        </div>
                        <div class="col-lg-6">
                            <label>Promo Type</label>
                            {{ Form::select('record[promo_type]',[''=>'Select Type','percent'=>'Percentage','flat'=>'Flat Amount'],$school->promo_type,['onchange'=>'toggleFields()','class'=>'form-control select2 promo_type']) }}
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-lg promo_type_containers percent_off_container">
                            <label>Percent Off</label>
                            <input value="{{ $school->percent_off }}" type="text" class="form-control percent_off" name="record[percent_off]"/>
                        </div>
                        <div class="col-lg promo_type_containers flat_amount_container">
                            <label>Flat Amount</label>
                            <input value="{{ $school->flat_amount }}" type="text" class="form-control flat_amount" name="record[flat_amount]"/>
                        </div>

                        <div class="col-lg">
                            <label>From Date</label>
                            <input value="{{ formatDate($school->from_date) }}" type="text" class="datepicker form-control" name="record[from_date]"/>
                        </div>

                        <div class="col-lg">
                            <label>To Date</label>
                            <input value="{{ formatDate($school->to_date) }}" type="text" class="datepicker form-control" name="record[to_date]"/>
                        </div>
                    </div>


                    <div class="text-right">
                        <a href="javascript:" onclick="removeForm()" class="mt-2 btn btn-success">Cancel</a>
                        @if(request()->id)
                        <button type="submit" class="mt-2 btn btn-success">Update</button>
                        @else
                        <button type="submit" class="mt-2 btn btn-success">Add</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function citiesDD(isChanged = null) {
        var $url = "{{ url('backend/cities-dd') }}";
        var sId = '{{$school->state_id}}';
        var cId = '{{$school->city_id}}';
        if (isChanged) {
            sId = $('#state_id').val();
            cId = '';
        }
        $url = $url + "?state_id=" + sId + "&city_id=" + cId;

        $.get($url, function (data) {
            $('#city_id').html(data.html);
//            $('.select2').select2();
        });
    }
    $(function(){
       toggleFields();
    });
    function toggleFields() {
        $('.promo_type_containers').addClass('d-none');
        if ($('.promo_type').val() == 'percent') {
            $('.percent_off_container').removeClass('d-none');
        }
        if ($('.promo_type').val() == 'flat') {
            $('.flat_amount_container').removeClass('d-none');
        }
    }
</script>