@extends('master')
@section('content')
<div class="mb-2">
    <h1 class="float-left">Promo Codes</h1>
    <div class="clearfix"></div>

    <div class="pull-left">
        <b>Legends: </b>
        <i style="padding: 1px 9px;" data-toggle="tooltip" data-placement="top" title="" class="rounded-circle bg-success ml-2 mr-2" data-original-title="Active"></i>
        <i style="padding: 1px 9px;" class="rounded-circle bg-danger mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactive"></i>
    </div>

    @can('schools-edit')
    <a onclick="event.preventDefault();loadForm(this)" class="btn btn-success float-right ml-2" href="<?= url('backend/promocodes-create') ?>">Create Promo Code</a>
    @endcan
    <!--<div class="d-flex">-->
    
        <a class="btn btn-outline-success float-right ml-2 <?=($expired)?'active':''?>" href="<?= url('backend/promo-codes/expired') ?>">Expired Promo Codes</a>
        <a class="btn btn-outline-success <?=(!$expired)?'active':''?> float-right ml-2" href="<?= url('backend/promo-codes') ?>">Active Promo Codes</a>

    <!--</div>-->
    <div class="clearfix"></div>
</div>
<div class="record-form"></div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row table-responsive" id="no-more-tables">
                    @include('promo_codes.table')
                </div>

            </div>
        </div>
    </div>
</div>
@include('partials.loadmorejs')
@endsection
@section('script')
@if(Session::has('outcome') && Session::get('outcome') != "")
<script>
    $(function () {
        $.toaster({priority: 'success', title: 'Success', message: "{{Session::get('outcome')}}"});
    })

</script>
@endif

@if(Session::has('promoerror') && Session::get('promoerror') != "")
<script>
    $(function () {
        $.toaster({priority: 'error', title: 'Error', message: "{{Session::get('promoerror')}}"});
    })

</script>
@endif

@endsection