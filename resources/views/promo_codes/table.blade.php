<table class="sortable table table-bordered table-small">
    <thead>
        <tr>
            <th>Promo Code</th>
            <th>Amount Off</th>
            <th>From Date</th>
            <th>To Date</th>
            <th>Type</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($schools as $school)
        <tr>
            <td>
                <?= $school->promo_code ?>
            </td>
            <td>
                <?= ($school->promo_type == 'flat')?"-".$school->flat_amount:"-".$school->percent_off."%" ?>
            </td>
            <td>
                <?= formatDate($school->from_date) ?>
            </td>
            <td>
                <?= formatDate($school->to_date) ?>
            </td>
            <td>
                <?= ucfirst($school->promo_type) ?>
            </td>
            <td>
                @can('schools-edit')
                <a href="{{ url('backend/change-status/promo/'.$school->id,$school                        ->status) }}">

                    <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                        {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                    </span>
                </a>

                @else
                <span class="span-status {{ $school->status == 1 ? 'active-span-bg': 'disabled-span-bg' }}">
                    {{ $school->status == 1 ? __('Active') : __('Blocked') }}
                </span>
                @endcan
            </td>



            <td nowrap>
                @can('schools-edit')
                <!--<a onclick="event.preventDefault(); loadForm(this)" href="<?= url('backend/promocodes-create/' . $school->id) ?>"
                   class="btn btn-small btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>-->
                <button type="submit"
                        onclick="event.preventDefault(); setUrl('{{ route('admin.promocodes.destroy', $school->id) }}'); deleteConfirm('destroy', 'You are about to delete the coupon, Are you sure?')"
                        class="btn btn-icon btn-round btn-danger btn-small btn-sm">
                    <i class="fa fa-trash"></i>
                </button>
                @endcan
            </td>
        </tr>
        @endforeach
        <!--@can('page.edit')
        <tr>

            <td colspan="6">
                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
            </td>

        </tr>    
        @endcan-->
    </tbody>
</table>
<form 
    id="destroy"
    method="post" id="destroy" 
    action="">
    @csrf
    @method('DELETE')
</form>
{{ $schools->links() }}
