@extends('layouts.front_theme')
@section('content')
<style>
    .z-index-0{
        z-index: 0;
    }    
    #right-side .active{
        z-index: 111 !important;
    }

    label.error{
        color:red;
    }
    .pagination .page-item.active{
        margin-top: 0 !important;
    }
    .jconfirm .jconfirm-box div.jconfirm-title-c .jconfirm-title{
        line-height:26px !important;
    }
    .service__icon{
        background-size: cover !important;
        background-position: center !important;
    }
</style>
<section class="myaccount">
    <div id="myaccount-wrapper">
        <div id="left-side">
            <ul>
                <li class="choose active">
                    <div class="icon active">
                        <i class="fa fa-dashboard fa-2x"></i>
                    </div>
                    Dashboard
                </li>


                <li class="pay">
                    <div class="icon">
                        <i class="fa fa-home fa-2x"></i>
                    </div>
                    My Schools
                </li>

                <li class="ship">
                    <div class="icon">
                        <i class="fa fa-edit fa-2x"></i>
                    </div>
                    My Profile
                </li>
                <li class="demand-liberariess">
                    <div class="icon">
                        <i class="fa fa-book fa-2x"></i>
                    </div>
                    <a target="_blank" style="color:rgba(51, 51, 51, 0.5);" target="_blank" href="<?= url('parent/demand_library') ?>" >
                        On-Demand Library
                    </a>
                </li>
                <li class="logout">
                    <div class="icon">
                        <i class="fa fa-sign-out fa-2x"></i>
                    </div>
                    <a href="javascript:" onClick="event.preventDefault();document.getElementById('logout-form').submit()">
                        <form action="{{url('parent/logout')}}" method="POST" style="display:none;" id="logout-form">
                            @csrf
                            <input type="hidden" name="is_parent" value="1">
                            @method('POST')
                        </form>
                        Log Out
                        <span class="pull-right"><i class="pg-power"></i></span>
                    </a>

                </li>
            </ul>
        </div>

        <div id="border">
            <div id="line" class="one"></div>
        </div>

        <div id="right-side" style="height: ">
            @if(Session::has('success'))
            <div class="row" style="justify-content: center;">
                <div class="col-11 pt-4">
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('success')}}
                    </div>
                </div>
            </div>
            @endif
            @if(Session::has('error'))
            <div class="row" style="justify-content: center;">
                <div class="col-11 pt-4">
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{Session::get('error')}}
                    </div>
                </div>
            </div>
            @endif
            @include('parents.partials.dashboard')
            @include('school_admin.partials.my_schools')
            @include('parents.partials.my_profile')
            @include('parents.partials.demand_liberaries')
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                &nbsp;
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h5 class="font-weight-bold">Link</h5>
                <iframe width="100%" height="200px" src=""  allowfullscreen></iframe>
                <h5 class="font-weight-bold">Description</h5>
                <div class="desc"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection
@section('script')
<script>

    $(document).ready(function () {

        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: true,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
        });
        $('.kids-code-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: true,
            dots: true,
            pauseOnHover: true,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
        });
        $('.testimonial-carousel').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5500,
            arrows: false,
            dots: false,
            pauseOnHover: true,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 1
                    }
                }]
        });
    });
    

// fn
    function accordion() {
        // this = the btn | icon & bg changed
        this.classList.toggle("is-open");

        // the acc-content
        const content = this.nextElementSibling;

        // IF open, close | else open
        if (content.style.maxHeight)
            content.style.maxHeight = null;
        else
            content.style.maxHeight = content.scrollHeight + "px";

    }
    
     function setHeight() {
         
        var $elems = $('#right-side').find('img, iframe','div','form');

// count them
var elemsCount = $elems.length;

// the loaded elements flag
var loadedCount = 0;

var right_content_height = parseInt($("#right-side .active").css('height')) + 120;
       
        $("#right-side").attr("style", "min-height:" + right_content_height + "px !important");
        
// attach the load event to elements
$elems.on('load', function () {
    // increase the loaded count 
    loadedCount++;

    // if loaded count flag is equal to elements count
    if (loadedCount == elemsCount) {
        // do what you want
        var right_content_height = parseInt($("#right-side .active").css('height')) + 120;
       
        $("#right-side").attr("style", "min-height:" + right_content_height + "px !important");
    } 
});

        
    }

    $(".choose").click(function () {

        $(".choose").addClass("active");
        $(".choose > .icon").addClass("active");
        $(".pay").removeClass("active");
        $(".wrap").removeClass("active");
        $(".ship").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".wrap > .icon").removeClass("active");
        $(".ship > .icon").removeClass("active");
        $("#line").addClass("one");
        $("#line").removeClass("two");
        $("#line").removeClass("three");
        $("#line").removeClass("four");
        setHeight();
        $(".demand-liberaries").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");
    });
    $(".pay").click(function () {
      getSchools();
        $(".pay").addClass("active");
        $(".pay > .icon").addClass("active");
        $(".choose").removeClass("active");
        $(".wrap").removeClass("active");
        $(".ship").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".wrap > .icon").removeClass("active");
        $(".ship > .icon").removeClass("active");
        $("#line").addClass("two");
        $("#line").removeClass("one");
        $("#line").removeClass("three");
        $("#line").removeClass("four");

        $(".demand-liberaries").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");
        setHeight();
    })
    $(".demand-liberaries").click(function () {
        $(".demand-liberaries").addClass("active");
        $(".demand-liberaries > .icon").addClass("active");

        $(".pay").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".choose").removeClass("active");
        $(".wrap").removeClass("active");
        $(".ship").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".wrap > .icon").removeClass("active");
        $(".ship > .icon").removeClass("active");
        $("#line").addClass("two");
        $("#line").removeClass("one");
        $("#line").removeClass("three");
        $("#line").removeClass("four");
        setHeight();
    })

    $(".wrap").click(function () {
        $(".wrap").addClass("active");
        $(".wrap > .icon").addClass("active");
        $(".pay").removeClass("active");
        $(".choose").removeClass("active");
        $(".ship").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".ship > .icon").removeClass("active");
        $("#line").addClass("three");
        $("#line").removeClass("two");
        $("#line").removeClass("one");
        $("#line").removeClass("four");
        $(".demand-liberaries").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");
        setHeight();
    })

    $(".ship").click(function () {
        $(".demand-liberaries").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");

        $(".ship").addClass("active");
        $(".ship > .icon").addClass("active");
        $(".pay").removeClass("active");
        $(".wrap").removeClass("active");
        $(".choose").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".wrap > .icon").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $("#line").addClass("four");
        $("#line").removeClass("two");
        $("#line").removeClass("three");
        $("#line").removeClass("one");
        setHeight();
    })



    $(".choose").click(function () {
        $("#first").addClass("active");
        $("#second").removeClass("active");
        $("#third").removeClass("active");
        $("#fourth").removeClass("active");
        $("#fifth").removeClass("active");
        setHeight();
    })
    $(".demand-liberaries").click(function () {
        $("#fifth").addClass("active");
        $("#first").removeClass("active");
        $("#second").removeClass("active");
        $("#third").removeClass("active");
        $("#fourth").removeClass("active");
        setHeight();
    })

    $(".pay").click(function () {
        $("#first").removeClass("active");
        $("#second").addClass("active");
        $("#third").removeClass("active");
        $("#fourth").removeClass("active");
        $("#fifth").removeClass("active");
        setHeight();
    })

    $(".wrap").click(function () {
        $("#first").removeClass("active");
        $("#second").removeClass("active");
        $("#third").addClass("active");
        $("#fourth").removeClass("active");
        $("#fifth").removeClass("active");
        setHeight();
    })

    $(".ship").click(function () {
        $("#first").removeClass("active");
        $("#second").removeClass("active");
        $("#third").removeClass("active");
        $("#fourth").addClass("active");
        $("#fifth").removeClass("active");
        setHeight();
    });
    $("#view-purchase-detail").click(function () {
        $(".choose").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".pay").removeClass("active");
        $(".demand-liberaries").removeClass("active");
        $(".wrap").addClass("active");
        $(".ship").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".wrap > .icon").addClass("active");
        $(".ship > .icon").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");
        $("#line").addClass("one");
        $("#line").removeClass("two");
        $("#line").removeClass("three");

        $("#line").removeClass("four");

        $("#line").removeClass("fifve");

        setHeight();
    });

    function loadForm($this) {
        $.get($this, function (data) {
            $('.form-container').html(data.html);
//            $('.select2').select2();
            $('.dropify').dropify();
            $('.hide-for-add').addClass('d-none');
            $('.pay').trigger('click');
            $('.datepicker').datepicker({
                autoclose: true
            });
//            $('.timepicker').timepicker();
        });

    }
   
    function getSchoolDetails($this) {
        var url = $this.href;
        $.get(url, function ($data) {
            $('.my-schools-module').html($data);
            setHeight();
        });
        return false;
    }
    function getClassDetails($this) {
        var url = $this.href;
        $.get(url, function ($data) {
            $('.my-schools-module').html($data);
            setHeight();
        });
        return false;
    }
    function getDetails($this) {
        var thisid = $this.id;

        $("#" + thisid).html("Please wait <i class='fa fa-refresh'></i>");
        $.get($this, function (data) {
            $('.purchase-order-details').html(data.html);
            $('.purchase-orders').addClass('d-none');
            setHeight();
            $("#" + thisid).html("View");
        });
        return false;

    }

    function removePD() {
        $('.purchase-order-details').html('');
        $('.purchase-orders').removeClass('d-none');
        $("#view-purchase-detail").html("View");
        $(".choose").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".pay").removeClass("active");
        $(".demand-liberaries").removeClass("active");
        $(".wrap").addClass("active");
        $(".ship").removeClass("active");
        $(".pay > .icon").removeClass("active");
        $(".wrap > .icon").addClass("active");
        $(".ship > .icon").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");
        $("#line").addClass("one");
        $("#line").removeClass("two");
        $("#line").removeClass("three");

        $("#line").removeClass("four");

        $("#line").removeClass("fifve");

        setHeight();
    }
    function removeForm() {
        $('.form-container').html('');
        $('.hide-for-add').removeClass('d-none');
        $('.pay').trigger('click');
        $(".pay").addClass("active");
        $(".pay > .icon").addClass("active");
        $(".choose").removeClass("active");
        $(".wrap").removeClass("active");
        $(".ship").removeClass("active");
        $(".choose > .icon").removeClass("active");
        $(".wrap > .icon").removeClass("active");
        $(".ship > .icon").removeClass("active");
        $("#line").addClass("two");
        $("#line").removeClass("one");
        $("#line").removeClass("three");
        $("#line").removeClass("four");

        $(".demand-liberaries").removeClass("active");
        $(".demand-liberaries > .icon").removeClass("active");

        setHeight();
    }

    function cancelManualSubscription(className, phoneNumber) {
        $.confirm({
            title: 'Subscription Cancellation - ' + className,
            content: 'Please Call at <a href="tel:' + phoneNumber + '">' + phoneNumber + '</a> to Cancel the Subscription',

            buttons: {
                close: function () {

                }
            }
        });
    }
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
//        $('#register-form').submit(function () {
        $("#register-form").validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                },
                confirm_password: {
                    equalTo: "#p-pass"
                }
            },
            messages: {
                login: {
                    required: "To pole jest wymagane!"
                }
            }
        })
//            return false;
//        });
    });

</script>
<script type="text/javascript">
    function getMore($this) {
        var url = $($this).attr('href');
        $('.dl-container').load(url);
        return false;
    }
    function makeSlug($value) {
//        alert($value);
        var $slug = $value.toLowerCase().replace(' ', '-');
        $('#slug').val($slug);
    }
    function setUrl($url) {
        $('form#destroy').attr('action', $url);
    }
    function setDetails(src, desc) {
        $('.modal-body iframe').attr('src', src);
        $('.modal-body .desc').html(desc);
    }
    function getSchools() {
        $.get('<?= url('school-admin/get-schools') ?>', function ($data) {
            $('.my-schools-module').html($data);
             setHeight();
        });
    }


    $(function () {
        $('#myModal').on('hidden.bs.modal', function () {
            $('.modal-body iframe').attr('src', '');
        });
        //setup before functions
        var typingTimer;     //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 seconds for example
        var $input = $('#search-dl');

//on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

//on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

//user is "finished typing," do something
        function doneTyping() {
            var $val = $('#search-dl').val();
            $val = $val.split(' ').join('%20');
            $('.dl-container').load("<?= url()->full() ?>?type=" + $val);
        }

    })
</script>
@endsection
