<div class="col-12 mb-3">
    <h2 class="mb-2" style="flex-grow: 1;">List Of Schools</h2>
</div>
<div class="row class__grid__page pl-2 pr-2">
    <?php
    $schools = $user->schools;
    ?>
    <!-- Start Single Courses -->
    @forelse($schools as $school)
    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
        <div class="courses">
            <div class="courses__thumb">
                <a onclick="return getSchoolDetails(this);"  href="<?= url('school-admin/school-details/' . $school->id) ?>">
                    <img src="<?= asset('images/uploads/' . $school->image) ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" alt="courses images">
                </a>
            </div>
            <div class="courses__inner">
                <div class="courses__wrap">
                    <div class="courses__date"><i class="fa fa-map-marker"></i><?= $school->address ?></div>
                    <div class="courses__content">
                        <h4><a onclick="return getSchoolDetails(this);" href="<?= url('school-admin/school-details/' . $school->id) ?>"><?= $school->name ?></a></h4>

                        <div class="wel__btn text-right">
                            <a onclick="return getSchoolDetails(this);" class="dcare__btn" href="<?= url('school-admin/school-details/' . $school->id) ?>">View Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
    <div class="col-lg-12 col-12 mt-5 pl-4">
        
    <h2 class="text-danger">No School Available</h2>
    </div>
    @endforelse
    <!-- End Single Courses -->
</div>
