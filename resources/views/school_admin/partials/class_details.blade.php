<section class="dcare__courses__area bg-image--1 pb-0">
    <div class="container">
        <div class="d-flex">
            <h2 class="mb-2" style="flex-grow: 1;">Class Details</h2>
            <a class="dcare__btn" onclick="return getSchoolDetails(this);" href="<?= url('school-admin/school-details/' . $school->school_id) ?>" >Back</a>
        </div>
        <div class="clearfix"></div>
        <div class="row ">
            <!-- Start Single Courses -->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="class__list bg-white" >
                    <div class="courses__thumb">
                        <a href="#">
                            <img src="<?= asset('images/uploads/' . $school->school->image) ?>" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" alt="class images" style="width:auto;max-height:400px;">
                        </a>
                    </div>
                    <div class="courses__inner">
                        <div class="courses__wrap" style="height:auto;">
                            <!--<div class="courses__type"></div>-->
                            <div class="courses__content">
                                <div class="class__title mb-3" >
                                    <h4 class="d-flex" style="width: 100%">
                                        <a style="flex-grow: 1" href="javascript:"><?= $school->title ?></a>
                                        Price  :<span style="color:#fe5629;">   $<?= $school->class_charges ?></span>
                                    </h4>

                                </div>

                                <ul class="school-details">
                                    <li class="mb-1"><i class="fa fa-building mr-2" style="font-size: 20px;"></i> <?= $school->school->name ?></li>
                                    <!--<li class="mb-1"><i class="fa fa-calendar mr-2" style="font-size: 20px;"></i> <?= formatDate($school->start_on) ?> - <?= formatDate($school->end_on) ?></li>-->
                                    <li class="mb-1"><i class="fa fa-clock-o mr-2" style="font-size: 20px;"></i> <?= formatTime($school->start_at) ?> - <?= formatTime($school->end_at) ?></li>
                                </ul>
                                <?php if ($school->weekly_off_days) { ?>
                                    <h5 class="mt-3 font-weight-bold mb-2">Week Days</h5>
                                    <?php
                                    $weekly_off_days = $school->weekly_off_days;
                                    if ($weekly_off_days) {
                                        $weekly_off_days = explode(',', $weekly_off_days);
                                        if ($weekly_off_days) {
                                            echo "<div style='padding-left:15px;'><ul style='list-style: circle;' class='d-flex'>";
                                            foreach ($weekly_off_days as $day):
                                                echo '<li style="width:60px;">' . weekDays()[$day] . '</li>';
                                            endforeach;
                                            echo "</ul></div>";
                                        }else {
                                            echo "<p>No Days</p>";
                                        }
                                    } else {
                                        echo "<p>No Days</p>";
                                    }
                                }
                                ?>
                                <?php
//                                     dump($school->teacher);
                                if ($school->teacher) {
                                    $rd = $school->teacher;
                                    ?>
                                    <div class="section-beauty">
                                    <h4 class="mt-3">Teacher</h4>

                                    <ul class="school-details">
                                        <li><span><i class="fa fa-user mr-2"></i> 
                                            <?= ucwords($rd->first_name . ' ' . $rd->last_name) ?></span>
                                            <span style="float:right"><i class="fa fa-envelope mr-2"></i> <a href="mailto:{{$rd->email}}"><?= $rd->email ?></a></span>
                                        </li>
                                    </ul>
                                    </div>
                                <?php }
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
<section class="dcare__courses__area bg-image--1 pb-0 mt-3">
    <div class="container">
        <div class="d-flex">
            <h2 class="mb-2" style="flex-grow: 1;">Students Details</h2>
        </div>
        <div class="clearfix"></div>
        <div class="row bg-white">
            <!-- Start Single Courses -->
            <div class="col-lg-12 col-md-12 col-sm-12">

                <table class="sortable table table-bordered table-small">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            
                            <?php
                            $roles = auth_user_roles();
                            ?>
                            @if(!in_array(2,$roles))
                            <th>Parent</th>
                            <th>Email</th>
                            <th>Phone</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        
                        
                        @foreach($students as $city)
                        <tr>
                            <td>
                                @if($city->student->image)
                                <img src="<?= asset('images/uploads/' . $city->student->image) ?>" width="100px" onerror="this.src='{{asset('frontend/images')}}/no-image.png'">
                                @else
                                <img src="<?= asset('frontend/images/no-image.png') ?>" width="100px">
                                @endif
                            </td>
                            <td>
                                <?= $city->student->first_name ?>
                            </td>
                            <td>
                                <?= $city->student->last_name ?>
                            </td>
                           
                            @if(!in_array(2,$roles))
                            <td>
                                <?= $city->student->user->first_name . ' ' . $city->student->user->last_name ?>
                            </td>
                            <td>
                                <?= $city->student->user->email ?>
                            </td>
                            <td>
                                <?= $city->student->user->phone1 ?>
                            </td>
                            @endif

                        </tr>
                        @endforeach
                        @can('page.edit')
                        <tr>

                            <td colspan="6">
                                <input type="submit" value="Update" class="btn btn-success m-auto d-block"/>
                            </td>

                        </tr>    
                        @endcan
                    </tbody>
                </table>

            </div>

        </div>

    </div>
</section>