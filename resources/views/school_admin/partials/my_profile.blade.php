<div class="z-index-0" id="fourth">
    <div class="col-12">
        <h2 style="margin-bottom:5px;">My Profile</h2>
         @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
                    {{$error}}
                </div>
@endforeach 
   
        <form action="{{url('school-admin/update-profile')}}" id="register-form" method="post">
            @csrf
            <?php
            $auth=Auth::user();
            ?>
            <!-- Name input -->
            <div class="row mb-2">
                <div class="col-lg-4">
                    <div class="form-outline">
                        <label class="form-label" for="registerName">First Name</label>
                        <input type="hidden" name="roles[]" value="3" class="form-control" />
                        <input type="hidden" name="is_parent" value="1" class="form-control" />
                        <input value="<?=$auth->first_name?>" type="text" name="first_name" id="registerName" class="form-control" />
                    </div>
                </div>
                <div class="col-lg-4 pl-1 pr-1">
                    <div class="form-outline">
                        <label class="form-label" for="registerName">Last Name</label>
                        <input value="<?=$auth->last_name?>" type="text" name="last_name" id="last_name" class="form-control" />
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-outline mb-2">
                        <label class="form-label" for="registerEmail">Email</label>
                        <input type="email" value="<?=$auth->email?>" name="email" id="r-email" class="form-control" />
                    </div>
                </div>
            </div>
             <div class="row mb-2">
                            <div class="col-lg-6 pr-1">
                                <div class="form-outline">
                                    <label class="form-label" for="phone1">Phone</label>
                                <input type="tel" name="phone1" value="<?=$auth->phone1?>" pattern="^\(\d{3}\) \d{3}-\d{4}?$" required id="phone1" class="form-control phone" />
                                </div>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <div class="form-outline">
                                    <label class="form-label" for="phone2">Alternate Phone</label>
                                <input type="tel" name="phone2" value="<?=$auth->phone2?>" pattern="^\(\d{3}\) \d{3}-\d{4}?$"  id="phone2" class="form-control phone" />
                                </div>
                            </div>
                        </div>
            <div class="row mb-2">
                <div class="col-lg-6">
                    <label class="form-label" for="registerEmail">Password</label>
                    <input type="password" name="password" id="p-pass" class="form-control" />
                </div>
                <div class="col-lg-6">
                    <label class="form-label" for="registerEmail">Confirm Password</label>
                    <input type="password" name="password_confirmation" id="p-pass" class="form-control" />
                </div>
            </div>

            <!-- Password input -->
            <!--                        <div class="form-outline mb-2">
                                        <label class="form-label" for="registerPassword">Password</label>
                                        <input type="password" id="registerPassword" class="form-control" />
                                    </div>-->

            <!-- Repeat Password input -->
            <!--                        <div class="form-outline mb-2">
                                        <label class="form-label" for="registerRepeatPassword">Repeat password</label>
                                        <input type="password" id="registerRepeatPassword" class="form-control" />
                                    </div>-->
            <!-- Submit button -->
            <div class="row">
                <div class="col-lg-12 text-right">
                    <button type="submit" class="dcare__btn mb-3" style="border: none;">Save Changes</button>
                </div>
            </div>
        </form>
    </div>
</div>