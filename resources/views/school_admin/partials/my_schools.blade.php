<div id="second" class="z-index-0">
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
        {{$error}}
    </div>
    @endforeach
    <!--<div class="row">-->
    <div class="my-schools-module" style="width:100%">
        @include('school_admin.partials.card')
    </div>
    <!--</div>-->

    <!--    <div class="icon big">
        <i class="fa fa-users fa-4x"></i>
    </div>-->


    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at viverra est, eu finibus mauris. Quisque tempus vestibulum fringilla. Morbi tortor eros, sollicitudin eu arcu sit amet, aliquet sagittis dolor. </p>-->
</div>