<style>
    .class__list:hover .no-scale{
        margin-bottom: -100px !important;
    }
</style>
<section class="dcare__courses__area bg-image--1 pb-0">
    <div class="container">
        <div class="row ">
            <!-- Start Single Courses -->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="d-flex">
                    <h2 class="mb-2" style="flex-grow: 1;">School Details</h2>
                    <a class="dcare__btn" href="javascript:;" onclick="event.preventDefault();getSchools()" style="line-height: 35px;height: 35px;">Back</a>
                </div>
                <div class="class__list bg-white ">
                    <div class="courses__thumb">
                        <a href="#">
                            <img src="<?= asset('images/uploads/' . $school->image) ?>" alt="class images" onerror="this.src='{{asset('frontend/images')}}/logo-noimage.png'" style="width:auto;max-height:400px;">
                        </a>
                    </div>
                    <div class="courses__inner no-scale" style="margin-bottom:0 !important;">
                        <div class="courses__wrap" style="height:auto;">
                            <div class="courses__content">
                                <div class="class__title">
                                    <h4><a href="javascript:"><?= $school->name ?></a></h4>
                                </div>
                                <ul class="school-details">
                                    <li><i class="fa fa-phone mr-2"></i> 
                                        <?php
                                        $format_phone = substr($school->contact, -10, -7) . "-" .
                                                substr($school->contact, -7, -4) . "-" .
                                                substr($school->contact, -4);
                                        echo "<a href='tel:$format_phone'>" . $format_phone . "</a>";
                                        ?>
                                    </li>
                                    <li><i class="fa fa-envelope mr-2"></i> <a href="mailto:{{$school->email}}"><?= $school->email ?></a></li>
                                    <li><i class="fa fa-map-marker mr-2"></i> <?= $school->address ?>, <?= $school->city->city ?>, <?= $school->state->state ?></li>
                                </ul>
                                <?php
                                if ($school->regional_director) {
                                    $rd = $school->regional_director;
                                    ?>
                                    <div class="section-beauty">
                                    <h4 class="mt-3">Regional Director</h4>

                                    <ul class="school-details">
                                        <li><span><i class="fa fa-user mr-2"></i> 
                                            <?= ucwords($rd->first_name . ' ' . $rd->last_name) ?></span>
                                            <span style="float:right"><i class="fa fa-envelope mr-2"></i> <a href="mailto:{{$rd->email}}"><?= $rd->email ?></a></span>
                                        </li>

                                    </ul>
                                    @if($rd->files)
                                    <?php
                                    $files = explode(',', $rd->files);
                                    ?>
                                    <h5 class="mt-3">
                                        <i class="fa fa-paperclip mr-2"></i> Attachments
                                    </h5>
                                    <ul class="school-details">
                                        @foreach($files as $key=>$file)
                                        <li> 
                                             <?=$key+1?>. <a href="<?= asset('user_files/' . $file); ?>"><?= $file ?></a>
                                        </li>
                                        @endforeach

                                    </ul>
                                    </div>
                                    @endif
                                <?php }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- Start Our Service Area -->
<section class="junior__service bg-image--1 section-padding--bottom" id="classes-gird">
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>School Classes</h2>
            </div>
        </div>
        <div class="row">
            <!-- Start Single Service -->
            <?php
            $classes = $school->school_classes->where('status', 1);
//            dd($classes);
            ?>
            @foreach($classes as $c=>$subSection)
            <?php
            if ($school->image == "") {
                $school_image = asset('frontend/images') . "/logo-noimage.png";
            } else {
                $school_image = asset('/images/uploads/' . $school->image);
            }
            ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12" style="margin-top: 80px;">
                <div class="service bg--white border__color wow fadeInUp">
                    <div class="service__icon" style="background-image: url('<?= $school_image ?>')">
                        <!--<img src="" alt="icon images">-->
                    </div>
                    <div class="service__details">
                        <h6>
                            <a href="<?= url('frontend/class-details/' . $subSection->id) ?>"><?= $subSection->title ?></a>
                            <!--<b>Start From: </b> <?= formatDate($subSection->start_on) ?>--> 

                            <?php if ($subSection->weekly_off_days) { ?>
                                <b>Week Days: </b>
                                <?php
                                $weekly_off_days = $subSection->weekly_off_days;
                                if ($weekly_off_days) {
                                    $weekly_off_days = explode(',', $weekly_off_days);
                                    if ($weekly_off_days) {
                                        echo "";
                                        foreach ($weekly_off_days as $day):
                                            echo '<span>' . weekDays()[$day] . '</span>&nbsp;&nbsp;';
                                        endforeach;
                                        echo "";
                                    }else {
                                        echo "<p>No Days Found</p>";
                                    }
                                } else {
                                    echo "<p>No Days Found</p>";
                                }
                            }
                            ?>
                            <br />
                            <p>&nbsp;</p>
                            <b>Timing: </b> 
                            <?= formatTime($subSection->start_at) ?> - <?= formatTime($subSection->end_at) ?> 
                        </h6>
                        <div class="service__btn">
                            <a class="dcare__btn--2" onclick="return getClassDetails(this)" href="<?= url('school-admin/class-details/' . $subSection->id) ?>">Details<i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Single Service -->
        </div>
    </div>
</section>
<!-- End Our Service Area -->
