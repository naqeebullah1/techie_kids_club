@extends('master')

@section('content')
<style>
    .editable-clear-x {
        display: none !important;
    }

    .form-control {
        padding: .6rem 1rem !important;
    }

    .table-responsive th,
    .table-responsive td {
        white-space: nowrap;
        border: 1px solid #ebedf2 !important;
    }

    .modal-lg {
        width: 1250px !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <a href="<?= route('admin.testimonials.index') ?>" class="pull-right mb-2 btn btn-success">Go Back</a>

        <div class="card">
            <div class="card-header">
                <div class="card-title pull-left">
                    @if ($type == 'create')
                    Create New Testimonial Video
                    @php
                    $path = route('admin.testimonials.store');
                    @endphp
                    @else
                    Update Testimonial Video
                    @php
                    $path = route('admin.testimonials.update', $testimonial->id);
                    @endphp
                    @endif
                </div>
            </div>
            <div class="card-body">

                <form method="post" enctype="multipart/form-data" action="{{ $path }}">
                    @if ($type == 'update')
                    @method('patch')
                    @endif
                    @csrf
                   
                    <div class="row">
                        
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Type</label>
                            <select id="type" name="type" class="form-control chosen @error('type') is-invalid @enderror">
                                <option value="">Select Type</option>
                                <option value="1" @if($testimonial->type == 1) {{ "selected='selected'" }} @endif>Parent</option>
                                <option value="2" @if($testimonial->type == 2) {{ "selected='selected'" }} @endif>School</option>
                            </select>
                            
                            @error('type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label class="labeltypechange">Parent Name</label>
                            <input type="text" id="parent_name" class="form-control @error('parent_name') is-invalid @enderror"
                                   value="{{ old('parent_name') ?? $testimonial->parent_name }}" name="parent_name" />
                            @error('parent_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label class="hideforschool">Relation With Child</label>
                            <input type="text" id="relation_with_child" class="form-control @error('relation_with_child') is-invalid @enderror"
                                   value="{{ old('relation_with_child') ?? $testimonial->relation_with_child }}" name="relation_with_child" />
                            @error('relation_with_child')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>School</label>
                            <input type="text" id="school" class="form-control @error('school') is-invalid @enderror"
                                   value="{{ old('school') ?? $testimonial->school }}" name="school" />
                            @error('school')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                            <label>Comment</label>
                            <textarea id="description" class="form-control @error('description') is-invalid @enderror"
                                      name="description"
                                      >{{ old('description') ?? $testimonial->description }}</textarea>

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
  <div class="row">
                    <div class="col-md-12">
        <label for="image" class="col-md-12 col-form-label text-md-left">Image <br>
            <span class="text-danger">Preferred image dimensions to upload is (Rendered aspect ratio:370*377)</span>
        </label>
        <input type="file" accept="image/png, image/gif, image/jpeg" class="dropify @error('image')  @enderror" @if($testimonial->link)
        data-default-file="{{ asset('frontend/images/testimonial/'.$testimonial->link) }}"
        @endif
            name="link">
        @error('link')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-12 text-right">
                            <button type="submit" class="mt-2 btn btn-success">
                                @if ($type == 'create')
                                Add
                                @else
                                Update
                                @endif
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $("#type").on('change',function(){
       if($(this).val() == 2) {
           $("#relation_with_child").attr('type','hidden').attr('value','Teacher');
           $(".hideforschool").hide();
           $(".labeltypechange").text('Teacher Name');
       }
       else if($(this).val() == 1) {
           $("#relation_with_child").attr('type','text').attr('value',"{{$testimonial->relation_with_child}}");
           $(".hideforschool").show();
           $(".labeltypechange").text('Parent Name');
       }
    });
</script>
@endsection