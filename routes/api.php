<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('subscribed-confirmation', 'Parent\ParentsController@subscribedConfirmation')->name('subscribed.confirmation');

Route::post('/login', 'Parent\API\ParentsController@login');
Route::post('/register','Parent\API\ParentsController@registerParent');
Route::post('/demand_library','Parent\API\ParentsController@demandLibrary');
Route::post('/childrens','Parent\API\ParentsController@childrens');

Route::get('/states','Parent\API\ParentsController@states');
Route::post('/cities','Parent\API\ParentsController@cities_by_state_id');
Route::post('/find_school','Parent\API\ParentsController@find_school');
Route::post('/child_store','Parent\API\ParentsController@childStore');
Route::post('/update_profile','Parent\API\ParentsController@updateProfile');

