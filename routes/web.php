<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    dd(Artisan::output());
});
//testApi
//Before authentication
Route::get('/refresh-token', 'Admin\UsersController@refreshToken');
Route::get('/test-api', 'PagesController@testApi');
Route::get('/test-api-redirect', 'PagesController@testApiRes');
Route::get('/process-csv', 'AdmissionsController@processCSV');

Route::group(['middleware' => 'beforeAuth'], function() {
    Route::get('/', function() {
        $franchise = IDToSlug(1);
        return redirect('front-franchise/' . $franchise->slug);
    });
    Route::get('/backend', function() {
        $franchise = IDToSlug(1);
        return redirect('front-franchise/' . $franchise->slug . '/backend');
    });
    Route::get('/front-franchise/{franchise_id}', 'FrontendController@index');
    Route::get('subscriptionpmtid', 'Parent\ParentsController@subscriptionpmtid');
    Route::post('save-subscription', 'Parent\ParentsController@saveSubscription');
//    Route::get('/', 'Admin\UsersController@login');
    Route::get('front-franchise/{franchise_id}/page/{type?}', 'FrontendController@index')->name('front');
    Route::get('front-franchise/{franchise_id}/backend', 'Admin\UsersController@login');

    Route::get('backend/login', 'Admin\UsersController@login')->name('backend.login');
    Route::get('front-franchise/{franchise_id}/backend/forgotPassword', 'Admin\UsersController@recoverPassword');
    Route::get('front-franchise/{franchise_id}/backend/resetPassword/{token}', 'Admin\UsersController@resetPassword');
    Route::get('view-service/{id}', 'FrontendController@service')->name('view-service');
    Route::get('service-detail/{id}', 'FrontendController@serviceDetails')->name('service-details');
    Route::get('front-franchise/{franchise_id}/privacy-policy', 'FooterController@privacy');
    Route::get('front-franchise/{franchise_id}/terms-conditions', 'FooterController@terms');

    Route::get('view-service-detail/{id}', 'FrontendController@serviceDetail')->name('view-service-detail');

    Route::post('franchise/{franchise_id}/dologin', 'Admin\UsersController@login');
    Route::post('front-franchise/{franchise_id}/send-message', 'FrontendController@sendMessage')->name('sendMessage');
    Route::post('front-franchise/{franchise_id}/recoverPassword', 'Admin\UsersController@recoverPassword');


//    Route::post('changePassword', 'Admin\UsersController@changePassword');
    Route::get('gallery/{slug}', 'FrontendController@gallery');
    Route::get('contact', 'FrontendController@contact');
    Route::get('front-franchise/{franchise_id}/find-your-school', 'FrontendController@findYourSchool');
//    Route::get('cities-dd', 'Admin\SchoolsController@citiesDD');

    Route::get('/front-franchise/{franchise_id}/frontend/school-details/{id}', 'FrontendController@schoolDetails');
    Route::get('/front-franchise/{franchise_id}/frontend/class-details/{id}', 'FrontendController@classDetails');

    Route::get('front-franchise/{franchise_id}/parentlogin', 'FrontendController@login');
    Route::get('front-franchise/{franchise_id}/update-cart/{id?}', 'FrontendController@updateCart');
    Route::get('front-franchise/{franchise_id}/checkout', 'FrontendController@checkout');
    Route::post('store-order', 'FrontendController@storeOrder');
    Route::match(['get', 'post'], 'front-franchise/{franchise_id}/view-cart', 'FrontendController@viewCart');
    Route::get('front-franchise/{franchise_id}/forgotPassword', 'Admin\UsersController@recoverParentPassword');
    Route::get('front-franchise/{franchise_id}/resetPassword/{token}', 'Admin\UsersController@resetParentPassword');
});
Route::get('front-franchise/{franchise_id}/cities-dd', 'Admin\SchoolsController@citiesDD')->name('cities-dd');
Route::get('front-franchise/{franchise_id}/schools-dd', 'Admin\SchoolsController@schoolsDD');
Route::get('schools-parents-dd', 'Admin\SchoolsController@schoolsParentsDD');
Route::get('school-classes-dd', 'Admin\SchoolsController@schoolClasses');
Route::get('load-school-classes', 'Admin\SchoolsController@loadSchoolClasses');
Route::get('school-classes-parents-dd', 'Admin\SchoolsController@schoolClassesWithParents');
Route::post('front-franchise/{franchise_id}/changePassword', 'Admin\UsersController@changePassword');
Route::post('front-franchise/{franchise_id}/newsletters', 'NewsletterController@store')->name('newsletter.create');
Route::get('front-franchise/{franchise_id}/unsub-newsletter', 'NewsletterController@unsub')->name('newsletter.unsub');


//After authentication with prefix:backend
Route::prefix('backend')->name('admin.')->middleware('checkAuth')->group(function() {
    Route::resource('users', 'Admin\UsersController');
    Route::match(['get', 'post'], '/users-list/{role_id?}', 'Admin\UsersController@index')->name('users.index');
    Route::match(['get', 'post'], '/parent-list/{role_id?}', 'Admin\UsersController@parentlist')->name('users.parentlist');

    Route::resource('roles', 'Admin\RolesController');
    Route::get('user-profile', 'Admin\UsersController@profileEdit');
    Route::get('reset-password-manually/{email}', 'Admin\UsersController@resetpasswordmanually');
    Route::get('change-password', 'Admin\UsersController@changePassword');
    Route::get('dashboard', 'PagesController@dashboard');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::patch('user-profile', 'Admin\UsersController@updateProfile')->name('user.profile');


    //for home page banner
    // Route::resource('banners', 'BannerController')->except('show');

    Route::get('banners/{type?}', 'BannerController@index')->name('banners.index');
    Route::post('banners/store', 'BannerController@store')->name('banners.store');
    Route::delete('banners/destroy/{id}', 'BannerController@destroy')->name('banners.destroy');

    Route::post('bannar_ajax_upload', 'BannerController@bannar_ajax_upload')->name('bannar_ajax_upload');
    Route::post('banners/sort-order', 'BannerController@sortData')->name('banner.sort.order');
    Route::post('banners-upload', 'TrendingProductController@upload')->name('upload.widget.image');
    Route::get('change-status/{table}/{id}/{status}', 'BannerController@changeStatus')->name('admin.banner.change.status');

    Route::resource('pages', 'PagesController')->except('show');
    Route::get('page-details/{pageId}', 'PagesController@pageDetails')->name('page.details');
    Route::get('contact-details/{pageId}', 'PagesController@contactDetails')->name('contact.details');
    Route::get('pages/edit/{id?}', 'PagesController@index');
    Route::get('page-details/create/{pageId}', 'PagesController@pageDetailCreate')->name('page.details.create');
    Route::post('page-details/store/{pageId}', 'PagesController@pageDetailStore')->name('page.details.store');
    Route::get('page-details/edit/{id}', 'PagesController@pageDetailEdit')->name('page.details.edit');
    Route::put('page-details/update/{id}', 'PagesController@pageDetailUpdate')->name('page.details.update');

    Route::post('categories/store', 'GalleriesController@categoriesStore')->name('categories.store');
    Route::get('categories', 'GalleriesController@categories')->name('categories.index');
    Route::get('categories/edit/{id}', 'GalleriesController@categoriesEdit')->name('categories.edit');
    Route::get('categories/create', 'GalleriesController@categoriesCreate')->name('categories.create');
    Route::put('categories/update/{id}', 'GalleriesController@categoriesUpdate')->name('categories.update');

    Route::get('demand-liberaries', 'DemandLiberariesController@index')->name('demand_liberaries.index');
    Route::get('demand-liberaries/create', 'DemandLiberariesController@create')->name('demand_liberaries.create');
    Route::post('demand-liberaries/store', 'DemandLiberariesController@store')->name('demand_liberaries.store');
    Route::get('demand-liberaries/edit/{id}', 'DemandLiberariesController@edit')->name('demand_liberaries.edit');
    Route::put('demand-liberaries/update/{id}', 'DemandLiberariesController@update')->name('demand_liberaries.update');
    Route::delete('demand-liberaries/destroy/{id}', 'DemandLiberariesController@destroy')->name('demand.destroy');


    Route::get('galleries/{id}', 'GalleriesController@index')->name('galleries');
    Route::post('galleries/store/{id}', 'GalleriesController@store')->name('gelleries.store');
    Route::delete('galleries/destroy/{id}', 'GalleriesController@destroy')->name('galleries.destroy');
    Route::post('ck-editor/uploads', 'BannerController@ckeditorUpload')->name('ckeditor.upload');

    Route::delete('category/destroy/{id}', 'GalleriesController@destroyCategory')->name('category.destroy');
    Route::delete('page_details/destroy/{id}', 'PagesController@pageDetailDestroy')->name('page_details.destroy');
//    Route::post('galleries/store/{id}', 'GalleriesController@store')->name('gelleries.store');

    Route::get('contacts', 'PagesController@contacts');
    Route::delete('contacts/destroy/{id}', 'PagesController@ContactsDestroy')->name('contact.destroy');

    Route::post('pages/updatetype', 'PagesController@updateType')->name('pages.updatetype');
    Route::get('services/{type?}', 'ServicesController@index')->name('services.index');
    Route::post('services/store', 'ServicesController@store')->name('services.store');
    Route::delete('services/destroy/{id}', 'ServicesController@destroy')->name('services.destroy');

    Route::resource('testimonials', 'TestimonialController');
    Route::resource('teams', 'TeamController');

    Route::resource('footers', 'FooterController');

    Route::match(['get', 'post'], 'states', 'Admin\StatesController@index');
    Route::get('states/create/{id?}', 'Admin\StatesController@create');
    Route::post('states/store', 'Admin\StatesController@store')->name('states.store');
    Route::delete('states-destroy/{id}', 'Admin\StatesController@destroy')->name('states.destroy');

    /*
     * Districts
     */


    Route::match(['get', 'post'], 'districts', 'Admin\DistrictsController@index');
    Route::get('districts/create/{id?}', 'Admin\DistrictsController@create');
    Route::post('districts/store', 'Admin\DistrictsController@store')->name('districts.store');
    Route::delete('districts-destroy/{id}', 'Admin\DistrictsController@destroy')->name('districts.destroy');
    /*
     * Price Settings
     */

    Route::match(['get', 'post'], 'price-settings', 'Admin\PriceSettingsController@index');
    Route::get('price-settings/create/{id?}', 'Admin\PriceSettingsController@create');
    Route::post('price-settings/store', 'Admin\PriceSettingsController@store')->name('price-settings.store');
    Route::delete('price-settings/{id}', 'Admin\PriceSettingsController@destroy')->name('price-settings.destroy');
    Route::get('get-pd/{type}', 'Admin\PriceSettingsController@pd');

    Route::match(['get', 'post'], 'cities/{state_id}', 'Admin\CitiesController@index');
    Route::get('cities-create/{state_code}/{id?}', 'Admin\CitiesController@create');
    Route::post('cities-store', 'Admin\CitiesController@store')->name('cities.store');
    Route::delete('cities-destroy/{id}', 'Admin\CitiesController@destroy')->name('cities.destroy');



    Route::match(['get', 'post'], 'schools', 'Admin\SchoolsController@index');
    Route::get('schools-create/{id?}', 'Admin\SchoolsController@create');
    Route::post('schools-store', 'Admin\SchoolsController@store')->name('schools.store');
    Route::delete('schools-destroy/{id}', 'Admin\SchoolsController@destroy')->name('schools.destroy');
    /*
     * Promo Codes
     */

    Route::match(['get', 'post'], 'promo-codes/{expired?}', 'PromoCodesController@index');
    Route::get('promocodes-create/{id?}', 'PromoCodesController@create');
    Route::post('promocodes-store', 'PromoCodesController@store')->name('promocodes.store');
    Route::delete('promocodes-destroy/{id}', 'PromoCodesController@destroy')->name('promocodes.destroy');

    Route::get('cities-dd', 'Admin\SchoolsController@citiesDD');
    Route::get('class-dd', 'AdmissionsController@classDD')->name('class-dd');
    Route::get('child-dd', 'AdmissionsController@childDD')->name('child-dd');


    /*
     * Notifications Route
     */
    Route::match(['get', 'post'], 'notifications', 'NotificationsController@index');
    Route::get('notifications-create/{id?}', 'NotificationsController@create');
    Route::post('notifications-store', 'NotificationsController@store')->name('notifications.store');
    Route::delete('notifications-destroy/{id}', 'NotificationsController@destroy')->name('notifications.destroy');
    Route::get('notification-file/remove/{id}', 'NotificationsController@removeFile');


    /*
     * Broadcasts Route
     */
    Route::match(['get', 'post'], 'broadcasts', 'BroadcastsController@index');
    Route::get('broadcasts-create/{id?}', 'BroadcastsController@create');
    Route::post('broadcasts-store', 'BroadcastsController@store')->name('broadcasts.store');
    Route::delete('broadcasts-destroy/{id}', 'BroadcastsController@destroy')->name('broadcasts.destroy');


    Route::match(['get', 'post'], 'school-classes/{id}', 'Admin\SchoolClassesController@index');
    Route::get('school-classes-create/{school_id}/{class_id?}', 'Admin\SchoolClassesController@create');
    Route::post('school-classes-store', 'Admin\SchoolClassesController@store')->name('school.classes.store');
    Route::delete('schools-classes-destroy/{id}', 'Admin\SchoolClassesController@destroy')->name('schools.classes.destroy');


    Route::get('admissions', 'AdmissionsController@index');
    Route::post('search_stripe', 'AdmissionsController@search_stripe')->name('admission.search_stripe');
    Route::get('questions/{type?}', 'QuestionsController@index')->name('questions.index');
    Route::post('questions/store', 'QuestionsController@store')->name('questions.store');
    Route::delete('questions/destroy/{id}', 'QuestionsController@destroy')->name('questions.destroy');

    Route::get('reports/search-all', 'Admin\ReportsController@searchAll')->name('reports.index');
    Route::get('reports/students-summary-report', 'Admin\ReportsController@studentsSummary');
    Route::get('dl-file-destroy/{id?}/{file?}', 'DemandLiberariesController@dlFileDestroy')->name('reports.index');

    Route::get('user-file/remove/{id}', 'Admin\UsersController@removeFile');
    Route::match(['get', 'post'], 'scheduler', 'Admin\SchedulerController@index');
    Route::post('notes-store', 'Admin\SchedulerController@notesStore');

    Route::match(['get', 'post'], 'vacant-time', 'Admin\SchedulerController@vacantTime');

    Route::get('notes-form', 'Admin\SchedulerController@noteForm');
    Route::get('teacher-gr', 'Admin\ReportsController@teacherGR');
    Route::get('teacher-summary-gr', 'Admin\ReportsController@teacherSummary');

    Route::match(['get', 'post'], 'attendance_sheets/create', 'AttendanceSheetsController@create')->name('attendance_sheets.create');
    Route::post('attendance_sheets/store', 'AttendanceSheetsController@store')->name('attendance_sheets.store');
    Route::match(['get', 'post'], 'attendance_sheets/index', 'AttendanceSheetsController@index')->name('attendance_sheets.index');
    Route::get('attendance_sheets/detail/{id}', 'AttendanceSheetsController@details')->name('attendance_sheets.details');

    /*
     * Franchise
     */

    Route::resource('franchises', 'FranchisesController');
    Route::get('franchises-create/{id?}', 'FranchisesController@create');
    Route::get('franchises-store', 'FranchisesController@store');
    Route::get('default_for_all/{table}/{id}', 'FranchisesController@defaultForAll')->name('default_for_all');

    Route::get('assign-franchise-to-user/{user_id}', 'FranchisesController@userFranchise');
    Route::post('user-franchise-store', 'FranchisesController@userFranchiseStore')->name('user.franchise.store');

    Route::get('newsletters', 'NewsletterController@index')->name('newsletter.index');
    Route::get('newsletter-design', 'NewsletterController@designForm')->name('newsletter.design');
    Route::post('newsletter-design', 'NewsletterController@designStore')->name('newsletter.designStore');
});

Route::post('backend/load-vacant-time', 'Admin\SchedulerController@loadVacantTime');
Route::get('backend/get-notes-form', 'Admin\SchedulerController@notesForm');
Route::post('backend/notes-delete', 'Admin\SchedulerController@deleteNote');
Route::match(['get', 'post'], 'backend/scheduler-data', 'Admin\SchedulerController@schedulerData');

Route::get('backend/get-teacher', 'Admin\SchedulerController@getTeachers');
Route::get('load-students/{id}', 'Admin\SchoolClassesController@loadStd');

Route::prefix('parent')->name('parent.')->middleware('checkAuth')->group(function() {
    Route::get('/front-franchise/{franchise_id}/dashboard', 'Parent\ParentsController@dashboard');
    Route::get('/front-franchise/{franchise_id}/sms-subscription', 'Parent\ParentsController@smsSubscription');
    Route::get('children/{user_id?}', 'Parent\ParentsController@children');
    Route::get('children-create/{id?}', 'Parent\ParentsController@childCreate');
    Route::post('children/store/{user_id?}', 'Parent\ParentsController@childStore')->name('children.store');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::post('update-profile', 'Parent\ParentsController@updateProfile');
    Route::get('children-delete/{id}', 'Parent\ParentsController@deleteChild');
    Route::get('add-details/{id}', 'Parent\ParentsController@admissionDetails');
    Route::get('/front-franchise/{franchise_id}/demand_library', 'Parent\ParentsController@demandLibrary');
    Route::get('/front-franchise/{franchise_id}/notifications', 'Parent\ParentsController@notifications');
    Route::get('/front-franchise/{franchise_id}/creditcard', 'Parent\ParentsController@listcreditcard');
    Route::post('add-card', 'Parent\ParentsController@addcreditcard');
    Route::get('notification-details/{id}', 'Parent\ParentsController@notificationDetail');
    Route::get('notification-list', 'Parent\ParentsController@notificationList');
    Route::get('apply-promocode/{promocode}', 'Parent\ParentsController@applyPromo');

    Route::get('stripesuccess', function () {
        return view('stripesuccess');
    });
    Route::post('stripepay', 'Parent\ParentsController@stripepay');
    Route::post('stripepayPMT', 'Parent\ParentsController@stripepayPMT');
    Route::get('cancelsubscription/{id}', 'Parent\ParentsController@cancelstripesubscription');
});

Route::prefix('school-admin')->name('school_admin.')->middleware('checkAuth')->group(function() {
    Route::get('/front-franchise/{franchise_id}/dashboard', 'Parent\ParentsController@dashboard');
    Route::get('school-details/{school_id}', 'SchoolAdminsController@schoolDetails');
    Route::get('get-schools', 'SchoolAdminsController@getSchools');
    Route::get('class-details/{class_id}', 'SchoolAdminsController@getClass');

    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::post('update-profile', 'Parent\ParentsController@updateProfile');
    Route::get('demand_library', 'Parent\ParentsController@demandLibrary');
});



Route::post('/front-franchise/{franchise_id}/register-parent', 'Admin\UsersController@store')->name('register-parent');

Route::prefix('teacher')->name('teacher.')->middleware('checkAuth')->group(function() {
    Route::get('reports/search-all', 'Admin\ReportsController@searchAll')->name('reports.index');

    Route::get('demand-liberaries', 'DemandLiberariesController@index')->name('demand_liberaries.index');
    Route::get('dashboard', 'PagesController@dashboard');
    Route::match(['get', 'post'], 'schools', 'Admin\SchoolsController@index');
    Route::match(['get', 'post'], 'school-classes/{id}', 'Admin\SchoolClassesController@index');
    Route::post('logout', 'Admin\UsersController@logout')->name('logout');
    Route::get('change-password', 'Admin\UsersController@changePassword');
    Route::match(['get', 'post'], 'scheduler', 'Admin\SchedulerController@index');
    Route::match(['get', 'post'], 'vacant-time', 'Admin\SchedulerController@vacantTime');



    Route::match(['get', 'post'], 'attendance_sheets/create', 'AttendanceSheetsController@create')->name('attendance_sheets.create');
    Route::post('attendance_sheets/store', 'AttendanceSheetsController@store')->name('attendance_sheets.store');
    Route::match(['get', 'post'], 'attendance_sheets/index', 'AttendanceSheetsController@index')->name('attendance_sheets.index');
    Route::get('attendance_sheets/detail/{id}', 'AttendanceSheetsController@details')->name('attendance_sheets.details');
});
Route::get('thanks', 'FrontendController@thanks');
Route::get('update_price', 'Parent\ParentsController@update_price');
Route::get('update_price_stripe', 'Parent\ParentsController@update_price_stripe');
Route::get('get-liberary-details/{id}', 'DemandLiberariesController@getDetail');

Route::get('demo', function () {
    return redirect('/page/schedule-a-demo');
});
Route::get('school-partnerships', function () {
    return redirect('/page/school-partnerships');
});
Route::get('enroll-your-school.html', function () {
    return redirect('/page/schedule-a-demo');
});

Route::get('enrichmentatschool.html', function () {
    return redirect('/page/classes');
});
Route::get('enroll-your-child.html', function () {
    return redirect('/page/classes');
});
Route::get('contact', function () {
    return redirect('/page/contact');
});

